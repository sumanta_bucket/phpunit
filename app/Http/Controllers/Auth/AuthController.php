<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Services\TranslationService;
use App\Models\Token;
use Illuminate\Support\Carbon;

/**
 * Authenticate user operations
 * @group Auth
 * @author manpreetkaur
 */
class AuthController extends BaseController
{
    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    private $request;

    /**
     * Create a new controller instance.
     * @bodyParam  \Illuminate\Http\Request  $request
     * @response void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * protected create a new token function.
     * @bodyParam  \App\Model\User $user required create the user token from user information
     * @response json array
     */
    protected function jwt(User $user)
    {
        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued.
            'exp' => time() + 60 * 60 * 24 // Expiration time
        ];

        // As you can see we are passing `JWT_SECRET` as the second parameter that will
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'));
    }

    /**
     * Authenticate a user and return the token if the provided credentials are correct.
     * @bodyParam email string required the email of the user who is logged in .
     * @bodyParam password string required The password of the user.
     * @response json array
     */
    public function authenticate(User $user)
    {
        $date     = Carbon::now()->subDay(1)->toDateTimeString();
        Token::where('created_at', '<', $date)->delete();
        $messages = TranslationService::customMesages(1);
        //validate the request for some conditions
        $this->validate($this->request,
            [
            'email' => 'required|email',
            'password' => 'required'
            ], $messages);
        // Find the user by email
        $user     = User::where('email', $this->request->input('email'))->with([
                'roles', 'permissions', 'title'])->first();
        //if no user found then return with specific error
        if (!$user) {
            // You wil probably have some sort of helpers or whatever
            // to make sure that you have the same response format for
            // differents kind of responses. But let's return the
            // below respose for now.
            return response()->json([
                    'error' => TranslationService::lang('forgot_password_email_not_found',
                        config('constant.default_lang'))
                    ], 401);
        }
        //check whether the user has confirmed the account or not unaconfirmed account not allowed to login 
        if (!$user->confirmed) {
            return response()->json([
                    'error' => TranslationService::lang('confirm_account_error',
                        config('constant.default_lang'))
                    ], 401);
        }
        //if the user status is not active then the user is not able to login and retuen with error
        if ($user->status != 1) {
            return response()->json([
                    'error' => TranslationService::lang('user_account_blocked',
                        config('constant.default_lang'))
                    ], 401);
        }
        // if user found then verify the password and generate the token
        if (Hash::check($this->request->input('password'), $user->password)) {
            //get the user role from roles object associated with user
            $user->role          = $user->roles->pluck('name');
            $user['permissions'] = $user->permissions->pluck('name', 'id');
            //send user data as a response parameter
            $userData            = $user->only(['id', 'title_id', 'first_name', 'last_name',
                'email',
                'language', 'avatar', 'address', 'city', 'zip', 'role', 'mobile',
                'permissions', 'title', 'avatar_path', 'full_name']);
            $token               = $this->jwt($user);
            $userData['token']   = $token;
            Token::create(['user_id' => $user->id, 'token' => $token]);
            //return token as well as other required user details 
            return response()->json($userData, 200);
        }
        // Bad Request response or unauthorized user
        return response()->json([
                'error' => TranslationService::lang('login_unsuccessful',
                    config('constant.default_lang'))
                ], 401);
    }

    /**
     * Logout user
     * @param Request $request
     * @response json array type
     */

    public function logout(Request $request)
    {
        $token    = $request->header('Authorization');
        $response = Token::where('token', $token)->delete();
        return response()->json([
                'error' => TranslationService::lang('logout_successfully',
                    config('constant.default_lang'))
                ], 200);
    }
}