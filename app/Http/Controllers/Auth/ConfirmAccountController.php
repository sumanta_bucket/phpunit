<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
 
/**
 * ConfirmAccount operations when user is created send confirmation to user
 * @group Auth
 * @author mnapreet kaur
 */
class ConfirmAccountController extends Controller
{
    protected $userRepository;

    /**
     * Constructor for this controller
     * @bodyParam  App\Repositories\UserRepository  $userRepository
     * @response void
     */

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * callback for the confirm account
     * @bodyParam type string required $token
     * @response json string
     */

    public function confirm(string $token)
    {
        //call the repo function to  perform the required action
        $confirmed = $this->userRepository->confirm($token);
        //retun the response to user
        return $confirmed;
    }
}