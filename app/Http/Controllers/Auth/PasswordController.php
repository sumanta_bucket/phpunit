<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;
use App\Models\PasswordReset;
use App\Services\UserService;
use App\Models\User;
use App\Services\TranslationService;

/**
 * Forgot password operations
 * @group Auth
 * @author manpreetkaur
 */
class PasswordController extends Controller
{
    /**
     * @var $token
     * @var $expires
     */
    protected $token;
    protected $expires = 60 *30;

    /**
     * send the reset password link to user
     * @param Request $request
     * @bodyParam email string required  the email of the user
     * @bodyParam appurl string required  the front-end url to send as a email
     * @respone json array
     */
    public function sendPasswordResetLink(Request $request)
    {
        $messages = TranslationService::customMesages(config('constant.default_user_language'));
        //validate the request for specific conditions
        $this->validate($request,
            ['email' => 'required|email', 'appurl' => 'required'], $messages);
        $email    = $request['email'];
        $path     = $request['appurl'];
        $user     = User::where('email', $email)->first();
        //if the user is not found then return with specific response code
        if (!empty($user)) {
            //send the reset password email
            $response = UserService::passwordResetMail($user,$path);
            if ($response) {
                return response()->json(['success' => TranslationService::lang('email_sent_to',
                            config('constant.default_lang'), $request['email'])],
                        200);
            }
            return response()->json(['error' => TranslationService::lang('failed_to_sent_email',
                        config('constant.default_lang'))], 500);
        }
        return response()->json(['error' => TranslationService::lang('forgot_password_email_not_found',
                    config('constant.default_lang'))], 401);
    }

    /**
     * Change the user password
     * @param Request $request
     * @bodyParam token string required the token send to user by email to change
     * @bodyParam password string required new password
     * @bodyParam password_confirmation string required password confirmation
     * @response type json array
     */
    public function resetPassword(Request $request)
    {
        //validate the request
        $this->_validatePasswordResetRequest($request);
        $password_reset = PasswordReset::where(['token' => $request['token']])->where('created_at',
                '>=', Carbon::now()->subMinutes($this->expires))->first();
        if (!empty($password_reset)) {
            //find the user with this email
            $user = User::where('email', $password_reset->email)->first();
            if (!empty($user)) {
                if (Hash::check($request['password'], $user->password)) {
                    // The passwords match...
                    return response()->json(['error' => TranslationService::lang('password_same_as_old',
                                config('constant.default_lang'))], 422);
                }
                //change the password
                $user->password = Hash::make($request['password']);
                if ($user->save()) {
                    PasswordReset::where('email', $user->email)->delete();
                    return response()->json(['success' => TranslationService::lang('reset_successfully',
                                config('constant.default_lang'))], 200);
                }
            }
            return response()->json(['error' => TranslationService::lang('user_not_found',
                        config('constant.default_lang'))], 401);
        }
        return response()->json(['error' => TranslationService::lang('invalid_token',
                    config('constant.default_lang'))], 401);
    }

    /**
     * Validate request for required parameters
     * confirm the password_confirmation is same as password
     * @param type $request request
     * @response json array
     */
    private function _validatePasswordResetRequest($request)
    {
        //get the translated error messages
        $messages = TranslationService::customMesages(config('constant.default_lang'));
        $this->validate($request,
            [
            'token' => 'required',
            'password' => 'required|min:6',
            'password_confirmation' => 'required|same:password',
            ], $messages);
    }
}