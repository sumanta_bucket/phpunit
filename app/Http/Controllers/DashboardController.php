<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repositories\DashboardRepository;

/**
 * Dashboard Controller
 *
 * dashboard  related functions like check users activity
 * @group Dashboard
 * @author manpreetkaur
 */
class DashboardController extends Controller
{
    protected $dashboad_repository;
    /**
     * Constructor for this controller
     * @param DashboardRepository $dashboard_repo
     */
    public function __construct(DashboardRepository $dashboard_repo)
    {
        $this->dashboad_repository= $dashboard_repo;
    }

    /**
     * List all activities
     * @response array type this function return all the activity performed by any of the user
     */
    public function index($activities=null)
    {
        $response = $this->dashboad_repository->records($activities); //call the repository function to perform the required action
        return $response;
    }
}