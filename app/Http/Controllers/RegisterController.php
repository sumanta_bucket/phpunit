<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Services\TranslationService;

/**
 * Register Controller Description
 *
 * Create new user related functions
 * @group Auth
 * @author manpreetkaur
 */
class RegisterController extends Controller
{
    protected $userRepository;

    /**
     * Constructor for this controller
     * @bodyParam  App\Repositories\UserRepository  $userRepository
     * @response void
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Create new user
     * @bodyParam first_name string required the name of the user.
     * @bodyParam email string required the email of the user.
     * @bodyParam password string required the password of the user.
     * @bodyParam password_confirmation string required the password confirmation.
     */
    public function store(Request $request)
    {
        $this->_validateRequest($request);
        //call create method from user repository
        $user = $this->userRepository->create($request->only('first_name',
                'email', 'password'));
        if ($user) {
            return response()->json(['success' => TranslationService::lang('user_created',
                        config('constant.default_lang'))], 200);
        }

        return response()->json(['error' => TranslationService::lang('db_error_heading',
                    config('constant.default_lang'))], 500);
    }

    /**
     * Validate use request
     * @param type $request
     */
    private function _validateRequest($request)
    {
        $messages = TranslationService::customMesages(config('constant.default_lang'));
        $this->validate($request,
            [
            'first_name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6,max:15',
            'password_confirmation' => 'required|same:password|min:6,max:15',
            ], $messages);
    }

}