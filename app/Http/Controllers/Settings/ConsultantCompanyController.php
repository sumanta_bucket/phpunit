<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ConsultantCompanyRepository;
use App\Services\PrepareDataTableService;
use App\Services\TranslationService;

/**
 * Consultant Company Controller
 *
 * Manage the CRUD related operations
 * @group ConsultantCompany
 * @author manpreetkaur
 */
class ConsultantCompanyController extends Controller
{
    protected $consultant_company_repository;

    /**
     * Constructor for this controller
     * @param ConsultantCompanyRepository $consultant_company_repository
     */
    public function __construct(ConsultantCompanyRepository $consultant_company_repository)
    {
        $this->consultant_company_repository = $consultant_company_repository;
    }

    /**
     * List all consultant companies
     * 
     * List all the consultant companies with specific sorting order, search and limit
     * @bodyParam order.0.dir string required the direction of the records in ASC or DESC direction
     * @bodyParam order.0.column string required the order by column name in ASC or DESC direction
     * @bodyParam search.value  string required the value to be searched in the records
     * @bodyParam start integer required the value from start getting records
     * @bodyParam length integer required the number of records want to fetch
     * @response json array type in variable data
     */
    public function index(Request $request)
    {
        $this->validateIndexRequest($request);
        //make the order of the column as it is displayed in the data table
        $columnNames       = [
            0 => 'id',
            1 => 'name'
        ];
        $searchableColumns = [$columnNames[0], $columnNames[1]]; //get the searchable columns
        $direction         = $request->input('order')[0]['dir']; //get the direction of the records ASC or DESC
        $sort_column       = $columnNames[intval($request->input('order')[0]['column'])]; // get the order by specific column
        $search            = $request->input('search')['value'];  // ht the value to be searched in the records
        //call common serivce for table results
        $response          = PrepareDataTableService::getTableDataWithMultipleRealtions('\ConsultantCompany',
                $sort_column, $direction, $request->input('start'),
                $request->input('length'), $searchableColumns, $search);
        if ($response['total'] != 0) {
            return response()->json(['success' => TranslationService::lang('success',
                        $request->auth->language), 'draw' => $request->input('draw'),
                    'recordsTotal' => $response['total'],
                    'recordsFiltered' => $request->input('length'), 'data' => $response['records']],
                    200);
        }
        //if no data found then return the empty records 
        return response()->json(['success' => TranslationService::lang('success',
                    $request->auth->language), 'draw' => 0, 'recordsTotal' => 0,
                'recordsFiltered' => 0, 'data' => []], 200);
    }

    /**
     * create a consultant company
     * @bodyParam name string required the name of the consultant company
     * @response json array type
     */
    public function store(Request $request)
    {
        //validate the request for required or other validation of the parameters
        $this->_validateRequest($request);
        $data     = $request->all();  //get data from the request
        $response = $this->consultant_company_repository->create($data);
        if ($response['status'] == 1) {
            return response()->json(['success' => TranslationService::lang('consulant_company_created',
                        $request->auth->language), 'data' => $response['data']],
                    200);
        }
        return response()->json(['error' => TranslationService::lang('error_consulant_company_creation',
                    $request->auth->language)], 422);
    }

    /**
     * update a consultant company
     * @bodyParam id integer required the id of the record to update
     * @bodyParam name string required the value to update
     * @response json array type
     */
    public function update(Request $request, int $id)
    {
        $request->request->add(['id' => $id]); //add new parameter to the request
        $this->_validateRequest($request);
        $data     = $request->all(); //get records from the request
        $response = $this->consultant_company_repository->update($id, $data);
        if ($response['status'] == 1) {
            return response()->json(['success' => TranslationService::lang('consulant_company_updated',
                        $request->auth->language), 'data' => $response['data']],
                    200);
        }
        return response()->json(['error' => TranslationService::lang('error_consulant_company_updation',
                    $request->auth->language)], 422);
    }

    /**
     * Delete a consultant company
     * @bodyParam id integer required the record id which we want to delete
     * @response json array type
     */
    public function delete(Request $request, int $id)
    {
        $request->request->add(['id' => $id]); // add new parameter to request for validtion
        $this->validate($request,
            ['id' => 'required|numeric|exists:consultant_companies,id,deleted_at,NULL']); //validate the request
        $response = $this->consultant_company_repository->delete($id); // call the delete function of the repository
        if ($response) {
            return response()->json(['success' => TranslationService::lang('consulant_company_deleted',
                        $request->auth->language)], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_consulant_company_deletion',
                    $request->auth->language)], 422);
    }

    /**
     * validate the store user request for required or other condition validations
     * @bodyParam type $request
     */
    private function _validateRequest($request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $this->validate($request,
            [
            'name' => 'required|string|max:191',
            'id' => 'sometimes|required|exists:consultant_companies,id,deleted_at,NULL'
            ], $messages);
    }

    /**
     * Private function to validate the request 
     * @param type $request
     */
    private function validateIndexRequest($request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $rules    = config('constant.index_request_validation'); 
        $this->validate($request, $rules, $messages);
    }
}