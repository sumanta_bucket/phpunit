<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Repositories\ConsultantRepository;
use Illuminate\Http\Request;
use App\Services\TranslationService;
use App\Services\PrepareDataTableService;

/**
 * Consultant Controller
 *
 * Manage the CRUD related operations
 * @group Consultants
 * @author manpreetkaur
 */
class ConsultantController extends Controller
{
    protected $consultantRepository;

    /**
     * Constructor for this controller
     * @bodyParam  App\Repositories\ConsultantRepository  $consultantRepository
     * @@response void
     */
    public function __construct(ConsultantRepository $consultantRepository)
    {
        $this->consultantRepository = $consultantRepository;
    }

    /**
     * List all consultants
     *
     * List all the consultants with specific sorting order, search and limit
     * @bodyParam order.0.dir string required the direction of the records in ASC or DESC direction
     * @bodyParam order.0.column string required the order by column name in ASC or DESC direction
     * @bodyParam search.value  string required the value to be searched in the records
     * @bodyParam start integer required the value from start getting records
     * @bodyParam length integer required the number of records want to fetch
     * @response json array type in variable data
     */
    public function index(Request $request)
    {
        $this->validateIndexRequest($request);
        //array of the searchble or order by columns as they are in data table
        $columnNames       = [
            0 => 'id',
            1 => 'user.first_name',
            2 => 'consultantTitle.name',
            3 => 'consultantCompany.name',
            4 => 'user.mobile',
            5 => 'user.email'
        ];
        $searchableColumns = [$columnNames[0], $columnNames[1], $columnNames[2],
            $columnNames[3], $columnNames[4], $columnNames[5]];
        $direction         = $request->input('order')[0]['dir']; //direction of the records
        $sort_column       = $columnNames[intval($request->input('order')[0]['column'])]; //order of the columns
        $search            = $request->input('search')['value']; // get the search value 
        // relations array with their searchable field names 
        $relations         = [
            'user' => ['first_name', 'mobile', 'email'],
            'consultantTitle' => ['name'],
            'consultantCompany' => ['name']
        ];
        $hasRelation       = 'user'; // has relaton name
        // cal the common service method to get the records 
        $response          = PrepareDataTableService::getTableDataWithMultipleRealtions('\Consultant',
                $sort_column, $direction, $request->input('start'),
                $request->input('length'), $searchableColumns, $search, true,
                $relations, [], [], $hasRelation, []);

        if ($response['total'] != 0) {
            return response()->json(['success' => TranslationService::lang('success',
                        $request->auth->language), 'draw' => $request->input('draw'),
                    'recordsTotal' => $response['total'],
                    'recordsFiltered' => $request->input('length'), 'data' => $response['records']],
                    200);
        }
        //if no record found then return empty data
        return response()->json(['success' => TranslationService::lang('success',
                    $request->auth->language), 'draw' => 0, 'recordsTotal' => 0,
                'recordsFiltered' => 0, 'data' => []], 200);
    }

    /**
     * Create a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam user.first_name string required the first name of the consultant user
     * @bodyParam user.email string required the email of the consultant user
     * @bodyParam user.mobile string required the mobile of the consultant user
     * @bodyParam consultant_company.id integer required the id of consultant company
     * @bodyParam consultant_title.id integer optional the id of the consultant title
     * @response json array type return the success error message with proper response code
     */
    public function store(Request $request)
    {
        $this->_validateConsultantRequest($request);
        $response = $this->consultantRepository->create($request->all(),
            $request->auth->id);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('consultant_created',
                        $request->auth->language), 'data' => $response], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_consultant_creation',
                    $request->auth->language)], 422);
    }

    /**
     * Retrieve a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam id integer required the id of the record to be fetched
     * @response json array type
     */
    public function edit(Request $request, int $id)
    {
        $request->request->add(['id' => $id]);
        $this->validate($request,
            ['id' => 'required|exists:consultants,id,deleted_at,NULL']);
        $response = $this->consultantRepository->getRecord($id);

        if (!empty($response)) {
            return response()->json(['success' => TranslationService::lang('success',
                        $request->auth->language), 'data' => $response], 200);
        }

        return response()->json(['error' => TranslationService::lang('no_record_found',
                    $request->auth->language)], 404);
    }

    /**
     * Update a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam id integer required the id of the record to be updated
     * @bodyParam user.id integer required the user id of the updated consultant record
     * @bodyParam user.first_name string required the first name of the consultant user
     * @bodyParam user.email string required the email of the consultant user
     * @bodyParam user.mobile string required the mobile of the consultant user
     * @bodyParam consultant_company.id integer required the id of consultant company
     * @bodyParam consultant_title.id integer optional the id of the consultant title
     * @response json array type
     */
    public function update(Request $request, int $id)
    {
        $request->request->add(['id' => $id]);
        $this->_validateUpdateConsultantRequest($request);
        $response = $this->consultantRepository->update($request->all(), $id);
        if ($response == 'success') {
            return response()->json(['success' => TranslationService::lang('consultant_updated',
                        $request->auth->language)], 200);
        } elseif ($response == 'not_found') {
            return response()->json(['error' => TranslationService::lang('record_with_this_id_not_found',
                        $request->auth->language)], 404);
        } else {
            return response()->json(['error' => TranslationService::lang('error_consultant_updation',
                        $request->auth->language)], 422);
        }
    }

    /**
     * Delete a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam id integer required the id of the record being deleted
     * @response type
     */
    public function destroy(Request $request, int $id)
    {
        $request->request->add(['id' => $id]);
        $this->validate($request,
            ['id' => 'required|exists:consultants,id,deleted_at,NULL']);
        $response = $this->consultantRepository->delete($id);
        //check for the response code and return the response accordingly
        if ($response == 'success') {
            return response()->json(['success' => TranslationService::lang('consultant_deleted',
                        $request->auth->language)], 200);
        } elseif ($response == 'not_found') {
            return response()->json(['error' => TranslationService::lang('record_with_this_id_not_found',
                        $request->auth->language)], 404);
        } else {
            return response()->json(['error' => TranslationService::lang('error_consultant_delete',
                        $request->auth->language)], 422);
        }
    }

    /**
     * Validate the request for required or other validations
     * @param type $request
     */
    private function _validateConsultantRequest($request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $this->validate($request,
            [
            'user.first_name' => 'required|max:191',
            'user.email' => 'required|max:191|email|unique:users,email',
            'user.mobile' => 'max:20',
            'consultant_company.id' => 'required|numeric|exists:consultant_companies,id,deleted_at,NULL',
            'consultant_title.id' => 'nullable|numeric|exists:consultant_titles,id,deleted_at,NULL',
            'notify' => 'max:1',
            ], $messages);
    }

    /**
     * validate the update consultant request
     * @param type $request
     */
    private function _validateUpdateConsultantRequest($request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $this->validate($request,
            [
            'user.id' => 'required|numeric|exists:users,id,deleted_at,NULL'],
            $messages);
        $id       = $request['user']['id'];
        $this->validate($request,
            [
            'user.first_name' => 'required|max:191',
            'user.email' => 'required|email|max:191|unique:users,email,'.$id.',id',
            'user.mobile' => 'max:20',
            'consultant_company.id' => 'required|numeric|exists:consultant_companies,id,deleted_at,NULL',
            'consultant_title.id' => 'nullable|numeric|exists:consultant_titles,id,deleted_at,NULL',
            'notify' => 'max:1',
            'id' => 'required|numeric|exists:consultants,id,deleted_at,NULL'
            ], $messages);
    }

    /**
     * validate the index function request
     * @param type $request
     */
    private function validateIndexRequest($request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $rules    = config('constant.index_request_validation');
        $this->validate($request, $rules, $messages);
    }
}