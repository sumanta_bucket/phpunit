<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Repositories\ConsultantTitleRepository;
use Illuminate\Http\Request;
use App\Services\TranslationService;
use App\Services\PrepareDataTableService;

/**
 * ConsultantTitleController operations
 *
 * Manage the CRUD related operations
 * @group ConsultantTitle
 * @author manpreetkaur
 */
class ConsultantTitleController extends Controller
{
    protected $consultant_title_repo;

    public function __construct(ConsultantTitleRepository $consultant_title_repository)
    {
        $this->consultant_title_repo = $consultant_title_repository;
    }

    /**
     * List all consultant titles
     *
     * List all the consultant titles with specific sorting order, search and limit
     * @bodyParam order.0.dir string required the direction of the records in ASC or DESC direction
     * @bodyParam order.0.column string required the order by column name in ASC or DESC direction
     * @bodyParam search.value  string required the value to be searched in the records
     * @bodyParam start integer required the value from start getting records
     * @bodyParam length integer required the number of records want to fetch
     * @response json array type in variable data
     */
    public function index(Request $request)
    {
        $this->validateIndexRequest($request);
        //make list of searchable
        $columnNames       = [
            0 => 'id',
            1 => 'name'
        ];
        $searchableColumns = [$columnNames[0], $columnNames[1]];
        $direction         = $request->input('order')[0]['dir'];
        $sort_column       = $columnNames[intval($request->input('order')[0]['column'])];
        $search            = $request->input('search')['value'];
        //call common serivce for table results
        $response          = PrepareDataTableService::getTableDataWithMultipleRealtions('\ConsultantTitle',
                $sort_column, $direction, $request->input('start'),
                $request->input('length'), $searchableColumns, $search);
        if ($response['total'] != 0) {
            return response()->json(['success' => TranslationService::lang('success',
                        $request->auth->language), 'draw' => $request->input('draw'),
                    'recordsTotal' => $response['total'],
                    'recordsFiltered' => $request->input('length'), 'data' => $response['records']],
                    200);
        }
        return response()->json(['success' => TranslationService::lang('success',
                    $request->auth->language), 'draw' => 0, 'recordsTotal' => 0,
                'recordsFiltered' => 0, 'data' => []], 200);
    }

    /**
     * create a consultant company
     * @bodypPram name string required the name of the consultant company
     * @response json array type
     */
    public function store(Request $request)
    {
        //validate the request
        $this->_validateRequest($request);
        $data     = $request->all();
        $response = $this->consultant_title_repo->create($data);
        if ($response['status'] == 1) {
            return response()->json(['success' => TranslationService::lang('consulant_title_created',
                        $request->auth->language), 'data' => $response['data']],
                    200);
        }
        return response()->json(['error' => TranslationService::lang('error_consulant_title_creation',
                    $request->auth->language)], 422);
    }

    /**
     * update a consultant company
     * @bodyParam id integer required the of the record to update
     * @bodyParam name string required the value to update
     * @response json array type
     */
    public function update(Request $request, int $id)
    {
        $request->request->add(['id' => $id]);
        $this->_validateRequest($request);
        $data     = $request->all();
        $response = $this->consultant_title_repo->update($id, $data);
        if ($response['status'] == 1) {
            return response()->json(['success' => TranslationService::lang('consulant_title_updated',
                        $request->auth->language), 'data' => $response['data']],
                    200);
        }
        return response()->json(['error' => TranslationService::lang('error_consulant_title_updation',
                    $request->auth->language)], 422);
    }

    /**
     * Delete a consultant company
     * @bodyParam id integer required the record id which we want to delete
     * @response json array type
     */
    public function delete(Request $request, int $id)
    {
        $request->request->add(['id' => $id]);
        $this->validate($request,
            ['id' => 'required|numeric|exists:consultant_titles,id,deleted_at,NULL']);
        $response = $this->consultant_title_repo->delete($id);

        if ($response) {
            return response()->json(['success' => TranslationService::lang('consulant_title_deleted',
                        $request->auth->language)], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_consulant_title_deletion',
                    $request->auth->language)], 422);
    }

    /**
     * validate the consultant title request
     * @param type $request
     */
    private function _validateRequest($request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $this->validate($request,
            [
            'name' => 'required|string|max:191',
            'id' => 'sometimes|required|exists:consultant_titles,id,deleted_at,NULL'
        ],$messages);
    }

    /**
     * validate the index function request
     * @param type $request
     */
    private function validateIndexRequest($request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $rules    = config('constant.index_request_validation');
        $this->validate($request, $rules, $messages);
    }
}