<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ContactPersonRepository;
use App\Services\TranslationService;
use App\Services\PrepareDataTableService;

/**
 * ContactPersonController operations
 *
 * Manage the CRUD related methods
 * @group ContactPerson
 * @author 
 */
class ContactPersonController extends Controller
{
    /**
     * local variable
     * @var type
     */
    protected $contactPersonRepository;

    /**
     * constructor of this controller
     * @param ContactPersonRepository $contactPersonRepository
     */
    public function __construct(ContactPersonRepository $contactPersonRepository)
    {
        $this->contactPersonRepository = $contactPersonRepository;
    }

    /**
     * List all the contact persons
     *
     * List all the contact persons with specific sorting order, search and limit
     * @bodyParam status integer required the type of record we want to fetch
     * @bodyParam order.0.dir string required the direction of the records in ASC or DESC direction
     * @bodyParam order.0.column string required the order by column name in ASC or DESC direction
     * @bodyParam search.value  string required the value to be searched in the records
     * @bodyParam start integer required the value from start getting records
     * @bodyParam length integer required the number of records want to fetch
     * @response json array type in variable data
     */
    public function index(Request $request,$status)
    {
        $userStatus = config('constant.user_status');
        if (in_array($status, ['0', '1']) == false) {
            return response()->json(['error' => TranslationService::lang('invalid_user_status',
                        $request->auth->language)], 422);
        }
        $this->_validateIndexRequest($request);
        //columns array as they are in the data table
        $columnNames = [
            'id',
            'user.title.option_value',
            'user.first_name',
            'user.last_name',
            'user.mobile',
            'user.telephone',
            'user.email',
            'customers.user',
            'user.notes'
        ];

        $searchableColumns = [$columnNames[0], $columnNames[1], $columnNames[2],
            $columnNames[3], $columnNames[4], $columnNames[5], $columnNames[6]];
        $direction         = $request->input('order')[0]['dir'];
        $sort_column       = $columnNames[intval($request->input('order')[0]['column'])];
        $search            = $request->input('search')['value'];
        $relations         = [
            'user' => ['first_name', 'last_name', 'address', 'city', 'mobile', 'telephone',
                'email'],
            'user.title' => ['option_value'],
            'customers.user' => ['first_name'],
            'customers.user.title' => ['option_value']
        ];
        $whereCondition    = [];
        $whereHasCondition = [];
        $hasRelation       = 'user';
        //user status 0=>archived records ,1=>active records
        if ($status == 0) {
            $response = $this->contactPersonRepository->trashedRecords($search,
                $sort_column, $direction, $request->input('length'),
                $request->input('start'));
        } else {
            //call service function for datatable records 
            $response = PrepareDataTableService::getTableDataWithMultipleRealtions('\ContactPerson',
                    $sort_column, $direction, $request->input('start'),
                    $request->input('length'), $searchableColumns, $search,
                    true, $relations, $whereCondition, [], $hasRelation,
                    $whereHasCondition);
        }
        if ($response['total'] != 0) {
            return response()->json(['success' => TranslationService::lang('success',
                        $request->auth->language), 'draw' => $request->input('draw'),
                    'recordsTotal' => $response['total'],
                    'recordsFiltered' => $request->input('length'), 'data' => $response['records']],
                    200);
        }
        return response()->json(['success' => TranslationService::lang('success',
                    $request->auth->language), 'draw' => 0, 'recordsTotal' => 0,
                'recordsFiltered' => 0, 'data' => []], 200);
    }

    /**
     * Retrieve Contact person titles
     * @response json type array
     */
    public function getContactPersonTitles(Request $request)
    {
        $titles = $this->contactPersonRepository->getContactPersonTitle();
        return response()->json(['success' => TranslationService::lang('success',
                    $request->auth->language), 'titles' => $titles], 200);
    }

    /**
     * create a contact person
     * @bodyParam user.first_name string required the first name of the user
     * @bodyParam user.last_name string optional the last name of the user
     * @bodyParam user.email email required the email of the user
     * @bodyParam user.mobile string optional the phone number of the user
     * @bodyParam user.avatar file optional the photo of the user of type:png,jpg,jpeg etc
     * @bodyParam user.notify boolean optional whether to notify the user or not
     * @bodyParam user.notes text optional the extra information regarding the user
     * @bodyParam user.title_id integer optional the title of the user
     * @bodyParam user.telephone string optional the telephone number of the user
     * @response json array type
     */
    public function store(Request $request)
    {
        $this->_validateContactPersonRequest($request);
        $data     = $request->all();
        $response = $this->contactPersonRepository->create($data,
            $request->auth->id);
        if ($response['status']) {
            return response()->json(['success' => TranslationService::lang('contact_person_created',
                        $request->auth->language), 'data' => $response['data']],
                    200);
        }
        return response()->json(['success' => TranslationService::lang('error_contact_person_creation',
                    $request->auth->language)], 422);
    }

    /**
     * Retrieve contact person
     * @bodyParam id integer required the id of the record we want to get
     * @response json type array
     */
    public function edit(Request $request, int $id)
    {
        $request->request->add(['id' => $id]);
        //validate the request id
        $this->validate($request,
            ['id' => 'required|numeric|exists:contact_persons,id,deleted_at,NULL']);
        $response = $this->contactPersonRepository->getContactPerson($id);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('success',
                        $request->auth->language), 'data' => $response], 200);
        }
        return response()->json(['success' => TranslationService::lang('not_found',
                    $request->auth->language)], 404);
    }

    /**
     * Update a contact person
     * @bodyParam id integer required the id of the contact person to update get using route
     * @bodyParam user.id integer required the user_id of the contact person
     * @bodyParam user.first_name string required the first name of the user
     * @bodyParam user.last_name string optional the last name of the user
     * @bodyParam user.email email required the email of the user
     * @bodyParam user.mobile string optional the phone number of the user
     * @bodyParam user.avatar file optional the photo of the user of type:png,jpg,jpeg etc
     * @bodyParam user.notify boolean optional whether to notify the user or not
     * @bodyParam user.notes text optional the extra information regarding the user
     * @bodyParam user.title_id integer optional the title of the user
     * @bodyParam user.telephone string optional the telephone number of the user
     * @response json array type 
     */
    public function update(Request $request, int $id)
    {
        $request->request->add(['id' => $id]);
        $this->_validateContactPersonRequest($request, 1);
        $data     = $request->all();
        $response = $this->contactPersonRepository->update($id, $data);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('contact_person_updated',
                        $request->auth->language)], 200);
        }
        return response()->json(['success' => TranslationService::lang('error_contact_person_updation',
                    $request->auth->language)], 422);
    }

    /**
     * Delete a contact person
     * @bodyParam id integer required the id of the record to be deleted
     * @response json array type
     */
    public function destroy(Request $request, int $id)
    {
        $request->request->add(['id' => $id]);
        //validate the request id
        $this->validate($request,
            ['id' => 'required|numeric|exists:contact_persons,id,deleted_at,NULL']);
        $response = $this->contactPersonRepository->delete($id);
        if ($response == 'success') {
            return response()->json(['success' => TranslationService::lang('contact_person_deleted',
                        $request->auth->language)], 200);
        } elseif ($response == 'not_found') {
            return response()->json(['error' => TranslationService::lang('record_with_this_id_not_found',
                        $request->auth->language)], 404);
        } else {
            return response()->json(['error' => TranslationService::lang('error_contact_person_delete',
                        $request->auth->language)], 422);
        }
    }

    /**
     * Restore a contact person
     * @bodyParam id integer required the contact person id to be restored
     * @return type
     */
    public function restore(Request $request, int $id)
    {
        $request->request->add(['id' => $id]);
        //validate the request id
        $this->validate($request,
            ['id' => 'required|numeric|exists:contact_persons,id,deleted_at,NOT_NULL']);
        $response = $this->contactPersonRepository->restore($id);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('contact_person_restored',
                        $request->auth->language)], 200);
        }
        return response()->json(['success' => TranslationService::lang('error_contact_person_restore',
                    $request->auth->language)], 422);
    }

    /**
     * change contact person status like active, archived or suspended
     * @bodyParam user_id integer required the id of the user whose status we want to update
     * @bodyParam status integer required the status value
     * @response json array type
     */
    public function changeUserStatus(Request $request, int $user_id, int $status)
    {
        $request['user_status'] = [1, 2, 3];
        /* add parameters to request for validation */
        $request->request->add([
            'user_id' => $user_id,
            'status' => $status
        ]);
        /* validate the attribute for specific condition */
        $this->validate($request,
            [
            'user_id' => 'required|integer|exists:users,id,deleted_at,NULL',
            'status' => 'required|integer|in_array:user_status.*'
        ]);
        $status_array           = config('constant.user_status_array');
        $status_value           = $status_array[$status];
        $response               = $this->contactPersonRepository->changeStatus($user_id,
            $status);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('contact_person_'.$status_value,
                        $request->auth->language)], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_contact_person_'.$status_value,
                    $request->auth->language)], 500);
    }

    /**
     * validate the request for specific condition
     * @param type $request
     */
    private function _validateContactPersonRequest($request, $update = 0)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $rules    = [
            'user.title_id' => 'numeric|exists:options,id,deleted_at,NULL',
            'user.first_name' => 'required|string|max:191',
            'user.notify' => 'sometimes|nullable|boolean',
            'user.avatar' => 'sometimes|nullable|file|mimes:jpeg,png,gif|max:10240',
            'user.mobile' => 'sometimes|nullable|string|max:12',
            'user.telephone' => 'sometimes|nullable|string|max:12',
            'user.notes' => 'sometimes||nullable',
            'user.last_name' => 'sometimes|nullable|string|max:191',
            'id' => 'sometimes|required|numeric|exists:contact_persons,id,deleted_at,NULL',
            'user.id' => 'sometimes|required|numeric|exists:users,id,deleted_at,NULL'
        ];
        if ($update) {
            $this->validate($request,
                ['user.id' => 'required|numeric|exists:users,id,deleted_at,NULL'],
                $messages);
            $id                  = $request['user']['id'];
            $rules['user.email'] = 'required|email|max:191|unique:users,email,'.$id.',id';
        } else {
            $rules['user.email'] = 'required|email|max:191|unique:users,email';
        }

        $this->validate($request, $rules, $messages);
    }

    /**
     * Index function validation
     * @param type $request
     */
    private function _validateIndexRequest($request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $rules    = config('constant.index_request_validation');
        $this->validate($request, $rules, $messages);
    }
}