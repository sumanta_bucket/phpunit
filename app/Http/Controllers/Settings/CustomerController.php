<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Repositories\CustomerRepository;
use Illuminate\Http\Request;
use App\Services\TranslationService;
use App\Services\PrepareDataTableService;

/**
 * Customers controller operations
 *
 * Manage the CURD related operations
 * @group Customers
 * @author manpreetkaur
 */
class CustomerController extends Controller
{
    /*
     * local variable for repository
     */
    protected $customerRepository;

    /**
     * Constructor for the controller
     * @param CustomerRepository $customerRepository
     * @return void
     */
    public function __construct(CustomerRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * List all customers
     * @bodyParam order.0.dir string required the direction of the records in ASC or DESC direction
     * @bodyParam order.0.column string required the order by column name in ASC or DESC direction
     * @bodyParam search.value  string required the value to be searched in the records
     * @bodyParam start integer required the value from start getting records
     * @bodyParam length integer required the number of records want to fetch
     * @response json array type in variable data
     */
    public function index(Request $request, int $status)
    {
        $userStatus = config('constant.user_status');
        if (!in_array($status, $userStatus)) {
            return response()->json(['error' => TranslationService::lang('invalid_user_status',
                        $request->auth->language)], 422);
        }
        $this->_validateIndexRequest($request);
        //the searchable columns array as they are listed in table 
        $columnNames = [
            0 => 'id',
            1 => 'user.first_name',
            2 => 'user.address',
            3 => 'user.city',
            4 => 'user.mobile',
            5 => 'user.email',
        ];

        $searchableColumns = [$columnNames[0], $columnNames[1], $columnNames[2],
            $columnNames[3], $columnNames[4], $columnNames[5]];
        $direction         = $request->input('order')[0]['dir'];
        $sort_column       = $columnNames[intval($request->input('order')[0]['column'])];
        $search            = $request->input('search')['value'];
        $relations         = [
            'user' => ['first_name', 'last_name', 'address', 'city', 'mobile', 'telephone',
                'email'],
            'user.title' => ['option_value'],
            'contact_persons.user' => ['first_name'],
            'contact_persons.user.title' => ['option_value']
        ];
        $hasRelation       = 'user';
        //when status=0 then archieved records are fetched
        if ($status == 0) {
            $response = $this->customerRepository->trashedRecords($search,
                $sort_column, $direction, $request->input('length'),
                $request->input('start'));
        } else { //when status=1 then active records are fetched
            $response = PrepareDataTableService::getTableDataWithMultipleRealtions('\Customer',
                    $sort_column, $direction, $request->input('start'),
                    $request->input('length'), $searchableColumns, $search,
                    true, $relations, [], [], $hasRelation, []);
        }
        $title = $this->customerRepository->contactPersonTitles(); // get contact person titles

        if ($response['total'] != 0) {
            return response()->json(['success' => TranslationService::lang('success',
                        $request->auth->language), 'draw' => $request->input('draw'),
                    'recordsTotal' => $response['total'],
                    'recordsFiltered' => $request->input('length'), 'data' => $response['records'],
                    'titles' => $title], 200);
        }
        return response()->json(['success' => TranslationService::lang('success',
                    $request->auth->language), 'draw' => 0, 'recordsTotal' => 0,
                'recordsFiltered' => 0, 'data' => []], 200);
    }

    /**
     * Create a customer
     * @bodyParam user.first_name string required the first_name field of the user
     * @bodyParam user.email string required the email field of the user
     * @bodyParam user.address string optional the address field of the user
     * @bodyParam user.city string optional the city field of the user
     * @bodyParam user.zip string optional the zip code of the customer
     * @bodyParam user.mobile string optional the mobile number of the customer
     * @bodyParam user.notes string optional extra information about the user
     * @bodyParam user.notify integer optional whether to notify the customer about account or not 1=true ,0=false
     * @bodyParam address2 string optional the address2 of the customer
     * @bodyParam country string optional the country code of the customer
     * @bodyParam categories.*. id integer optional the category id of the customer
     * @bodyParam contact_persons.*.id integer optional the contact person id of the customer    
     * @response json array type
     */
    public function store(Request $request)
    {
        $this->_validateCustomerRequest($request);
        $response = $this->customerRepository->create($request->all(),
            $request->auth->id);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('customer_created',
                        $request->auth->language)], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_customer_creation',
                    $request->auth->language)], 500);
    }

    /**
     * Retrieve a customer
     * @bodyParam id integer required the id of the record being fetchd
     * @response json array type
     */
    public function edit(Request $request, int $id)
    {
        $customer = $this->customerRepository->getCustomer($id);
        if (!empty($customer)) {
            return response()->json(['success' => TranslationService::lang('success',
                        $request->auth->language), 'data' => $customer], 200);
        }
        return response()->json(['error' => TranslationService::lang('no_record_found',
                    $request->auth->language)], 404);
    }

    /**
     * update a customer
     * @bodyParam id integer required the customer id to be updated
     * @bodyParam user.id integer required the user_id of the customer being updated
     * @bodyParam user.first_name string required the first_name field of the user
     * @bodyParam user.email string required the email field of the user
     * @bodyParam user.address string optional the address field of the user
     * @bodyParam user.city string optional the city field of the user
     * @bodyParam user.zip string optional the zip code of the customer
     * @bodyParam user.mobile string optional the mobile number of the customer
     * @bodyParam user.notes string optional extra information about the user
     * @bodyParam user.notify integer optional whether to notify the customer about account or not 1=true ,0=false
     * @bodyParam address2 string optional the address2 of the customer
     * @bodyParam country string optional the country code of the customer
     * @bodyParam categories.*. id integer optional the category id of the customer
     * @bodyParam contact_persons.*.id integer optional the contact person id of the customer
     * @response json array type
     */
    public function update(Request $request, int $id)
    {
        $request->request->add(['id' => $id]);
        $this->_validateCustomerRequest($request,1);
        $response = $this->customerRepository->update($request->all(), $id);

        if ($response == 'success') {
            return response()->json(['success' => TranslationService::lang('customer_updated',
                        $request->auth->language)], 200);
        } elseif ($response == 'not_found') {
            return response()->json(['error' => TranslationService::lang('record_with_this_id_not_found',
                        $request->auth->language)], 404);
        } else {
            return response()->json(['error' => TranslationService::lang('error_customer_updation',
                        $request->auth->language)], 422);
        }
    }

    /**
     * Delete a customer
     * @bodyParam id integer required the id if customer being deleted
     * @response json array type
     */
    public function destroy(Request $request, int $id)
    {
        //add new parameter to request
        $request->request->add(['id' => $id]);
        $this->validate($request,
            ['id' => 'required|exists:customers,id,deleted_at,NULL']);

        $response = $this->customerRepository->delete($id);
        if ($response == 'success') {
            return response()->json(['success' => TranslationService::lang('customer_deleted',
                        $request->auth->language)], 200);
        } elseif ($response == 'not_found') {
            return response()->json(['error' => TranslationService::lang('record_with_this_id_not_found',
                        $request->auth->language)], 404);
        } else {
            return response()->json(['error' => TranslationService::lang('error_customer_delete',
                        $request->auth->language)], 422);
        }
    }

    /**
     * Detach all the contact persons attached to the customer
     * @bodyParam customer_id integer required the id of the customer from where we want detach
     * @bodyParam contact_person_id integer required the id of the contact person to be detached
     * @response json array type
     */
    public function detachContacts(Request $request, $customer_id,
                                   $contact_person_id)
    {
        $request->request->add(['customer_id' => $customer_id, 'contact_person_id' => $contact_person_id]);
        $this->validate($request,
            [
            'customer_id' => 'required|numeric|exists:customers,id,deleted_at,NULL|digits_between:1,11',
            'contact_person_id' => 'required|numeric|exists:contact_persons,id,deleted_at,NULL|digits_between:1,11'
        ]);
        $response = $this->customerRepository->detachRecords($customer_id,
            $contact_person_id);
        if (!empty($response)) {
            return response()->json(['success' => TranslationService::lang('detached_successfully',
                        $request->auth->language)], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_detach',
                    $request->auth->language)], 422);
    }

    /**
     * Restore a customers
     * @bodyParam id integer required the id of the customer which we want to restore
     * @response json array type
     */
    public function restoreCustomers(Request $request, int $id)
    {
        $request->request->add(['id' => $id]);
        //validate the request id
        $this->validate($request,
            ['id' => 'required|numeric|exists:customers,id,deleted_at,NOT_NULL']);
        $response = $this->customerRepository->restore($id);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('customer_restored',
                        $request->auth->language)], 200);
        }
        return response()->json(['success' => TranslationService::lang('error_customer_restore',
                    $request->auth->language)], 422);
    }

    /**
     * Validate the request parameter for required or other validation
     * @param type $request
     */
    private function _validateCustomerRequest($request, $update = 0)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $rules    = [
            'user.first_name' => 'required|max:191',
            'user.address' => 'max:191',
            'user.city' => 'max:191',
            'user.zip' => 'max:10',
            'user.notify' => 'max:1',
            'address2' => 'max:191',
            'country' => 'max:10',
            'categories' => 'sometimes|array',
            'categories.*.id' => 'required|exists:options,id,deleted_at,NULL',
            'contact_persons' => 'nullable|array',
            'contact_persons.*.id' => 'nullable||exists:contact_persons,id,deleted_at,NULL',
            'id' => 'sometimes|required|integer|exists:customers,id,deleted_at,NULL'
        ];

        if ($update) {
            $this->validate($request,
                ['user.id' => 'required|numeric|exists:users,id,deleted_at,NULL'],
                $messages);
            $id                  = $request['user']['id'];
            $rules['user.email'] = 'required|email|max:191|unique:users,email, '.$id.',id';
        } else {
            $rules['user.email'] = 'required|email|max:191|unique:users,email';
        }
        $this->validate($request, $rules, $messages);
    }
    /*
     * validate the index function request
     */

    private function _validateIndexRequest($request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $rules    = config('constant.index_request_validation');
        $this->validate($request, $rules, $messages);
    }
}
