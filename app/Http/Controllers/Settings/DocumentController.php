<?php

namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\TranslationService;
use App\Repositories\DocumentRepository;
use App\Services\DocumentPathService;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;

/**
 * DocumentController
 * @group documents
 * @author manpreetkaur
 */
class DocumentController extends Controller
{
    protected $documentRepository;

    public function __construct(DocumentRepository $docRepo)
    {
        $this->documentRepository = $docRepo;
    }

    /**
     * upload the documents to existing project
     * @bodyParam project_id integer required the id of the project in which we are uploading the documents
     * @bodyParam document array required the document being uploaded and is of type PDF
     * @response json array type
     */
    public function uploadDocument(Request $request, int $project_id)
    {
        $message  = TranslationService::customMesages($request->auth->language);
        $request->request->add(['project_id' => $project_id]);
        $this->validate($request,
            [
            'document' => 'required|array',
            'document.*' => 'required|mimes:pdf|max:10240',
            'project_id' => 'required|exists:projects,id,deleted_at,NULL'
            ], $message);
        $data     = $request->all();
        $response = $this->documentRepository->uploadDocuments($data);
        if ($response['status']) {
            return response()->json(['success' => TranslationService::lang('document_uploaded_successfully',
                        $request->auth->language), 'data' => $response['data']],
                    200);
        }
        return response()->json(['error' => TranslationService::lang('error_document_uploaded',
                    $request->auth->language)], 422);
    }

    /**
     * upload the documents to existing project
     * @bodyParam id integer required the id of the document being deleted
     * @response json array type
     */
    public function deleteDocument(Request $request, int $id)
    {
        $request->request->add(['id' => $id]);
        $message  = TranslationService::customMesages($request->auth->language);
        $this->validate($request,
            [
            'id' => 'required|exists:documents,id'
            ], $message);
        $response = $this->documentRepository->deleteDocument($id);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('document_deleted',
                        $request->auth->language)], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_document_delete',
                    $request->auth->language)], 422);
    }

    /**
     * get the document encoded path
     * @param Request $request
     * @param string $path
     * @return type
     */
    public function getDocument(Request $request, string $path)
    {
        $response = DocumentPathService::decodePath($path);
        if ($response['status'] == 1) {
            $type     = mime_content_type($response['path']);
            $file     = File::get($response['path']);
            $response = Response::make($file, 200);
            $response->header('Content-Type', $type);
            return $response;
        } else {
            return response()->json(['error' => TranslationService::lang($response['error'],
                        1)], 404);
        }
    }
}