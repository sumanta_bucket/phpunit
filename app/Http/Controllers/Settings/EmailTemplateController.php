<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\EmailTemplateRepository;
use App\Services\TranslationService;
use App\Services\PrepareDataTableService;

/**
 * EmailTemplate Operations
 *
 * Manage the CRUD related operations
 * @group EmailTemplate
 * @author manpreetkaur
 */
class EmailTemplateController extends Controller
{
    protected $emailTemplateRepository;

    /**
     * Constructor for this controller
     * @bodyParam  App\Repositories\EmailTemplateRepository  $templateRepository
     * @@response void
     */
    public function __construct(EmailTemplateRepository $templateRepository)
    {
        $this->emailTemplateRepository = $templateRepository;
    }

    /**
     * List all email templates
     *
     * List all the email templates with specific sorting order, search and limit
     * @bodyParam order.0.dir string required the direction of the records in ASC or DESC direction
     * @bodyParam order.0.column string required the order by column name in ASC or DESC direction
     * @bodyParam search.value  string required the value to be searched in the records
     * @bodyParam start integer required the value from start getting records
     * @bodyParam length integer required the number of records want to fetch
     * @response json array type in variable data
     */
    public function index(Request $request)
    {
        $this->_validateIndexRequest($request);
        $searchableColumns = $columnNames       = [
            'emailTemplateType.type',
            'name',
            'emailTemplateType.name',
            'is_default'
        ];

        $direction         = $request->input('order')[0]['dir']; //direction in which we want to fetch records like ASC or DESC
        $sort_column       = $columnNames[intval($request->input('order')[0]['column'])]; //sort by column name
        $search            = $request->input('search')['value']; //value to be search
        //with relation name 
        $relations         = [
            'emailTemplateType' => ['type', 'name']
        ];
        $whereRelation     = ['name', 'is_default']; // where condition in the model
        $whereCondition    = [];
        $whereHasCondition = [];
        $hasRelation       = 'emailTemplateType';  //has relation in the model
        //call the common service function to get records
        $response          = PrepareDataTableService::getTableDataWithMultipleRealtions('\EmailTemplate',
                $sort_column, $direction, $request->input('start'),
                $request->input('length'), $searchableColumns, $search, true,
                $relations, $whereCondition, $whereRelation, $hasRelation,
                $whereHasCondition);
        if ($response['total'] != 0) {
            return response()->json(['success' => TranslationService::lang('success',
                        $request->auth->language), 'draw' => $request->input('draw'),
                    'recordsTotal' => $response['total'],
                    'recordsFiltered' => $request->input('length'), 'data' => $response['records']],
                    200);
        }
        return response()->json(['success' => TranslationService::lang('success',
                    $request->auth->language), 'draw' => 0, 'recordsTotal' => 0,
                'recordsFiltered' => 0, 'data' => []], 200);
    }

    /**
     * create a email template
     * @bodyParam email_template_type_id number required the email type template id
     * @bodyParam name string required the name of the template
     * @bodyParam template string required the body of the email template
     * @response json array type
     */
    public function store(Request $request)
    {
        $this->_validateTemplateRequest($request);
        $data     = $request->all();
        $template = $this->emailTemplateRepository->create($data);
        if ($template) {
            return response()->json(['success' => TranslationService::lang('template_created',
                        $request->auth->language)], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_template_creation',
                    $request->auth->language)], 500);
    }

    /**
     * Retrieve a email template
     * @bodyParam id integer required get record of specific id
     * @response json array type
     */
    public function edit(Request $request, int $id)
    {
        $request->request->add(['id' => $id]);
        $messages = TranslationService::customMesages($request->auth->language);
        $this->validate($request,
            [
            'id' => 'required|numeric|exists:email_templates,id,deleted_at,NULL'
            ], $messages);
        $record   = $this->emailTemplateRepository->getSpecificEmailTemplate($id);
        if (!empty($record)) {
            return response()->json(['success' => TranslationService::lang('success',
                        $request->auth->language), 'data' => $record], 200);
        }
        return response()->json(['error' => TranslationService::lang('no_record_found',
                    $request->auth->language)], 404);
    }

    /**
     * Update a Email template
     * @bodyParam id integer required this is as a route parameter like update/2
     * @bodyParam email_template_type_id number required the email type template id
     * @bodyParam name string required the name of the template
     * @bodyParam template string required the body of the email template
     * @response json array
     */
    public function update(Request $request, int $id)
    {
        $request->request->add(['id' => $id]);
        $this->_validateTemplateRequest($request);
        $data     = $request->all();
        $response = $this->emailTemplateRepository->update($data, $id);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('template_updated',
                        $request->auth->language)], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_template_updation',
                    $request->auth->language)], 500);
    }

    /**
     * Delete a email template
     * @bodyParam id integer required this field is passed in route like /delete/1
     * @response json array
     */
    public function destroy(Request $request, int $id)
    {
        $request->request->add(['id' => $id]);
        $messages = TranslationService::customMesages($request->auth->language);
        $this->validate($request,
            [
            'id' => 'required|numeric|exists:email_templates,id,deleted_at,NULL'
            ], $messages);
        $deleted  = $this->emailTemplateRepository->delete($id);
        if ($deleted) {
            return response()->json(['success' => TranslationService::lang('template_deleted',
                        $request->auth->language)], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_template_delete',
                    $request->auth->language)], 500); 
    }

    /**
     * Get Email Template types
     * @param Request $request
     * @boyparam type int required this is as a route parameter like /getType/1, 1= email and 2= sms
     * @response json array
     */
    public function getEmailTemplateType(Request $request, int $type)
    {
        $check = in_array($type, [1, 2]);
        if (!$check) {
            return response()->json(['error' => TranslationService::lang('invalid_email_template_type',
                        $request->auth->language)], 422);
        }

        $emailType = $this->emailTemplateRepository->getEmailTypeTemplate($type);

        if (!empty($emailType)) {
            return response()->json(['success' => TranslationService::lang('success',
                        $request->auth->language), 'data' => $emailType], 200);
        }
        return response()->json(['error' => TranslationService::lang('no_record_found',
                    $request->auth->language)], 404);
    }

    /**
     * Get Email Type Placeholder
     * @param Request $request
     * @bodyParam id int required this is id of the email type template
     * @response json array type
     */
    public function getEmailTypePlaceholders(Request $request, int $id)
    {
        $request->request->add(['id' => $id]);
        $messages = TranslationService::customMesages($request->auth->language);
        $this->validate($request,
            [
            'id' => 'required|numeric|exists:email_template_types,id,deleted_at,NULL'
            ], $messages);
        $placeholders = $this->emailTemplateRepository->getEmailTypePlaceholders($id);
        if (!empty($placeholders)) {
            return response()->json(['success' => TranslationService::lang('success',
                        $request->auth->language), 'data' => $placeholders], 200);
        }
        return response()->json(['error' => TranslationService::lang('no_record_found',
                    $request->auth->language)], 404);
    }

    /**
     * Test Sms Template method
     * @bodyParam phone_number string required the phone number on which we send sms please add country code before phone number like +91
     * @bodyParam template_body string required template body which we send as sms body
     * @response json array
     */
    public function testSmsTemplate(Request $request)
    {
        $this->_validateTestSmsTemplateRequest($request);
        $response = $this->emailTemplateRepository->sendSms($request['phone_number'],
            $request['template_body']);
        if ($response == "queued") {
            return response()->json(['success' => TranslationService::lang('success',
                        $request->auth->language)], 200);
        }
        return response()->json(['error' => $response], 400);
    }

    /**
     * Test email template method
     * @param Request $request
     * @bodyParam email string required the email to which we send the template for test
     * @bodyParam template_body string required template body which we send as email body
     * @response json array type
     */
    public function testEmailTemplate(Request $request)
    {
        $this->_validateEmailTemplateRequest($request);
        $data     = $request->all();
        $response = $this->emailTemplateRepository->sendEmail($data);
        if ($response == true) {
            return response()->json(['success' => TranslationService::lang('email_sent_to',
                        $request->auth->language, $request['email'])], 200);
        }
        return response()->json(['error' => $response], 400);
    }

    /**
     * Change default template
     * @bodyParam id integer required the id of email template whose default value we want to change
     * @response json array type
     */
    public function changeDefaultTemplate(Request $request, int $id)
    {
        $request->request->add(['id' => $id]);
        $messages = TranslationService::customMesages($request->auth->language);
        $this->validate($request,
            [
            'is_default' => 'required|boolean',
            'id' => 'required|numeric|exists:email_templates,id,deleted_at,NULL'],
            $messages);
        $data     = $request->all();
        $response = $this->emailTemplateRepository->changeDefaultStatus($id,
            $data);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('default_template_updated',
                        $request->auth->language)], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_template_default',
                    $request->auth->language)], 500);
    }

    /**
     * Validate Http Request method
     * check variables for required or other validations
     * @response json array
     */
    private function _validateTemplateRequest($request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $this->validate($request,
            [
            'email_template_type_id' => 'required|integer|exists:email_template_types,id,deleted_at,NULL',
            'name' => 'required|string|max:191',
            'template' => 'required|string',
            'subject' => 'required|string|max:191',
            'id' => 'sometimes|required|numeric|exists:email_templates,id,deleted_at,NULL'
            ], $messages);
    }

    /**
     * Validate function for sms template method
     * @param type $request
     */
    private function _validateTestSmsTemplateRequest($request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $this->validate($request,
            [
            'phone_number' => 'required|numeric',
            'template_body' => 'required'
            ], $messages);
    }

    /**
     * validate email template method
     * @param type $request
     */
    private function _validateEmailTemplateRequest($request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $this->validate($request,
            [
            'email' => 'required|email|max:191',
            'template_body' => 'required',
            'subject' => 'required|string|max:191',
            ], $messages);
    }

    /**
     * validate the index function request
     * @param type $request
     */
    private function _validateIndexRequest($request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $rules    = config('constant.index_request_validation');
        $this->validate($request, $rules, $messages);
    }
}