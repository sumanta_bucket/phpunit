<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Services\TranslationService;
use App\Repositories\EmployeeRepository;
use Illuminate\Http\Request;
use App\Services\PrepareDataTableService;

/**
 * Employee Controller operations
 *
 * Manage the CRUD related operations
 * @group Employee
 * @author manpreetkaur
 */
class EmployeeController extends Controller
{
    /* local variable */
    protected $employee_repository;

    /**
     * constructor for this controller
     * @param EmployeeRepository $employee_repo
     */
    public function __construct(EmployeeRepository $employee_repo)
    {
        $this->employee_repository = $employee_repo;
    }

    /**
     * List all employees
     *
     * List all the employees with specific sorting order, search and limit
     * @bodyParam order.0.dir string required the direction of the records in ASC or DESC direction
     * @bodyParam order.0.column string required the order by column name in ASC or DESC direction
     * @bodyParam search.value  string required the value to be searched in the records
     * @bodyParam start integer required the value from start getting records
     * @bodyParam length integer required the number of records want to fetch
     * @response json array type in variable data
     */
    public function index(Request $request, $status)
    {

        if (in_array($status, ['0', '1']) == false) {
            return response()->json(['error' => TranslationService::lang('invalid_user_status',
                        $request->auth->language)], 422);
        }
        $this->_validateIndexRequest($request);
        $searchableColumns = $columnNames       = [
            'user.title.option_value',
            'user.first_name',
            'user.address',
            'user.city',
            'user.zip',
            'user.mobile',
            'user.email'
        ];
        $where             = [];
        $direction         = $request->input('order')[0]['dir'];
        $sort_column       = $columnNames[intval($request->input('order')[0]['column'])];
        $sort_column       = $request->input('draw') === 1 ? 'id' : $sort_column;
        $search            = $request->input('search')['value'];
        //with the user relation
        $hasRelation       = 'user';
        $withRelation      = true;
        $relations         = [
            'user' => ['first_name', 'last_name', 'email', 'mobile', 'address', 'city',
                'zip'],
            'user.title' => ['option_value']
        ];
        if ($status == 0) {
            $response = $this->employee_repository->trashedRecords($search,
                $sort_column, $direction, $request->input('length'),
                $request->input('start'));
        } else {
            $searchArr = explode(' ', $search);
            if ($search != null && count($searchArr) == 2) {
                $response = PrepareDataTableService::getTableDataWithMultipleRealtions('\Employee',
                        $sort_column, $direction, 0, PHP_INT_MAX,
                        $searchableColumns, $searchArr[0], $withRelation,
                        $relations, $where, [], $hasRelation);
                $result   = $response['records']->filter(function ($value, $key) use($searchArr) {
                    if ($value->last_name == null) return true;
                    return stripos($value->last_name, $searchArr[1]) !== -1 ? true
                            : false;
                });
                $response['total'] = $result->count();
                if ($direction == 'asc') {
                    $response['records'] = $result->sortBy($sort_column,
                            SORT_NATURAL | SORT_FLAG_CASE)->values()->splice($request->input('start'))->take($request->input('length'));
                } else {
                    $response['records'] = $result->sortByDesc($sort_column,
                            SORT_NATURAL | SORT_FLAG_CASE)->values()->splice($request->input('start'))->take($request->input('length'));
                }
            } else {
                $response = PrepareDataTableService::getTableDataWithMultipleRealtions('\Employee',
                        $sort_column, $direction, $request->input('start'),
                        $request->input('length'), $searchableColumns, $search,
                        $withRelation, $relations, $where, [], $hasRelation);
            }
        }
        //call common serivce for table results
        //if response has some results then return results otherwise return empty data
        if ($response['total'] != 0) {
            return response()->json(['success' => TranslationService::lang('success',
                        $request->auth->language), 'draw' => $request->input('draw'),
                    'recordsTotal' => $response['total'],
                    'recordsFiltered' => $request->input('length'), 'data' => $response['records']],
                    200);
        }
        return response()->json(['success' => TranslationService::lang('success',
                    $request->auth->language), 'draw' => 0, 'recordsTotal' => 0,
                'recordsFiltered' => 0, 'data' => []], 200);
    }

    /**
     * Create a employee
     * @bodyParam first_name string required the first name field of the employee
     * @bodyParam last_name string required the last name field of the employee
     * @bodyParam email string required the email field of the employee
     * @bodyParam address string optional the address field of the employee
     * @bodyParam city string optional the city field of the employee
     * @bodyParam zip string optional the zip code of the employee
     * @bodyParam mobile string optional the mobile of the employee
     * @bodyParam title_id integer optional the title of employee
     * @bodyParam notify boolean optional whether to notify or not of account creation
     * @bodyParam relatives array optional the relatives of the employee
     * @response json array type
     */
    public function store(Request $request)
    {
        $this->_validateEmployeeRequest($request);
        $data     = $request->all();
        $response = $this->employee_repository->create($data, $request->auth->id);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('employee_created',
                        $request->auth->language)], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_employee_creation',
                    $request->auth->language)], 500);
    }

    /**
     * Retrieve a employee
     * @bodyParam id integer required the id of the employee whose records  we want to fetch
     * @response json array type
     */
    public function edit(Request $request, int $id)
    {
        $request->request->add(['id' => $id]);
        $this->validate($request,
            [
            'id' => 'required|numeric|exists:employees,id,deleted_at,NULL'
        ]);
        $response = $this->employee_repository->edit($id);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('success',
                        $request->auth->language), 'data' => $response], 200);
        }
        return response()->json(['error' => TranslationService::lang('no_record_found',
                    $request->auth->language)], 404);
    }

    /**
     * update a employee
     * @bodyParam id integer required the id of the employee whose records are being updated
     * @bodyParam first_name string required the first name field of the employee
     * @bodyParam last_name string required the last name field of the employee
     * @bodyParam email string required the email field of the employee
     * @bodyParam address string optional the address field of the employee
     * @bodyParam city string optional the city field of the employee
     * @bodyParam zip string optional the zip code of the employee
     * @bodyParam mobile string optional the mobile of the employee
     * @bodyParam title_id integer optional the title of employee
     * @bodyParam notify boolean optional whether to notify or not of account creation
     * @bodyParam relatives array optional the relatives of the employee
     * @response type json array
     */
    public function update(Request $request, int $id)
    {
        $request->request->add(['id' => $id]);
        $this->_validateEmployeeUpdateRequest($request);
        $data     = $request->all();
        $response = $this->employee_repository->update($id, $data);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('employee_updated',
                        $request->auth->language)], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_employee_updation',
                    $request->auth->language)], 500);
    }

    /**
     * delete a employee
     * @bodyParam Request request post the default route parameter
     * @bodyParam id integer required the employee id being deleted
     * @response json array type
     */
    public function destroy(Request $request, int $id)
    {
        //add new parameter to request
        $request->request->add(['id' => $id]);
        $this->validate($request,
            [
            'id' => 'required|numeric|exists:employees,id,deleted_at,NULL'
        ]);

        $response = $this->employee_repository->delete($id);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('employee_deleted',
                        $request->auth->language)], 200);
        }
        return response()->json(['error' => TranslationService::lang('employee_deleted',
                    $request->auth->language)], 500);
    }

    /**
     * Restore a employee
     * @bodyParam Request request required the default route parameter
     * @bodyParam id integer required the id of the employee we want to restore
     * @response json array type
     */
    public function restore(Request $request, int $id)
    {
        $request->request->add(['id' => $id]);
        //validate the request id
        $this->validate($request,
            ['id' => 'required|numeric|exists:employees,id,deleted_at,NOT_NULL']);
        $response = $this->employee_repository->restore($id);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('employee_restored',
                        $request->auth->language)], 200);
        }
        return response()->json(['success' => TranslationService::lang('error_employee_restore',
                    $request->auth->language)], 422);
    }

    /**
     * validate the request for some conditions
     * @param type $request
     */
    private function _validateEmployeeRequest($request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $rules    = [
            'first_name' => 'required|string|max:191',
            'last_name' => 'nullable|string|max:191',
            'email' => 'required|email|unique:users|max:191',
            'address' => 'nullable|string',
            'city' => 'nullable|string|max:191',
            'zip' => 'nullable|string|max:10',
            'mobile' => 'nullable|max:10',
            'title_id' => 'nullable|numeric|exists:options,id,deleted_at,NULL',
            'notify' => 'sometimes|nullable|boolean'
        ];
        $data     = $request->all();
        if (array_key_exists('relatives', $data)) {
            $relative_key                         = array_keys($request['relatives']);
            $request->request->add(['relative_key' => $relative_key]);
            $rules['relative_key.*']              = 'exists:relatives,id,deleted_at,NULL';
            $rules['relatives.*.relationship_id'] = 'required|exists:relationships,id,deleted_at,NULL';
        }
        $this->validate($request, $rules, $messages);
    }

    /**
     * validate the employee update request
     * @param type $request
     */
    private function _validateEmployeeUpdateRequest($request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $this->validate($request,
            ['user_id' => 'required|numeric|exists:users,id,deleted_at,NULL'],
            $messages);

        $rules = [
            'id' => 'required|numeric|exists:employees,id,deleted_at,NULL',
            'first_name' => 'required|string|max:191',
            'last_name' => 'nullable|string|max:191',
            'email' => 'required|email|unique:users,email,'.$request['user_id'].',id|max:191',
            'address' => 'nullable|string',
            'city' => 'nullable|string|max:191',
            'zip' => 'nullable|string|max:10',
            'mobile' => 'nullable|max:10',
            'title_id' => 'nullable|numeric|exists:options,id,deleted_at,NULL',
            'notify' => 'sometimes|nullable|boolean'
        ];
        $data  = $request->all();
        if (array_key_exists('relatives', $data)) {
            $relative_key                         = array_keys($request['relatives']);
            $request->request->add(['relative_key' => $relative_key]);
            $rules['relative_key.*']              = 'exists:relatives,id,deleted_at,NULL';
            $rules['relatives.*.relationship_id'] = 'required|exists:relationships,id,deleted_at,NULL';
        }
        $this->validate($request, $rules, $messages);
    }

    /**
     * validate the index function for request parameters
     * @param type $request
     */
    private function _validateIndexRequest($request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $rules    = config('constant.index_request_validation');
        $this->validate($request, $rules, $messages);
    }
}