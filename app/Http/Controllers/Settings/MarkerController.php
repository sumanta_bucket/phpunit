<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Repositories\MarkerRepository;
use Illuminate\Http\Request;
use App\Services\TranslationService;

/**
 * Marker Controller operations
 *
 * Manage the CRUD related operations
 * @group Markers
 * @author manpreetkaur
 */
class MarkerController extends Controller
{
    protected $markerRepository;

    /**
     * Constructor for this controller
     * @param MarkerRepository $markerRepository
     */
    public function __construct(MarkerRepository $markerRepository)
    {
        $this->markerRepository = $markerRepository;
    }

    /**
     * Get the required data for marker creation
     * @bodyParam Request request get the default parameter of request
     * @bodyParam type integer required pass this as url parameter
     * @response json array type
     */
    public function getRequiredDataForNewMarker(Request $request, int $type)
    {
        $typeCheck = in_array($type, config('constant.marker_types'));
        if (!$typeCheck) {
            return response()->json(['error' => TranslationService::lang('wrong_type',
                        $request->auth->language)], 422);
        }
        $response = $this->markerRepository->getRequiredInformation($type);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('success',
                        $request->auth->language), 'data' => $response], 200);
        }
        return response()->json(['error' => TranslationService::lang('not_found',
                    $request->auth->language)], 404);
    }

    /**
     * create a marker
     * @bodyParam type integer required the type of the marker like 1=>insulation and 2=>firesealing this should be passed through route
     * @bodyparam section_id integer required the foreign key for section table
     * @bodyparam hole_type integer required the type like beam or pillar or column
     * @bodyparam fire_class_id integer required the fire class for marker
     * @bodyparam status integer required the status of the marker being added 1=>completed, 2=>damaged, 3=>pending
     * @bodyparam photo file optional the photo array with multiple photos
     * @bodyparam notes string optional the extra notes added for the informational purpose
     * @bodyparam x-axis decimal required location of the marker on pdf in vertical way
     * @bodyparam y-axis decimal required location of the marker on pdf in horizontal way
     * @bodyparam predefined integer required the predefined value of the marker being added
     * @bodyparam fire_class_id integer required the fire class related to the marker
     * @bodyparam added_by_name string required the name of the person who added the marker
     * @bodyparam marker_date date required the on which the marker added
     * @bodyparam quantity integer required the quantity for marker [note]required in case of type =2
     * @bodyparam firesealing_type_id integer optional the fire sealing id for the relation [note]required in case of type =2
     * @bodyparam penetration_type_id integer required the fire sealing id for the relation [note]required in case of type =2
     * @bodyparam dimension1 string required the fire sealing id for the relation [note]required in case of type =2
     * @bodyparam dimension2 string required the fire sealing id for the relation [note]required in case of type =2
     * @bodyparam insulation_material_id string required the fire sealing id for the relation [note]required in case of type =1
     * @bodyparam insulation_profile_id string required the fire sealing id for the relation [note]required in case of type =1
     * @bodyparam dimensions string required the fire sealing id for the relation [note]required in case of type =1
     * @bodyparam thickness integer optional [note]required in case of type =1
     * @bodyparam prescribed integer optional [note]required in case of type =1
     * @bodyparam measured integer optional [note]required in case of type =1
     * @response json type array with required details
     */
    public function store(Request $request, int $section_id, int $type)
    {
        $typeCheck = in_array($type, config('constant.marker_types')); //check whether the entered type exists or not
        if (!$typeCheck) {
            return response()->json(['error' => TranslationService::lang('wrong_type',
                        $request->auth->language)], 422);
        }
        $request->request->add(['section_id' => $section_id]);
        $this->_validateMarkerRequest($request, $type,1);
        $data                = $request->all();
        $role                = $request->auth->roles->pluck('name');
        $data['status_type'] = $this->_getMarkerStatusType($role[0],
            $data['status'], $data['predefined']); //get the marker status_type according t the logged in user
        //call create marker funtion
        $response            = $this->markerRepository->createMarker($data,
            $type, $request->auth);
        if ($response['status']) {
            return response()->json(['success' => TranslationService::lang('marker_created',
                        $request->auth->language), 'data' => $response['data']],
                    200);
        }
        return response()->json(['error' => TranslationService::lang('error_marker_creation',
                    $request->auth->language)], 422);
    }

    /**
     * Update a marker
     * @bodyParam id integer required the id of the marker
     * @bodyParam type integer required the type of the marker like 1=>insulation and 2=>firesealing this should be passed through route
     * @bodyparam section_id integer required the foreign key for section table
     * @bodyparam hole_type integer required the type like beam or pillar or column
     * @bodyparam fire_class_id integer required the fire class for marker
     * @bodyparam status integer required the status of the marker being added 1=>completed, 2=>damaged, 3=>pending
     * @bodyparam photo file optional the photo array with multiple photos
     * @bodyparam notes string optional the extra notes added for the informational purpose
     * @bodyparam x-axis decimal required location of the marker on pdf in vertical way
     * @bodyparam y-axis decimal required location of the marker on pdf in horizontal way
     * @bodyparam predefined integer required the predefined value of the marker being added
     * @bodyparam fire_class_id integer required the fire class related to the marker
     * @bodyparam added_by_name string required the name of the person who added the marker
     * @bodyparam marker_date date required the on which the marker added
     * @bodyparam quantity integer required the quantity for marker [note]required in case of type =2
     * @bodyparam firesealing_type_id integer optional the fire sealing id for the relation [note]required in case of type =2
     * @bodyparam penetration_type_id integer required the fire sealing id for the relation [note]required in case of type =2
     * @bodyparam dimension1 string required the fire sealing id for the relation [note]required in case of type =2
     * @bodyparam dimension2 string required the fire sealing id for the relation [note]required in case of type =2
     * @bodyparam insulation_material_id string required the fire sealing id for the relation [note]required in case of type =1
     * @bodyparam insulation_profile_id string required the fire sealing id for the relation [note]required in case of type =1
     * @bodyparam dimensions string required the fire sealing id for the relation [note]required in case of type =1
     * @bodyparam thickness integer optional [note]required in case of type =1
     * @bodyparam prescribed integer optional [note]required in case of type =1
     * @bodyparam measured integer optional [note]required in case of type =1
     * @response json type array with required details
     */
    public function update(Request $request, int $section_id, int $id, int $type)
    {
        $typeCheck = in_array($type, config('constant.marker_types'));
        if (!$typeCheck) {
            return response()->json(['error' => TranslationService::lang('wrong_type',
                        $request->auth->language)], 422);
        }
        $request->request->add(['section_id' => $section_id]);
        $request->request->add(['id' => $id]);
        $this->_validateMarkerRequest($request, $type);
        $data                = $request->all();
        $role                = $request->auth->roles->pluck('name');
        $data['status_type'] = $this->_getMarkerStatusType($role[0],
            $data['status'], $data['predefined']);

        $response = $this->markerRepository->updateMarker($id,
            $request->auth->id, $type, $data);
        if ($response['status']) {
            return response()->json(['success' => TranslationService::lang('marker_updated',
                        $request->auth->language), 'data' => $response['data']],
                    200);
        }
        return response()->json(['error' => TranslationService::lang('error_marker_updation',
                    $request->auth->language)], 422);
    }

    /**
     * Delete a marker
     * @bodyParam id integer required the id of the marker to be delete
     * @response json array type
     */
    public function destroy(Request $request, int $id, int $type)
    {
        $typeCheck = in_array($type, config('constant.marker_types'));
        if (!$typeCheck) {
            return response()->json(['error' => TranslationService::lang('wrong_type',
                        $request->auth->language)], 422);
        }
        $response = $this->markerRepository->delete($id, $type);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('marker_deleted',
                        $request->auth->language)], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_marker_deletion',
                    $request->auth->language)], 422);
    }

    /**
     * delete the individual marker photo
     * @bodyParam id integer required the id of the marker_photo to which we want to delte
     * @response json array type
     */
    public function deleteMarkerPhoto(Request $request, int $id, int $type)
    {

        $typeCheck = in_array($type, config('constant.marker_types'));
        if (!$typeCheck) {
            return response()->json(['error' => TranslationService::lang('wrong_type',
                        $request->auth->language)], 422);
        }
        $response = $this->markerRepository->deleteMarkerPhoto($id, $type);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('photo_deleted',
                        $request->auth->language)], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_photo_deletion',
                    $request->auth->language)], 422);
    }

    /**
     * get status type from role and predefined
     * @param type $role
     * @param type $status
     * @param type $predefined
     * @return int
     */
    private function _getMarkerStatusType($role, $status, $predefined)
    {
        $type = 0;
        if (($role == 'employee' || $role == 'planner' || $role == 'admin') && $status
            == 1 && $predefined == 0) {
            $type = 2;
        } elseif (($role == 'employee' || $role == 'planner' || $role == 'admin')
            && $status == 3 && $predefined == 0) {
            $type = 3;
        } elseif (($role == 'employee' || $role == 'planner' || $role == 'admin')
            && $status == 1 && $predefined == 1) {
            $type = 4;
        } elseif (($role == 'employee' || $role == 'planner' || $role == 'admin')
            && $status == 3 && $predefined == 1) {
            $type = 5;
        } elseif (($role == 'customer' || $role == 'planner' || $role == 'admin'
            || $role == 'employee') && $status == 2) {
            $type = 6;
        } else {
            $type = 1;
        }
        return $type;
    }

    /**
     * validate marker request
     * @param type $request
     */
    private function _validateMarkerRequest($request, int $type,$add=0)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $rules    = [
            'section_id' => 'required|numeric|exists:sections,id,deleted_at,NULL',
            'hole_type' => 'required|numeric',
            'status' => 'required|numeric',
            'photo.*' => 'sometimes|required|file|mimes:jpeg,png,gif|max:10240',
            'notes' => 'string',
            'y_axis' => 'required|numeric',
            'x_axis' => 'required|numeric',
            'predefined' => 'required|numeric',
            'fire_class_id' => 'required|numeric|exists:fire_classes,id,deleted_at,NULL',
            'marker_date' => 'required|date|date_format:Y-m-d',
        ];
        if($add){
          $rules['added_by'] = 'required|exists:users,id,deleted_at,NULL';
        }

        if ($type == 2) {
            $rules['quantity']            = 'required|numeric';
            $rules['firesealing_type_id'] = 'required|numeric|exists:firesealing_types,id,deleted_at,NULL';
            $rules['penetration_type_id'] = 'required|numeric|exists:penetration_types,id,deleted_at,NULL';
            $rules['dimension1']          = 'required|string';
            $rules['dimension2']          = 'string|max:191';
            $rules['id']                  = 'sometimes|required|exists:firesealing_markers,id,deleted_at,NULL';
        } else {
            $rules['insulation_material_id'] = 'required|numeric|exists:insulation_materials,id,deleted_at,NULL';
            $rules['insulation_profile_id']  = 'required|numeric|exists:insulation_profiles,id,deleted_at,NULL';
            $rules['dimensions']             = 'required|string|max:191';
            $rules['prescribed']             = 'sometimes|numeric';
            $rules['measured']               = 'nullable';
            $rules['thickness']              = 'sometimes|numeric';
            $rules['id']                     = 'sometimes|required|numeric|exists:insulation_markers,id,deleted_at,NULL';
        }
        $this->validate($request, $rules, $messages);
    }
}