<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Repositories\OptionRepository;
use Illuminate\Http\Request;
use App\Services\TranslationService;
use App\Services\PrepareDataTableService;

/**
 * Option Controller operations
 *
 * Manage the CRUD operations for title for different user roles
 * @group Options
 * @author manpreetkaur
 */
class OptionController extends Controller
{
    /**
     * local variables of the controller
     * @var type
     */
    protected $optionRepository;
    private $options;

    /**
     * Constructor of this controller
     * @param OptionRepository $optionRepository
     */
    public function __construct(OptionRepository $optionRepository)
    {
        $this->optionRepository = $optionRepository;
        $this->options          = config('constant.options');
    }

    /**
     * List all options
     *
     * List all the options with specific sorting order, search and limit
     * @bodyParam option_name string required the type of option we want to fetch
     * @bodyParam order.0.dir string required the direction of the records in ASC or DESC direction
     * @bodyParam order.0.column string required the order by column name in ASC or DESC direction
     * @bodyParam search.value  string required the value to be searched in the records
     * @bodyParam start integer required the value from start getting records
     * @bodyParam length integer required the number of records want to fetch
     * @response json array type in variable data
     */
    public function index(Request $request, string $option_name)
    {
        $request->request->add(['option' => $option_name]);
        $request['options'] = $this->options;
        $this->validate($request,
            [
            'option' => 'required|string|in_array:options.*'
        ]);

         $this->_validateIndexRequest($request);
        $columnNames       = [
            0 => 'option_value'
        ];
        $searchableColumns = [$columnNames[0]];
        $direction         = $request->input('order')[0]['dir'];
        $sort_column       = $columnNames[intval($request->input('order')[0]['column'])];
        $search            = $request->input('search')['value'];
        $where             = ['option_name' => $option_name];
        //call common serivce for table results
        $response          = PrepareDataTableService::getTableDataWithMultipleRealtions('\Option',
                $sort_column, $direction, $request->input('start'),
                $request->input('length'), $searchableColumns, $search, false,
                [], $where);
        if ($response['total'] != 0) {
            return response()->json(['success' => TranslationService::lang('success',
                        $request->auth->language), 'draw' => $request->input('draw'),
                    'recordsTotal' => $response['total'],
                    'recordsFiltered' => $request->input('length'), 'data' => $response['records']],
                    200);
        }
        return response()->json(['success' => TranslationService::lang('success',
                    $request->auth->language), 'draw' => 0, 'recordsTotal' => 0,
                'recordsFiltered' => 0, 'data' => []], 200);
    }

    /**
     * Create a option
     * @bodyParam option_name string required the option whose value we want to create like customer_category or customer_title
     * @bodyParam option_value string required the option value 
     * @response json type array
     */
    public function store(Request $request)
    {
        $this->_validateOptionRequest($request);
        $data     = $request->all();
        $response = $this->optionRepository->create($data);
        if ($response['status']) {
            return response()->json(['success' => TranslationService::lang($request['option_name'].'_created',
                        $request->auth->language), 'id' => $response['id']], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_'.$request['option_name'].'_creation',
                    $request->auth->language)], 422);
    }

    /**
     * Update a option
     * @bodyParam id integer required the id of the option model whose value we want to update
     * @bodyParam option_name string required the option whose value we want to create like customer_category or customer_title
     * @bodyParam option_value string required the option value
     * @response json type array
     */
    public function update(Request $request, int $id)
    {
        $request->request->add(['option_id' => $id]);
        $this->_validateOptionRequest($request);
        $data     = $request->all();
        $response = $this->optionRepository->update($id, $data);
        if ($response) {
            return response()->json(['success' => TranslationService::lang($request['option_name'].'_updated',
                        $request->auth->language)], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_'.$request['option_name'].'_updation',
                    $request->auth->language)], 422);
    }

    /**
     * Delete a options
     * @bodyParam id integer required the id of the option to be deleted
     * @response json type array
     */
    public function destroy(Request $request, int $id)
    {
        $request->request->add(['option_id' => $id]);
        $this->validate($request,
            [
            'option_id' => 'required|numeric|exists:options,id,deleted_at,NULL'
        ]);
        $response = $this->optionRepository->delete($id);
        if ($response['status']) {
            return response()->json(['success' => TranslationService::lang($response['option_name'].'_deleted',
                        $request->auth->language)], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_'.$request['option_name'].'_deletion',
                    $request->auth->language)], 422);
    }

    /**
     * validate option request function
     * @bodyParam array $request
     */
    private function _validateOptionRequest($request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $request['options'] = $this->options;
        $this->validate($request,
            [
            'option_name' => 'required|string|in_array:options.*',
            'option_value' => 'required|string|max:191',
            'option_id' => 'sometimes|required|numeric|exists:options,id,deleted_at,NULL'
            ], $messages);
    }


    /**
     * validate the index function request
     * @param type $request
     */
    private function _validateIndexRequest($request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $rules    = config('constant.index_request_validation');
        $this->validate($request, $rules, $messages);
    }
}