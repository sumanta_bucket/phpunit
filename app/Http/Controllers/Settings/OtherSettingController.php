<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\OtherSettingsRepository;
use App\Services\TranslationService;
use App\Services\PrepareDataTableService;
use Illuminate\Support\Facades\Schema;

/**
 * Other Setting Controller operations
 *
 * Manage the CRUD related operations
 * @group OtherSettings
 * @author manpreetkaur
 */
class OtherSettingController extends Controller
{
    /**
     * local variable
     * @var type
     */
    protected $otherSettingsRepository;

    /*
     * local variable
     * @var $tables 
     * */
    protected $tables = ["fireboards", "firesealing_types"];

    /**
     * Constructor for this controller
     * @param OtherSettingsRepository $otherSettingsRepository
     */
    public function __construct(OtherSettingsRepository $otherSettingsRepository)
    {
        $this->otherSettingsRepository = $otherSettingsRepository;
    }

    /**
     * List all other settings
     *
     * List all the other settings with specific sorting order, search and limit
     * @bodyParam section string required the name of the table from which the records are fetched
     * @bodyParam type string optional the type for fireclasses like 1=>insulation and 2=>firesealing
     * @bodyParam order.0.dir string required the direction of the records in ASC or DESC direction
     * @bodyParam order.0.column string required the order by column name in ASC or DESC direction
     * @bodyParam search.value  string required the value to be searched in the records
     * @bodyParam start integer required the value from start getting records
     * @bodyParam length integer required the number of records want to fetch
     * @response json array type and variable name is data
     */
    public function index(Request $request, string $section, string $type = null)
    {
        //find the table name from section variable in the config array
        $modelName = config('constant.model_name.'.$section);
        //check whether the model is empty or not
        if ($modelName == null) {
            return response()->json(['error' => TranslationService::lang('db_table_name_required',
                        $request->auth->language)], 400);
        }
        $this->validateIndexRequest($request);
        $checkColumn = Schema::hasColumn(config('constant.table.'.$section),
                'class_type');
        if ($checkColumn == 'insulation_profiles') {
            $columnNames = [
                0 => 'id',
                1 => 'name'
            ];
        } else {
            $columnNames = [
                0 => 'id',
                1 => 'name',
                2 => 'documents.documnent'
            ];
        }
        $where = [];
        if ($type != null && $checkColumn) {
            $typeName = config('constant.fire_class.'.$type);
            $where    = ($typeName != null) ? ['class_type' => $typeName] : [];
        }
        $searchableColumns = [$columnNames[0], $columnNames[1]];
        $direction         = $request->input('order')[0]['dir'];
        $sort_column       = $columnNames[intval($request->input('order')[0]['column'])];
        $search            = $request->input('search')['value'];
        // With relation for tables which have documents
        $withRelation      = false;
        $relations         = [];
        $hasRelation       = null;
        if (in_array($section, $this->tables)) {
            $hasRelation  = 'documents';
            $withRelation = true;
            $relations    = [
                'documents' => ['document'],
            ];
        } elseif ($section == 'material_settings') {
            $hasRelation  = 'documents';
            $withRelation = true;
            $relations    = [
                'documents' => ['document'],
                'material_type' => ['name', 'slug']
            ];
            $relationName = ['documents', 'material_type'];
        } else {

        }
        $whereRelation =['name'];
        //call common serivce for table results
        $response = PrepareDataTableService::getTableDataWithMultipleRealtions($modelName,
                $sort_column, $direction, $request->input('start'),
                $request->input('length'), $searchableColumns, $search,
                $withRelation, $relations, $where, $whereRelation, $hasRelation);

        if ($response['total'] != 0) {
            return response()->json(['success' => TranslationService::lang('success',
                        $request->auth->language), 'draw' => $request->input('draw'),
                    'recordsTotal' => $response['total'],
                    'recordsFiltered' => $request->input('length'), 'data' => $response['records']],
                    200);
        }
        return response()->json(['success' => TranslationService::lang('success',
                    $request->auth->language), 'draw' => 0, 'recordsTotal' => 0,
                'recordsFiltered' => 0, 'data' => []], 200);
    }

    /**
     * create a other settings
     * @param Request request post the default request parameter
     * @bodyparam section string required the name of the table in which records are created
     * @bodyParam type string optional the type for fireclasses like 1=>insulation and 2=>firesealing
     * @bodyparam name string required the name of the value of the name field
     * @bodyparam document file optional the document to be uploaded
     * @bodyparam class_type int optional fire resistance type like 1=>insulation, 2=> firesealing
     * @bodyparam material_type_id int optional material type id 
     * @return type
     */
    public function store(Request $request, string $section, string $type = null)
    {
        //find the name in the config array corresponding to the config array
        $table = config('constant.table.'.$section);
        //check whetehr the table name is empty or not 
        if ($table == null) {
            return response()->json(['error' => TranslationService::lang('db_table_name_required',
                        $request->auth->language)], 400);
        }
        $this->_validateRequest($request, $table);
        $data        = $request->all();
        $checkColumn = Schema::hasColumn(config('constant.table.'.$section),
                'class_type');
        if ($type != null && $checkColumn) {
            $typeName           = config('constant.fire_class.'.$type);
            $data['class_type'] = ($typeName != null) ? $typeName : '';
        }

        $response = $this->otherSettingsRepository->create($table, $data);
        if ($response['status'] == 1) {
            return response()->json(['success' => TranslationService::lang($table.'_created',
                        $request->auth->language)], 200);
        } elseif ($response['status'] == "HY000") {
            return response()->json(['error' => TranslationService::lang('missing_required_field_document',
                        $request->auth->language)], 422);
        } elseif ($response['status'] == "42S02") {
            return response()->json(['error' => TranslationService::lang('db_error_heading',
                        $request->auth->language)], 422);
        } else {
            return response()->json(['error' => TranslationService::lang('no_record_created',
                        $request->auth->language)], 400);
        }
    }

    /**
     * Retrieve a other settings
     * @bodyParam section string required the name of the table from which we want to fetch the records
     * @bodyParam id integer required the id of the record being fetched
     * @response json array type
     */
    public function edit(Request $request, int $id, string $section)
    {
        //find table name corresponding to the section in config array
        $table = config('constant.table.'.$section);
        //check whether the name is empty or not 
        if ($table == null) {
            return response()->json(['error' => TranslationService::lang('db_table_name_required',
                        $request->auth->language)], 400);
        }
        $message  = TranslationService::customMesages($request->auth->language);
        $request->request->add(['id' => $id]);
        $this->validate($request,
            [
            'id' => 'required|numeric|exists:'.$table.',id,deleted_at,NULL'
            ], $message);
        $response = $this->otherSettingsRepository->getRecords($table, $id);
        if ($response['status'] == 1) {
            return response()->json(['success' => TranslationService::lang('edited',
                        $request->auth->language), 'data' => $response['records']],
                    200);
        } elseif ($response['status'] == 0) {
            return response()->json(['error' => TranslationService::lang('no_record_found',
                        $request->auth->language)], 400);
        } else {
            return response()->json(['error' => TranslationService::lang('db_error_heading',
                        $request->auth->language)], 422);
        }
    }

    /**
     * Update a other settings
     * @bodyParam section string required the name of the table
     * @bodyParam type string optional the type for fireclasses like 1=>insulation and 2=>firesealing
     * @bodyParam id integer required the id of the record being updated
     * @bodyparam name string required the name of the value of the name field
     * @bodyparam document file optional the document for to be uploaded
     * @bodyparam class_type integer optional fire resistance type like 1=>insulation, 2=> firesealing
     * @bodyparam material_type_id integer optional the material type id
     * @response json array type
     */
    public function update(Request $request, int $id, string $section,
                           string $type = null)
    {
        //find table name corresponding to the section
        $table = config('constant.table.'.$section);
        //check whetehr the table exists or not 
        if ($table == null) {
            return response()->json(['error' => TranslationService::lang('db_table_name_required',
                        $request->auth->language)], 400);
        }

        $request->request->add(['id' => $id]);
        $this->_validateRequest($request, $table);
        $data        = $request->all();
        $checkColumn = Schema::hasColumn(config('constant.table.'.$section),
                'class_type');  // check whether the table exists or not
        if ($type != null && $checkColumn) {
            $typeName           = config('constant.fire_class.'.$type);
            $data['class_type'] = ($typeName != null) ? $typeName : '';
        }
        $response = $this->otherSettingsRepository->update($table, $id,
            $request->all());
        if ($response['status'] == 1) {
            return response()->json(['success' => TranslationService::lang($table.'_updated',
                        $request->auth->language)], 200);
        } elseif ($response['status'] == 0) {
            return response()->json(['error' => TranslationService::lang('no_record_found',
                        $request->auth->language)], 400);
        } else {
            return response()->json(['error' => TranslationService::lang('db_error_heading',
                        $request->auth->language)], 422);
        }
    }

    /**
     * Delete a other settings
     * @bodyParam section string required the name of the table
     * @bodyParam id integer the id of the record being deleted
     * @response json array type
     */
    public function destroy(Request $request, int $id, string $section)
    {
        $table = config('constant.table.'.$section);
        //check whether the table name null
        if ($table == null) {
            return response()->json(['error' => TranslationService::lang('db_table_name_required',
                        $request->auth->language)], 400);
        }

        $request->request->add(['id' => $id]);
        $message  = TranslationService::customMesages($request->auth->language);
        $this->validate($request,
            [
            'id' => 'required|numeric|exists:'.$table.',id,deleted_at,NULL'
            ], $message);
        //call the repo function to perform the specific task
        $response = $this->otherSettingsRepository->delete($table, $id);
        //check response code and return the response accordingly
        if ($response['status'] == 1) {
            return response()->json(['success' => TranslationService::lang($table.'_deleted',
                        $request->auth->language)], 200);
        } elseif ($response['status'] == 0) {
            return response()->json(['error' => TranslationService::lang('no_record_found',
                        $request->auth->language)], 400);
        } else {
            return response()->json(['error' => TranslationService::lang('db_error_heading',
                        $request->auth->language)], 422);
        }
    }

    /**
     * validate parameter function for required of some other validations
     * @param type $request
     */
    private function _validateRequest($request, $table_name)
    {
        $message = TranslationService::customMesages($request->auth->language);
        $this->validate($request,
            [
            'name' => 'required|max:191',
            'document' => 'sometimes|required|array',
            'material_type_id' => 'sometimes|required|numeric|exists:material_types,id,deleted_at,NULL',
            'id' => 'sometimes|numeric|exists:'.$table_name.',id,deleted_at,NULL'
            ], $message);
    }

    /**
     * resolver to get the required details throughout the app
     * @bodyParam Request request get the default route parameter
     * @bodyParam type string required the type of record want to get
     * @return type
     */
    public function getDefaultData(Request $request,
                                   string $type = 'other_settings')
    {
        $response = $this->otherSettingsRepository->getDefaultData($type);
        return response()->json(['success' => TranslationService::lang('success',
                    $request->auth->language), 'data' => $response], 200);
    }

    /**
     * Private function to validate the request
     * @param type $request
     */
    private function validateIndexRequest($request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $rules    = config('constant.index_request_validation');
        $this->validate($request, $rules, $messages);
    }
}