<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Repositories\ProjectRepository;
use Illuminate\Http\Request;
use App\Services\TranslationService;
use App\Services\PrepareDataTableService;

/**
 * Project Controller operations
 *
 * Manage the CRUD related operations
 * @group Projects
 * @author manpreetkaur
 */
class ProjectController extends Controller
{
    protected $projectRepository;

    /**
     * Constructor for this controller
     * @param ProjectRepository $projectRepository
     */
    public function __construct(ProjectRepository $projectRepository)
    {
        $this->projectRepository = $projectRepository;
    }

    /**
     * List all projects
     *
     * List all the projects with specific sorting order, search and limit
     * @bodyParam status integer required the type of records we want to fetch 1= active and 2= archived
     * @bodyParam order.0.dir string required the direction of the records in ASC or DESC direction
     * @bodyParam order.0.column string required the order by column name in ASC or DESC direction
     * @bodyParam search.value  string required the value to be searched in the records
     * @bodyParam start integer required the value from start getting records
     * @bodyParam length integer required the number of records want to fetch
     * @response json array type and variable name is data
     */
    public function index(Request $request, int $status)
    {
        if (!in_array($status, [0, 1])) {
            return response()->json(['error' => TranslationService::lang('invalid_status',
                        $request->auth->language)], 422);
        }

        $this->validateIndexRequest($request);
        $columnNames       = [
            0 => 'name',
            1 => 'customer.user.first_name',
            2 => 'consultants.user.first_name',
            3 => 'workplace.name',
            4 => 'contactPersons.user.first_name',
            5 => 'project_number'
        ];
        $direction         = $request->input('order')[0]['dir'];
        $sort_column       = $columnNames[intval($request->input('order')[0]['column'])];
        $search            = $request->input('search')['value'];
        //the searchable columns order as they are present in the data table
        $searchableColumns = [
            0 => 'name',
            1 => 'customer.user',
            2 => 'consultants.user',
            3 => 'workplace',
            4 => 'contactPersons.user',
            5 => 'project_number'
        ];

        //required for relations and their fields to be searched
        $keyValue = [
            'name',
            'customer.user' => 'first_name',
            'consultants.user' => 'first_name',
            'workplace' => 'name',
            'contactPersons.user' => 'first_name',
            'project_number'
        ];

        $roles = $request->auth->getRoleNames(); // get user name 
        if ($status == 0) {
            $response = $this->projectRepository->trashedRecords($search,
                $sort_column, $direction, $request->input('length'),
                $request->input('start'), $searchableColumns, $keyValue,
                $roles[0], $request->auth->id);
        } else {
            $response = $this->projectRepository->getAllProjects($request->input('start'),
                $request->input('length'), $sort_column, $direction, $search,
                $searchableColumns, $keyValue, $roles[0], $request->auth->id);
        }

        if ($response['total'] != 0) {
            return response()->json(['success' => TranslationService::lang('success',
                        $request->auth->language), 'draw' => $request->input('draw'),
                    'recordsTotal' => $response['total'],
                    'recordsFiltered' => $request->input('length'), 'data' => $response['records']],
                    200);
        }
        return response()->json(['success' => TranslationService::lang('success',
                    $request->auth->language), 'draw' => 0, 'recordsTotal' => 0,
                'recordsFiltered' => 0, 'data' => []], 200);
    }

    /**
     * Create a project
     * @bodyParam name string required the name of the project
     * @bodyParam customer_id integer required the customer related to project
     * @bodyParam workplace_id integer required the workplace related to project
     * @bodyParam consultant_id integer required the consultant related to project
     * @bodyParam old_penetrations integer optional old penetration yes or not
     * @bodyParam project_number integer optional project number of newly created project
     * @bodyParam _categories array optional related to project categories
     * @response json type array
     */
    public function store(Request $request)
    {
        $this->_validateProjectRequest($request);
        $data     = $request->all();
        $response = $this->projectRepository->create($data, $request->auth->id);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('project_saved',
                        $request->auth->language), 'data' => $response], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_project_creation',
                    $request->auth->language)], 422);
    }

    /**
     * Retrieve a project
     * @bodyParam id integer required the id of the project whose records are being fetched
     * @response json type array
     */
    public function edit(Request $request, int $id)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $request->request->add(['project_id' => $id]);
        $this->validate($request,
            [
            'project_id' => 'numeric|exists:projects,id,deleted_at,NULL'
            ], $messages);
        $response = $this->projectRepository->edit($id);
        if (!empty($response)) {
            return response()->json(['success' => TranslationService::lang('success',
                        $request->auth->language), 'data' => $response], 200);
        }

        return response()->json(['error' => TranslationService::lang('no_record_found',
                    $request->auth->language)], 404);
    }

    /**
     * Update a project
     * @bodyParam id integer required the id of the updated record
     * @bodyParam name string required the name of the project
     * @bodyParam customer_id integer required the customer related to project
     * @bodyParam workplace_id integer required the workplace related to project
     * @bodyParam consultant_id integer required the consultant related to project
     * @bodyParam old_penetrations integer optional old penetration yes or not
     * @bodyParam project_number integer optional project number of newly created project
     * @bodyParam _categories array optional to create project categories
     * @response json type array
     */
    public function update(Request $request, int $id)
    {
        $request->request->add(['project_id' => $id]);
        $this->_validateProjectRequest($request);
        $data     = $request->all();
        $response = $this->projectRepository->update($data, $id);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('project_updated',
                        $request->auth->language), 'data' => $id], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_project_updation',
                    $request->auth->language)], 422);
    }

    /**
     * Delete a project
     * @bodyParam id integer required the id of the record deleted
     * @response json type array
     */
    public function destroy(Request $request, int $id)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $request->request->add(['project_id' => $id]);
        $this->validate($request,
            [
            'project_id' => 'numeric|exists:projects,id,deleted_at,NULL'
            ], $messages);
        $response = $this->projectRepository->delete($id);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('project_deleted',
                        $request->auth->language)], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_project_deletion',
                    $request->auth->language)], 422);
    }

    /**
     * Get required data for project creation
     * @bodyParam project_id integer optional the project id whose records we want to fetch
     * @response json type array
     */
    public function getRequiredDetails(Request $request, int $project_id = null)
    {
        $user_id  = $request->auth->id; // logged in user id
        $response = $this->projectRepository->getRequiredData($project_id,
            $user_id);
        return response()->json(['success' => TranslationService::lang('success',
                    $request->auth->language), 'data' => $response], 200);
    }

    /**
     * Get required data for project creation
     * @bodyParam project_id integer optional the project id whose records we want to fetch
     * @response json type array
     */
    public function getProjectInformation(Request $request, int $project_id)
    {
        $user_id  = $request->auth->id; // logged in user id 
        $response = $this->projectRepository->getRequiredData($project_id,
            $user_id, 'information');
        return response()->json(['success' => TranslationService::lang('success',
                    $request->auth->language), 'data' => $response], 200);
    }

    /**
     * restore a project
     * @bodyParam id integer required the id of the record to be restored
     * @response json type array
     */
    public function restore(Request $request, int $id)
    {
        $request->request->add(['project_id' => $id]);
        $messages = TranslationService::customMesages($request->auth->language);
        $this->validate($request,
            [
            'project_id' => 'integer|exists:projects,id,deleted_at,NOT_NULL'
            ], $messages);
        $response = $this->projectRepository->restoreProject($id);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('project_restored',
                        $request->auth->language)], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_project_restored',
                    $request->auth->language)], 422);
    }

    /**
     * Validation function for add or update project request
     * @param type $request
     */
    private function _validateProjectRequest($request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $this->validate($request,
            [
            'name' => 'required||max:191',
            'customer_id' => 'required|numeric|exists:customers,id,deleted_at,NULL',
            'project_id' => 'sometimes|exists:projects,id,deleted_at,NULL',
            'project_number' => 'sometimes|nullable|max:10',
            'consultants' => 'nullable|sometimes|array',
            'contact_persons' => 'nullable|sometimes|array',
            'workplace' => 'nullable|sometimes|array',
            '_categories.*.project_number' => 'sometimes|nullable|max:10'
            ], $messages);
    }
    /*
     * Test function wil delete in future
     */

    public function testMultipelationService(Request $request)
    {
        $columnNames = [
            0 => 'id',
            1 => 'user.first_name',
            2 => 'user.address',
            3 => 'user.city',
            4 => 'user.mobile',
            5 => 'user.email',
        ];

        $searchableColumns = [$columnNames[0]];
        $direction         = $request['order'][0]['dir'];
        $sort_column       = $columnNames[intval($request['order'][0]['column'])];
        $search            = $request['search']['value'];
        $relations         = [
            'customer.user' => ['first_name'],
            'consultant.user' => ['first_name'],
            'workplace' => ['name'],
            'categories' => ['name']
        ];
        $whereRelation     = ['name', 'project_number'];
        $hasRelation       = 'customer.user';

        $response = PrepareDataTableService::getTableDataWithMultipleRealtions('\Project',
                $sort_column, $direction, $request['start'], $request['length'],
                [], $search, true, $relations, [], $whereRelation, $hasRelation);
        if ($response['total'] != 0) {
            return response()->json(['success' => TranslationService::lang('success',
                        $request->auth->language), 'draw' => $request['draw'], 'recordsTotal' => $response['total'],
                    'recordsFiltered' => $request['length'], 'data' => $response['records']],
                    200);
        }
        return response()->json(['success' => TranslationService::lang('success',
                    $request->auth->language), 'draw' => 0, 'recordsTotal' => 0,
                'recordsFiltered' => 0, 'data' => []], 200);
    }

    /**
     * Private function to validate the request
     * @param type $request
     */
    private function validateIndexRequest($request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $rules    = config('constant.index_request_validation');
        $this->validate($request, $rules, $messages);
    }
}