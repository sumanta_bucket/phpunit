<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ProjectDocumentationRepository;
use App\Services\TranslationService;

/**
 * Project Documentation Controller operation
 *
 * Manage the CRUD operation of floor, section and upload pdf in the project
 * @group ProjectDocumentation
 * @author manpreetkaur
 */
class ProjectDocumentationController extends Controller
{
    protected $documentationRepository;

    /**
     * constructor for this controller 
     */
    public function __construct(ProjectDocumentationRepository $repository)
    {
        $this->documentationRepository = $repository;
    }

    /**
     * create a floor with section
     * @bodyParam project_id integer required the id of the project
     * @bodyParam no_of_floors integer required the no of sections in each floor
     * @response json array type
     */
    public function store(Request $request, int $project_id)
    {
        //add new parameter to request
        $request->request->add(['project_id' => $project_id]);
        $this->_validateRequest($request);  //call to validate function
        $data     = $request->all();
        $response = $this->documentationRepository->create($project_id, $data);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('floors_created',
                        $request->auth->language), 'data' => $response], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_floors_creation',
                    $request->auth->language)], 422);
    }

    /**
     * Add section to existing floor
     * @bodyParam request Request optional the default parameter of the route
     * @bodyParam project_id integer required the id of project in which we want to add project
     * @bodyParam floor_id integer required the floor id of the project in which sections are added
     * @respone json array type
     */
    public function addSection(Request $request, int $project_id, int $floor_id)
    {
        //add the extra parameters to request to validate
        $request->request->add(['project_id' => $project_id]);
        $request->request->add(['floor_id' => $floor_id]);
        //validate the request
        $this->validate($request,
            [
            'project_id' => 'numeric|exists:projects,id,deleted_at,NULL',
            'floor_id' => 'numeric|exists:floors,id,deleted_at,NULL'
        ]);
        $data     = $request->all();
        //call to the repo function to perform the action
        $response = $this->documentationRepository->createSection($project_id,
            $floor_id, $data);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('section_created',
                        $request->auth->language), 'data' => $response], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_section_creation',
                    $request->auth->language)], 422);
    }

    /**
     * Get the details of specific floor
     * @bodyParam request Request optional the default parameter of the route
     * @bodyParam id integer required the id of the floor whose records we want to fetch
     * @responce json type array
     */
    public function edit(Request $request, int $id)
    {
        //add the floor id to the request as parameter
        $request->request->add(['floor_id' => $id]);
        //validate the request
        $this->validate($request,
            [
            'floor_id' => 'numeric|exists:floors,id,deleted_at,NULL'
        ]);
        $response = $this->documentationRepository->getRecord($id);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('success',
                        $request->auth->language), 'data' => $response], 200);
        }
        return response()->json(['error' => TranslationService::lang('no_record_found',
                    $request->auth->language)], 400);
    }

    /**
     * Delete a floor 
     * @bodyParam project_id integer required the id of the project whose floor we want to delete
     * @bodyParam id integer required the floor  id which we want to delete
     * @response json array type
     */
    public function deleteFloor(Request $request, int $project_id, int $id)
    {
        //add the parameters to request for validation
        $request->request->add(['project_id' => $project_id]);
        $request->request->add(['floor_id' => $id]);
        $this->validate($request,
            [
            'project_id' => 'required|numeric|exists:projects,id,deleted_at,NULL',
            'floor_id' => 'required|numeric|exists:floors,id,deleted_at,NULL'
        ]);
        $response = $this->documentationRepository->deleteFloor($project_id, $id);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('floor_deleted_succesfully',
                        $request->auth->language), 'data' => $response], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_floor_deletion',
                    $request->auth->language)], 422);
    }

    /**
     * Delete a section
     * @bodyParam project_id integer required the id of the associated project
     * @bodyParam section_id integer required the id of the associated section
     * @response json array type
     */
    public function deleteSection(Request $request, int $project_id,
                                  int $section_id)
    {
        //add the parameters to request for validation
        $request->request->add(['project_id' => $project_id]);
        $request->request->add(['section_id' => $section_id]);
        $this->validate($request,
            [
            'project_id' => 'required|numeric|exists:projects,id,deleted_at,NULL',
            'section_id' => 'required|numeric|exists:sections,id,deleted_at,NULL'
        ]);
        $response = $this->documentationRepository->deleteSection($project_id,
            $section_id);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('section_deleted_succesfully',
                        $request->auth->language), 'data' => $response], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_section_deletion',
                    $request->auth->language)], 422);
    }

    /**
     * Delete all floors of the project
     * @bodyParam project_id integer required the id of the associated project
     * @response json array type
     */
    public function deleteAllFloors(Request $request, int $project_id)
    {
        //add the parameters to request for validation
        $request->request->add(['project_id' => $project_id]);
        //validate the request
        $this->validate($request,
            [
            'project_id' => 'required|numeric|exists:projects,id,deleted_at,NULL'
        ]);
        $response = $this->documentationRepository->deleteAllFloors($project_id);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('floors_deleted_succesfully',
                        $request->auth->language), 'data' => $response], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_floor_deletion',
                    $request->auth->language)], 422);
    }

    /**
     * upload PDF of specific section
     * @bodyParam project_id integer required the project to which we want to upload document
     * @bodyParam section_id integer required the section in which the document is being uploaded
     * @bodyParam document file required the file which we want to upload of type PDF
     * @bodyParam page integer optional the page number of PDF whose image we want to create
     * @response json array type
     */
    public function uploadSectionDocument(Request $request, int $project_id,
                                          int $section_id)
    {
        $request->request->add(['project_id' => $project_id, 'section_id' => $section_id]);
        $this->_validateSectionDocumentRequest($request);
        $response = $this->documentationRepository->uploadSectionPdf($section_id,
            $request->all());
        if ($response['status'] == 1) {
            return response()->json(['success' => TranslationService::lang('document_uploaded_successfully',
                        $request->auth->language), 'data' => $response['data']],
                    200);
        }
        return response()->json(['error' => TranslationService::lang('error_document_upload',
                    $request->auth->language)], 422);
    }

    /**
     * Delete the uploaded PDF of section
     * @bodyParam project_id integer required  the project to which we want to delete the document
     * @bodyParam section_id integer required the section whose pdf we want to delete
     * @respone json array type
     */
    public function deleteSectionDocument(Request $request, int $project_id,
                                          int $section_id, string $type)
    {
        // check if the type key exists in request then assign to type valiable
        if (!in_array($type, ['firesealing', 'firesteel'])) {
            return response()->json(['error' => TranslationService::lang('invalid_type',
                        $request->auth->language)], 422);
        }
        $request->request->add(['project_id' => $project_id, 'section_id' => $section_id,
            'type' => $type]);
        $this->validate($request,
            [
            'project_id' => 'required|numeric|exists:projects,id,deleted_at,NULL',
            'section_id' => 'required|numeric|exists:sections,id,deleted_at,NULL',
        ]);
        $response = $this->documentationRepository->deleteSectionDocument($section_id,
            $type);
        if ($response == 'not_found') { //if section not found then return particular error message 
            return response()->json(['error' => TranslationService::lang('section_not_found',
                        $request->auth->language)], 404);
        } elseif ($response == 'deleted') { //if the documnet deleted then return succcess message
            return response()->json(['success' => TranslationService::lang('pdf_deleted_successfully',
                        $request->auth->language)], 200);
        } else { //else return the respective error
            return response()->json(['error' => TranslationService::lang('error_pdf_delete',
                        $request->auth->language)], 422);
        }
    }

    /**
     * Upload multiple pdf files in sections
     * @bodyParam project_id integer required the project id to whom we want to upload the file
     * @bodyParam documents array required the files array we want to upload  of type pdf only
     * @response json array type
     */
    public function uploadSectionAllPdf(Request $request, int $project_id)
    {
        //add new parameter to the request
        $request->request->add(['project_id' => $project_id]);
        $this->validatePdfRequest($request);
        $data     = $request->all();
        //call the documentation repository to perform action
        $response = $this->documentationRepository->sectionUploadMuliplePdf($data,
            $project_id);

        if ($response['status'] == 1) {
            return response()->json(['success' => TranslationService::lang('documents_uploaded_successfully',
                        $request->auth->language), 'data' => $response['data']],
                    200);
        } elseif ($response['status'] == 0) {
            return response()->json(['error' => TranslationService::lang('error_some_files_upload',
                        $request->auth->language), 'errors' => $response['error'],
                    'data' => $response['data']], 200);
        } else {
            return response()->json(['error' => TranslationService::lang('error_documents_upload',
                        $request->auth->language)], 422);
        }
    }

    /**
     * Delete section multiple PDF files of specific type 
     * @bodyParam project_id integer required the project id whose sections we want to delete
     * @bodyParam type Request optional the default parameter of the route type possible values are 1=>insulation ,2=> fire sealing
     * @bodyParam request Request optional the default parameter of the route
     * @return type
     */
    public function deleteSectionAllPdf(Request $request, int $project_id,
                                        int $type)
    {
        $request['types'] = config('constant.marker_types'); //get the marker types
        //add new parameter to request
        $request->request->add(['project_id' => $project_id]);
        $request->request->add(['type' => $type]);
        //validate the request 
        $this->validate($request,
            ['type' => 'required|in_array:types.*', 'project_id' => 'required|integer|exists:projects,id,deleted_at,NULL']);
        $data             = $request->all();
        //delete multiple section pdfs
        $response         = $this->documentationRepository->deleteSectionMultiplePdf($project_id,
            $data);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('all_documents_deleted',
                        $request->auth->language)], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_documents_delete',
                    $request->auth->language)], 422);
    }

    /**
     * Retrieve all  markers of particular section
     * @bodyParam section_id integer required the id of the section whose marker we want to get
     * @bodyParam type string required the type like firesealing or insulation
     * @return type
     */
    public function getSectionMarkers(Request $request, int $section_id,
                                      int $type)
    {
        $request['types'] = config('constant.marker_types');
        $request->request->add(['section_id' => $section_id]);
        $request->request->add(['type' => $type]);
        $this->validate($request,
            [
            'section_id' => 'required|integer|exists:sections,id,deleted_at,NULL',
            'type' => 'required|integer|in_array:types.*'
        ]);
        $response         = $this->documentationRepository->getSectionMarkers($section_id,
            $type, $request->auth->id);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('success',
                        $request->auth->language), 'data' => $response], 200);
        }
        return response()->json(['error' => TranslationService::lang('no_record_found',
                    $request->auth->language)], 404);
    }

    /**
     * Change the status for floor or section
     * @bodyParam project_id integer required the id of the project
     * @bodyParam table string required the name of the table like floor or section
     * @bodyParam id integer required the id of the table like section or floor id
     * @bodyParam status integer required the status to update like 0 or 1
     * @response json array type
     */
    public function changeFloorSectionActiveState(Request $request,
                                                  int $project_id)
    {
        $request->request->add(['project_id' => $project_id]);
        $this->_validateActiveInactiveRequest($request);
        $data     = $request->all();
        $response = $this->documentationRepository->changeStatusForFloorSection($project_id,
            $data);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('status_changed_successfully',
                        $request->auth->language)], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_status_change',
                    $request->auth->language)], 404);
    }

    /**
     * validate add floor request for required or some other expressions
     * @bodyParam request Request optional the default parameter of the route
     */
    private function _validateRequest(Request $request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $this->validate($request,
            [
            'floors' => 'required',
            'floors.*.sections' => 'required|integer|min:1',
            'project_id' => 'integer|exists:projects,id,deleted_at,NULL'
            ], $messages);
    }

    /**
     * validation upload section pdf request
     * @bodyParam type $request
     */
    private function _validateSectionDocumentRequest($request)
    {
        $message = TranslationService::customMesages($request->auth->language);
        $this->validate($request,
            [
            'document' => 'required|mimes:pdf|max:10240',
            'section_id' => 'required|numeric|exists:sections,id,deleted_at,NULL',
            'project_id' => 'required|numeric|exists:projects,id,deleted_at,NULL',
            'page' => 'integer'
            ], $message);
    }

    /**
     * Update multiple PDF validate request
     * @bodyParam type $request
     */
    private function validatePdfRequest($request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $this->validate($request,
            [
            'project_id' => 'required|numeric|exists:projects,id,deleted_at,NULL',
            'type' => 'required|numeric|max:191',
            'documents' => 'required',
            'documents.*' => 'required|mimes:pdf|max:10240'
            ], $messages);
    }

    /**
     * Local validation function of request
     * @bodyParam type $request
     */
    private function _validateActiveInactiveRequest($request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $this->validate($request,
            [
            'project_id' => 'required|numeric|exists:projects,id,deleted_at,NULL',
            'table' => 'required|string|max:191',
            'id' => 'required|numeric',
            'type' => 'required|numeric',
            'status' => 'required|numeric'
            ], $messages);
    }

    /**
     * update the last active floor or section
     * @bodyParam request Request optional the default parameter of the route
     * @bodyParam project_id integer required the id of the project
     * @bodyParam floor_id integer required the id of the floor
     * @bodyParam section_id integer required the id of the section
     * @bodyParam user_id integer required the id of the user who is logged in 
     * @bodyParam type integer required the type of floor whether it is 1=insulation or 2=firesealing
     * @response json array type
     */
    public function updateLastSection(Request $request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $this->validate($request,
            [
            'project_id' => 'required|numeric|exists:projects,id,deleted_at,NULL',
            'floor_id' => 'required|numeric|exists:floors,id,deleted_at,NULL',
            'section_id' => 'required|numeric|exists:sections,id,deleted_at,NULL',
            'type' => 'required|integer',
            ], $messages);
        $data     = $request->all();
        $response = $this->documentationRepository->updateLastSection($request->auth->id,
            $data);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('lastsection_updated',
                        $request->auth->language)], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_lastsection_update',
                    $request->auth->language)], 500);
    }

    /**
     * Get the last active section of particular user
     * @bodyParam request Request optional the default parameter of the route
     * @bodyParam project_id integer required the project whose section we want to get
     * @bodyParam type integer required the type of section get like 1=insulation,2=firesealing
     * @response json array type
     */
    public function getLastSection(Request $request, int $project_id, int $type)
    {
        $user_id  = $request->auth->id;
        $response = $this->documentationRepository->lastSectionRecord($project_id,
            $type, $user_id);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('success',
                        $request->auth->language), 'data' => $response], 200);
        }
        return response()->json(['success' => TranslationService::lang('no_record_found',
                    $request->auth->language), 'data' => []], 200);
    }
}