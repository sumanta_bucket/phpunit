<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\RelationshipRepository;
use App\Services\TranslationService;
use App\Services\PrepareDataTableService;

/**
 * Relationship Controller operations
 *
 * Manage CRUD related operations
 * @group Relationships
 * @author manpreetkaur
 */
class RelationshipController extends Controller
{
    /**
     * Local variable for this controller
     * @var type
     */
    protected $relationship_repository;

    /**
     * Constructor for this controller
     */
    public function __construct(RelationshipRepository $relationship_repo)
    {
        $this->relationship_repository = $relationship_repo;
    }

    /**
     * List all relationships
     *
     * List all the employees with specific sorting order, search and limit
     * @bodyParam order.0.dir string required the direction of the records in ASC or DESC direction
     * @bodyParam order.0.column string required the order by column name in ASC or DESC direction
     * @bodyParam search.value  string required the value to be searched in the records
     * @bodyParam start integer required the value from start getting records
     * @bodyParam length integer required the number of records want to fetch
     * @response json array type and variable name is data
     */
    public function index(Request $request)
    {
        $this->_validateIndexRequest($request);
        $columnNames       = [
            0 => 'id',
            1 => 'name'
        ];
        $searchableColumns = [$columnNames[1]];
        $direction         = $request->input('order')[0]['dir'];
        $sort_column       = $columnNames[intval($request->input('order')[0]['column'])];
        $search            = $request->input('search')['value'];
        //call common serivce for table results
        $response          = PrepareDataTableService::getTableDataWithMultipleRealtions('\Relationship',
                $sort_column, $direction, $request->input('start'),
                $request->input('length'), $searchableColumns, $search);
        //if response has some results then return results otherwise return empty data
        if ($response['total'] != 0) {
            return response()->json(['success' => TranslationService::lang('success',
                        $request->auth->language), 'draw' => $request->input('draw'),
                    'recordsTotal' => $response['total'],
                    'recordsFiltered' => $request->input('length'), 'data' => $response['records']],
                    200);
        }
        return response()->json(['success' => TranslationService::lang('success',
                    $request->auth->language), 'draw' => 0, 'recordsTotal' => 0,
                'recordsFiltered' => 0, 'data' => []], 200);
    }

    /**
     * Create a relationship
     * @bodyParam name string required the name of the relationship
     * @response json array type
     */
    public function store(Request $request)
    {
        $this->_validateRequest($request);
        $data     = $request->all();
        $response = $this->relationship_repository->create($data);

        if ($response) {
            return response()->json(['success' => TranslationService::lang('relationship_created',
                        $request->auth->language)], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_relationship_creation',
                    $request->auth->language)], 404);
    }

    /**
     * Retrieve a relationship
     * @bodyParam id integer required the id of the record to be fetched
     * @response json array type
     */
    public function edit(Request $request, int $id)
    {
        $request->request->add(['id' => $id]);
        $messages = TranslationService::customMesages($request->auth->language);
        $this->validate($request,
            [
            'id' => 'required|numeric|exists:relationships,id,deleted_at,NULL'
            ], $messages);
        $response = $this->relationship_repository->getRelations($id);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('success',
                        $request->auth->language), 'data' => $response], 200);
        }
        return response()->json(['error' => TranslationService::lang('no_record_found',
                    $request->auth->language)], 500);
    }

    /**
     * Update a relationship
     * @bodyParam id integer required the id of the record to be updated
     * @bodyParam name string required the name of the relationship to be edited
     * @response json array type
     */
    public function update(Request $request, int $id)
    {
        $request->request->add(['id' => $id]);
        $this->_validateRequest($request);
        $data     = $request->all();
        $response = $this->relationship_repository->update($id, $data);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('relationship_updated',
                        $request->auth->language)], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_relationship_updation',
                    $request->auth->language)], 500);
    }

    /**
     * Delete a relationship
     * @bodyParam id integer required the id of the record to be deleted
     * @response json array type
     */
    public function destroy(Request $request, int $id)
    {
        $request->request->add(['id' => $id]);
        $messages = TranslationService::customMesages($request->auth->language);
        $this->validate($request,
            [
            'id' => 'required|numeric|exists:relationships,id,deleted_at,NULL'
            ], $messages);
        $response = $this->relationship_repository->delete($id);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('relationship_deleted',
                        $request->auth->language)], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_relationship_deletion',
                    $request->auth->language)], 500);
    }

    /**
     * validate the request for specific attributes 
     * @param type $request
     */
    private function _validateRequest($request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $this->validate($request,
            [
            'name' => 'required|string|max:191',
            'id' => 'sometimes|required|numeric|exists:relationships,id,deleted_at,NULL'
            ], $messages);
    }
    /*
     * validate th eindex function request
     */

    private function _validateIndexRequest($request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $rules    = config('constant.index_request_validation');
        $this->validate($request, $rules, $messages);
    }
}