<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\RelativeRepository;
use App\Services\TranslationService;
use App\Services\PrepareDataTableService;

/**
 * Relative Controller operations
 * @group Relatives
 * Manage CRUD related operations
 * @author manpreetkaur
 */
class RelativeController extends Controller
{
    /**
     * local variable
     * @var type
     */
    private $relative_repository;

    /**
     * Constructor for this controller
     * @param RelativeRepository $relativeRepo
     */
    public function __construct(RelativeRepository $relativeRepo)
    {
        $this->relative_repository = $relativeRepo;
    }

    /**
     * Retrieve all relationships
     * @response json array type
     */
    public function getRelationships(Request $request)
    {
        $response = $this->relative_repository->getRelationships();
        if ($response) {
            return response()->json(['success' => TranslationService::lang('success',
                        $request->auth->language), 'data' => $response], 200);
        }
        return response()->json(['error' => TranslationService::lang('no_record_found',
                    $request->auth->language)], 404);
    }

    /**
     * List all relatives
     *
     * List all the relatives with specific sorting order, search and limit
     * @bodyParam order.0.dir string required the direction of the records in ASC or DESC direction
     * @bodyParam order.0.column string required the order by column name in ASC or DESC direction
     * @bodyParam search.value  string required the value to be searched in the records
     * @bodyParam start integer required the value from start getting records
     * @bodyParam length integer required the number of records want to fetch
     * @response json array type and variable name is data
     */
    public function index(Request $request)
    {
        $this->_validateIndexRequest($request);
        //searchable columns array as they are in the data table
        $searchableColumns = $columnNames       = [
            'employee.user.first_name',
            'relative.full_name',
            'relationship.name'
        ];

        $direction   = $request->input('order')[0]['dir'];  // direction of the records
        $sort_column = $columnNames[intval($request->input('order')[0]['column'])]; //order by column name
        $search      = $request->input('search')['value']; //serach value in the table records
        //relation name of the table and there attributes name to sarched in case of search query
        $relations   = [
            'employee.user' => ['first_name', 'last_name'],
            'relative' => ['first_name', 'last_name', 'email'],
            'relationship' => ['name'],
            'employee.user.title' => ['option_value']
        ];
        $hasRelation = 'relative'; //has relation with table
        //call the service function to get the results
        $response    = PrepareDataTableService::getTableDataWithMultipleRealtions('\EmployeeRelative',
                $sort_column, $direction, $request->input('start'),
                $request->input('length'), $searchableColumns, $search, true,
                $relations, [], [], $hasRelation, []);
        if ($response['total'] != 0) {
            return response()->json(['success' => TranslationService::lang('success',
                        $request->auth->language), 'draw' => $request->input('draw'),
                    'recordsTotal' => $response['total'],
                    'recordsFiltered' => $request->input('length'), 'data' => $response['records']],
                    200);
        }
        return response()->json(['success' => TranslationService::lang('success',
                    $request->auth->language), 'draw' => 0, 'recordsTotal' => 0,
                'recordsFiltered' => 0, 'data' => []], 200);
    }

    /**
     * Create a relative
     * @bodyParam relative.first_name string required the first name of the relative
     * @bodyParam relative.last_name string required the lat name of the relative
     * @bodyParam relative.email string required the email of the relative
     * @bodyParam relative.mobile string required the mobile number of the relative
     * @bodyParam relationship_id integer required the relationship id of the relationship
     * @bodyParam employee_id integer required the employee associated with relative
     * @response json array type
     */
    public function store(Request $request)
    {
        $this->_validateRequest($request);
        $data     = $request->all();
        $response = $this->relative_repository->create($data);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('relative_created',
                        $request->auth->language)], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_relative_creation',
                    $request->auth->language)], 500);
    }

    /**
     * Update a relative
     * @bodyParam id integer required the id of record being updated passed as route parameter
     * @bodyParam relative.first_name string required the first name of the relative
     * @bodyParam relative.last_name string required the lat name of the relative
     * @bodyParam relative.email string required the email of the relative
     * @bodyParam relative.mobile string required the mobile number of the relative
     * @bodyParam relationship_id integer required the relationship id of the relationship id
     * @bodyParam employee_id integer required the employee associated with relative
     * @response json array type
     */
    public function update(Request $request, int $id)
    {
        $request->request->add(['relative_id' => $id]);
        $this->_validateRequest($request, 1);
        $data     = $request->all();
        $response = $this->relative_repository->update($id, $data);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('relative_updated',
                        $request->auth->language)], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_relative_updation',
                    $request->auth->language)], 500);
    }

    /**
     * Delete a relative
     * @bodyParam id integer required the id of the relative which we want to delete
     * @response json array type
     */
    public function destroy(Request $request, int $id)
    {
        $request->request->add(['id' => $id]);
        $this->validate($request,
            [
            'id' => 'required|exists:relatives,id,deleted_at,NULL'
        ]);
        $response = $this->relative_repository->delete($id);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('relative_deleted',
                        $request->auth->language)], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_relative_deletion',
                    $request->auth->language)], 500);
    }

    /**
     * validate the request for some parameters
     * @param type $request
     */
    private function _validateRequest($request, $update = 0)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $rules    = [
            'relative.first_name' => 'required|string|max:191',
            'relative.last_name' => 'nullable|string|max:191',
            'relative.mobile' => 'required|max:20',
            'relative.email' => 'nullable|email|max:191',
            'employee_id' => 'required|exists:employees,id,deleted_at,NULL',
            'relationship_id' => 'required|integer|exists:relationships,id,deleted_at,NULL'
        ];
        if ($update) {
            $this->validate($request,
                [
                'relative_id' => 'required|exists:relatives,id,deleted_at,NULL',
                'id' => 'required|exists:employee_relative,id'
                ], $messages);
        }
        $this->validate($request, $rules, $messages);
    }

    /**
     * validate the index function request
     */
    private function _validateIndexRequest($request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $rules    = config('constant.index_request_validation');
        $this->validate($request, $rules, $messages);
    }
}