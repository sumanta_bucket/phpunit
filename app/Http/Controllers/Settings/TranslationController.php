<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Translation;
use App\Services\TranslationService;
use Illuminate\Support\Facades\Cache;
use App\Services\PrepareDataTableService;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use App\Models\User;

/**
 * Translation Controller operations
 *
 * Manage CRUD related operations
 * @group Translations
 * @author manpreetkaur
 */
class TranslationController extends Controller
{

    /**
     * List all translations
     *
     * List all the translations with specific sorting order, search and limit
     * @bodyParam order.0.dir string required the direction of the records in ASC or DESC direction
     * @bodyParam order.0.column string required the order by column name in ASC or DESC direction
     * @bodyParam search.value  string required the value to be searched in the records
     * @bodyParam start integer required the value from start getting records
     * @bodyParam length integer required the number of records want to fetch
     * @response json array type in variable data
     */
    public function index(Request $request)
    {
        $this->validateIndexRequest($request);
        $columnNames = [
            0 => 'id',
            1 => 'short_name',
            2 => 'english',
            3 => 'svenska'
        ];

        $searchableColumns = [$columnNames[1], $columnNames[2], $columnNames[3]];
        $direction         = $request->input('order')[0]['dir'];
        $sort_column       = $columnNames[intval($request->input('order')[0]['column'])];
        $search            = $request->input('search')['value'];
        $translation       = PrepareDataTableService::getTableData('\Translation',
                $sort_column, $direction, $request->input('start'),
                $request->input('length'), $searchableColumns, $search);
        if ($translation['total'] != 0) {
            return response()->json(['success' => TranslationService::lang('success',
                        $request->auth->language), 'draw' => $request->input('draw'),
                    'recordsTotal' => $translation['total'],
                    'recordsFiltered' => $request->input('length'), 'data' => $translation['records']],
                    200);
        }
        return response()->json(['success' => TranslationService::lang('success',
                    $request->auth->language), 'draw' => 0, 'recordsTotal' => 0,
                'recordsFiltered' => 0, 'data' => []], 200);
    }

    /**
     * Create a translation
     * @bodyParam short_name string required the short name of translated string
     * @bodyParam english string required the English value of translated string
     * @bodyParam svenska string required the  Sweden value of translated string
     * @response type json
     */
    public function store(Request $request)
    {
        $this->_validateTranslationRequest($request);
        $translation = Translation::create([
                'short_name' => $request['short_name'],
                'english' => $request['english'],
                'svenska' => $request['svenska']
        ]);

        if ($translation) {
            return response()->json(['success' => TranslationService::lang('translation_created',
                        $request->auth->language)], 200);
        }

        return response()->json(['error' => TranslationService::lang('error_translation_creation',
                    $request->auth->language)], 500);
    }

    /**
     * Retrieve a translation
     * @bodyParam id integer required the id of the model to fetch
     * @response json array type with http response code
     */
    public function edit(Request $request, int $id)
    {
        $request->request->add(['id' => $id]);
        $message     = TranslationService::customMesages($request->auth->language);
        $this->validate($request,
            ['id' => 'required|numeric|exists:translations,id,deleted_at,NULL'],
            $message);
        $translation = Translation::where('id', $id)->first();
        if (!empty($translation)) {
            return response()->json(['success' => TranslationService::lang('success',
                        $request->auth->language), 'data' => $translation], 200);
        }
        return response()->json(['error' => TranslationService::lang('not_found',
                    $request->auth->language)], 404);
    }

    /**
     * update a translation
     * @bodyParam short_name string required the short name of translated string
     * @bodyParam english string required the English value of translated string
     * @bodyParam svenska string required the  Sweden value of translated string
     * @bodyParam id int required the id of the updated required
     * @response type json array
     */
    public function update(Request $request, int $id)
    {
        $request->request->add(['id' => $id]);
        $this->_validateTranslationRequest($request);
        $language = 'svenska';
        if ($request->auth->language == 1) {
            $language = 'english';
        }
        //update data in table
        $translation = Translation::where('id', $id)->update(['short_name' => $request['short_name'],
            'english' => $request['english'],
            'svenska' => $request['svenska']]);

        if ($translation) {
            /* on update delete cache file of updated short_code to reflect the changes */
            Cache::pull($language);
            $updateData = Translation::select('short_name', $language)->pluck($language,
                'short_name');
            Cache::put($language, $updateData->toArray(), 60);
            return response()->json(['success' => TranslationService::lang('translation_updated',
                        $request->auth->language)], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_translation_updation',
                    $request->auth->language)], 500);
    }

    /**
     * Delete a translation
     * @bodyParam id integer required the id of the record to delete
     * @response type json array
     */
    public function destroy(Request $request, int $id)
    {
        $request->request->add(['id' => $id]);
        $messages    = TranslationService::customMesages($request->auth->language);
        $this->validate($request,
            ['id' => 'required|numeric|exists:translations,id,deleted_at,NULL'],
            $messages);
        $translation = Translation::where('id', $id)->delete();

        if ($translation) {
            return response()->json(['success' => TranslationService::lang('translation_deleted',
                        $request->auth->language)], 200);
        }

        return response()->json(['error' => TranslationService::lang('error_translation_delete',
                    $request->auth->language)], 500);
    }

    /**
     * get all translated strings when user logged in
     * @response json array type with http response code
     */
    public function getLanguageTranslation(Request $request)
    {
        //get the user language
        $user_lang = $request->auth->language;
        $lang      = 'svenska';
        if ($user_lang == 1) {
            $lang = 'english';
        }
        //get the language from db table Translation
        $translation = Translation::select('short_name', $lang)->pluck($lang,
            'short_name');
        if ($translation->isNotEmpty()) {
            $finalArray = [];
            if (Cache::has($lang)) {
                $finalArray = Cache::get($lang);
            } else {
                Cache::put($lang, $translation->toArray(), 60);
                $finalArray = $translation->toArray();
            }
            return response()->json(['success' => TranslationService::lang('success',
                        $request->auth->language), 'data' => $finalArray], 200);
        }
        return response()->json(['error' => TranslationService::lang('no_record_found',
                    $request->auth->language)], 404);
    }

    /**
     * Get all the translated string without user login
     * @response json array type
     */
    public function getTranslation(Request $request)
    {
        //get the user language
        $token     = $request->header('Authorization');
        $login     = false;
        $user_lang = config('constant.default_lang');
        if ($token) {
            try {
                $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
                $user = User::where('id', $credentials->sub)->first();
                $user_lang   = $user->language;
                $login       = true;
            } catch (ExpiredException $e) {
                $login = false;
            } catch (Exception $e) {
               $login = false;
            }
        }

        $lang = 'svenska'; //default user language
        if ($user_lang == 1) {
            $lang = 'english';
        }
        //get the language from db table Translation
        $translation = Translation::select('short_name', $lang)->pluck($lang,
            'short_name');
        if ($translation->isNotEmpty()) {
            $finalArray = [];
            if (Cache::has($lang)) { //check the cache has already teh same language
                $finalArray = Cache::get($lang);
            } else {
                Cache::put($lang, $translation->toArray(), 60);
                $finalArray = $translation->toArray();
            }
            return response()->json(['success' => TranslationService::lang('success',
                        $user_lang), 'data' => $finalArray, 'login' => $login],
                    200);
        }
        return response()->json(['error' => TranslationService::lang('no_record_found',
                    $user_lang)], 404);
    }

    /**
     * Validate Request for required parameters
     * @param type $request
     */
    private function _validateTranslationRequest($request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $this->validate($request,
            [
            'short_name' => 'required|max:100',
            'english' => 'required',
            'svenska' => 'required',
            'id' => 'sometimes|numeric|exists:translations,id,deleted_at,NULL'
            ], $messages);
    }

    /**
     * validate the index function request
     * @param type $request
     */
    private function validateIndexRequest($request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $rules    = config('constant.index_request_validation');
        $this->validate($request, $rules, $messages);
    }
}