<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Services\TranslationService;
use App\Services\PrepareDataTableService;
use App\Services\UserService;
use App\Repositories\ContactPersonRepository;

/**
 * User Controller operations
 *
 * Manage user profile related operations
 * @group Users
 * @author manpreet
 */
class UserController extends Controller
{
    protected $userRepository;

    /**
     * Constructor for this controller
     * @bodyParam  App\Repositories\UserRepository  $userRepository
     * @@response void
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * List all users
     *
     * List all the employees with specific sorting order, search and limit
     * @bodyParam order.0.dir string required the direction of the records in ASC or DESC direction
     * @bodyParam order.0.column string required the order by column name in ASC or DESC direction
     * @bodyParam search.value  string required the value to be searched in the records
     * @bodyParam start integer required the value from start getting records
     * @bodyParam length integer required the number of records want to fetch
     * @response json array type and variable name is data
     */
    public function index(Request $request)
    {
        $this->_validateIndexRequest($request);
        $searchableColumns = $columnNames       = [
            0 => 'full_name',
            1 => 'roles.name',
            2 => 'email',
            3 => 'status'
        ];

        $direction     = $request->input('order')[0]['dir'];
        $sort_column   = $columnNames[intval($request->input('order')[0]['column'])];
        $search        = $request->input('search')['value'];
        $relations     = [
            'roles' => ['name'],
            'permissions' => ['name']
        ];
        $hasRelation   = 'roles';
        $where         = [['id', "<>", 1]];
        $whereRelation = ['first_name', 'last_name', 'email', 'status'];
        $searchArr     = explode(' ', $search);
        if ($search != null && count($searchArr) == 2) {
            $response = PrepareDataTableService::getTableDataWithMultipleRealtions('\User',
                    $sort_column, $direction, 0, PHP_INT_MAX,
                    $searchableColumns, $search, true, $relations, $where,
                    $whereRelation, $hasRelation, []);

            $result = $response['records']->filter(function ($value, $key) use($searchArr) {
                if ($value->last_name == null) return true;
                return stripos($value->last_name, $searchArr[1]) !== -1 ? true : false;
            });
            $response['total'] = $result->count();
            if ($direction == 'asc') {
                $response['records'] = $result->sortBy($sort_column,
                        SORT_NATURAL | SORT_FLAG_CASE)->values()->splice($request->input('start'))->take($request->input('length'));
            } else {
                $response['records'] = $result->sortByDesc($sort_column,
                        SORT_NATURAL | SORT_FLAG_CASE)->values()->splice($request->input('start'))->take($request->input('length'));
            }
        } else {
            $response = PrepareDataTableService::getTableDataWithMultipleRealtions('\User',
                    $sort_column, $direction, $request->input('start'),
                    $request->input('length'), $searchableColumns, $search,
                    true, $relations, $where, $whereRelation, $hasRelation, []);
        }

        if ($response['total'] != 0) {
            return response()->json(['success' => TranslationService::lang('success',
                        $request->auth->language), 'draw' => $request->input('draw'),
                    'recordsTotal' => $response['total'],
                    'recordsFiltered' => $request->input('length'), 'data' => $response['records']],
                    200);
        }
        return response()->json(['success' => TranslationService::lang('success',
                    $request->auth->language), 'draw' => 0, 'recordsTotal' => 0,
                'recordsFiltered' => 0, 'data' => []], 200);
    }

    /**
     * Change a language of logged in user
     * @bodyParam lang integer required The language to be changed 1= english ,2= svenska
     * @reponse json array type
     */
    public function changeUserLanguage(Request $request, int $lang)
    {
        $checkLang = in_array($lang, [1, 2]); //check whether the input language is invalid
        if (!$checkLang) {
            return response()->json(['error' => TranslationService::lang('invalid_lang',
                        $request->auth->language)], 422);
        }
        $userid   = $request->auth->id;
        //call to change language function
        $response = $this->userRepository->changeLanguage($userid, $lang);
        if ($response['status'] == 'not_exist') {
            return response()->json(['error' => TranslationService::lang('user_not_found',
                        $lang)], 404);
        } elseif ($response['status'] == 'same_as_old') {
            return response()->json(['error' => TranslationService::lang('already_in_this_language',
                        $lang)], 400);
        } elseif ($response['status'] == 'success') {
            return response()->json([
                    'success' => TranslationService::lang('translation_updated',
                        $lang),
                    'message' => TranslationService::lang('success', $lang)]
                    , 200);
        } else {
            return response()->json(['error' => TranslationService::lang('updation_failed',
                        $request->auth->language)], 400);
        }
    }

    /**
     * upload user profile picture
     * @bodyParam image file required the profile picture to  be uploaded of mimes:jpeg,png,gif,jpg
     * @response json array type
     */
    public function uploadPicture(Request $request)
    {
        $this->_validatePictureUploadRequest($request);
        $data     = $request->all();
        $response = $this->userRepository->uploadPhoto($request->auth->id, $data);
        if ($response['status'] == 1) {
            return response()->json(['success' => TranslationService::lang('picture_uploaded',
                        $request->auth->language), 'avatar_path' => $response['path']],
                    200);
        }
        return response()->json(['success' => TranslationService::lang('error_picture_uploaded',
                    $request->auth->language)], 422);
    }

    /**
     * Update a user profile
     * @bodyParam first_name string required the first name of the user
     * @bodyParam last_name string optional the last name of the user
     * @bodyParam title_id integer optional the title id of the user
     * @bodyParam mobile string optional the mobile number of the user
     * @bodyParam address string optional the address of the user
     * @bodyParam city string optional the city of the user
     * @bodyParam zip string optional the zip of the user
     * @return type
     */
    public function updateProfile(Request $request)
    {
        $this->_validateUpdateUserRequest($request);
        $data     = $request->all();
        $response = $this->userRepository->updateProfile($request->auth->id,
            $data);
        if ($response['status'] == 1) {
            return response()->json(['success' => TranslationService::lang('profile_updated',
                        $request->auth->language), 'data' => $response['data']],
                    200);
        } elseif ($response['status'] == 2) {
            return response()->json(['error' => TranslationService::lang('user_not_found',
                        $request->auth->language)], 422);
        } else {
            return response()->json(['error' => TranslationService::lang('error_profile_update',
                        $request->auth->language)], 422);
        }
    }

    /**
     * Change a user password
     * @bodyParam current_password string required the current password of the user
     * @bodyParam new_password string required the new password which user want to change
     * @bodyParam new_password_confirmation string required this same as new password this is for confirmation purpose only
     * @response json array type
     */
    public function changePassword(Request $request)
    {
        $this->_validateChangePasswordRequest($request);
        $data     = $request->all();
        $response = $this->userRepository->changeUserPassword($request->auth->id,
            $data);
        if ($response['status'] == 1) {
            return response()->json(['success' => TranslationService::lang('password_changed',
                        $request->auth->language)], 200);
        } elseif ($response['status'] == 2) {
            return response()->json(['error' => TranslationService::lang('same_as_old_password',
                        $request->auth->language)], 422);
        } elseif ($response['status'] == 3) {
            return response()->json(['error' => TranslationService::lang('current_password_not_match',
                        $request->auth->language)], 422);
        } else {
            return response()->json(['error' => TranslationService::lang('error_password_change',
                        $request->auth->language)], 422);
        }
    }

    /**
     * Update user profile by admin
     * @bodyParam first_name string required the first name of the user
     * @bodyParam email string required the email of the user admin can change user email
     * @bodyParam status integer required the status of the user active or inactive
     * @bodyParam last_name string optional last name of the user
     * @bodyParam permissions array optional admin can change user permissions
     * @bodyParam notify boolean optional notify field whether to end email to the user being edited or not
     * @response json array type
     */
    public function editUser(Request $request)
    {
        $this->_validateEditUserRequest($request);
        $data     = $request->all();
        $response = $this->userRepository->editUserDetails($data);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('user_details_edited',
                        $request->auth->language)], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_user_edit',
                    $request->auth->language)], 422);
    }

    /**
     * Retrieve a logged in  user
     * @response json array type
     */
    public function getUserDetails(Request $request)
    {
        $user_id  = $request->auth->id; // get the logged in user id
        $response = $this->userRepository->fetchUserData($user_id);
        if ($response['status'] == 1) {
            return response()->json(['data' => $response['data']], 200);
        }
    }

    /**
     * Change user status from active to inactive or vice-versa
     * @param user_id integer required the user id whose status we want to update
     * @param status integer required the status value to be updated
     * @response json array type
     */
    public function changeUserStatus(Request $request, int $user_id, int $status)
    {
        $request['user_status'] = config('constant.user_status');
        /* add parameters to request for validation */
        $request->request->add([
            'user_id' => $user_id,
            'status' => $status
        ]);
        /* validate the attribute for specific condition */
        $this->validate($request,
            [
            'user_id' => 'required|integer|exists:users,id,deleted_at,NULL',
            'status' => 'required|integer|in_array:user_status.*'
        ]);
        $status_array           = config('constant.user_status_array');
        $status_value           = $status_array[$status];
        $repo                   = new ContactPersonRepository();
        $response               = $repo->changeStatus($user_id, $status);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('status_changed',
                        $request->auth->language)], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_status_changed',
                    $request->auth->language)], 500);
    }

    /**
     * notify the user about changes
     * @bodyParam user_id integer required the id of the user being notified
     * @return type
     */
    public function notifyUser(Request $request, int $user_id)
    {
        $request->request->add(['id' => $user_id]);
        $this->validate($request,
            ['id' => 'required|exists:users,id,deleted_at,NULL']);

        $response = $this->userRepository->notifyUser($user_id);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('user_notified',
                        $request->auth->language)], 200);
        }
        return response()->json(['error' => TranslationService::lang('error_user_notify',
                    $request->auth->language)], 500);
    }

    /**
     * Get the user roles and permissions
     * @bodyParam user_id integer required the id of the user being fetched
     * @response json array type
     */
    public function userRolesPermissions(Request $request, int $user_id)
    {
        $request->request->add(['id' => $user_id]);
        $this->validate($request,
            ['id' => 'required|exists:users,id,deleted_at,NULL']);
        $response = $this->userRepository->userRolesPermissions($user_id);
        if ($response['status'] == 1) {
            return response()->json(['success' => TranslationService::lang('success',
                        $request->auth->language), 'data' => $response['data']],
                    200);
        }
    }

    /**
     * validate the change password request
     * @param type $request
     */
    private function _validateChangePasswordRequest($request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $this->validate($request,
            [
            'current_password' => 'required|min:6,max:15',
            'new_password' => 'required|min:6,max:15',
            'new_password_confirmation' => 'required|same:new_password|min:6,max:15',
            ], $messages);
    }

    /**
     * validate upload profile picture request
     * @param type $request
     */
    private function _validatePictureUploadRequest($request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $this->validate($request,
            [
            'image' => 'required|file|mimes:jpeg,png,gif,jpg|max:10240'
            ], $messages);
    }

    /**
     * update user request validation function
     * @param type $request
     */
    private function _validateUpdateUserRequest($request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $this->validate($request,
            [
            'first_name' => 'required|string|max:191',
            'last_name' => 'nullable|string|max:191',
            'title_id' => 'sometimes|integer|exists:options,id,deleted_at,NULL',
            'mobile' => 'max:10',
            'address' => 'nullable|string',
            'city' => 'nullable|string',
            'zip' => 'nullable|string'
            ], $messages);
    }

    /**
     * validate edit user request by admin user
     * @param Request $request
     */
    private function _validateEditUserRequest(Request $request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $this->validate($request,
            [
            'id' => 'required|numeric|exists:users,id,deleted_at,NULL'
            ], $messages);  
        $this->validate($request,
            [
            'first_name' => 'required|string|max:191',
            'email' => 'required|email|unique:users,email,'.$request->input('id').',id|max:191',
            'status' => 'required|integer',
            'last_name' => 'nullable|sometimes|string|max:191',
            'permissions' => 'array',
            'notify' => 'nullable|boolean'
            ], $messages);
    }

    /**
     * validate data table request function
     * @param type $request
     */
    private function _validateIndexRequest($request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $rules    = config('constant.index_request_validation');
        $this->validate($request, $rules, $messages);
    }
}