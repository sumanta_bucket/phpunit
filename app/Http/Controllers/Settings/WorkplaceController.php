<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\WorkplaceRepository;
use App\Services\TranslationService;
use App\Services\PrepareDataTableService;

/**
 * Workplace Controller operations
 *
 * Manage CRUD related operations
 * @group Workplace
 * @author manpreetkaur
 */
class WorkplaceController extends Controller
{
    protected $workplace_repository;

    /**
     * Constructor for this controller
     */
    public function __construct(WorkplaceRepository $repository)
    {
        $this->workplace_repository = $repository;
    }

    /**
     * List all workplaces
     *
     * List all the workplaces with specific sorting order, search and limit
     * @bodyParam order.0.dir string required the direction of the records in ASC or DESC direction
     * @bodyParam order.0.column string required the order by column name in ASC or DESC direction
     * @bodyParam search.value  string required the value to be searched in the records
     * @bodyParam start integer required the value from start getting records
     * @bodyParam length integer required the number of records want to fetch
     * @response json array type in variable data
     */
    public function index(Request $request)
    {
        $this->_validateIndexRequest($request);
        $columnNames       = [
            0 => 'name',
            1 => 'zipcode',
            2 => 'place',
            3 => 'used_by'
        ];
        $searchableColumns = [$columnNames[0], $columnNames[1], $columnNames[2]];
        $direction         = $request->input('order')[0]['dir'];
        $sort_column       = $columnNames[intval($request->input('order')[0]['column'])];
        $search            = $request->input('search')['value'];
        //call common serivce for table results
        $response          = PrepareDataTableService::getTableDataWithMultipleRealtions('\Workplace',
                $sort_column, $direction, $request->input('start'),
                $request->input('length'), $searchableColumns, $search);
        if ($response['total'] != 0) {
            return response()->json(['success' => TranslationService::lang('success',
                        $request->auth->language), 'draw' => $request->input('draw'),
                    'recordsTotal' => $response['total'],
                    'recordsFiltered' => $request->input('length'), 'data' => $response['records']],
                    200);
        }
        return response()->json(['success' => TranslationService::lang('success',
                    $request->auth->language), 'draw' => 0, 'recordsTotal' => 0,
                'recordsFiltered' => 0, 'data' => []], 200);
    }

    /**
     * Create a workplace
     * @bodyParam name string required the name of the workplace
     * @bodyParam zipcode integer required the zipcode of the workplace
     * @bodyParam place string required the place of the workplace
     * @response json array type
     */
    public function store(Request $request)
    {
        $this->_validateWorkplaceRequest($request);
        $data     = $request->all();
        $response = $this->workplace_repository->create($data);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('workplace_created',
                        $request->auth->language), 'data'=>$response], 200);
        }
        return response()->json(['success' => TranslationService::lang('error_workplace_creation',
                    $request->auth->language)], 500);
    }

    /**
     * Update a workplace
     * @bodyaram id integer required the id of the record  being updated
     * @bodyaram name string required the name of the workplace
     * @bodyaram zipcode integer required the zipcode of the workplace
     * @bodyaram place string required the place of the workplace
     * @response json array type
     */
    public function update(Request $request, int $id)
    {
        $request->request->add(['id' => $id]);
        $this->_validateWorkplaceRequest($request);
        $data     = $request->all();
        $response = $this->workplace_repository->update($id, $data);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('workplace_updated',
                        $request->auth->language)], 200);
        }
        return response()->json(['success' => TranslationService::lang('error_workplace_updation',
                    $request->auth->language)], 500);
    }

    /**
     * Delete a workplace
     * @bodyparam id integer required the id of the record to delete
     * @response json array type
     */
    public function destroy(Request $request, int $id)
    {
        $request->request->add(['id' => $id]);
        $this->validate($request,
            ['id' => 'required|integer|exists:workplaces,id,deleted_at,NULL']);
        $response = $this->workplace_repository->delete($id);
        if ($response) {
            return response()->json(['success' => TranslationService::lang('workplace_deleted',
                        $request->auth->language)], 200);
        }
        return response()->json(['success' => TranslationService::lang('error_workplace_deletion',
                    $request->auth->language)], 500);
    }

    /**
     * validate the workplace request private method
     * @param type $request
     */
    private function _validateWorkplaceRequest($request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $this->validate($request,
            [
            'name' => 'required|string|max:191',
            'zipcode' => 'string|max:10',
            'place' => 'string|max:191',
            'id' => 'sometimes|required|integer|exists:workplaces,id,deleted_at,NULL'
        ],$messages);
    }

    /**
     * validate index function request
     * @param type $request
     */
    private function _validateIndexRequest($request)
    {
        $messages = TranslationService::customMesages($request->auth->language);
        $rules    = config('constant.index_request_validation');
        $this->validate($request, $rules, $messages);
    }
}