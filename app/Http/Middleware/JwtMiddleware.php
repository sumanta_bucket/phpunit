<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use App\Models\User;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use App\Services\TranslationService;

class JwtMiddleware
{

    public function handle($request, Closure $next, $guard = null)
    {

        $token = $request->header('Authorization');

        if (!$token) {
            // Unauthorized response if token not there
            return response()->json([
                    'error' => TranslationService::lang('missing_token_error',
                        config('constant.default_lang'))
                    ], 401);
        }

        try {
            $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
        } catch (ExpiredException $e) {
            \App\Models\Token::where('token', $token)->delete();
            return response()->json([
                    'error' => TranslationService::lang('token_expired_error',
                        config('constant.default_lang'))
                    ], 400);
        } catch (Exception $e) {
            \App\Models\Token::where('token', $token)->delete();
            return response()->json([
                    'error' => $e->getMessage()
                    ], 400);
        }


        $user = User::where('id', $credentials->sub)->where('status', 1)->first();
        if (!empty($user)) {
            $tokenConfirm = \App\Models\Token::where('token',$token)->first();
            if (!$tokenConfirm) {
                return response()->json([
                        'error' => TranslationService::lang('user_not_found',
                            config('constant.default_lang'))
                        ], 401);
            }
            $request->auth = $user;
            return $next($request);
        }
        return response()->json([
                'error' => TranslationService::lang('user_not_found',
                    config('constant.default_lang'))
                ], 401);
        // Now let's put the user in the request class so that you can grab it from there
    }
}