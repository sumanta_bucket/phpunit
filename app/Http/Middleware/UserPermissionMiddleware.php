<?php

namespace App\Http\Middleware;

use Closure;
use App\Services\TranslationService;

/**
 * UserPermissionMiddleware
 *
 * check whether the user is permitted to access the route or not
 * @group Middleware
 * @author manpreetkaur
 */
class UserPermissionMiddleware
{
    public function handle($request, Closure $next, $permission)
    {
        $permissions = is_array($permission) ? $permission : explode('|',
                $permission);
        foreach ($permissions as $permission) {
            if ($request->auth->can($permission)) {
                return $next($request);
            }
        }
        return response()->json(['error' => TranslationService::lang('insufficient_permission',
                    $request->auth->language)], 403);
    }
}