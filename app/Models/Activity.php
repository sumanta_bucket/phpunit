<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Services\TranslationService;

/**
 * Description of Activity
 * The all users activities are stored in this model
 * @author manpreetkaur
 */
class Activity extends Model
{

    use SoftDeletes;
    protected $table    = 'activities';
    protected $tables   = ['deleted_at'];
    protected $fillable = ['user_id', 'type', 'project_id', 'marker_id', 'history_type'];
    protected $appends  = ['time', 'project'];

    /*
     * one to many relation with user 
     */

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    /*
     * added project as attribute for conditional purpose instead of project relation
     */

    public function getProjectAttribute()
    {
        //if history type is new user the project is return as a user
        if ($this->history_type == 2) {
            return User::where('id', $this->project_id)->first();
        } else {
            return Project::where('id', $this->project_id)->first();
        }
    }
    /*
     * custom atribute time which return time in easy understandable form
     */

    public function getTimeAttribute()
    {
        $user = app()->request->auth;
        $arr  = explode(" ", $this->updated_at->diffForHumans());
        return $arr[0]." ".TranslationService::lang($arr[1], $user->language)." ".TranslationService::lang($arr[2],
                $user->language);
    }
}