<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Description of Category
 *
 * @author manpreetkaur
 */
class Category extends Model
{

    use SoftDeletes;
    protected $tables = ['deleted_at'];
    protected $fillable = ['name'];

    /**
     * return many to many relationship
     * @return type
     */
    public function projects()
    {
        return $this->belongsToMany('App\Models\Project', 'project_categories',
                'category_id', 'project_id');
    }

    public function firesealing_markers()
    {
        return $this->hasMany(FiresealingMarker::class);
    }
}