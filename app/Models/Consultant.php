<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Description of Consultant
 * @author manpreetkaur
 */
class Consultant extends Model
{

    use SoftDeletes;
    protected $tables   = ['deleted_at'];
    protected $fillable = ['user_id','consultant_company_id','consultant_title_id'];
    protected $hidden   = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function consultantTitle()
    {
        return $this->belongsTo('App\Models\ConsultantTitle');
    }

    public function consultantCompany()
    {
        return $this->belongsTo('App\Models\ConsultantCompany');
    }

    public function projects()
    {
        return $this->belongsToMany('App\Models\Project', 'project_consultants');
    }
}