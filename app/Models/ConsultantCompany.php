<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Description of ConsultantCompany
 *
 * @author 
 */
class ConsultantCompany extends Model
{
    use SoftDeletes;

    protected $tables   = ['deleted_at'];
    protected $fillable =['name'];

    public function consultant()
    {
        return $this->hasOne('App\Models\Consultant');
    }

}