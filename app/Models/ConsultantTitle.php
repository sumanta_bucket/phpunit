<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Description of ConsultantTitle
 *
 * @author 
 */
class ConsultantTitle extends Model
{

    use SoftDeletes;
    protected $tables   = ['deleted_at'];
    protected $fillable = ['name'];

    public function consultant()
    {
        return $this->hasOne(Consultant::class);
    }
}