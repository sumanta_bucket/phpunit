<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Description of ContactPerson
 * @author manpreetkaur
 */
class ContactPerson extends Model
{

    use SoftDeletes;
    protected $fillable = ['user_id'];
    protected $table    = 'contact_persons';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function customers()
    {
        return $this->belongsToMany(Customer::class);
    }

    public function projects()
    {
        return $this->belongsToMany('App\Models\Project',
                'project_contact_person');
    }

    public function projectCategories()
    {
        return $this->belongsToMany('App\Models\ProjectCategory',
                'project_category_contact_persons');
    }
}