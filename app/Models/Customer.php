<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Description of Customer
 * @author manpreetkaur
 */
class Customer extends Model
{

    use SoftDeletes;
    protected $fillable = ['user_id', 'organization', 'country', 'invoice_email',
        'address2', 'other_invoice_address', 'other_invoice_city', 'other_shipping_address',
        'other_shipping_city'];
    protected $tables   = ['deleted_at'];
    protected $hidden   = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function projets()
    {
        return $this->hasMany('App\Models\Project');
    }

    public function categories()
    {
        return $this->belongsToMany(Option::class, 'customer_category',
                'customer_id', 'category_id');
    }

    public function contact_persons()
    {
        return $this->belongsToMany(ContactPerson::class);
    }
}