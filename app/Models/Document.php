<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\DocumentPathService;

/**
 * Description of Document
 *
 * @author 
 */
class Document extends Model
{
    protected $fillable = ['project_id', 'document'];
    protected $appends  = ['document_name','url'];

    public function getUrlAttribute()
    {
        //get encoded path from the service and return
        $path = DocumentPathService::encodePath('documents/'.$this->document);
        return env('APP_URL').'v1/document/'.$path;
    }

    public function getDocumentNameAttribute()
    {
        //esplode to get the orginal file name
        $exp = explode('___', $this->getOriginal('document'));
        if (isset($exp[1])) return $exp[1];
        return $this->document; 
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }
}