<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * Description of EmailTemplate
 * @author manpreetkaur
 */
class EmailTemplate extends Model
{
    use SoftDeletes;
    
    protected $fillable =['email_template_type_id','name','template','is_default','deletes','subject'];
    protected $tables   = ['deleted_at'];

    /**
     * Defined relation with email template type
     * @return type
     */
    public function emailTemplateType()
    {
        return $this->belongsTo('App\Models\EmailTemplateType');
    }
}