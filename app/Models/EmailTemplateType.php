<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Description of EmailTemplateType
 *
 * @author manpreetkaur
 */
class EmailTemplateType extends Model
{
    use SoftDeletes;

    protected $fillable = ['type','name','placeholder'];
    protected $tables   = ['deleted_at'];

    /**
     * Defined relation with email template one to many
     * @return type
     */
    public function emailTemplate()
    {
        return $this->hasMany('App\Models\EmailTemplate');
    }
}