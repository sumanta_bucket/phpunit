<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Description of Employee
 * @author manpreetkaur
 */
class Employee extends Model
{

    use SoftDeletes;
    protected $tables   = ['deleted_at'];
    protected $fillable = ['user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function relatives()
    {
       return $this->belongsToMany(Relative::class,'employee_relative','employee_id')->withPivot('relationship_id');

    }
}