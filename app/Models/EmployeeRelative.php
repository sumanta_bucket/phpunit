<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of EmployeeRelative
 *
 * @author manpreetkaur
 */
class EmployeeRelative extends Model
{
    protected $table = 'employee_relative';
    protected $fillable = ['relationship_id','employee_id','relative_id'];
    public $timestamps = false;

    public function relationship()
    {
        return $this->belongsTo(Relationship::class, 'relationship_id', 'id');
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id', 'id');
    }

    public function relative()
    {
        return $this->belongsTo(Relative::class, 'relative_id', 'id');
    }
}