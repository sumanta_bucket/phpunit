<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Description of FireClass
 * @author manpreetkaur
 */
class FireClass extends Model
{

    use SoftDeletes;
    protected $table    = 'fire_classes';
    protected $tables   = ['deleted_at'];
    protected $fillable = ['name','class_type'];

    public function firesealing_marker()
    {
        return $this->hasOne('App\Models\FiresealingMarker');
    }
    public function insulation_marker()
    {
        return $this->hasOne('App\Models\InsulationMarker');
    }
}