<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Description of FireboardSetting
 * @author manpreetkaur
 */
class FireboardSetting extends Model
{

    use SoftDeletes;
    protected $tables   = ['deleted_at'];
    protected $fillable = ['name'];

    public function documents()
    {
        return $this->hasMany(FireboardSettingDocument::class);
    }

    protected static function boot()
    {
        parent::boot();
        static::deleting(function($fireboard) {
            $fireboard->documents()->get()->each->delete();
        });
    }
}