<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of FiresealingMarker
 *
 * @author mnapreekaur
 */
class FiresealingMarker extends Model
{

    protected $fillable = ['section_id', 'penetration_type_id', 'hole_type', 'firesealing_type_id',
        'quantity',
        'fire_class_id', 'dimension1', 'dimension2', 'status', 'predefined', 'notes',
        'updated_by',
        'added_by', 'x_axis', 'y_axis', 'status_type', 'added_by_name', 'marker_date','category_id'];
    protected $appends  = ['hole_name'];

    public function getHoleNameAttribute()
    {
        $firesealing_holes = config('constant.hole_type_firesealing');
        return $firesealing_holes[$this->hole_type];
    }

    public function firesealing_type()
    {
        return $this->belongsTo(FiresealingType::class);
    }

    public function fire_class()
    {
        return $this->belongsTo(FireClass::class);
    }

    public function penetration_type()
    {
        return $this->belongsTo(PenetrationType::class);
    }

    public function section()
    {
        return $this->belongsTo(Section::class);
    }

    public function photos()
    {
        return $this->hasMany(FiresealingMarkerPhoto::class);
    }

    public function added_by()
    {
        return $this->belongsTo(User::class, 'added_by');
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    protected static function boot()
    {
        parent::boot();
        static::deleting(function($marker) {
            $photos = $marker->photos()->get();
            foreach ($photos as $photo) {
                $marker_image = base_path('public/uploads/images/').$photo->photo;
                if (file_exists($marker_image)) {
                    unlink($marker_image);
                }
                $photo->delete();
            }
        });
    }

}