<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\DocumentPathService;

/**
 * Description of FiresealingMarkerPhoto
 *
 * @author 
 */
class FiresealingMarkerPhoto extends Model
{

    protected $fillable=['firesealing_marker_id','photo'];
    protected $appends = ['photo_path'];

    public function getPhotoPathAttribute() 
    {
        $path= DocumentPathService::encodePath('images/'.$this->photo);
        return env('APP_URL').'v1/document/'.$path;
    }

    public function firesealing_marker()
    {
        return $this->belongsTo(FiresealingMarker::class);
    }
}