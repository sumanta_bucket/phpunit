<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Description of FiresealingType
 * @author manpreetkaur
 */
class FiresealingType extends Model
{

    use SoftDeletes;
    protected $tables   = ['deleted_at'];
    protected $fillable = ['name'];

    public function documents()
    {
        return $this->hasMany(FiresealingTypeDocument::class);
    }

    public function firesealing_marker()
    {
        return $this->hasOne('App\Models\FiresealingMarker');
    }
    
    protected static function boot()
    {
        parent::boot();
        static::deleting(function($firesealing) {
            $firesealing->documents()->get()->each->delete();
        });
    }
}