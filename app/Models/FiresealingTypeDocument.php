<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\DocumentPathService;

/**
 * Description of FiresealingType
 * @author manpreetkaur
 */
class FiresealingTypeDocument extends Model
{
    public $fillable   = ['firesealing_type_id', 'document'];
    protected $appends = ['document_name'];

    public function getDocumentAttribute($value)
    {
        $path = DocumentPathService::encodePath('documents/'.$value);
        return env('APP_URL').'v1/document/'.$path;
    }

    public function getDocumentNameAttribute()
    {
        $exp = explode('___', $this->getOriginal('document'));
        if (isset($exp[1])) return $exp[1];
        return $this->document;
    }
}