<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Floor
 * @author manpreetkaur
 */
class Floor extends Model
{
    protected $fillable = ['project_id', 'total_sections', 'is_fireseal_active',
        'is_firesteel_active'];

    public function project()
    {
        return $this->belongsTo('App\Models\Project');
    }

    public function sections()
    {
        return $this->hasMany('App\Models\Section');
    }

    /**
     * on delete parent delete child relations 
     */
    protected static function boot()
    {
        parent::boot();
        static::deleting(function($floor) {
            $floor->sections()->get()->each->delete();
        });
    }

}