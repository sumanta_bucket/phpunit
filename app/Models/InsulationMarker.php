<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of InsulationMarker
 *
 * @author manpreetkaur
 */
class InsulationMarker extends Model
{
    protected $fillable = ['section_id', 'penetration_type_id', 'hole_type', 'insulation_profile_id',
        'insulation_material_id', 'fire_class_id', 'dimensions', 'prescribed', 'measured',
        'thickness', 'status', 'predefined', 'notes',
        'updated_by',
        'added_by', 'x_axis', 'y_axis', 'status_type', 'added_by_name', 'marker_date'];
    protected $appends  = ['hole_name', 'material_type'];

    public function getHoleNameAttribute()
    {
        $insulation_holes = config('constant.hole_type_insulation');
        return $insulation_holes[$this->hole_type];
    }

    public function getMaterialTypeAttribute()
    {
        return $this->insulation_material ? $this->insulation_material->material_type
                : [];
    }

    public function insulation_profile()
    {
        return $this->belongsTo(InsulationProfile::class);
    }

    public function insulation_material()
    {
        return $this->belongsTo(InsulationMaterial::class);
    }

    public function fire_class()
    {
        return $this->belongsTo(FireClass::class);
    }

    public function penetration_type()
    {
        return $this->belongsTo(PenetrationType::class);
    }

    public function section()
    {
        return $this->belongsTo(Section::class);
    }

    public function photos()
    {
        return $this->hasMany(InsulationMarkerPhoto::class);
    }

    public function added_by()
    {
        return $this->belongsTo(User::class, 'added_by');
    }

    protected static function boot()
    {
        parent::boot();
        static::deleting(function($marker) {
            $photos = $marker->photos()->get();
            foreach ($photos as $photo) {
                $marker_image = base_path('public/uploads/images/').$photo->photo;
                if (file_exists($marker_image)) {
                    unlink($marker_image);
                }
                $photo->delete();
            }
        });
    }
}