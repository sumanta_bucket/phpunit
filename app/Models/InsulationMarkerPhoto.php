<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\DocumentPathService;

/**
 * Description of InsulationMarkerPhoto
 *
 * @author
 */
class InsulationMarkerPhoto extends Model
{

    protected $fillable = ['insulation_marker_id', 'photo'];
    protected $appends = ['photo_path'];

    public function getPhotoPathAttribute()
    {
        $path= DocumentPathService::encodePath('images/'.$this->photo);
        return env('APP_URL').'v1/document/'.$path;
    }

    public function insulation_marker()
    {
        return $this->belongsTo('App\Models\InsulationMarker');
    }
}