<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Description of InsulationMaterial
 * @author manpreetkaur
 */
class InsulationMaterial extends Model {

    use SoftDeletes;

    protected $tables = ['deleted_at'];
    protected $fillable = ['name','material_type_id'];

    public function insulation_marker()
    {
        return $this->hasOne('App\Models\InsulationMarker');
    }

    public function documents() {
        return $this->hasMany(InsulationMaterialDocument::class);
    }

    public function material_type()
    {
        return $this->belongsTo('App\Models\MaterialType');
    }
}
