<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Description of InsulationProfile
 * @author manpreetkaur
 */
class InsulationProfile extends Model
{

    use SoftDeletes;
    protected $tables   = ['deleted_at'];
    protected $fillable = ['name'];

    public function insulation_marker()
    {
        return $this->hasOne('App\Models\InsulationMarker');
    }
}