<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Description of Lastsection
 *
 * @author manpreetkaur
 */
class Lastsection extends Model
{

    use SoftDeletes;
    protected $tables   = ['deleted_at'];
    protected $fillable = ['project_id', 'floor_id', 'section_id', 'type', 'user_id'];
    protected $appends  = ['has_pdf'];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function section()
    {
        return $this->belongsTo(Section::class);
    }

    public function getHasPdfAttribute()
    {
        $section = $this->section;
        if ($this->type == 1 && $section->document_steel) return true;
        if ($this->type == 2 && $section->document) return true;
        return false;
    }
}