<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Services\TranslationService;

/**
 * Description of MarkerType
 *
 * @author manpreetkaur
 */
class MaterialType extends Model
{

    use SoftDeletes;
    protected $tables   = ['deleted_at'];
    protected $fillable = ['name', 'slug'];

    public function getSlugAttribute($value)
    {
        $language = app()->request->auth->language;
        return TranslationService::lang($value, $language);
    }

    public function insulation_marterial()
    {
        return $this->hasOne('App\Models\InsulationMaterial');
    }
}