<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Description of Option
 *
 * @author manpreetkaur
 */
class Option extends Model
{
    use SoftDeletes;
    protected $tables   = ['deleted_at'];
    protected $fillable =['option_name','option_value'];

    public function customers()
    {
        return $this->belongsToMany(Customer::class,'customer_category','category_id','customer_id');
    }
    public function users()
    {
        return $this->hasMany(ContactPerson::class,'id','title_id');
    }
}