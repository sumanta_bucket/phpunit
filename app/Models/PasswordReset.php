<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of PasswordReset
 *
 * @author pc8
 */
class PasswordReset extends Model
{
    protected $table = 'password_resets';

    protected $fillable = ['email','token'];
}