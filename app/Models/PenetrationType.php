<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Description of PenetrationType
 *
 * @author manpreetkaur
 */
class PenetrationType extends Model
{

    use SoftDeletes;
    protected $tables   = ['deleted_at'];
    protected $fillable = ['name', 'slug'];

    public function insulation_marker()
    {
        return $this->hasOne('App\Models\InsulationMarker');
    }

    public function firesealing_marker()
    {
        return $this->hasOne('App\Models\FiresealingMarker');
    }
}