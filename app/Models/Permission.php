<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/**
 * Description of Permission
 *
 * @author pc8
 */
class Permission extends Model
{
    protected $fillable = ['name','guard_name'];
    public function users()
    {
        return $this->belongsToMany('App\Models\User');
    }
}