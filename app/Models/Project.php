<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Floor;

/**
 * Description of Project Model 
 * @author manpreetkaur
 */
class Project extends Model
{

    use SoftDeletes;
    protected $tables  = ['deleted_at'];
    protected $fillable = ['user_id','customer_id','workplace_id','name','project_number','status','old_penetrations'];
    protected $appends = ['has_pdf'];  //appended new column to the table

    /**
     * appended parameter for checking whether its section has pdf or not
     * @return boolean
     */
    public function getHasPdfAttribute()
    {
        $floors = Floor::where('project_id', $this->id)->whereHas('sections',
                function($query) {
                $query->where('document', '!=', NULL)->orWhere('document_steel',
                    '!=', NULL);
            })->get();
        if ($floors->isNotEmpty()) {
            return true;
        }
        return false;
    }

    /**
     * has one relation with customer
     * @return type
     */
    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }

    /**
     * has one relation with the workplace
     * @return type
     */
    public function workplace()
    {
        return $this->belongsTo('App\Models\Workplace');
    }

    /**
     * Many to many relationship with consultant
     * @return type
     */
    public function consultants()
    {
        return $this->belongsToMany('App\Models\Consultant',
                'project_consultants');
    }

    /**
     * Represent has many relationship with categories
     * @return type
     */
    public function _categories()
    {
        return $this->hasMany('App\Models\ProjectCategory', 'project_id');
    }

     /**
     * Represent many to many relationship with categories
     * @return type
     */
    public function categories()
    {
        return $this->belongsToMany('App\Models\Category', 'project_categories',
                'project_id', 'category_id')->withPivot(['sub_contractor', 'customer_id',
                'project_number']);
    }

    /**
     * Representing has many relationship with floors
     * @return type
     */
    public function floors()
    {
        return $this->hasMany('App\Models\Floor');
    }

    /**
     * representing many to many relationship contact persons
     * @return type
     */
    public function contactPersons()
    {
        return $this->belongsToMany('App\Models\ContactPerson','project_contact_person');
    }

    /**
     * Delete the dependent records when the project is deleted
     */
//    protected static function boot()
//    {
//        parent::boot();
//        static::deleting(function($project) {
//            $project->floors()->get()->each->delete();
//            $project->_categories()->get()->each->delete();
//        });
//    }

    /**
     * representing the has many relationship with last active section of the project
     * @return type
     */
    public function last_sections()
    {
        return $this->hasMany(Lastsection::class);
    }
     public function documents()
    {
        return $this->hasMany(Document::class);
    }

}