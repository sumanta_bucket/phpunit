<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Description of ProjectCategory
 * @author manpreetkaur
 */
class ProjectCategory extends Model
{

    use SoftDeletes;
    protected $fillable = ['project_id', 'category_id', 'sub_contractor', 'customer_id',
        'project_number'];
    protected $tables   = ['deleted_at'];
    protected $hidden   = ['id'];

    public function contactPersons()
    {
        return $this->belongsToMany('App\Models\ContactPerson',
                'project_category_contact_persons');
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}