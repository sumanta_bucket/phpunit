<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/**
 * Description of ProjectConsultant
 *
 * @author
 */
class ProjectConsultant extends Model
{
    protected $table = 'project_consultants';
    protected $fillable = ['project_id','consultant_id'];
}