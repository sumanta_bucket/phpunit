<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/**
 * Description of ProjectContactPerson
 *
 * @author pc8
 */
class ProjectContactPerson extends Model
{
    protected $table = 'project_contact_person';
    protected $fillable = ['project_id','contact_person_id'];
}