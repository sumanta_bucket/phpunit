<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Description of Relationship
 *
 * @author manpreetkaur
 */
class Relationship extends Model
{

    use SoftDeletes;
    protected $tables = ['deleted_at'];
    protected $fillable =['name'];

}