<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Description of Relative
 * @author manpreetkaur
 */
class Relative extends Model
{

    use SoftDeletes;
    protected $tables   = ['deleted_at'];
    protected $fillable = ['first_name', 'last_name', 'email',
        'mobile'];
    protected $appends  = ['full_name'];

    public function getFullNameAttribute()
    {
        return $this->last_name ? $this->first_name.' '.$this->last_name : $this->first_name;
    }

}