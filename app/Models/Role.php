<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Role
 *
 * @author pc8
 */
class Role extends Model
{

    protected $fillable = ['name', 'guard_name'];
    public function isAdmin()
    {
        return $this->name === config('access.users.admin_role');
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User');
    }
}