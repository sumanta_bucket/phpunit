<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\DocumentPathService;

/**
 * Description of Section Model
 * @author manpreetkaur
 */
class Section extends Model
{
    protected $fillable = ['floor_id', 'name', 'document', 'image', 'document_steel',
        'image_steel', 'is_fireseal_active', 'is_firesteel_active'];
    protected $appends  = ['document_name', 'document_steel_name', 'base_url', 'has_insulation_marker',
        'has_firesealing_marker', 'insulation_path', 'firesealing_path']; //append extra parameters other than table columns

    //The Accessor for document column name

    public function getDocumentNameAttribute()
    {
        $exp = explode('___', $this->document);
        if (isset($exp[1])) return $exp[1];
        return $this->document;
    }

    //Return base URL
    public function getBaseUrlAttribute()
    {
        return url('uploads/');
    }

    //The Accessor for document_steel column name
    public function getDocumentSteelNameAttribute()
    {
        $exp = explode('___', $this->document_steel);
        if (isset($exp[1])) return $exp[1];
        return $this->document_steel;
    }

    public function getFiresealingPathAttribute()
    {
        $path = DocumentPathService::encodePath('documents/'.$this->document);
        return env('APP_URL').'v1/document/'.$path;
    }

    public function getInsulationPathAttribute()
    {
        $path = DocumentPathService::encodePath('documents/'.$this->document_steel);
        return env('APP_URL').'v1/document/'.$path;
    }
    /*
     * relationship with the section
     */

    public function floor()
    {
        return $this->belongsTo('App\Models\Floor');
    }

    public function firesealing_markers()
    {
        return $this->hasMany('App\Models\FiresealingMarker');
    }

    public function insulation_markers()
    {
        return $this->hasMany('App\Models\InsulationMarker');
    }

    //The Accessor for has_insulation_marker column name
    public function getHasInsulationMarkerAttribute()
    {
        return $this->insulation_markers()->exists();
    }

    public function getHasFiresealingMarkerAttribute()
    {
        return $this->firesealing_markers()->exists();
    }

    /**
     * on delete parent delete child relations
     */
    protected static function boot()
    {
        parent::boot();
        static::deleting(function($section) {
            $section->insulation_markers()->get()->each->delete();
            $section->firesealing_markers()->get()->each->delete();
        });
    }
}