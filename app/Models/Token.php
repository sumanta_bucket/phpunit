<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/**
 * Description of Token
 * @author manpreetkaur
 */
class Token extends Model
{
   protected $fillable = ['user_id','token'];
}