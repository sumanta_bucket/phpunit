<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Description of Translation
 * Translation Model 
 * @author manpreet
 */
class Translation extends Model
{
    use SoftDeletes;
    
    protected $fillable = ['short_name', 'english', 'svenska'];
    protected $tables   = ['deleted_at'];

}