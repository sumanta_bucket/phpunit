<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model implements AuthenticatableContract, AuthorizableContract,
    CanResetPasswordContract
{

    use Authenticatable,
        Authorizable,
        CanResetPassword,
        Notifiable,
        HasRoles,
        SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'email', 'password', 'middle_name', 'last_name', 'confirmation_code',
        'status', 'confirmed', 'mobile', 'notify', 'address', 'city', 'zip', 'status',
        'notes', 'avatar', 'language', 'telephone'
    ];
    protected $tables   = ['deleted_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden     = [
        'password', 'created_at', 'updated_at', 'deleted_at'
    ];
    protected $appends    = ['full_name', 'avatar_path'];
    /* default guard name provided by the plugin automatically not required but need to define to assign the  role
      use the assignRole() functiion to assign the role and  assignPermission to assign the permission to
      assign the permission to user we can assign multiple permissions to the users at at the same tom using array also */
    protected $guard_name = 'api';

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->last_name ? $this->first_name.' '.$this->last_name : $this->first_name;
    }
    /*
     * change the default value of the last name
     */

    public function getLastNameAttribute($value)
    {
        return $value ?? '';
    }
    /*
     * email attribute value change function
     */

    public function getEmailAttribute($email)
    {
        return strpos($email, config('constant.email_prefix')) !== false ? NULL : $email;
    }
    /*
     * full path of the avatar  of the user
     */

    public function getAvatarPathAttribute()
    {
        return $this->avatar ? env('APP_URL').'uploads/images/'.$this->avatar : './assets/images/user.png';
    }
    /*
     * many o many user relationship
     */

    public function roles()
    {
        return $this->belongsToMany('App\Models\Role')->withPivot('role_id');
    }
    /*
     * many to many permissions relationship
     */

    public function permissions()
    {
        return $this->belongsToMany('App\Models\Permission');
    }
    /*
     * has one user of role customer
     */

    public function customer()
    {
        return $this->hasOne('App\Models\Customer');
    }
    /*
     * has one user of role consultant
     */

    public function consultant()
    {
        return $this->hasOne('App\Models\Consultant');
    }
    /*
     * has one user of role contact_person
     */

    public function contact_person()
    {
        return $this->hasOne(ContactPerson::class);
    }
    /*
     * has one user of role employee
     */

    public function employee()
    {
        return $this->hasOne(Employee::class);
    }
    /*
     * has one relation with title table
     */

    public function title()
    {
        return $this->belongsTo(Option::class, 'title_id', 'id');
    }
    /*
     * has one relation with activities each user perform
     */

    public function acitvity()
    {
        return $this->hasMany(Activity::class);
    }
}