<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Description of Workplace
 *
 * @author manpreet
 */
class Workplace extends Model
{

    use SoftDeletes;
    protected $tables   = ['deleted_at'];
    protected $fillable = ['name', 'zipcode', 'place'];    
    protected $appends =['used_by'];

    /*
     * used_by used to give the count of project in which this workplace is used
     */
    public function getUsedByAttribute()
    {
        $count = Project::where('workplace_id',$this->id)->count();
        return $count;
    }

    /*
     * has one relation with the project
     */
    public function project()
    {
        return $this->hasOne(Project::class);
    }
}