<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Description of MarkerAddedNotification
 *
 * @author manpreetkaur
 */
class MarkerAddedNotification extends Notification
{

    public $template;

    public function __construct($template_body)
    {
        $this->template = $template_body;
    }

    public function via($notifiable)
    {
        return ['mail']; 
    }

    public function toMail($notifiable)
    {
        $message = new MailMessage();
        $message->subject($this->template['subject'])
            ->view('mail.mail_layout', ['content' => $this->template['body']]);
        return $message; 
    }
}