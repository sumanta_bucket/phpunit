<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Description of reset password 
 * @author manpreet
 */
class ResetPass extends Notification
{

//    use Queueable;
    public $user;
    public $template_body;

    public function __construct($user, $template_body)
    {
        $this->user          = $user;
        $this->template_body = $template_body;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        $message = new MailMessage();
        $message->subject($this->template_body['subject'])
            ->view('mail.mail_layout', ['content' => $this->template_body['body']]);
        return $message;
    }
}