<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Description of sendAccountInformation
 * @author manpreet
 */
class SendAccountInformation extends Notification
{
//    use Queueable;
    protected $username;
    protected $language;
    protected $template;

    public function __construct($user_name, $template_body)
    {
        $this->username = $user_name;
        $this->template = $template_body;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        $message = new MailMessage();
        $message->subject($this->template['subject'])
            ->view('mail.mail_layout', ['content' => $this->template['body']]);
        return $message;
    }
}