<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use App\Services\TranslationService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Description of UserNeedConfirmation
 *
 * @author manpreetkaur
 */
class UserNeedsConfirmation extends Notification implements ShouldQueue
{
    use Queueable;
    
    protected $confirmation_code;
    protected $username;

    /*
     * Constructor for this controller
     */

    public function __construct($confirmation_code, $username)
    {
        $this->confirmation_code = $confirmation_code;
        $this->username          = $username;
    }
    /* method that describe notification channel */

    public function via($notifiable)
    {
        return ['mail'];
    }
    /* send mail to the user to confirm the account */

    public function toMail($notifiable)
    {
        $message = new MailMessage();
        $message->greeting('Hello'." ".$this->username.'!')
            ->subject(TranslationService::lang('confirm_account_mail', 2))
            ->line(TranslationService::lang('click_forgot_password_link', 2))
            ->action(TranslationService::lang('confirm_account', 2),
                env('APP_URL').'/v1/auth/confirmAccount/'.$this->confirmation_code)
            ->line(TranslationService::lang('common_email_message_for_ignore', 2));
        return $message;
    }
}