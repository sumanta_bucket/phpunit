<?php

namespace App\Repositories;

use App\Models\ConsultantCompany;
use Illuminate\Support\Facades\DB;

/**
 * Description of ConsultantCompanyRepository
 * @author
 */
class ConsultantCompanyRepository
{

    /**
     * create new consultant companies
     * @param array $data
     * @return type
     */
    public function create(array $data)
    {
        return DB::transaction(function() use($data) {
                $company = ConsultantCompany::create(['name' => $data['name']]);
                if ($company) {
                    return ['status' => 1, 'data' => $company];
                }
                return ['status' => 0];
            });
    }

    /**
     * update the records of specific id
     * @param int $id
     * @param array $data
     * @return type
     */
    public function update(int $id, array $data)
    {
        return DB::transaction(function() use($data, $id) {
                $company = ConsultantCompany::where('id', $id)->update(['name' => $data['name']]);
                if ($company) {
                    return ['status' => 1, 'data' => $company];
                }
                return ['status' => 0];
            });
    }

    /**
     * Delete the record of specific id
     * @param int $id
     * @return boolean11
     */
    public function delete(int $id)
    {
        $delete = ConsultantCompany::where('id', $id)->delete();
        if ($delete) {
            return true;
        }
        return false;
    }
}