<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Consultant;
use Illuminate\Support\Facades\Hash;
use App\Models\ConsultantCompany;
use App\Models\ConsultantTitle;
use App\Notifications\SendAccountInformation;
use App\Services\UserService;

/**
 * Description of ConsultantRepository
 * @author manpreetkaur
 */
class ConsultantRepository
{

    /**
     * GET the consultant titles for consultant creation
     * @return type
     */
    public function getConsultantTitle()
    {
        $data = ConsultantTitle::select('id', 'name')->get();
        return $data;
    }

    /**
     * GET the consultant company for consultant creation
     * @return type
     */
    public function getConsultantCompany()
    {
        $data = ConsultantCompany::select('id', 'name')->get();
        return $data;
    }

    /**
     * Create new Consultant 
     * @param array $data
     * @return type
     */
    public function create(array $data,int $add_by)
    {
        return DB::transaction(function() use ($data,$add_by) {
                //create new user with role consultant
                $user             = new User();
                $user->first_name = $data['user']['first_name'];
                $user->email      = $data['user']['email'];
                $user->mobile     = $data['user']['mobile'] ?? NULL;
                $user->password   = Hash::make(config('constant.default_user_password'));
                $user->notify     = $data['user']['notify'] ?? 0;

                //if user is ceated successfully then create the consutant 
                if ($user->save()) {
                    $user->assignRole('consultant');
                    $permissions = config('auth.consultant_default_permission');
                    $user->givePermissionTo($permissions);
                    if ($user['notify'] == 1) { //if the user notify is true then send email to the user of account creation
                        $details    = [
                            "EMAIL" => $user->email,
                            "PASSWORD" => config('constant.default_user_password')
                        ];
                        $email_body = UserService::getEmailBody('user_registration',
                                $details);   //get the email body from email template active for sedn mail
                        $user->notify(new SendAccountInformation($user->first_name,
                            $email_body));
                    }
                    UserService::addHistory($user->id, 2,$add_by);  //  insert record in the history table
                    //create consultant
                    $consultant                        = new Consultant();
                    $consultant->user_id               = $user->id;
                    $consultant->consultant_company_id = $data['consultant_company']['id'];
                    $consultant->consultant_title_id   = $data['consultant_title']['id']
                            ?? NULL;
                    if ($consultant->save()) {
                        return Consultant::where('id', $consultant->id)->with(['user',
                                'consultantTitle'])->first();
                    }
                }
                return false;
            });
    }

    /**
     * get consultant of the specific id
     * @param int $id
     * @return type
     */

    public function getRecord(int $id)
    {
        $consultant = Consultant::with('user')->where('id', $id)->first();
        return $consultant;
    }

    /**
     * update records of consultant of specific Id
     * @param array $data
     * @param int $id
     */
    public function update(array $data, int $id)
    {
        $consultant = Consultant::where('id', $id)->first();
        if (!empty($consultant)) {
            //get user_id and then get User Record
            $user_id  = $consultant->user->id;
            $userInfo = User::where('id', $user_id)->first();
            if (!empty($userInfo)) {
                $userInfo->first_name = $data['user']['first_name'];
                $userInfo->email      = $data['user']['email'];
                $userInfo->mobile     = $data['user']['mobile'] ?? NULL;
                $userInfo->notify     = $data['user']['notify'] ?? 0;
                //update User record and then update Consultant Record
                if ($userInfo->save()) {
                    $consultant->consultant_company_id = $data['consultant_company']['id'];
                    $consultant->consultant_title_id   = $data['consultant_title']['id']
                            ?? NULL;
                    if ($consultant->save()) {
                        return 'success';
                    }
                }
            }
            return false;
        }
        return 'not_found';
    }

    /**
     * Delete the user with given Id
     * @return boolean|string
     */
    public function delete(int $id)
    {
        $consultant = Consultant::where('id', $id)->has('user')->with('user')->first();
        //return not_found if record with given id not exists
        if (empty($consultant)) {
            return 'not_found';
        }
        //delete the user and then delete consultant
        if ($consultant->user->delete()) {
            $consultantDelete = $consultant->delete();

            if ($consultantDelete) {
                return 'success';
            }
        }
        return false;
    }
}