<?php

namespace App\Repositories;

use App\Models\ConsultantTitle;
use Illuminate\Support\Facades\DB;

/**
 * Description of ConsultantTitleRepository
 * @author manpreetkaur
 */
class ConsultantTitleRepository
{

    /**
     * create new consultant title
     * @param array $data
     * @return type
     */
    public function create(array $data)
    {
        return DB::transaction(function() use($data) {
                $title = ConsultantTitle::create(['name' => $data['name']]);
                if ($title) {
                    return ['status' => 1, 'data' => $title];
                }
                return ['status' => 0];
            });
    }

    /**
     * update consultant title
     * @param int $id
     * @param array $data
     * @return type
     */
    public function update(int $id, array $data)
    {
        return DB::transaction(function() use($data, $id) {
                $consultant = ConsultantTitle::where('id', $id)->update(['name' => $data['name']]);
                if ($consultant) {
                    return ['status' => 1, 'data' => $consultant];
                }
                return ['status' => 0];
            });
    }

    /**
     * Delete the record of specific id
     * @param int $id
     * @return boolean11
     */
    public function delete(int $id)
    {
        $delete = ConsultantTitle::where('id', $id)->delete();
        if ($delete) {
            return true;
        }
        return false;
    }
}