<?php

namespace App\Repositories;

use App\Models\ContactPerson;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Services\FileUploadService;
use Illuminate\Support\Facades\DB;
use App\Models\Option;
use App\Notifications\SendAccountInformation;
use App\Services\UserService;

/**
 * Description of ContactPersonRepository
 * @author
 */
class ContactPersonRepository
{

    /**
     * Get the title for contact person create functionality
     * @return type
     */
    public function getContactPersonTitle()
    {
        $titles = Option::where('option_name', 'contact_person_title')->get();
        return $titles;
    }

    /**
     * Create new contact person
     * @param array $data
     * @return type
     */
    public function create(array $data,int $add_by)
    {
        return DB::transaction(function() use ($data,$add_by) {
                //create new user with role contact-person
                $user             = new User();
                $user->first_name = $data['user']['first_name'];
                $user->email      = $data['user']['email'];
                $user->password   = Hash::make(config('constant.default_user_password'));
                if (array_key_exists('title_id', $data['user'])) {
                    $user->title_id = $data['user']['title_id'];
                }
                if (array_key_exists('mobile', $data['user'])) {
                    $user->mobile = $data['user']['mobile'];
                }
                $user->notify = $data['user']['notify'] ?? 0;
                $user->status = 1;
                if (array_key_exists('notes', $data['user'])) {
                    $user->notes = $data['user']['notes'];
                }
                if (array_key_exists('last_name', $data['user'])) {
                    $user->last_name = $data['user']['last_name'];
                }
                if (array_key_exists('telephone', $data['user'])) {
                    $user->telephone = $data['user']['telephone'];
                }
                if (array_key_exists('avatar', $data['user'])) {
                    $filename = FileUploadService::uploadFile($data['user']['avatar'],
                            'images');
                    if ($filename) {
                        $user->avatar = $filename;
                    }
                }
                $user->language = config('constant.default_user_language');
                //if user is created successfully then create the contact person
                if ($user->save()) {
                    UserService::addHistory($user->id, 2,$add_by);
                    $user->assignRole('contact-person');
                    $permissions = config('auth.contact_person_default_permission');
                    $user->givePermissionTo($permissions);
                    if ($user->notify) {
                        $details    = [
                            "EMAIL" => $user->email,
                            "PASSWORD" => config('constant.default_user_password')
                        ];
                        $email_body = UserService::getEmailBody('user_registration',
                                $details);
                        $user->notify(new SendAccountInformation($user->first_name,
                            $email_body));
                    }
                    $contact_person = $user->contact_person()->create();
                    if (array_key_exists('customer_id', $data)) {
                        $contact_person->customers()->attach($data['customer_id']);
                    }
                    if ($contact_person) {
                        $result = ContactPerson::where('id', $contact_person->id)->with('user.title')->first();
                        return ['status' => true, 'data' => $result];
                    }
                }
                return ['status' => false];
            });
    }

    /**
     * get the contact person of specific id
     * @param int $id
     * @return type
     */
    public function getContactPerson(int $id)
    {
        $persons = ContactPerson::where('id', $id)->has('user')->with('user.title')->first();
        return $persons;
    }

    /**
     * update contact person data
     * @param int $id
     * @param array $data
     * @return type
     */
    public function update(int $id, array $data)
    {
        return DB::transaction(function() use($data, $id) {
                $user             = User::where('id', $data['user']['id'])->first();
                $user->first_name = $data['user']['first_name'];
                $user->email      = $data['user']['email'];
                $user->password   = Hash::make(config('constant.default_user_password'));
                if (array_key_exists('title_id', $data['user'])) {
                    $user->title_id = $data['user']['title_id'];
                }
                if (array_key_exists('mobile', $data['user'])) {
                    $user->mobile = $data['user']['mobile'];
                }
                if (array_key_exists('notify', $data['user'])) {
                    $user->notify = $data['user']['notify'];
                }
                if (array_key_exists('notes', $data['user'])) {
                    $user->notes = $data['user']['notes'];
                }
                if (array_key_exists('last_name', $data['user'])) {
                    $user->last_name = $data['user']['last_name'];
                }
                if (array_key_exists('telephone', $data['user'])) {
                    $user->telephone = $data['user']['telephone'];
                }
                if (array_key_exists('avatar', $data['user'])) {
                    $filename = FileUploadService::uploadFile($data['user']['avatar'],
                            'images');
                    if ($filename) {
                        $user->avatar = $filename;
                    }
                }
                if ($user->save()) {
                    return true;
                }
                return false;
            });
    }

    public function delete(int $id)
    {
        $contact_person = ContactPerson::where('id', $id)->has('user')->with('user')->first();
        //return not_found if record with given id not exists
        if (empty($contact_person)) {
            return 'not_found';
        }
        //delete the user and then delete contact_person
        if ($contact_person->user->delete()) {
            $contact_person_delete = $contact_person->delete();

            if ($contact_person_delete) {
                return 'success';
            }
        }
        return false;
    }

    public function changeStatus(int $user_id, int $status)
    {
        $user = User::where('id', $user_id)->update(['status' => $status]);
        if ($user) {
            return true;
        }
        return false;
    }

    public function restore(int $id)
    {
        $contact = ContactPerson::with(['user' => function($q) {
                    $q->onlyTrashed();
                }])->onlyTrashed()->find($id);
        if (!empty($contact)) {
            $user_id        = $contact->user->id;
            $userRestore    = User::withTrashed()->find($user_id)->restore();
            $contactRestore = $contact->restore();
            if ($userRestore && $contactRestore) return true;
        }
        return false;
    }

    public function trashedRecords($searchValue = null, $column = 'id',
                                   $direction = 'dsc', $limit = 10, $offset = 0)
    {
        if ($searchValue != null) {
//            \DB::enableQueryLog();
            $records = ContactPerson::whereHas('user',
                    function($query) use($searchValue) {
                    $query->where(function($query) use($searchValue) {
                        $query->where('first_name', 'like', '%'.$searchValue.'%')->orWhere('last_name',
                                'like', '%'.$searchValue.'%')->orWhere('address',
                                'like', '%'.$searchValue.'%')
                            ->orWhere('city', 'like', '%'.$searchValue.'%')->orWhere('mobile',
                            'like', '%'.$searchValue.'%')->orWhere('telephone',
                            'like', '%'.$searchValue.'%')->orWhere('email',
                            'like', '%'.$searchValue.'%');
                    })->onlyTrashed();
                })->orWhereHas('user.title',
                    function($query1) use($searchValue) {
                    $query1->where('option_value', 'like', '%'.$searchValue.'%');
                })->onlyTrashed()->with(['user' => function($q) {
                        $q->withTrashed();
                    }, 'user.title' => function($q1) {
                        $q1->withTrashed();
                    }, 'customers.user' => function($q2) {
                        $q2->withTrashed();
                    }])->get();
        } else {
            $records     = ContactPerson::with(['user' => function($q) {
                        $q->withTrashed();
                    }, 'user.title' => function($q1) {
                        $q1->withTrashed();
                    }, 'customers.user' => function($q2) {
                        $q2->withTrashed();
                    }])->onlyTrashed()->get(); 
        }

        $total = $records->count(); 
        if ($direction == 'asc') {
            $records = $records->sortBy($column)->values()->splice($offset)->take($limit);
        } else {
            $records = $records->sortByDesc($column)->values()->splice($offset)->take($limit);
        }
        $data  = ['total' => $total,
            'records' => $records];
        return $data;
    }
}