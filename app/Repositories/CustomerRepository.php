<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Models\Customer;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Notifications\SendAccountInformation;
use App\Models\Option;
use App\Services\UserService;

/**
 * Description of CustomerRepository
 * CRUD operations descriptions
 * @author manpreetkaur
 */
class CustomerRepository
{

    /**
     * Get all records in the Model
     * @return type collection
     */
    public function getRecords()
    {
        $response = Customer::has('user')->with('user')->get();
        return $response;
    }

    /**
     * get the contact person title
     * @return type
     */
    public function contactPersonTitles()
    {
        $titles = Option::where('option_name', 'contact_person_title')->get();
        return $titles;
    }

    /**
     * Create new record 
     * @param array $data
     * @return type
     */
    public function create(array $data, int $added_by)
    {
        return DB::transaction(function () use ($data, $added_by) {
                //create new customer record
                $user                    = new User();
                $user->first_name        = $data['user']['first_name'];
                $user->email             = $data['user']['email'];
                $user->address           = $data['user']['address'] ?? NULL;
                $user->city              = $data['user']['city'] ?? NULL;
                $user->mobile            = $data['user']['mobile'] ?? NULL;
                $user->zip               = $data['user']['zip'] ?? NULL;
                $user->password          = Hash::make(config('constant.default_user_password'));
                $user->confirmation_code = md5(uniqid(mt_rand(), true));
                $user->notes             = $data['user']['notes'] ?? NULL;
                $user->notify            = $data['user']['notify'] ?? 0;
                $user->status            = 1;
                //if user is created then create the customer from user id and assign roles and permissions
                if ($user->save()) {
                    $user->assignRole('customer'); //assign the role to user
                    UserService::addHistory($user->id, 2, $added_by);  // add history of the newly added user to activity controller
                    $permissions = config('auth.customer_default_permission'); //assign permissions to user
                    $user->givePermissionTo($permissions);
                    //if user notify is 1 then send email to the newly created user
                    if ($user['notify'] == 1) {
                        $details    = [
                            "EMAIL" => $user->email,
                            "PASSWORD" => config('constant.default_user_password')
                        ];
                        //get email body fro commmon service
                        $email_body = UserService::getEmailBody('user_registration',
                                $details);
                        //notify user about account creation
                        $user->notify(new SendAccountInformation($user->first_name,
                            $email_body));
                    }
                    //create new customer
                    $customer           = new Customer();
                    $customer->user_id  = $user->id;
                    $customer->country  = $data['country'] ?? NULL;
                    $customer->address2 = $data['address2'] ?? NULL;
                    if (array_key_exists('invoice_email', $data)) {
                        $customer->invoice_email = 1;
                    } else {
                        $customer->invoice_email = 0;
                    }
                    $customer->other_invoice_address  = $data['other_invoice_address']
                            ?? NULL;
                    $customer->other_invoice_city     = $data['other_invoice_city']
                            ?? NULL;
                    $customer->other_shipping_address = $data['other_shipping_address']
                            ?? NULL;
                    $customer->other_shipping_city    = $data['other_shipping_city']
                            ?? NULL;
                    if ($customer->save()) {
                        // add category of cutomer
                        if (array_key_exists('categories', $data)) {
                            $categoryIds = array_column($data['categories'],
                                'id');
                            $customer->categories()->sync($categoryIds); // sync user categories
                            if (array_key_exists('contact_persons', $data) && !empty($data['contact_persons'])) {
                                $contactPersonIds = array_column($data['contact_persons'],
                                    'id');
                                $customer->contact_persons()->sync($contactPersonIds); // sync conatct-persons related to the customer
                            }
                        }
                        return true;
                    }
                }
                return false;
            });
    }

    /**
     * Get single record from model of specific Id
     * @param id integer required the customer id whose records we want to fetch
     * @response json array type
     */
    public function getCustomer(int $id)
    {
        $customer = Customer::has('user')->with(['user.title', 'categories', 'contact_persons.user.title'])->where('id',
                $id)->first();
        return $customer;
    }

    /**
     * Update record in the model of given Id
     * @param array $data
     * @param int $id
     * @return boolean
     */
    public function update(array $data, int $id)
    {
        //get the customer record of specific Id
        $customer = Customer::where('id', $id)->has('user')->with('user')->first();

        if (empty($customer)) {
            return 'not_found';
        }
        $user_id = $customer->user->id;
        $user    = User::where('id', $user_id)->first();
        if (empty($user)) {
            return 'not_found';
        }
        $user->first_name = $data['user']['first_name'];
        $user->email      = $data['user']['email'] ?? NULL;
        $user->address    = $data['user']['address'] ?? NULL;
        $user->city       = $data['user']['city'] ?? NULL;
        $user->mobile     = $data['user']['mobile'] ?? NULL;
        $user->zip        = $data['user']['zip'] ?? NULL;
        $user->notes      = $data['user']['notes'] ?? NULL;
        $user->notify     = $data['user']['notify'] ?? NULL;
        if ($user->save()) {
            $customer->country                = $data['country'] ?? NULL;
            $customer->address2               = $data['address2'] ?? NULL;
            $customer->other_invoice_address  = $data['other_invoice_address'] ?? NULL;
            $customer->other_invoice_city     = $data['other_invoice_city'] ?? NULL;
            $customer->other_shipping_address = $data['other_shipping_address'] ?? NULL;
            $customer->other_shipping_city    = $data['other_shipping_city'] ?? NULL;
            if ($customer->save()) {
                if (array_key_exists('categories', $data)) {
                    $categoryIds = array_column($data['categories'], 'id');
                    $customer->categories()->sync($categoryIds); //sync customer categories
                    if (array_key_exists('contact_persons', $data) && !empty($data['contact_persons'])) {
                        $contactPersonIds = array_column($data['contact_persons'],
                            'id');
                        $customer->contact_persons()->sync($contactPersonIds); //sync customer contact persons
                    }
                }
                return 'success';
            }
        }
        return false;
    }

    /**
     * Soft Delete the records
     * @param int $id
     * @return boolean
     */
    public function delete(int $id)
    {
        $customer = Customer::where('id', $id)->has('user')->with('user')->first();
        if (empty($customer)) {
            return 'not_found';
        }
        if ($customer->user->delete()) {
//            $customer->categories()->detach();
//            $customer->contact_persons()->detach();
            $customerDelete = $customer->delete();

            if ($customerDelete) {
                return 'success';
            }
        }
        return false;
    }

    /*
     * detach the contact persons related to customer
     */
    public function detachRecords(int $customer_id, int $contact_person_id)
    {
        $customer = Customer::find($customer_id);
        $detach   = $customer->contact_persons()->detach($contact_person_id); //detach() function delete the relationship
        if ($detach) {
            return true;
        }
        return false;
    }

    /**
     * Get the trashed customer records
     * @param type $searchValue
     * @param type $column
     * @param type $direction
     * @param type $limit
     * @param type $offset
     * @return type
     */

    public function trashedRecords($searchValue = null, $column = 'id',
                                   $direction = 'dsc', $limit = 10, $offset = 0)
    {
        if ($searchValue != null) {
            $records = Customer::whereHas('user',
                    function($query) use($searchValue) {
                    $query->where(function($query) use($searchValue) {
                        $query->where('first_name', 'like', '%'.$searchValue.'%')->orWhere('last_name',
                                'like', '%'.$searchValue.'%')->orWhere('address',
                                'like', '%'.$searchValue.'%')
                            ->orWhere('city', 'like', '%'.$searchValue.'%')->orWhere('mobile',
                            'like', '%'.$searchValue.'%')->orWhere('telephone',
                            'like', '%'.$searchValue.'%')->orWhere('email',
                            'like', '%'.$searchValue.'%');
                    })->onlyTrashed();
                })->orWhereHas('user.title',
                    function($query1) use($searchValue) {
                    $query1->where('option_value', 'like', '%'.$searchValue.'%');
                })->onlyTrashed()->with(['user' => function($q) {
                        $q->withTrashed();
                    }, 'user.title' => function($q1) {
                        $q1->withTrashed();
                    }, 'contact_persons.user' => function($q2) {
                        $q2->withTrashed();
                    }, 'contact_persons.user.title' => function($q2) {
                        $q2->withTrashed();
                    }])->get();
        } else {
            $records     = Customer::with(['user' => function($q) {
                        $q->withTrashed();
                    }, 'user.title' => function($q1) {
                        $q1->withTrashed();
                    }, 'contact_persons.user' => function($q2) {
                        $q2->withTrashed();
                    }, 'contact_persons.user.title' => function($q2) {
                        $q2->withTrashed();
                    }])->onlyTrashed()->get();
        }

        $total = $records->count();
        if ($direction == 'asc') {
            $records = $records->sortBy($column)->values()->splice($offset)->take($limit);
        } else {
            $records = $records->sortByDesc($column)->values()->splice($offset)->take($limit);
        }
        $data  = ['total' => $total,
            'records' => $records];
        return $data; 
    }


    /**
     * restore the archived(trashed) records
     * @bodyParam id integer required the id of the customer being restored
     * @return boolean
     */

    public function restore(int $id)
    {
        $customer = Customer::with(['user' => function($q) {
                    $q->onlyTrashed();
                }])->onlyTrashed()->find($id);
        if (!empty($customer)) {
            $user_id         = $customer->user->id;
            $userRestore     = User::withTrashed()->find($user_id)->restore();
            $customerRestore = $customer->restore();
            if ($userRestore && $customerRestore) return true;
        }
        return false;
    }
}