<?php

namespace App\Repositories;

use App\Models\Employee;
use App\Models\Customer;
use App\Models\Project;
use App\Models\Activity;
use Illuminate\Support\Carbon;

/**
 * Description of DashboardRepository
 * @author manpreetkaur
 */
class DashboardRepository
{

    /**
     * function which get the records required for user dashboard
     * @return type
     */
    public function records($activity)
    {
        $record                       = []; //declaration of the aray
        $record['customers']          = Customer::withTrashed()->count();  // get all the customers
        $record['employee']           = Employee::count();  //get all the employees
        $record['ongoing_projects']   = Project::count(); //get all the ongoing projects
        $record['completed_projects'] = Project::onlyTrashed()->count();    //get the projects which are complted
        $date                         = Carbon::today()->subDays(7);
        if ($activity != null) {
            $activities = Activity::has('user')->with(['user.roles'])->orderBy('created_at',
                    'DESC')->where('created_at', '>=', $date)->get(); // get all the acitivities performed by any of the user of a week
            $filtered   = $activities->filter(function ($value, $key) {
                return $value->project != NULL;
            });

            $record['activities'] = $filtered->values();
        }
        return $record;  // return the resulted array
    }
}