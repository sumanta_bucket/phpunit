<?php

namespace App\Repositories;

use App\Models\Document;
use App\Services\FileUploadService;

/**
 * Description of DocumentRepository
 *
 * @author pc8
 */
class DocumentRepository
{

    public function uploadDocuments(array $data)
    {
        foreach ($data['document'] as $document) {
            $name               = FileUploadService::uploadFile($document,
                    'documents');
            $create             = new Document();
            $create->project_id = $data['project_id'];
            $create->document   = $name;
            $create->save();
        }
        $data = Document::where('project_id', $data['project_id'])->orderBy('created_at','DESC')->get();
        return ['status' => true, 'data' => $data];
    }

    public function deleteDocument(int $id)
    {
        $document = Document::where('id', $id)->first();
        if (empty($document)) {
            return false;
        }
        $file = base_path('public/uploads/documents/').$document->document;
        if (file_exists($file)) {
            unlink($file);
        }
        if ($document->delete()) {
            return true;
        }
        return false;
    }
}