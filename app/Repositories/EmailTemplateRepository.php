<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Models\EmailTemplate;
use App\Services\TranslationService;
use App\Models\EmailTemplateType;
use Twilio\Rest\Client as TwilioClient;
use Twilio\Exceptions\TwilioException;
use Illuminate\Support\Facades\Mail;

/**
 * Description of EmailTemplateRepository
 *
 * @author manpreetkaur
 */
class EmailTemplateRepository
{

    /**
     * Get all email template records
     * @return type collection
     */
    public function getAll()
    {
        $template = EmailTemplate::has('emailTemplateType')->with('emailTemplateType')->get();
        return $template;
    }

    /**
     * create new email template records
     */
    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
                $template = EmailTemplate::create([
                        'email_template_type_id' => $data['email_template_type_id'],
                        'name' => $data['name'],
                        'template' => $data['template'],
                        'subject' => $data['subject'],
                        'is_default' => 0,
                        'deletes' => 1
                ]);
                if ($template) {
                    return true;
                }
                return false;
            });
    }

    /**
     * update email template of specific id
     * @return boolean
     */
    public function update(array $data, int $id)
    {
        $template = EmailTemplate::where(['id' => $id, 'deletes' => 1])->update([
            'email_template_type_id' => $data['email_template_type_id'],
            'name' => $data['name'],
            'template' => $data['template'],
            'subject' => $data['subject']
        ]);
        if ($template) {
            return true;
        }
        return false;
    }

    /**
     * delete email template record of specific id
     */
    public function delete(int $id)
    {
        $template  = EmailTemplate::where('id', $id)->first();
        $count     = EmailTemplate::where(['deletes' => 1, 'email_template_type_id' => $template->email_template_type_id])->where('id',
                '!=', $id)->count();
        $isDefault = $template->is_default;
        if ($count === 0 || $isDefault) {
            EmailTemplate::where(['deletes' => 0, 'email_template_type_id' => $template->email_template_type_id])->update([
                'is_default' => 1]);
        }
        if ($template->delete()) {
            return true;
        }
        return false;
    }

    /**
     * get records of specific template
     */
    public function getSpecificEmailTemplate(int $id)
    {
        $data = EmailTemplate::has('emailTemplateType')->with('emailTemplateType')->where('id',
                $id)->first();
        return $data;
    }

    /**
     * Get Email template type of specific type like email or sms 
     * @param int $type
     * @return type collection
     */
    public function getEmailTypeTemplate(int $type)
    {
        $records = EmailTemplate::whereHas('emailTemplateType',
                function($query)use($type) {
                $query->where('type', $type);
            })->with('emailTemplateType')->get();
        return $records;
    }

    /**
     * Get placeholder of email type
     * @param int $id
     * @return type
     */
    public function getEmailTypePlaceholders(int $id)
    {
        $records = EmailTemplateType::select('id', 'placeholder')->where('id',
                $id)->first();
        return $records;
    }

    /**
     * Test sms template function body
     * @param string $phone_number
     * @param string $template_body
     * @return type
     */
    public function sendSms(string $phone_number, string $template_body)
    {
        $twilioAccountSid = env('TWILIO_SID');
        $twilioAuthToken  = env('TWILIO_TOKEN');
        $fromAddress      = env('TWILIO_NUMBER');

        $twilioClient = new TwilioClient($twilioAccountSid, $twilioAuthToken);
        try {
            $response = $twilioClient->messages->create(
                // the number you'd like to send the message to
                $phone_number,
                array(
                // A Twilio phone number you purchased at twilio.com/console
                'from' => $fromAddress,
                // the body of the text message you'd like to send
                'body' => $template_body
            ));
            return $response->status;
        } catch (TwilioException $ex) {
            return $ex->getMessage();
        }
    }

    /**
     * Test email template function body
     * @param string $email
     * @param string $template_body
     * @return boolean
     */
    public function sendEmail(array $data)
    {
        $data['body']    = $data['template_body'];
        $data['subject'] = $data['subject'];
        $data['email']   = $data['email'];
        //send the mail to the user usinf Mail facade
        try {
            Mail::send('mail.mail_layout', ['content' => $data['template_body']],
                function ($message) use ($data) {
                $message->to($data['email'])
                    ->subject($data['subject'])
                    ->setBody($data['body']); // for HTML rich messages
            });
            return true;
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
    } 

    /**
     * change the default template to send email
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function changeDefaultStatus(int $id, array $data)
    {
        $template = EmailTemplate::where('id', $id)->first();

        if ($data['is_default']) {
            EmailTemplate::where('id', '!=', $id)->where('email_template_type_id',
                $template->email_template_type_id)->update(['is_default' => 0]);

            $template->is_default = 1;
        } else {
            EmailTemplate::where('id', '!=', $id)->where(['email_template_type_id' => $template->email_template_type_id,
                'deletes' => 0])->update([
                'is_default' => 1]);
            $template->is_default = 0;
        }

        if ($template->save()) {
            return true;
        }
        return false;
    }
}