<?php

namespace App\Repositories;

use App\Models\Employee;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Notifications\SendAccountInformation;
use App\Models\EmployeeRelative;
use App\Models\Relationship;
use App\Services\UserService;

/**
 * Description of EmployeeRepository
 *
 * @author manpreetkaur
 */
class EmployeeRepository
{

    //create new employee function
    public function create(array $data, int $added_by)
    {
        return DB::transaction(function() use($data, $added_by) {
                $user             = new User();
                $user->first_name = $data['first_name'];
                $user->email      = $data['email'];
                $user->title_id   = $data['title_id'] ?? NULL;
                $user->last_name  = $data['last_name'] ?? NULL;
                $user->address    = $data['address'] ?? NULL;
                $user->city       = $data['city'] ?? NULL;
                $user->zip        = $data['zip'] ?? NULL;
                $user->mobile     = $data['mobile'] ?? NULL;
                $user->notify     = $data['notify'] ?? 0;
                $user->password   = Hash::make(config('constant.default_user_password'));
                if ($user->save()) {
                    UserService::addHistory($user->id, 2, $added_by);
                    $user->assignRole('employee');
                    $permissions = config('auth.employee_default_permissions');
                    $user->givePermissionTo($permissions);
                    if ($user->notify) {
                        $details    = [
                            "EMAIL" => $user->email,
                            "PASSWORD" => config('constant.default_user_password')
                        ];
                        $email_body = UserService::getEmailBody('user_registration',
                                $details);
                        $user->notify(new SendAccountInformation($user->first_name,
                            $email_body));
                    }
                    $employee = $user->employee()->create();
                    if (array_key_exists('relatives', $data)) {
                        $employee->relatives()->sync($data['relatives']);
                    }
                    return true;
                }
                return false;
            });
    }

    public function edit(int $id)
    {
        $data = Employee::has('user')->with(['user.title', 'relatives'])->where('id',
                $id)->first();
        $data->relatives->map(function($item) {
            $item->relationship = Relationship::find($item->pivot->relationship_id)
                    ? Relationship::find($item->pivot->relationship_id)->name : "";
            return $item;
        });
        return $data;
    }

    //update the employee
    public function update(int $id, array $data)
    {
        return DB::transaction(function() use($id, $data) {
                $employee                  = Employee::where('id', $id)->first();
                $user_id                   = $employee->user_id;
                $updated_data              = [
                    'first_name' => $data['first_name'],
                    'email' => $data['email'],
                ];
                $updated_data['title_id']  = $data['title_id'] ?? NULL;
                $updated_data['last_name'] = $data['last_name'] ?? NULL;
                $updated_data['address']   = $data['address'] ?? NULL;
                $updated_data['city']      = $data['city'] ?? NULL;
                $updated_data['mobile']    = $data['mobile'] ?? NULL;
                $updated_data['zip']       = $data['zip'] ?? NULL;
                $user                      = User::where('id', $user_id)->update($updated_data);
                if ($user) {
                    if (array_key_exists('relatives', $data)) {
                        $employee->relatives()->sync($data['relatives']);
                    }
                    return true;
                }
                return false;
            });
    }

    public function delete(int $id)
    {
        $employee = Employee::where('id', $id)->has('user')->with('user')->first();
        if ($employee->user->delete()) {
            $employee->relatives()->detach();
            if ($employee->delete()) {
                return true;
            }
        }
        return false;
    }

    public function restore(int $id)
    {
        $employee = Employee::with(['user' => function($q) {
                    $q->onlyTrashed();
                }])->onlyTrashed()->find($id);
        if (!empty($employee)) {
            $user_id         = $employee->user->id;
            $userRestore     = User::withTrashed()->find($user_id)->restore();
            $employeeRestore = $employee->restore();
            if ($userRestore && $employeeRestore) return true;
        }
        return false;
    }

    public function trashedRecords($searchValue = null, $column = 'id',
                                   $direction = 'dsc', $limit = 10, $offset = 0)
    {
        if ($searchValue != null) {
            $records = Employee::whereHas('user',
                    function($query) use($searchValue) {
                    $query->where(function($query) use($searchValue) {
                        $query->where('first_name', 'like', '%'.$searchValue.'%')->orWhere('last_name',
                                'like', '%'.$searchValue.'%')->orWhere('address',
                                'like', '%'.$searchValue.'%')
                            ->orWhere('city', 'like', '%'.$searchValue.'%')->orWhere('mobile',
                            'like', '%'.$searchValue.'%')->orWhere('telephone',
                            'like', '%'.$searchValue.'%')->orWhere('email',
                            'like', '%'.$searchValue.'%');
                    })->onlyTrashed();
                })->orWhereHas('user.title',
                    function($query1) use($searchValue) {
                    $query1->where('option_value', 'like', '%'.$searchValue.'%');
                })->onlyTrashed()->with(['user' => function($q) {
                        $q->onlyTrashed();
                    }, 'user.title' => function($q1) {
                        $q1->withTrashed();
                    }])->get();
        } else {
            $records     = Employee::with(['user' => function($q) {
                        $q->onlyTrashed();
                    }, 'user.title' => function($q1) {
                        $q1->withTrashed();
                    }])->onlyTrashed()->get();
        }

        $total = $records->count();
        if ($direction == 'asc') {
            $records = $records->sortBy($column)->values()->splice($offset)->take($limit);
        } else {
            $records = $records->sortByDesc($column)->values()->splice($offset)->take($limit);
        }
        $data  = ['total' => $total,
            'records' => $records];
        return $data;
    }
}