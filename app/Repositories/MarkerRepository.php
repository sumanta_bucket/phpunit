<?php

namespace App\Repositories;

use App\Models\FireClass;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Services\FileUploadService;
use App\Services\UserService;
use Illuminate\Support\Facades\Notification;
use App\Notifications\MarkerAddedNotification;
use App\Models\{
    FiresealingType,
    InsulationMaterial,
    InsulationProfile,
    FireboardSetting,
    PenetrationType,
    InsulationMarker,
    FiresealingMarker,
    InsulationMarkerPhoto,
    FiresealingMarkerPhoto,
    User,
    MaterialType,
    Section,
    Project,
    Employee
};

/**
 * Description of MarkerRepository
 * @author manpreetkaur
 */
class MarkerRepository
{

    /**
     * Get the data required for marker creation
     * @param int $type
     * @return type
     */
    public function getRequiredInformation(int $type)
    {
        $records                         = [];
        $records['fireclass']            = FireClass::where('class_type', $type)->get();
        $records['insulation_materials'] = InsulationMaterial::with('material_type')->get();
        $records['firesealing_types']    = FiresealingType::get();
        $records['insulation_profiles']  = InsulationProfile::get();
        $records['fireboard_settings']   = FireboardSetting::get();
        $records['penetration_types']    = PenetrationType::get();
        $records['material_types']       = MaterialType::get();
        $records['users']                = User::select('id', 'first_name',
                'last_name')->get();
        $records['employees']            = Employee::with('user.title')->get();
        return $records;
    }

    /**
     * create new marker
     * @param array $data
     * @param int $user_id
     * @param int $type
     * @return type
     */
    public function createMarker(array $data, int $type, $user)
    {
        return DB::transaction(function() use($data, $user, $type) {
                $insertData  = [
                    'section_id' => $data['section_id'],
                    'hole_type' => $data['hole_type'],
                    'status' => $data['status'],
                    'predefined' => $data['predefined'] ?? 0,
                    'notes' => $data['notes'] ?? NULL,
                    'updated_by' => $user['id'],
                    'added_by' => $data['added_by'],
                    'added_by_name' => $user['first_name'],
                    'x_axis' => $data['x_axis'],
                    'y_axis' => $data['y_axis'],
                    'status_type' => $data['status_type'],
                    'marker_date' => Carbon::parse($data['marker_date'])->format('Y-m-d')
                ];
                $photosArray = [];
                //if photos uploaded by the user then upload file to server and add path to the respective table
                if (array_key_exists('photo', $data)) {
                    foreach ($data['photo'] as $file) {
                        $filename      = FileUploadService::uploadFile($file,
                                'images');
                        if ($filename) $photosArray[] = ['photo' => $filename];
                    }
                }
                $section    = Section::where('id', $data['section_id'])->with('floor')->first();
                $project_id = $section->floor->project_id;
                //if type of marker is 2 then create the Firesealing  marker
                if ($type == 2) {
                    $insertData['penetration_type_id'] = $data['penetration_type_id'];
                    $insertData['firesealing_type_id'] = $data['firesealing_type_id'];
                    $insertData['fire_class_id']       = $data['fire_class_id'];
                    $insertData['dimension1']          = $data['dimension1'];
                    $insertData['quantity']            = $data['quantity'];
                    $insertData['category_id']         = $data['category_id'] ?? NULL;
                    if (array_key_exists('dimension2', $data)) {
                        $insertData['dimension2'] = $data['dimension2'];
                    }
                    $marker = FiresealingMarker::create($insertData);
                    //if marker is created
                    if ($marker) {
                        if (!empty($photosArray)) {
                            $markerObject = FiresealingMarker::find($marker->id);
                            $markerObject->photos()->createMany($photosArray);
                        }
                        //add the data to the ativity table about marker cration
                        UserService::addHistory($user['id'], 3, $project_id,
                            $marker->id, 2);
                        $result = FiresealingMarker::with(['fire_class',
                                'firesealing_type',
                                'section', 'photos', 'penetration_type', 'added_by','category'])->where('id',
                                $marker->id)->first();
                        if ($user->hasRole('customer')) {
                            //get the project details to send as email
                            $userNew       = User::where('id', $data['added_by'])->first();
                            $projectDetail = Project::where('id', $project_id)->select('name')->first();
                            $details       = [
                                'PROJECT_NAME' => $projectDetail->name,
                                'SECTION_NAME' => $data['section_id'],
                                'X_AXIS' => $data['x_axis'],
                                'Y_AXIS' => $data['y_axis'],
                                'ADDED_BY' => $userNew->first_name
                            ];
                            //when the marker is added by the customer then send email to all admin's
                            $this->_notifyAdminAboutMakerAdded($details);
                        }
                        return ['status' => true, 'data' => $result];
                    }
                } else { //create the insulation marker in case of type =1
                    $insertData['insulation_profile_id']  = $data['insulation_profile_id'];
                    $insertData['insulation_material_id'] = $data['insulation_material_id'];
                    $insertData['fire_class_id']          = $data['fire_class_id'];
                    $insertData['dimensions']             = $data['dimensions'];
                    $insertData['thickness']              = $data['thickness'] ?? NULL;
                    $insertData['prescribed']             = $data['prescribed'] ?? NULL;
                    $insertData['measured']               = $data['measured'] ?? NULL;
                    //create the insulation marker 
                    $marker                               = InsulationMarker::create($insertData);
                    if (!empty($photosArray)) {
                        $markerObject = InsulationMarker::find($marker->id);
                        $markerObject->photos()->createMany($photosArray);
                    }
                    //add history to the activities table about project creation
                    UserService::addHistory($user['id'], 3, $project_id,
                        $marker->id, 1);
                    $result = InsulationMarker::with(['insulation_material.material_type',
                            'fire_class',
                            'insulation_profile', 'section', 'photos', 'added_by'])->where('id',
                            $marker->id)->first();
                    //if user has role customer then send email to user
                    if ($user->hasRole('customer')) {
                        $userNew       = User::where('id', $data['added_by'])->first();
                        $projectDetail = Project::where('id', $project_id)->select('name')->first();
                        $details       = [
                            'PROJECT_NAME' => $projectDetail->name,
                            'SECTION_NAME' => $data['section_id'],
                            'X_AXIS' => $data['x_axis'],
                            'Y_AXIS' => $data['y_axis'],
                            'ADDED_BY' => $userNew->first_name
                        ];
                        //notify the admins about marker add when added bu the customer
                        $this->_notifyAdminAboutMakerAdded($details);
                    }
                    return ['status' => true, 'data' => $result];
                }
                return ['status' => false];
            });
    }

    /**
     * update marker of specific id
     * @param int $id
     * @param int $user_id
     * @param int $type
     * @param array $data
     * @return type
     */
    public function updateMarker(int $id, int $user_id, int $type, array $data)
    {
        return DB::transaction(function() use($data, $user_id, $id, $type) {

                $updatedData = [
                    'section_id' => $data['section_id'],
                    'hole_type' => $data['hole_type'],
                    'status' => $data['status'],
                    'predefined' => $data['predefined'] ?? 0,
                    'notes' => $data['notes'] ?? NULL,
                    'updated_by' => $user_id,
                    'x_axis' => $data['x_axis'],
                    'y_axis' => $data['y_axis'],
                    'status_type' => $data['status_type'],
                    'marker_date' => Carbon::parse($data['marker_date'])->format('Y-m-d')
                ];
                $photosArray = [];
                if (array_key_exists('photo', $data)) {
                    foreach ($data['photo'] as $file) {
                        $filename      = FileUploadService::uploadFile($file,
                                'images');
                        if ($filename) $photosArray[] = ['photo' => $filename];
                    }
                }
                $section    = Section::where('id', $data['section_id'])->with('floor')->first();
                $project_id = $section->floor->project_id;
                if ($type == 2) {
                    $updatedData['penetration_type_id'] = $data['penetration_type_id'];
                    $updatedData['firesealing_type_id'] = $data['firesealing_type_id'];
                    $updatedData['fire_class_id']       = $data['fire_class_id'];
                    $updatedData['dimension1']          = $data['dimension1'];
                    $updatedData['quantity']            = $data['quantity'];
                    $updatedData['dimension2']          = $data['dimension2'] ?? NULL;
                    $updatedData['category_id']         = $data['category_id'] ?? NULL;
                    //update the firesealing marker
                    $marker                             = FiresealingMarker::where('id',
                            $id)->update($updatedData);
                    if ($marker) {
                        if (!empty($photosArray)) {
                            $markerObject = FiresealingMarker::find($id);
                            $markerObject->photos()->createMany($photosArray);
                        }
                        //send email to the admin when cutomer update the marker
                        UserService::addHistory($user_id, 4, $project_id, $id, 2);
                        $result = FiresealingMarker::with(['fire_class', 'firesealing_type',
                                'section', 'photos', 'penetration_type', 'added_by','category'])->where('id',
                                $id)->first();
                        return ['status' => true, 'data' => $result];
                    }
                } else {
                    $updatedData['insulation_profile_id']  = $data['insulation_profile_id'];
                    $updatedData['insulation_material_id'] = $data['insulation_material_id'];
                    $updatedData['fire_class_id']          = $data['fire_class_id'];
                    $updatedData['dimensions']             = $data['dimensions'];
                    $updatedData['thickness']              = $data['thickness'] ?? NULL;
                    $updatedData['prescribed']             = $data['prescribed']
                            ?? NULL;
                    $updatedData['measured']               = $data['measured'] ?? NULL;
                    $marker                                = InsulationMarker::where('id',
                            $id)->update($updatedData);  //update the insulation marker
                    if ($marker) {
                        if (!empty($photosArray)) {
                            $markerObject = InsulationMarker::find($id);
                            $markerObject->photos()->createMany($photosArray);
                        }
                        //send email to the admin when cutomer update the marker
                        UserService::addHistory($user_id, 4, $project_id, $id, 1);
                        $result = InsulationMarker::with(['insulation_material.material_type',
                                'fire_class',
                                'insulation_profile', 'section', 'photos', 'added_by'])->where('id',
                                $id)->first();
                        return ['status' => true, 'data' => $result];
                    }
                }
                return ['status' => false];
            });
    }

    /**
     * delete marker with particular id
     * @param int $id
     * @return boolean
     */
    public function delete(int $id, int $type)
    {
        //if type=1 then delete the insulation marker 
        if ($type == 1) {
            $marker = InsulationMarker::where('id', $id)->with('photos')->first();
            if (!empty($marker)) {
                if (!empty($marker->photos)) {
                    //unlink the marker photos
                    $this->_commonDeleteCode($marker->photos);
                }
                //delete marker photos records
                $marker->photos()->delete();
                $delete = $marker->delete();  //delete the marker
                if ($delete) return true;
            }
        }else {
            //if type =2 then delete the firesealing marekr
            $marker = FiresealingMarker::where('id', $id)->with('photos')->first();
            if (!empty($marker)) {
                //delete firesealing marker photos
                if (!empty($marker->photos)) {
                    //unlink marker photos
                    $this->_commonDeleteCode($marker->photos);
                }
                //delte the marker photos records
                $marker->photos()->delete();
                //delete the marker
                $delete = $marker->delete();
                if ($delete) return true;
            }
        }
        return false;
    }

    /**
     * unlink the file from storage local function
     * @param type $data
     * @return boolean
     */
    private function _commonDeleteCode($data)
    {
        foreach ($data as $photos) {
            $file = base_path('public/uploads/images/').$photos->photo;
            if (file_exists($file)) {
                unlink($file);  //unlink if the file exists
            }
        }
        return true;
    }

    /**
     * only delete the marker photo 
     * @param int $id
     * @param int $type
     * @return boolean
     */
    public function deleteMarkerPhoto(int $id, int $type)
    {
        if ($type == 1) { //if the type =1 insulation marker
            $markerPhoto = InsulationMarkerPhoto::where('id', $id)->first();
        } else {
            $markerPhoto = FiresealingMarkerPhoto::where('id', $id)->first();
        }
        if (!empty($markerPhoto)) {
            $file = base_path('public/uploads/images/').$markerPhoto->photo;
            if (file_exists($file)) {
                unlink($file);
            }
            $delete = $markerPhoto->delete();
            if ($delete) {
                return true;
            }
        }
        return false;
    }

    /**
     * send mail to admin when marker added by the customer
     * @param array $details
     * @return type
     */
    private function _notifyAdminAboutMakerAdded(array $details)
    {
        $users         = User::role('admin')->get();
        $template_body = UserService::getEmailBody('marker_added', $details);
        $response      = Notification::send($users,
                new MarkerAddedNotification($template_body));
        return $response;
    }
}