<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Models\Option;

/**
 * Description of OptionRepository
 * @author manpreetkaur
 */
class OptionRepository
{

    public function getOptions($option_name)
    {
        $options = Option::where('option_name', $option_name)->get();
        if ($options->isNotEmpty()) {
            return $options;
        }
        return false;
    }

    /**
     * create new options
     * @param array $data
     * @return type
     */
    public function create(array $data)
    {
        return DB::transaction(function() use($data) {
                $option = Option::create(['option_name' => $data['option_name'],
                        'option_value' => $data['option_value']]);
                if ($option) {
                    return ['status' => true, 'id' => $option->id];
                }
                return ['status' => false];
            });
    }

    /**
     * Update existing options of specific id
     * @param int $id
     * @param array $data
     * @return type
     */
    public function update(int $id, array $data)
    {
        return DB::transaction(function() use($data, $id) {
                $option = Option::where('id', $id)->update(['option_name' => $data['option_name'],
                    'option_value' => $data['option_value']]);
                if ($option) {
                    return true;
                }
                return false;
            });
    }

    /**
     * delete option of specific id
     * @param int $id
     * @return boolean
     */
    public function delete(int $id)
    {
        $option = Option::where('id', $id)->first();
        if (empty($option)) {
            return ['status' => false];
        }
        $option_name = $option->option_name;
        if ($option->delete()) {
            return ['status' => true, 'option_name' => $option_name];
        }
        return ['status' => false, 'option_name' => $option_name];
    }
}