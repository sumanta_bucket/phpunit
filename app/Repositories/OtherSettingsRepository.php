<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use App\Services\FileUploadService;
use Illuminate\Support\Facades\Schema;
use App\Models\{
    FireboardSetting,
    InsulationMaterial,
    FiresealingType,
    InsulationProfile,
    FireClass,
    MaterialType,
    PenetrationType,
    Option,
    ContactPerson,
    Employee,
    Relationship,
    Relative,
    Permission
};

/**
 * Description of OtherSettingsRepository
 * @author manpreetkaur
 */
class OtherSettingsRepository
{
    private $models;

    function __construct()
    {
        $this->models = [
            "fireboard_settings" => new FireboardSetting,
            "insulation_materials" => new InsulationMaterial,
            "firesealing_types" => new FiresealingType,
            "insulation_profiles" => new InsulationProfile,
            "fire_classes" => new FireClass
        ];
    }

    /**
     * List all records
     * @param string $table
     * @return type
     */
    public function getAll(string $table)
    {
        try {
            $record = DB::table($table)->where('deleted_at', null)->get();
            if ($record->isNotEmpty())
                    return ['status' => 1, 'records' => $record];
        } catch (\Exception $ex) {
            return ['status' => $ex->getCode()];
        }
        return ['status' => 0];
    }

    /**
     * Create new record
     * @param string $table
     * @param array $data
     * @return type
     */
    public function create(string $table, array $data)
    {
        $store = [
            'name' => $data['name'],
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ];
        if (array_key_exists('class_type', $data)) {
            $store['class_type'] = $data['class_type'];
        }
        if (array_key_exists('material_type_id', $data)) {
            $store['material_type_id'] = $data['material_type_id'];
        }
        try {
            $response = DB::table($table)->insertGetId($store);
            if ($response) {
                $eloquentObject = $this->models[$table]::find($response);
                if (array_key_exists('document', $data)) {
                    $documentsArray = [];
                    foreach ($data['document'] as $file) {
                        $filename         = FileUploadService::uploadFile($file,
                                'documents');
                        if ($filename)
                                $documentsArray[] = ['document' => $filename];
                    }
                    if ($eloquentObject->documents()->createMany($documentsArray))
                            return ['status' => 1];
                }
                return ['status' => 1];
            }
        } catch (\Exception $ex) {
            return ['status' => $ex->getCode()];
        }
        return ['status' => 0];
    }

    /**
     * Get record of specific id
     * @param string $table
     * @param int $id
     * @return type
     */
    public function getRecords(string $table, int $id)
    {
        try {
            $record = DB::table($table)->where('id', $id)->first();
            if (!empty($record)) return ['status' => 1, 'records' => $record];
        } catch (\Exception $ex) {
            return ['status' => $ex->getCode()];
        }
        return ['status' => 0];
    }

    /**
     * Update record of specific id
     * @param string $table
     * @param int $id
     * @param array $data
     * @return type
     */
    public function update(string $table, int $id, array $data)
    {
        $eloquentObject = $this->models[$table]::find($id);
        if (array_key_exists('document', $data)) {
            $documentIds     = [];
            $createDocuments = [];
            foreach ($data['document'] as $document) {
                if (is_numeric($document)) {
                    array_push($documentIds, $document);
                } else {
                    $filename = FileUploadService::uploadFile($document,
                            'documents');
                    if ($filename) {
                        $temp = [
                            'document' => $filename
                        ];
                        array_push($createDocuments, $temp);
                    }
                }
            }
            $toDelete = [];
            //if the ids exists then delete other files
            if (!empty($documentIds)) {
                $toDelete = $this->models[$table]::where('id', $id)->with(['documents' => function($q) use($documentIds) {
                            $q->whereNotIn('id', $documentIds);
                        }])->first();
                foreach ($toDelete->documents as $pdfData) {
                    $file = base_path('public/uploads/documents/').$pdfData->getOriginal('document');
                    if (file_exists($file)) unlink($file);
                }
                $this->models[$table]::where('id', $id)->with(['documents' => function($q) use($documentIds) {
                        $q->whereNotIn('id', $documentIds)->delete();
                    }])->first();
            } else {
                $toDelete = $this->models[$table]::where('id', $id)->with('documents')->first();
                foreach ($toDelete->documents as $pdfData) {
                    $file = base_path('public/uploads/documents/').$pdfData->getOriginal('document');
                    if (file_exists($file)) unlink($file);
                }
                $this->models[$table]::where('id', $id)->with(['documents' => function($q) {
                        $q->delete();
                    }])->first();
            }
            if (!empty($createDocuments)) {
                $eloquentObject->documents()->createMany($createDocuments);
            }
        }
        $eloquentObject->name = $data['name'];
        if (array_key_exists('material_type_id', $data)) {
            $eloquentObject->material_type_id = $data['material_type_id'];
        }
        if ($eloquentObject->save()) {
            return ['status' => 1];
        }
        return ['status' => 0];
    }

    /**
     * Delete record from table of specific Id
     * @param string $table
     * @param int $id
     * @return type
     */
    public function delete(string $table, int $id)
    {
        $eloquentObject = $this->models[$table]::find($id);
        if (isset($eloquentObject->documents)) {
            $allDocs = $eloquentObject->documents;
            foreach ($allDocs as $values) { 
                $file = base_path('public/uploads/documents/').$values->getOriginal('document');
                if (file_exists($file)) unlink($file);
            }
        }
        $delete = $eloquentObject->delete();
        if ($delete) {
            return ['status' => 1];
        }
        return ['status' => 0];
    }

    public function getDefaultData(string $type)
    {
        $records = [];
        switch ($type) {
            case "other_settings":
                $records['material_type']        = MaterialType::get();
                return $records;
                break;
            case "contact_persons":
                $records['contact_person_title'] = Option::select('id',
                        'option_value')->where('option_name',
                        'contact_person_title')->get();
                return $records;
                break;
            case "customers":
                $records['contact_persons']      = ContactPerson::select('id',
                        'user_id')->has('user')->with(['user.title'])->get();
                $records['customer_categories']  = Option::select('id',
                        'option_value')->where('option_name',
                        'customer_category')->get();
                return $records;
                break;
            case "relationships":
                $records['employees']            = Employee::with(['user', 'relatives'])->get();
                $records['relationships']        = Relationship::select('id',
                        'name')->get();
                $records['relatives']            = Relative::get();
                $records['employee_title']       = Option::where('option_name',
                        'employee_title')->get();
                return $records;
                break;
            case "users":
                $records['permissions']          = Permission::whereBetween('id', [2, 14])->get();
                return $records;
                break;
            case "consultants":
                $records['consultant_titles']    = \App\Models\ConsultantTitle::select('id',
                        'name')->get();
                $records['consultant_companies'] = \App\Models\ConsultantCompany::select('id',
                        'name')->get();
                return $records;
                break;
            default:
                return $records;
        }
    }
}