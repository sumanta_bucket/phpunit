<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Models\Floor;
use App\Models\Section;
use App\Services\FileUploadService;
use App\Services\UserService;
use Spatie\PdfToImage\Pdf;
use App\Services\DocumentPathService;
use App\Models\{
    Lastsection,
    InsulationMarker,
    InsulationMarkerPhoto,
    FiresealingMarker,
    FiresealingMarkerPhoto
};

/**
 * Description of ProjectDocumentationRepository
 * @author manpreetkaur
 */
class ProjectDocumentationRepository
{

    /**
     * Get all Floors
     * @return type
     */
    public function getAllRecords()
    {
        $records = Floor::get();
        return $records;
    }

    /**
     * create new floors and sections of the floors
     * @param int $project_id
     * @param array $data
     * @return type
     */
    public function create(int $project_id, array $data)
    {
        return DB::transaction(function () use ($data, $project_id) {
                foreach ($data['floors'] as $floorData) {
                    //create the floor of project
                    $floor = Floor::create([
                            'project_id' => $project_id,
                            'total_sections' => $floorData['sections']
                    ]);
                    //create the section of each floor
                    for ($i = 0; $i < $floorData['sections']; $i++) {
                        Section::create(['floor_id' => $floor->id]);
                    }
                }
                //get the all floors of the project
                $response = $this->_getProjectFloors($project_id);
                return $response;
            });
        return false;
    }
    /*
     * create the new section of the floor
     */

    public function createSection(int $project_id, int $floor_id, array $data)
    {
        $section = Section::create(['floor_id' => $floor_id]);
        if ($section) {
            $response = $this->_getProjectFloors($project_id);
            return $response;
        }
        return false;
    }

    /**
     * Get the floor of specific id
     * @param int $id
     * @return type
     */
    public function getRecords(int $id)
    {
        $data = Floor::where('id', $id)->with('sections')->first();
        return $data;
    }
    /*
     * local function to get the floor with sections
     */

    private function _getProjectFloors(int $project_id)
    {
        $response = Floor::where('project_id', $project_id)->select('id')->with('sections:id,floor_id,document,document_steel')->get();
        return $response;
    }

    /**
     * Delete the flood of specific id
     * @param int $project_id
     * @param int $id
     * @return boolean
     */
    public function deleteFloor(int $project_id, int $id)
    {
        return DB::transaction(function () use ($id, $project_id) {
                $floor   = Floor::find($id);
                $section = Section::where('floor_id', $id)->first();
                if (!empty($section)) {
                    $this->_deleteSection($section->id);
                }
                $floor->delete();
                $response = $this->_getProjectFloors($project_id);
                return $response;
            });
        return false;
    }
    /*
     * delete all floors
     */

    public function deleteAllFloors(int $project_id)
    {

        return DB::transaction(function () use ($project_id) {
                $floors = Floor::where('project_id', $project_id)->get();
                foreach ($floors as $floor) {
                    $section = Section::where('floor_id', $floor->id)->first();
                    if (!empty($section)) {
                        $this->_deleteSection($section->id);
                    }
                    $floor->delete();
                }
                return true;
            });
        return false;
    }

    /**
     * Delete the section of specific id
     * @param int $section_id
     * @return boolean
     */
    public function deleteSection(int $project_id, int $section_id)
    {
        $deleteSection = $this->_deleteSection($section_id);
        if ($deleteSection) {
            $response = $this->_getProjectFloors($project_id);
            return $response;
        }
        return false;
    }
    /*
     * local function to delete the section
     */

    private function _deleteSection($section_id)
    {
        $deleteSection = Section::find($section_id);
        $marker1       = InsulationMarker::where('section_id',
                $deleteSection->id)->delete();
        $marker2       = FiresealingMarker::where('section_id',
                $deleteSection->id)->delete();
        ($deleteSection->document_steel != NULL) ? $this->_unlinkSectionDocuments($deleteSection->document_steel,
                    'pdf') : false;
        ($deleteSection->image_steel != NULL) ? $this->_unlinkSectionDocuments($deleteSection->image_steel,
                    'image') : false;
        ($deleteSection->document != NULL) ? $this->_unlinkSectionDocuments($deleteSection->document,
                    'pdf') : false;
        ($deleteSection->image != NULL) ? $this->_unlinkSectionDocuments($deleteSection->image,
                    'image') : false;
        $delete        = $deleteSection->delete();
        if ($delete) return true;
        return false;
    }

    /**
     * upload the section pdf document
     * @param int $section_id
     * @param array $data
     * @return boolean
     */
    public function uploadSectionPdf(int $section_id, array $data)
    {
        $pageno        = $data['page'] ?? 1;
        $filename      = FileUploadService::uploadFile($data['document'],
                'documents');
        if (!$filename) ['status' => 0];
        $imageName     = $this->_convertPdfToImageUsingName($filename, $pageno);
        $document_name = explode('___', $filename);
        $section       = Section::where('id', $section_id)->first();
        if (array_key_exists('type', $data) && $data['type'] == 'firesealing') {
            $section->document = $filename;
            $section->image    = $imageName;
        } else {
            $section->document_steel = $filename;
            $section->image_steel    = $imageName;
        }
        if ($section->save()) {
            return ['status' => 1, 'data' => $section];
        }
        return ['status' => 0];
    }

    /**
     * delete the section of specific id
     * @param int $section_id
     * @return string
     */
    public function deleteSectionDocument(int $section_id, string $type)
    {
        $section = Section::where('id', $section_id)->first();
        //if no section found return
        if (empty($section)) {
            return 'not_found';
        }
        //check type of the document to delete if fireseling then column document and image
        if ($type == 'firesealing') {

            $query = FiresealingMarker::where('section_id', $section->id)->delete();
            ($section->document != NULL) ? $this->_unlinkSectionDocuments($section->document,
                        'pdf') : false;
            ($section->image != NULL) ? $this->_unlinkSectionDocuments($section->image,
                        'image') : false;

            $section->document = NULL;
            $section->image    = NULL;
            //save the updated section records
            if ($section->save()) {
                return 'deleted';
            }
            return false;
        }
        //if type is insulation structural steel then columns document_steel and image steel
        $query = InsulationMarker::where('section_id', $section->id)->delete();
        ($section->document_steel != NULL) ? $this->_unlinkSectionDocuments($section->document_steel,
                    'pdf') : false;
        ($section->image_steel != NULL) ? $this->_unlinkSectionDocuments($section->image_steel,
                    'image') : false;

        $section->document_steel = NULL;
        $section->image_steel    = NULL;
        if ($section->save()) {
            return 'deleted';
        }
        return false;
    }

    /**
     * unlink the files from storage
     * @param type $name
     * @param type $type
     * @return type
     */
    private function _unlinkSectionDocuments($name, $type)
    {
        if ($type == 'image') {
            $file = base_path('public/uploads/images/').$name;
            if (file_exists($file)) {
                return unlink($file);
            }
        }
        $file = base_path('public/uploads/documents/').$name;
        if ($file) {
            if (file_exists($file)) {
                return unlink($file);
            }
        }
        return false;
    }

    /**
     * convert the uploaded pdf file to image of type .jpg
     * @param type $pdfFile
     * @return string
     */
    private function _convertPdfToImage($pdfFile)
    {
        $imageName     = date('d-m-y-h-i-s-___').preg_replace('/\s+/', '-',
                trim(pathinfo($pdfFile->getClientOriginalName(),
                        PATHINFO_FILENAME))).'.jpg';
        $directoryPath = base_path('public/uploads/images');
        if (!file_exists($directoryPath)) {
            mkdir($directoryPath, 0777, true);
        }
        $pdf = new Pdf($pdfFile);
        dd($pdf);
        $pdf->saveImage($directoryPath.'/'.$imageName);
        return $imageName;
    }

    /**
     * convert the uploaded pdf file to image of type .jpg
     * @param type $pdfFile
     * @return string
     */
    private function _convertPdfToImageUsingName($pdfFile, $page = 1)
    {
        $imageName     = uniqid().preg_replace('/\s+/', '-',
                trim(pathinfo($pdfFile, PATHINFO_FILENAME))).'.jpg';
        $directoryPath = base_path('public/uploads/images');
        if (!file_exists($directoryPath)) {
            mkdir($directoryPath, 0777, true);
        }
        $pdfDoc = base_path("public/uploads/documents/$pdfFile");
        $pdf    = new Pdf($pdfDoc);
        $pdf->setPage($page)->saveImage($directoryPath.'/'.$imageName);
        return $imageName;
    }

    /**
     * upload multiple pdf files of the section
     * @param array $data
     * @return boolean
     */
    public function sectionUploadMuliplePdf(array $data, int $project_id)
    {
        //upload multiple pdf files in sections
        $type   = $data['type'];
        $result = [];
        foreach ($data['documents'] as $key => $value) {
            $image_name = $this->_convertPdfToImageCustomName($value);
            $response   = FileUploadService::uploadDocument($value, 'documents');
            if ($response['status'] == 1) {
                if ($type == 2) {
                    $response = Section::where('id', $key)->update(['document' => $response['filename'],
                        'image' => $image_name]);
                } else {
                    $response = Section::where('id', $key)->update(['document_steel' => $response['filename'],
                        'image_steel' => $image_name]);
                }
            } else {
                $result[['section_id' => $key, 'error' => $response['error']]];
            }
        }
        $floors = Floor::where('project_id', $project_id)->with('sections')->get();
        if (!empty($result)) {
            return ['status' => 0, 'error' => $result, 'data' => $floors];
        }
        return ['status' => 1, 'data' => $floors];
    }

    /**
     * delete multiple sections pdf files 
     * @param array $data
     * @return boolean
     */
    public function deleteSectionMultiplePdf(int $project_id, array $data)
    {
        $type     = $data['type'];
        $sections = DB::table('projects')
                ->join('floors', 'projects.id', '=', 'floors.project_id')
                ->join('sections', 'floors.id', '=', 'sections.floor_id')
                ->select('sections.*')->where('projects.id', $project_id)->get();
        if (empty($sections)) {
            return false;
        }
        //unlink the section documents and empty the specific field
        foreach ($sections as $value) {
            $section = Section::where('id', $value->id)->first();
            if (!empty($section)) {
                if ($type == 2) {
                    if ($section->document !== null && $section->image !== null) {
                        $query = FiresealingMarker::where('section_id',
                                $section->id)->delete();
                        $file  = base_path('public/uploads/documents/').$section->document;
                        $image = base_path('public/uploads/images/').$section->image;
                        if (file_exists($file) && file_exists($image)) {
                            unlink($file);
                            unlink($image);
                        }
                        $section->document = NULL;
                        $section->image    = NULL;
                        $section->save();
                    }
                } else {
                    if ($section->document_steel !== null && $section->image_steel
                        !== null) {
                        $query = InsulationMarker::where('section_id',
                                $section->id)->delete();
                        $file  = base_path('public/uploads/documents/').$section->document_steel;
                        $image = base_path('public/uploads/images/').$section->image_steel;
                        if (file_exists($file) && file_exists($image)) {
                            unlink($file);
                            unlink($image);
                        }
                        $section->document_steel = NULL;
                        $section->image_steel    = NULL;
                        $section->save();
                    }
                }
            }
        }
        return true;
    }

    /**
     * Get all the markers of the sections
     * @param int $section_id
     * @param string $type here type 2= fire sealing and 1=insulation structural steel
     * @return boolean
     */
    public function getSectionMarkers(int $section_id, int $type, int $user_id)
    {
        $section = [];
        if ($type == 1) {
            $section = Section::where('id', $section_id)->with(['insulation_markers.added_by',
                    'insulation_markers.insulation_profile', 'insulation_markers.insulation_material.material_type',
                    'insulation_markers.fire_class', 'insulation_markers.photos',
                    'floor.project'])->first();
        } else {
            $section = Section::where('id', $section_id)->with(['firesealing_markers.added_by',
                    'firesealing_markers.fire_class', 'firesealing_markers.firesealing_type',
                    'firesealing_markers.penetration_type', 'firesealing_markers.photos',
                    'floor.project','firesealing_markers.category'])->first();
        }
        if (empty($section)) {
            return false;
        }
        $image_path = null;
        $image_name = ($type == 2) ? $section->image : $section->image_steel;
        if ($image_name != NULL) {
            $path12       = DocumentPathService::encodePath('images/'.$image_name);
            $image_path = env('APP_URL').'v1/document/'.$path12;
        }
        $section['image_path']    = $image_path;
        $project_id               = $section->floor->project->id;
        $response                 = $section->only(['id', 'document_name', 'document_steel_name',
            'image_path', 'insulation_markers', 'firesealing_markers', 'insulation_path',
            'firesealing_path', 'has_insulation_marker', 'has_firesealing_marker']);
        $response['last_section'] = UserService::lastSectionRecord($project_id,
                $type, $user_id);
        return $response;
    }

    /**
     * change status for floor or section of specific type
     * @param int $project_id
     * @param array $data
     * @return boolean
     */
    public function changeStatusForFloorSection(int $project_id, array $data)
    {
        if ($data['table'] == 'floor') {
            if ($data['type'] == 1) {
                if ($data['status'] == 1) {
                    Floor::where('project_id', $project_id)->update(['is_firesteel_active' => 0]);
                    $response = Floor::where(['id' => $data['id'], 'project_id' => $project_id])->update([
                        'is_firesteel_active' => 1]);
                    if ($response) return true;
                }
            } else {
                if ($data['status'] == 1) {
                    Floor::where('project_id', $project_id)->update(['is_fireseal_active' => 0]);
                    $response = Floor::where(['id' => $data['id'], 'project_id' => $project_id])->update([
                        'is_fireseal_active' => 1]);
                    if ($response) return true;
                }
            }
        } elseif ($data['table'] == 'section') {
            $floorIds = Floor::where('project_id', $project_id)->pluck('id');
            if ($data['type'] == 1) {
                if ($data['status'] == 1) {
                    Section::whereIn('floor_id', $floorIds)->update(['is_firesteel_active' => 0]);
                    $response = Section::where('id', $data['id'])->update(['is_firesteel_active' => 1]);
                    if ($response) return true;
                }
            } else {
                if ($data['status'] == 1) {
                    Section::whereIn('floor_id', $floorIds)->update(['is_fireseal_active' => 0]);
                    $response = Section::where('id', $data['id'])->update(['is_fireseal_active' => 1]);
                    if ($response) return true;
                }
            }
        }else {
            return false;
        }
    }

    /**
     * upload multiple files create image from pdf with custom name
     * @param type $pdfFile
     * @return string
     */
    private function _convertPdfToImageCustomName($pdfFile)
    {
        $imageName     = uniqid().'___blueprint.jpg';
        $directoryPath = base_path('public/uploads/images');
        if (!file_exists($directoryPath)) {
            mkdir($directoryPath, 0777, true);
        }
        $pdf = new Pdf($pdfFile);
        $pdf->saveImage($directoryPath.'/'.$imageName);
        return $imageName;
    }

    public function updateLastSection(int $user_id, array $data)
    {
        $lastSection             = Lastsection::where([
                'user_id' => $user_id,
                'project_id' => $data['project_id'],
                'type' => $data['type']
            ])->first() ?? new Lastsection;
        $lastSection->project_id = $data['project_id'];
        $lastSection->floor_id   = $data['floor_id'];
        $lastSection->section_id = $data['section_id'] ?? NULL;
        $lastSection->type       = $data['type'];
        $lastSection->user_id    = $user_id;

        if ($lastSection->save()) {
            return true;
        }
        return false;
    }

    public function lastSectionRecord(int $project_id, int $type, int $user_id)
    {
        $response = UserService::lastSectionRecord($project_id, $type, $user_id);
        return $response;
    }
}