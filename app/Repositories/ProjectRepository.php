<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Models\Project;
use App\Models\ProjectCategory;
use App\Models\Customer;
use App\Models\Consultant;
use App\Models\Workplace;
use App\Models\Category;
use App\Services\UserService;
use App\Models\{
    ContactPerson,
    Option,
    ConsultantTitle,
    ConsultantCompany,
    Lastsection,
    ProjectConsultant,
    ProjectContactPerson,
    InsulationMaterial,
    FiresealingType,
    Document
};
use Illuminate\Support\Carbon;

/**
 * Description of ProjectRepository
 * @author manpreetkaur
 */
class ProjectRepository
{

    /**
     * Get all records for data table
     * @param int $offset
     * @param int $limit
     * @param string $column
     * @param string $direction
     * @param type $searchValue
     * @respone json array type
     */
    public function getAllProjects(int $offset = 0, int $limit = 10,
                                   string $column = 'id',
                                   string $direction = 'asc',
                                   $searchValue = null, $serachColumns = [],
                                   $keyValue = [], $role, $user_id)
    {
        $main_query = Project::where('deleted_at', NULL);  //declair the object 
        if ($role != 'admin') {
            if ($role == 'customer') { //check if the role is customer then match the cusomer id
                $customer   = Customer::where('user_id', $user_id)->select('id')->first(); //get the cutomer id from loggedin user
                $main_query = Project::where('customer_id', $customer->id); //get project results
            } elseif ($role == 'consultant') { //check for consultant record
                $consultant = Consultant::where('user_id', $user_id)->select('id')->first(); //get consultant from logged in user
                $main_query = Project::whereIn('id',
                        ProjectConsultant::where('consultant_id',
                            $consultant->id)->get()->pluck('project_id'))->orWhere('user_id',
                    $user_id);    //get the project associated with the current user
            } elseif ($role == 'contact-person') { //contact person user
                $contact    = ContactPerson::where('user_id', $user_id)->select('id')->first(); //get contact person user records
                $userIds    = ProjectContactPerson::where('contact_person_id',
                        $contact->id)->get()->pluck('project_id'); // get project ids associated to current contact person
                $main_query = Project::whereIn('id', $userIds)->orWhere('user_id',
                    $user_id);
            } else {
                
            }
        }

        //check the serach is for individual column search
        $check = preg_match('/:::/', $searchValue);
        if ($check) {
            $searchText = explode(':::', $searchValue); //explode to get the search value and column number to search
            $value      = explode('::', $searchText[0]);
            if ($value[1] == 0 || $value[1] == 5) { //if column name to be searched is name or project_number
                $records = $main_query->where($serachColumns[$value[1]], 'like',
                        '%'.$searchText[1].'%')->with(['customer.user', 'workplace',
                        'categories', 'consultants.user',
                        'consultants.consultantTitle', 'contactPersons.user.title'])->get();
            } else { //search the value in the particular relation value
                $records = $main_query->whereHas($serachColumns[$value[1]],
                        function ($query) use ($searchText, $keyValue, $value, $serachColumns) {
                        $query->where($keyValue[$serachColumns[$value[1]]],
                            'like', '%'.$searchText[1].'%');
                    })->with(['customer.user', 'workplace',
                        'categories', 'consultants.user',
                        'consultants.consultantTitle', 'contactPersons.user.title'])->get();
            }
        } elseif ($searchValue != null) { //if search value not null then this works
            $records = $main_query->whereHas('customer.user',
                    function ($query) use ($searchValue) {
                    $query->where('first_name', 'like', '%'.$searchValue.'%');
                })->orWhereHas('consultants.user',
                    function ($query) use ($searchValue) {
                    $query->where('first_name', 'like', '%'.$searchValue.'%');
                })->orWhereHas('consultants.consultantTitle',
                    function ($query) use ($searchValue) {
                    $query->where('name', 'like', '%'.$searchValue.'%');
                })->orWhereHas('workplace',
                    function ($query) use ($searchValue) {
                    $query->where('name', 'like', '%'.$searchValue.'%');
                })->orWhereHas('categories',
                    function ($query) use ($searchValue) {
                    $query->where('name', 'like', '%'.$searchValue.'%');
                })->orWhereHas('contactPersons.user',
                    function ($query) use ($searchValue) {
                    $query->where('first_name', 'like', '%'.$searchValue.'%');
                })->orWhereHas('contactPersons.user.title',
                    function ($query) use ($searchValue) {
                    $query->where('option_value', 'like', '%'.$searchValue.'%');
                })->orWhere('project_number', 'like', '%'.$searchValue.'%')->orWhere('name',
                    'like', '%'.$searchValue.'%')->with(['customer.user', 'workplace',
                    'categories', 'consultants.user',
                    'consultants.consultantTitle', 'contactPersons.user.title'])->where('name',
                    'like', '%'.$searchValue.'%')->get();
        } else { // simple get the records and return the results
            $records = $main_query->with(['customer.user',
                    'workplace',
                    'categories', 'consultants.user', 'consultants.consultantTitle',
                    'contactPersons.user.title'])->get();
        }

        $total = $records->count();   //count number of records
        if ($direction == 'asc') {  //if direction is in ascending
            $records = $records->sortBy($column)->values()->splice($offset)->take($limit);
        } else {  //for descending direction
            $records = $records->sortByDesc($column)->values()->splice($offset)->take($limit);
        }
        $data = ['total' => $total,
            'records' => $records];
        return $data;
    }

    /**
     * Create new project code
     * @param array $data
     * @return boolean
     */
    public function create(array $data, int $user_id)
    {
        return DB::transaction(function () use ($data, $user_id) {
                $project                   = new Project();
                $project->name             = $data['name'];
                $project->customer_id      = $data['customer_id'];
                $project->workplace_id     = $data['workplace']['id'] ?? NULL;
                $project->old_penetrations = $data['old_penetrations'] ?? NULL;
                $project->project_number   = $data['project_number'] ?? NULL;
                $project->user_id          = $user_id;
                if ($project->save()) { //if project is created then add in activity table
                    UserService::addHistory($user_id, 1, $project->id);
                    $consultantIds     = array_column($data['consultants'] ?? [],
                            'id') ?? [];
                    $contactPersonsIds = array_column($data['contact_persons'] ?? [
                            ], 'id') ?? [];
                    $project->consultants()->sync($consultantIds);  //add many to many consultants
                    $project->contactPersons()->sync($contactPersonsIds);   //add many to many contact persons
                    if (empty($data['_categories'])) {
                        return $project->id;
                    }
                    $result = $this->getInsertedArray($project->id,
                        $data['_categories']); //many to many relationship with categories
                    if ($result) return $project->id;
                }
            });
        return false;
    }

    /**
     * Get specific id data
     * @param int $id
     * @return type
     */
    public function edit(int $id)
    {
        $data = Project::where('id', $id)->with(['_categories', 'customer', 'workplace'])->first();
        return $data;
    }

    /**
     * Update the existing record
     * @param array $data
     * @param int $id
     * @return boolean
     */
    public function update(array $data, int $id)
    {
        //create array with common values
        $updatedValues                     = [
            'customer_id' => $data['customer_id'],
            'name' => $data['name']
        ];
        $updatedValues['workplace_id']     = $data['workplace']['id'] ?? NULL;
        $updatedValues['project_number']   = $data['project_number'] ?? NULL;
        $updatedValues['old_penetrations'] = $data['old_penetrations'] ?? NULL;
        $project                           = Project::where('id', $id)->update($updatedValues);
        $projectObj                        = Project::find($id);
        $contactPersonsIds                 = [];
        $consultantIds                     = array_column($data['consultants'] ?? [
                ], 'id') ?? [];   //get the consultant ids fo consultants collection
        $contactPersonsIds                 = array_column($data['contact_persons']
                    ?? [], 'id') ?? []; //get contact-persons ids from contact persons collection
        $projectObj->consultants()->sync($consultantIds);   //update many to many relationship with consultants
        $projectObj->contactPersons()->sync($contactPersonsIds);  //update many to many relationship with contact persons
        //if empty project categories then return project id
        if (empty($data['_categories']) && $project) {
            $projectObj->_categories()->delete();
            return true;
        }
        $IdsToDelete = []; // update the categories related to project accordig to condition
        foreach ($data['_categories'] as $category) {
            array_push($IdsToDelete, $category['category_id']);
            $projectCategory = ProjectCategory::where(['project_id' => $id,
                    'category_id' => $category['category_id']])->first();
            if (array_key_exists('sub_contractor', $category)) {
                $values['sub_contractor'] = $category['sub_contractor'];
            } else {
                $values['sub_contractor'] = 0;
            }
            if (array_key_exists('customer_id', $category)) {
                $values['customer_id'] = $category['customer_id'];
            }
            if (array_key_exists('project_number', $category)) {
                $values['project_number'] = $category['project_number'];
            }
            $projectCatContactPersondIds = array_key_exists('contact_persons',
                    $category) ? array_column($category['contact_persons'], 'id')
                    : [];
            // if no project category is already added then update the records
            if (!empty($projectCategory)) {
                ProjectCategory::where(['project_id' => $id,
                    'category_id' => $category['category_id']])->update($values);
                $projectCategory->contactPersons()->sync($projectCatContactPersondIds);
            } else { // if no project category then create new one
                $values['project_id']  = $id;
                $values['category_id'] = $category['category_id'];
                $insertedRec           = ProjectCategory::create($values); //create new project category
                $insertedRec->contactPersons()->sync($projectCatContactPersondIds); //sync project category contact persons
            }
        }
        if (!empty($IdsToDelete)) { // the category already added deleted by user also delete from table 
            ProjectCategory::where('project_id', $id)->whereNotIn('category_id',
                $IdsToDelete)->delete();
        }
        if ($project) {
            return true;
        }
        return false;
    }

    /**
     * Delete a project
     * @bodyParam id integer required the id of the project being deleted
     * @response boolean value
     */
    public function delete(int $id)
    {
        $project = Project::find($id);
        $delete  = $project->delete();
        if ($delete) return true;
        return false;
    }

    /**
     * Get the required data for project creation
     * @return type
     */
    public function getRequiredData(int $project_id = null, int $user_id,
                                    string $type = null)
    {
        $response = [];
        if ($type != null && $type == 'information') {
            $firesealing_markers = \App\Models\FiresealingMarker::whereHas('section.floor',
                    function($query)use($project_id) {
                    $query->where('project_id', $project_id);
                })->get();
            $firesealing_types_ids = $firesealing_markers->pluck('firesealing_type_id');
            $insulation_markers    = \App\Models\InsulationMarker::whereHas('section.floor',
                    function($query)use($project_id) {
                    $query->where('project_id', $project_id);
                })->get();
            $insulation_markers_ids = $insulation_markers->pluck('insulation_material_id');
            $response['project']    = Project::where('id', $project_id)->with('floors')->first();
            if (!empty($insulation_markers_ids)) {
                $response['insulation_documents'] = InsulationMaterial::whereIn('id',
                        $insulation_markers_ids)->with(['documents',
                        'material_type'])->get();
            } else {
                $response['insulation_documents'] = [];
            }
            if (!empty($firesealing_types_ids)) {
                $response['firesealing_documents'] = FiresealingType::whereIn('id',
                        $firesealing_types_ids)->with('documents')->get();
            } else {
                $response['firesealing_documents'] = [];
            }
            $response['documents'] = Document::where('project_id', $project_id)->orderBy('created_at',
                    'DESC')->get();
        } else {

            $customers = Customer::with('user:id,first_name,last_name')->get(); // get customer with user
            //transfor the customers collection to get the specific fields
            $customers->transform(function($data) {
                $data['name'] = $data['user']['full_name'];
                return $data->only(['id', 'name']);
            });

            $response['customers']              = $customers;
            $response['consultants']            = Consultant::with(['user', 'consultantTitle'])->get();  //get consultant with user and title 
            $response['workplaces']             = Workplace::select('id', 'name')->get(); // get the workplace
            $response['categories']             = Category::select('id', 'name')->get(); //get the categories
            $response['contact_persons']        = ContactPerson::with('user.title')->get(); // get the contact person
            $response['contact_persons_titles'] = Option::select('id',
                    'option_value')->where('option_name', 'contact_person_title')->get();  //get contact person titles
            $response['consultant_titles']      = ConsultantTitle::select('id',
                    'name')->get();  //get consultant title
            $response['consultant_companies']   = ConsultantCompany::select('id',
                    'name')->get(); //get consultant company
            if ($project_id != null) {  //if your project id not null then get the project details
                $response['project']       = Project::where('id', $project_id)->with([
                        '_categories.contactPersons.user.title',
                        'customer', 'workplace', 'floors.sections', 'consultants.user',
                        'consultants.consultantTitle', 'contactPersons.user.title',
                        '_categories.category', 'documents'])->first();
                $response['last_sections'] = UserService::lastSectionRecord($project_id,
                        2, $user_id);   //get the last active section of the project
            }
        }

        return $response;
    }

    /**
     * Private function for making array for insert
     * @param type $project_id
     * @param type $data
     * @return array
     */
    private function getInsertedArray($project_id, $data)
    {
        //get the inserted array loop through the records
        foreach ($data as $category) {
            $temp['category_id'] = $category['category_id'];
            $temp['project_id']  = $project_id;
            $temp['created_at']  = Carbon::now();
            $temp['updated_at']  = Carbon::now();
            if (array_key_exists('sub_contractor', $category)) {
                $temp['sub_contractor'] = $category['sub_contractor'] ?? 0;
            } else {
                $temp['sub_contractor'] = 0;
            }
            if (array_key_exists('customer_id', $category)) {
                $temp['customer_id'] = $category['customer_id'];
            }
            if (array_key_exists('project_number', $category)) {
                $temp['project_number'] = $category['project_number'];
            }
            $projectCategory   = ProjectCategory::create($temp);
            $contactPersondIds = array_key_exists('contact_persons', $category) ? array_column($category['contact_persons'],
                    'id') : [];
            $projectCategory->contactPersons()->sync($contactPersondIds);
        }
        return true;
    }
    /*
     * restore the project of particular id
     */

    public function restoreProject(int $id)
    {
        $project = Project::withTrashed()->find($id)->restore(); //restore the deleted record
        if ($project) {
            return true;
        }
        return false;
    }
    /*
     * get the datatable records of trashed records
     */

    public function trashedRecords($searchValue = null, $column = 'id',
                                   $direction = 'dsc', $limit = 10, $offset = 0,
                                   $serachColumns = [], $keyValue = [], $role,
                                   $user_id)
    {
        $main_query;
        if ($role != 'admin') {
            if ($role == 'customer') {
                $customer   = Customer::where('user_id', $user_id)->select('id')->first();
                $main_query = Project::where('customer_id', $customer->id)->where('deleted_at',
                    '!=', NULL);
            } elseif ($role == 'consultant') {
                $consultant = Consultant::where('user_id', $user_id)->select('id')->first();
                $main_query = Project::whereIn('id',
                        ProjectConsultant::where('consultant_id',
                            $consultant->id)->get()->pluck('project_id'))->where('deleted_at',
                    '!=', NULL);
            } elseif ($role == 'contact-person') {
                $contact    = ContactPerson::where('user_id', $user_id)->select('id')->first();
                $main_query = Project::whereIn('id',
                        ProjectContactPerson::where('contact_person_id',
                            $contact->id)->get()->pluck('project_id'))->where('deleted_at',
                    '!=', NULL);
            } else {
                $main_query = Project::where('deleted_at', '!=', NULL);
            }
        } else {
            $main_query = Project::where('deleted_at', '!=', NULL);
        }
        //check if the search is only for individual column search
        $check = preg_match('/:::/', $searchValue);
        if ($check) {
            $searchText = explode(':::', $searchValue); //explode to get the searchable column and value
            $value      = explode('::', $searchText[0]);
            if ($value[1] == 0 || $value[1] == 5) { //if column name is name or project_number the search in the project table only
                $records = $main_query->where($serachColumns[$value[1]], 'like',
                        '%'.$searchText[1].'%')->onlyTrashed()->with(['customer.user',
                        'consultants.user',
                        'consultants.consultantTitle',
                        'workplace', 'categories', 'contactPersons.user.title'])->get();
            } else { // serach the value in the relationships
                $records = $main_query->whereHas($serachColumns[$value[1]],
                        function ($query) use ($searchText, $keyValue, $value, $serachColumns) {
                        $query->where($keyValue[$serachColumns[$value[1]]],
                            'like', '%'.$searchText[1].'%')->withTrashed();
                    })->onlyTrashed()->with(['customer.user', 'consultants.user',
                        'consultants.consultantTitle',
                        'workplace', 'categories', 'contactPersons.user.title'])->get();
            }
        } elseif ($searchValue != null) { // if search is not null then search value in name or project_number of project table
            $records = $main_query->orWhere('name', 'like', '%'.$searchValue.'%')->orWhere('project_number',
                    'like', '%'.$searchValue.'%')->onlyTrashed()->with(['customer.user',
                    'consultants.user',
                    'consultants.consultantTitle', 'workplace', 'categories', 'contactPersons.user.title'])->get();
        } else {
            $records = $main_query->with(['customer.user', 'consultants.user',
                    'consultants.consultantTitle', 'workplace', 'categories', 'contactPersons.user.title'])->onlyTrashed()->get();
        }

        $total = $records->count(); // get the total records 
        if ($direction == 'asc') { //if direction is ascending
            $records = $records->sortBy($column)->values()->splice($offset)->take($limit);
        } else {//direction is descending
            $records = $records->sortByDesc($column)->values()->splice($offset)->take($limit);
        }
        $data  = ['total' => $total,
            'records' => $records];
        return $data;
    }
}