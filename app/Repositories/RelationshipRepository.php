<?php

namespace App\Repositories;

use App\Models\Relationship;
use Illuminate\Support\Facades\DB;
use App\Models\EmployeeRelative;

/**
 * Description of EmployeeRelationshipRepository
 * @author manpreetkaur
 */
class RelationshipRepository
{

    /**
     * create new record
     * @param array $data
     * @return type
     */
    public function create(array $data)
    {
        return DB::transaction(function() use($data) {
                $relationship = Relationship::create(['name' => $data['name']]);
                if ($relationship) {
                    return true;
                }
                return false;
            });
    }

    /**
     * fetch the record of specific id
     * @param int $id
     * @return type
     */
    public function getRelations(int $id)
    {
        $data = Relationship::where('id', $id)->first();
        return $data;
    }

    /**
     * update the records
     * @param int $id
     * @param array $data
     * @return type
     */
    public function update(int $id, array $data)
    {
        return DB::transaction(function() use($id, $data) {
                $relationship = Relationship::where('id', $id)->update([
                    'name' => $data['name']]);
                if ($relationship) {
                    return true;
                }
                return false;
            });
    }

    /**
     * delete the record
     * @param int $id
     * @return boolean
     */
    public function delete(int $id)
    {
//        $employee_relative = EmployeeRelative::where('relationship_id', $id)->delete();
        $delete = Relationship::where('id', $id)->delete();
        if ($delete) {
            return true;
        }
        return false;
    }
}