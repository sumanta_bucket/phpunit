<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Models\Relative;
use App\Models\Relationship;
use App\Models\EmployeeRelative;

/**
 * Description of RelativeRepository
 * @author manpreetkaur
 */
class RelativeRepository
{

    public function getRelationships()
    {
        $data = Relationship::select('id', 'name')->get();
        return $data;
    }

    public function create(array $data)
    {
        return DB::transaction(function() use($data) {
                $inserteData              = [
                    'first_name' => $data['relative']['first_name']
                ];
                $inserteData['email']     = $data['relative']['email'] ?? NULL;
                $inserteData['last_name'] = $data['relative']['last_name'] ?? NULL;
                $inserteData['mobile']    = $data['relative']['mobile'] ?? NULL;
                $relative                 = Relative::create($inserteData);
                if ($relative) {
                    $employee_relative                  = new EmployeeRelative();
                    $employee_relative->relative_id     = $relative->id;
                    $employee_relative->employee_id     = $data['employee_id'];
                    $employee_relative->relationship_id = $data['relationship_id'];
                    if ($employee_relative->save()) {
                        return true;
                    }
                }
                return false;
            });
    }

    public function update(int $relative_id, array $data)
    {
        return DB::transaction(function() use($data, $relative_id) {
                $updatedData              = [
                    'first_name' => $data['relative']['first_name']
                ];
                $updatedData['email']     = $data['relative']['email'] ?? NULL;
                $updatedData['mobile']    = $data['relative']['mobile'] ?? NULL;
                $updatedData['last_name'] = $data['relative']['last_name'] ?? NULL;
                $relative                 = Relative::where('id', $relative_id)->update($updatedData);
                if ($relative) {
                    $employee_relative                  = EmployeeRelative::where('id',
                            $data['id'])->first();
                    $employee_relative->employee_id     = $data['employee_id'];
                    $employee_relative->relationship_id = $data['relationship_id'];
                    if ($employee_relative->save()) {
                        return true;
                    }
                }
                return false;
            });
    }

    public function delete(int $id)
    {
        $relative          = Relative::where('id', $id)->first();
        $employee_relative = EmployeeRelative::where('relative_id', $id)->delete();
        if ($employee_relative) {
            if ($relative->delete()) {
                return true;
            }
        }
        return false;
    }
}