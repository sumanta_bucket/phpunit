<?php

namespace App\Repositories;

use App\Models\User;
use App\Notifications\UserNeedsConfirmation;
use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Services\TranslationService;
use App\Services\FileUploadService;
use App\Models\ConsultantTitle;
use App\Models\Option;
use App\Notifications\SendAccountInformation;
use App\Services\UserService;
use Spatie\Permission\Models\Role;

/**
 * Description of UserRepository
 * INDEX,CREATE, UPDATE, DELETE functions
 * @author manpreetkaur
 */
class UserRepository
{

    /**
     * Find user by Confirmation code 
     * @param type $code
     * @return User
     * @throws GeneralException
     */
    private function findByConfirmationCode($code)
    {
        $user = User::where('confirmation_code', $code)
            ->first();

        if ($user instanceof User) {
            return $user;
        }

        return false;
    }

    /**
     * Create new users method
     * @return collection
     */
    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
                $user = User::create([
                        'first_name' => $data['first_name'],
                        'email' => $data['email'],
                        'confirmation_code' => md5(uniqid(mt_rand(), true)),
                        'status' => 1,
                        'password' => Hash::make($data['password']),
                        // If users require approval or needs to confirm email
                        'confirmed' => 0
                ]);
                if ($user) {
                    /*
                     * Add the default site role to the new user
                     */
                    $user->assignRole('admin');
                    $user->notify(new UserNeedsConfirmation($user->confirmation_code,
                        $user->first_name));
                    return $user;
                }
                return response()->json(['error' => TranslationService::lang('error_user_creation',
                            config('constant.default_lang'))], 500);
            });
    }
    /* confirm user callback method on click link from send mail for confirmation */

    public function confirm($code)
    {
        $user = $this->findByConfirmationCode($code);
        if (!$user) {
            return response()->json(['error' => TranslationService::lang('user_already_confirmed',
                        config('constant.default_lang'))], 404);
        }
        if ($user->confirmed == 1) {
            return response()->json(['error' => TranslationService::lang('user_already_confirmed',
                        config('constant.default_lang'))], 422);
        }
        //match the user confirmation with request code
        if ($user->confirmation_code == $code) {
            $user->confirmed = 1;

            if ($user->save()) {
                return response()->json(['success' => TranslationService::lang('confirmed_successfully',
                            config('constant.default_lang'))], 200);
            }
            return response()->json(['error' => TranslationService::lang('confirmation_error',
                        config('constant.default_lang'))], 500);
        }
        return response()->json(['error' => TranslationService::lang('invalid_confirmation_code',
                    config('constant.default_lang'))], 500);
    }

    /**
     * Change the language after login
     * @bodyParam id int required the user id whose language we want to change
     * @bodyParam lang int required the language shortcode which is updated
     * @return boolean
     */
    public function changeLanguage(int $id, int $lang)
    {
        $user = User::where('id', $id)->first();
        if (!$user) {
            return ['status' => 'not_exist'];
        }
        //require this condition later
//        if ($user->language == $lang) {
//            return ['status' => 'same_as_old'];
//        }

        $user->language = $lang;
        if ($user->save()) {
            return ['status' => 'success'];
        }
        return ['status' => 'not_updated'];
    }

    /**
     * change the user profile picture
     * @param int $id
     * @param array $data
     * @return boolean
     */
    public function uploadPhoto(int $id, array $data)
    {
        $filename = FileUploadService::uploadFile($data['image'], 'images');

        if ($filename) {
            $user         = User::where('id', $id)->first();
            $user->avatar = $filename;
            if ($user->save()) {
                return ['status' => 1, 'path' => $user['avatar_path']];
            }
        }
        return false;
    }

    /**
     * change user password
     * @param int $user_id
     * @param array $data
     * @return type
     */
    public function changeUserPassword(int $user_id, array $data)
    {
        $user = User::where('id', $user_id)->first();
        if (!Hash::check($data['current_password'], $user->password)) {
            return ['status' => 3];
        }
        if (Hash::check($data['new_password'], $user->password)) {
            return ['status' => 2];
        }
        $user->password = Hash::make($data['new_password']);
        if ($user->save()) {
            return ['status' => 1];
        }
        return ['status' => 0];
    }

    /**
     * update the user profile
     * @param int $user_id
     * @param array $data
     * @return type
     */
    public function updateProfile(int $user_id, array $data)
    {
        $user = User::where('id', $user_id)->with(['roles', 'permissions'])->first();
        if (empty($user)) {
            return ['status' => 2];
        }
        $user->first_name = $data['first_name'];
        $user->last_name  = $data['last_name'] ?? NULL;
        $user->address    = $data['address'] ?? NULL;
        $user->city       = $data['city'] ?? NULL;
        $user->zip        = $data['zip'] ?? NULL;
        $user->mobile     = $data['mobile'] ?? NULL;
        $user->title_id   = $data['title_id'] ?? NULL;

        if ($user->save()) {
            $user->role          = $user->roles->pluck('name');
            $user['permissions'] = $user->permissions->pluck('name', 'id');
            return ['status' => 1, 'data' => $user->only('address', 'avatar',
                    'avatar_path', 'city', 'email', 'first_name', 'full_name',
                    'id', 'language', 'last_name', 'middle_name', 'mobile',
                    'title_id', 'zip', 'role', 'permissions')];
        }
        return ['status' => 0];
    }

    /**
     * update user details by the admin
     * @param array $data
     * @return boolean
     */
    public function editUserDetails(array $data)
    {
        $user             = User::where('id', $data['id'])->first();
        $user->first_name = $data['first_name'];
        $user->email      = $data['email'];
        $user->status     = $data['status'] ?? 0;
        $user->last_name  = $data['last_name'] ?? NULL;
        $notify           = $data['notify'] ?? false;
        if ($notify) {
            $details    = [
                "EMAIL" => $user->email,
                "PASSWORD" => config('constant.default_user_password')
            ];
            $email_body = UserService::getEmailBody('user_registration',
                    $details);
            $user->notify(new SendAccountInformation($user->first_name,
                $email_body));
        }
        if (array_key_exists('permissions', $data)) {
            $user->syncPermissions($data['permissions']);
        }
        if ($user->save()) {
            return true;
        }
        return false;
    }

    /**
     * get the details of the specific user
     * @param int $user_id
     * @return type
     */
    public function fetchUserData(int $user_id)
    {
        $user                = User::where('id', $user_id)->with([
                'roles', 'permissions', 'title'])->first();
        $user->role          = $user->roles->pluck('name');
        $user['permissions'] = $user->permissions->pluck('name', 'id');
        $data['user_info']   = $user->only(['id', 'title_id', 'first_name', 'last_name',
            'email',
            'language', 'avatar', 'address', 'city', 'zip', 'role', 'mobile',
            'permissions', 'title', 'avatar_path']);
        ;
        $data['titles']      = [];
        if ($user['role'][0] == 'employee') {
            $data['titles'] = Option::select('id', 'option_value')->where('option_name',
                    'employee_title')->get();
        } elseif ($user['role'][0] == 'consultant') {
            $data['titles'] = ConsultantTitle::select('id',
                    'name as option_value')->get();
        } elseif ($user['role'][0] == 'contact-person') {
            $data['titles'] = Option::select('id', 'option_value')->where('option_name',
                    'contact_person_title')->get();
        }
        return ['status' => 1, 'data' => $data];
    }

    /**
     * notify user function 
     * @param int $user_id
     * @return boolean
     */
    public function notifyUser(int $user_id)
    {
        $user    = User::where('id', $user_id)->first();
        $details = [
            "EMAIL" => $user->email,
            "PASSWORD" => config('constant.default_user_password')
        ];

        $email_body = UserService::getEmailBody('user_registration', $details);
        $user->notify(new SendAccountInformation($user->first_name, $email_body));
        return true;
    }

    /**
     * get user roles and permissions
     * @param int $user_id
     * @return type
     */
    public function userRolesPermissions(int $user_id)
    {
        $user                    = User::where('id', $user_id)->with(['roles', 'permissions',
                'title'])->first();
        $roleId                  = $user->roles->pluck('id');
        $user->role              = $user->roles->pluck('name');
        $user->permissions       = $user->permissions->pluck('name', 'id');
        $role                    = Role::find($roleId[0]);
        $user['role_permission'] = $role->permissions()->get();
        return ['status' => 1, 'data' => $user];
    }
}