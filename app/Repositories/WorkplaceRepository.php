<?php

namespace App\Repositories;

use App\Models\Workplace;
use Illuminate\Support\Facades\DB;

/**
 * Description of Workplace
 * @author manpreetkaur
 */
class WorkplaceRepository
{

    /**
     * create new workplace
     * @param array $data
     * @return type
     */
    public function create(array $data)
    {
        return DB::transaction(function() use($data) {
                $insert    = [
                    'name' => $data['name'],
                    'zipcode' => $data['zipcode'] ?? NULL,
                    'place' => $data['place'] ?? NULL
                ];
                $workplace = Workplace::create($insert);
                if ($workplace) {
                    return Workplace::find($workplace->id);
                }
                return false;
            });
    }

    /**
     * update the workplace
     * @param int $id
     * @param array $data
     * @return type
     */
    public function update(int $id, array $data)
    {
        return DB::transaction(function() use($data, $id) {
                $update    = [
                    'name' => $data['name'],
                    'zipcode' => $data['zipcode'] ?? NULL,
                    'place' => $data['place'] ?? NULL
                ];
                $workplace = Workplace::where('id', $id)->update($update);
                if ($workplace) {
                    return true;
                }
                return false;
            });
    }

    /**
     * delete the workplace of specific id
     * @param int $id
     * @return boolean
     */
    public function delete(int $id)
    {
        $delete = Workplace::where('id', $id)->delete();

        if ($delete) {
            return true;
        }
        return false;
    }
}