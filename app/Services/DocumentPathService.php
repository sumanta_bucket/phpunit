<?php

namespace App\Services;

use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;

/**
 * Description of DocumentPathService
 *
 * @author manpreetkaur
 */
class DocumentPathService
{
    protected $salt = "brandSE";

    // encode the docs the name with token and salt to get the encrypted data
    static function encodePath($docName)
    {
        $salt  = (new self)->salt;
        $token = app()->request->headers->get('authorization');  //get the request token
        $enc   = encrypt($token.$salt.$docName);
        return $enc;
    }

    //decode the encoded fiile path and then return the actual file eith header as a response
    public static function decodePath($path)
    {
        $salt        = (new self)->salt;
        $decrypted  = decrypt($path); //decrypt the encryted data
        $actualData = explode($salt, $decrypted); //explode the derpted array to get the required data
        $token      = $actualData[0]; //get the token as a first parameter
        //check if the token provoded is valid token or not
        try {
            $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
        } catch (ExpiredException $e) {
            return ['error' => 'token_expired_error', 'status' => 0];
        } catch (Exception $e) {
            return ['error' => $e->getMessage(), 'status' => 0];
        }
        $fullpath= base_path('public/uploads/').$actualData[1];  // get the full file path
        return ['status' => 1, 'path' => $fullpath];
    }
}