<?php

namespace App\Services;

/**
 * Description of FileUploadService
 * @author manpreetkaur
 */
class FileUploadService
{

    public static function uploadFile($file, $directory)
    {
        $directoryPath = base_path('public/uploads/').$directory;
        $fileName      = uniqid().'___'.preg_replace('/\s+/', '-',
                trim($file->getClientOriginalName()));
        $len           = strlen($fileName);
        if ($len > 150)
                $fileName      = substr($fileName, 0, 50).'....'.substr($fileName,
                    $len - 50);

        if (!file_exists($directoryPath)) {
            mkdir($directoryPath, 0777, true);
        } if ($file->move($directoryPath, $fileName)) {
            return $fileName;
        } return false;
    }

    public static function uploadDocument($file, $directory)
    {
        try {
            $directoryPath = base_path('public/uploads/').$directory;
            if (!file_exists($directoryPath)) {
                mkdir($directoryPath, 0777, true);
            }
            $fileName = uniqid().'___blueprint.pdf';
            $file->move($directoryPath, $fileName);
            return ['status' => 1, 'filename' => $fileName];
        } catch (Exception $ex) {
            return ['status' => 0, 'error' => $ex->getMessage()];
        }
    }
}