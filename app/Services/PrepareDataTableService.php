<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;

/**
 * Description of PrepareDataTableService  
 * @author manpreetkaur
 */
class PrepareDataTableService
{

    /**
     * Get the records of the data-table
     * @bodyParam table string required the name of the table from where we want to get the records
     * @bodyParam column string optional by default id column we use for order by query
     * @bodyParam direction string optional direction of the order by query ASC or Desc by default we are using ASC
     * @bodyParam offset int optional by default we are using from 0
     * @bodyParam limit int optional no of records we want to fetch at a time by default we are using 10
     * @bodyParam searchColumn string optional searchable column name
     * @bodyParam searchValue string optional the value to search in the table
     * @bodyParam withRelation boolean optional check whether the table fetch data with some relation
     * @bodyParam relationName string optional relation name with other table
     * @bodyParam relationSearchField string optional relation searchable field name
     * @bodyParam whereCondition extra condition for getting results for specific column condition
     * @response collection type
     */
    public static function getTableData(string $table, string $column = 'id',
                                        string $direction = 'ASC',
                                        int $offset = 0, int $limit = 10,
                                        $searchColumns = [],
                                        $searchValue = null,
                                        $withRelation = false,
                                        $relationName = null,
                                        $relationSearchColumns = [],
                                        $whereCondition = [])
    {

        $tableName = "App\Models".$table;
        $total     = 0;
        $records   = [];

        // escape underscore as mysql assuming this as special character
        if ($searchValue !== null)
                $searchValue = str_replace("_", "\_", $searchValue);

        //check whether it is hasManny or other relation and is searchable
        if ($withRelation && $searchValue != null) {
            $records = $tableName::whereHas($relationName,
                    function ($query) use ($searchValue, $relationSearchColumns) {
                    foreach ($relationSearchColumns as $keywords) {
                        $query->orWhere($keywords, 'like', '%'.$searchValue.'%');
                    }
                })->with($relationName)->where($searchColumn, 'like',
                    '%'.$searchValue.'%')->get();
            $total = $records->count();
            if ($direction == 'asc') {
                $records = $records->sortBy($column)->values()->splice($offset)->take($limit);
            } else {
                $records = $records->sortByDesc($column)->values()->splice($offset)->take($limit);
            }
        } elseif ($withRelation && $searchValue == null) { // with relation and not searchable condition
            //get total records
            $records = $tableName::has($relationName)->with($relationName)->get();
            $total   = $records->count();
            if ($direction == 'asc') {
                $records = $records->sortBy($column)->values()->splice($offset)->take($limit);
            } else {
                $records = $records->sortByDesc($column)->values()->splice($offset)->take($limit);
            }
        } elseif ($searchValue != null && !$withRelation) { //with search and no realtion condition
            //get total records
            if (!empty($whereCondition)) {
                $total = $tableName::where($whereCondition)->orWhere(function ($query) use ($searchColumns, $searchValue) {
                        foreach ($searchColumns as $keyword) {
                            $query->orWhere($keyword, 'like',
                                '%'.$searchValue.'%');
                        }
                    })->count();
                $records = $tableName::where($whereCondition)->orWhere(function ($query) use ($searchColumns, $searchValue) {
                        foreach ($searchColumns as $keyword) {
                            $query->orWhere($keyword, 'like',
                                '%'.$searchValue.'%');
                        }
                    })->orderBy($column, $direction)->offset($offset)->limit($limit)->get();
            } else {
                $total = $tableName::where(function ($query) use ($searchColumns, $searchValue) {
                        foreach ($searchColumns as $keyword) {
                            $query->orWhere($keyword, 'like',
                                '%'.$searchValue.'%');
                        }
                    })->count();
                $records = $tableName::where(function ($query) use ($searchColumns, $searchValue) {
                        foreach ($searchColumns as $keyword) {
                            $query->orWhere($keyword, 'like',
                                '%'.$searchValue.'%');
                        }
                    })->orderBy($column, $direction)->offset($offset)->limit($limit)->get();
            }
        } else { //if no seach and no relation then it works
            //total data
            if (!empty($whereCondition)) {
                $total   = $tableName::where($whereCondition)->count();
                $records = $tableName::where($whereCondition)->orderBy($column,
                        $direction)->offset($offset)->limit($limit)->get();
            } else {
                $total   = $tableName::count();
                $records = $tableName::orderBy($column, $direction)->offset($offset)->limit($limit)->get();
            }
        }

        $data = ['total' => $total,
            'records' => $records];
        return $data;
    }

    /**
     * data table function for multi relation search
     * @param string $table
     * @param string $column
     * @param string $direction
     * @param int $offset
     * @param int $limit
     * @param type $searchColumns
     * @param type $searchValue
     * @param type $withRelation
     * @param type $relations
     * @param type $whereCondition
     * @param type $whereRelation
     * @param type $hasRelation
     * @return type
     */
    public static function getTableDataWithMultipleRealtions(string $table,
                                                             string $column = 'id',
                                                             string $direction = 'ASC',
                                                             int $offset = 0,
                                                             int $limit = 10,
                                                             $searchColumns = [],
                                                             $searchValue = null,
                                                             $withRelation = false,
                                                             $relations = [],
                                                             $whereCondition = [
    ], $whereRelation = [], $hasRelation = null, $whereHasCondition = [])
    {
        $tableName   = "App\Models".$table;
        $total       = 0;
        $records     = [];
        // escape underscore as mysql assuming this as special character
        if ($searchValue !== null)
                $searchValue = str_replace("_", "\_", $searchValue);
        //check whether it is hasManny or other relation and is searchable
        if ($withRelation && $searchValue != null) {
            $searchData = Collection::make(new $tableName);
            $with       = array_keys($relations);
            if (!empty($whereRelation)) {
                $query = $tableName::where($whereRelation[0], 'like',
                        '%'.$searchValue.'%');
                if (count($whereRelation) > 1) {
                    for ($i = 1; $i < count($whereRelation); $i++) {
                        $query->orWhere($whereRelation[$i], 'like',
                            '%'.$searchValue.'%');
                    }
                }
                if (!empty($whereHasCondition)) {
                    $searchData = $query->has($hasRelation)->whereHas($hasRelation,
                            function ($query) use ($whereHasCondition) {
                            $query->where($whereHasCondition);
                        })->with($with)->get();
                } else {
                    $searchData = $query->has($hasRelation)->with($with)->get();
                }
            }
            $count           = count($relations);
            $searchRelations = Collection::make(new $tableName);
            if ($count > 0) {
                $random = 0;
                $rawQuery;
                foreach ($relations as $key => $data) {
                    if ($random == 0) {
                        $rawQuery = $tableName::whereHas($key,
                                function ($q1) use ($searchValue, $data) {
                                foreach ($data as $k => $keywords) {
                                    if ($k == 0)
                                            $q1->where($keywords, 'like',
                                            '%'.$searchValue.'%');
                                    else
                                            $q1->orWhere($keywords, 'like',
                                            '%'.$searchValue.'%');
                                }
                            });
                    } else {
                        $rawQuery->orWhereHas($key,
                            function ($q2) use ($searchValue, $data) {
                            foreach ($data as $j => $keywords) {
                                if ($j == 0)
                                        $q2->where($keywords, 'like',
                                        '%'.$searchValue.'%');
                                else
                                        $q2->orWhere($keywords, 'like',
                                        '%'.$searchValue.'%');
                            }
                        });
                    }
                    $random++;
                }
                if (!empty($whereHasCondition)) {
                    $searchRelations = $rawQuery->has($hasRelation)->whereHas($hasRelation,
                            function ($q3) use ($whereHasCondition) {
                            $q3->where($whereHasCondition);
                        })->with($with)->get();
                } else {
                    $searchRelations = $rawQuery->has($hasRelation)->with($with)->get();
                }
            }
            $records = [];
            $total   = 0;
            if (!empty($searchRelations) || !empty($searchData)) {
                $records = $searchData->merge($searchRelations)->unique();
                $total   = $records->count();
                if ($direction == 'asc') {
                    $records = $records->sortBy($column,
                            SORT_NATURAL | SORT_FLAG_CASE)->values()->splice($offset)->take($limit);
                } else {
                    $records = $records->sortByDesc($column,
                            SORT_NATURAL | SORT_FLAG_CASE)->values()->splice($offset)->take($limit);
                }
            }
        } elseif ($withRelation && $searchValue == null) { // with relation and not searchable condition
            $with = array_keys($relations);
            if (!empty($whereCondition)) {
                if (!empty($whereHasCondition)) {
                    $records = $tableName::where($whereCondition)->has($hasRelation)->whereHas($hasRelation,
                            function ($query) use ($whereHasCondition) {
                            $query->where($whereHasCondition);
                        })->with($with)->get();
                } else {
                    $records = $tableName::where($whereCondition)->has($hasRelation)->with($with)->get();
                }
                $total = $records->count();
                if ($direction == 'asc') {
                    $records = $records->sortBy($column)->values()->splice($offset)->take($limit);
                } else {
                    $records = $records->sortByDesc($column)->values()->splice($offset)->take($limit);
                }
            } else {
                if (!empty($whereHasCondition)) {
                    $records = $tableName::has($hasRelation)->whereHas($hasRelation,
                            function ($query) use ($whereHasCondition) {
                            $query->where($whereHasCondition);
                        })->with($with)->get();
                } else {
                    $records = $tableName::has($hasRelation)->with($with)->get();
                }
                $total = $records->count();
                if ($direction == 'asc') {
                    $records = $records->sortBy($column,
                            SORT_NATURAL | SORT_FLAG_CASE)->values()->splice($offset)->take($limit);
                } else {
                    $records = $records->sortByDesc($column,
                            SORT_NATURAL | SORT_FLAG_CASE)->values()->splice($offset)->take($limit);
                }
            }
        } elseif ($searchValue != null && !$withRelation) { //with search and no realtion condition
            if (!empty($whereCondition)) {
                $records = $tableName::where($whereCondition)->where(function ($query) use ($searchColumns, $searchValue) {
                        foreach ($searchColumns as $r => $keyword) {
                            if ($r == 0)
                                    $query->where($keyword, 'like',
                                    '%'.$searchValue.'%');
                            else
                                    $query->orWhere($keyword, 'like',
                                    '%'.$searchValue.'%');
                        }
                    })->get();
                $total = $records->count();
            } else {
                $records = $tableName::where(function ($query) use ($searchColumns, $searchValue) {
                        foreach ($searchColumns as $m => $keyword) {
                            if ($m == 0)
                                    $query->where($keyword, 'like',
                                    '%'.$searchValue.'%');
                            else
                                    $query->orWhere($keyword, 'like',
                                    '%'.$searchValue.'%');
                        }
                    })->get();
                $total = $records->count();
            }
            if ($direction == 'asc') {
                $records = $records->sortBy($column,
                        SORT_NATURAL | SORT_FLAG_CASE)->values()->splice($offset)->take($limit);
            } else {
                $records = $records->sortByDesc($column,
                        SORT_NATURAL | SORT_FLAG_CASE)->values()->splice($offset)->take($limit);
            }
        } else { //if no seach and no relation then it works
            //total data
            if (!empty($whereCondition)) {
                $total   = $tableName::where($whereCondition)->count();
                $records = $tableName::where($whereCondition)->get();
            } else {
                $total   = $tableName::count();
                $records = $tableName::get();
            }
            if ($direction == 'asc') {
                $records = $records->sortBy($column,
                        SORT_NATURAL | SORT_FLAG_CASE)->values()->splice($offset)->take($limit);
            } else {
                $records = $records->sortByDesc($column,
                        SORT_NATURAL | SORT_FLAG_CASE)->values()->splice($offset)->take($limit);
            }
        }
        //get total records
        $data = ['total' => $total,
            'records' => $records];
        return $data;
    }
}