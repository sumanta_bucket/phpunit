<?php

namespace App\Services;

use App\Models\Translation;
use Illuminate\Support\Facades\Cache;

/**
 * Description of TranslationService
 * translate the language to particular language
 * @author manpreetkaur  
 */
class TranslationService
{
    /* translate the string to particular language and create cache if not already exist */

    public static function lang(string $short_name, int $lang,
                                string $vars = null)
    {
        Cache::pull('login_unsuccessfull');
        $language = 'english';
        if ($lang == 2) {
            $language = 'svenska';
        }


        //check whether the value exists in cache then get specific language
        $translatedString = Cache::has($short_name) ? Cache::get($short_name)[$language]
                : null;
        if (!$translatedString) {
            //if not found in cache then find in the database
            $response = Translation::select('english', 'svenska')->where('short_name',
                    $short_name)->first();
            if (!empty($response)) {
                Cache::pull($language);
                $data             = Translation::select('short_name', $language)->pluck($language,'short_name');
                Cache::put($language, $data, 60);
                $translatedString = $response->$language;
            }
        }


        if (!$translatedString) {
            $translatedString = $short_name;
        }

        /* put values of variables in string */
        if ($vars !== null) {
            $translatedString = vsprintf($translatedString, (array) $vars);
        }

        return $translatedString;
    }

    public static function customMesages(int $lang)
    {
        $custom_validation_message = [
            'required' => self::lang('required_validation', $lang),
            'same' => self::lang('same_validation', $lang),
            'size' => self::lang('size_validation', $lang),
            'between' => self::lang('between_validation', $lang),
            'in' => self::lang('in_validation', $lang),
            'integer' => self::lang('integer_validation', $lang),
            'numeric' => self::lang('numeric_validation', $lang),
            'exists' => self::lang('exists_validation', $lang),
            'file' => self::lang('file_validation', $lang),
            'mimes' => self::lang('mimes_validation', $lang),
            'string' => self::lang('string_validation', $lang),
            'max' => self::lang('max_validation', $lang),
            'min' => self::lang('min_validation', $lang),
            'date' => self::lang('date_validation', $lang),
            'date_format' => self::lang('date_format_validation', $lang),
            'in_array' => self::lang('in_array_validation', $lang),
            'email' => self::lang('email_validation', $lang),
            'unique' => self::lang('unique_validation', $lang),
            'digits_between' => self::lang('digits_between_validation', $lang),
            'boolean' => self::lang('boolean_between', $lang)
        ];
        return $custom_validation_message;
    }
}