<?php

namespace App\Services;

use Illuminate\Support\Carbon;
use App\Models\PasswordReset;
use App\Notifications\ResetPass;
use App\Models\EmailTemplate;
use App\Models\Lastsection;
use App\Models\Activity;

/**
 * Description of UserService
 *
 * @author manpreetkaur
 */
class UserService
{

    /**
     * Send reset pass mail service
     * @param type $user
     * @return boolean
     */
    public static function passwordResetMail($user, $path)
    {
        //generate unique token each time
        $token         = md5(uniqid(mt_rand(), true));
        PasswordReset::where('email', $user->email)->delete();
        //insert the generated token in the password reset table
        PasswordReset::insert(['email' => $user->email,
            'token' => $token,
            'created_at' => Carbon::now()->toDateTimeString()
        ]);
        $resetLink     = "$path/reset-password?token=$token&e=".base64_encode($user->email);
        $resetButton   = "<a href='$resetLink'>".TranslationService::lang('reset_password_title',
                config('constant.default_lang'))."</a>";
        //send the email to the related user
        $details       = [
            "RESET_PASSWORD_LINK" => $resetButton,
            "IP_ADDRESS" => $_SERVER['REMOTE_ADDR'],
        ];
        $template_body = self::getEmailBody('account_forgot_password', $details);
        $response      = $user->notify(new ResetPass($user, $template_body));

        if (!empty($response)) {
            return false;
        }
        return true;
    }

    public static function getEmailBody(string $template_key, array $data)
    {
        $template = EmailTemplate::whereHas('emailTemplateType',
                function($query) use($template_key) {
                $query->where(['name' => $template_key, 'type' => 1]);
            })->where('is_default', 1)->first();
        $response            = [];
        $response['subject'] = $template->subject;
        $response['body']    = $template->template;
        foreach ($data as $key => $value) {
            $response['body'] = str_replace("{{{$key}}}", $value,
                $response['body']);
        }
        return $response;
    }

    /**
     * Get last Active section details
     * @param int $project_id
     * @param int $type
     * @param int $user_id
     * @return type
     */
    public static function lastSectionRecord(int $project_id, int $type,
                                             int $user_id)
    {
        $response = Lastsection::where(['project_id' => $project_id,
                'user_id' => $user_id, 'type' => $type])->orderBy('updated_at',
                'DESC')->first();
        return $response;
    }

    public static function addHistory($user_id, $history_type,
                                      $project_id = NULL, $marker_id = NULL,
                                      $type = NULL)
    {
        $activity               = new Activity();
        $activity->user_id      = $user_id;
        $activity->project_id   = $project_id;
        $activity->history_type = $history_type;
        $activity->marker_id    = $marker_id;
        $activity->type         = $type;
        if ($activity->save()) return true;
        return false;
    }
}