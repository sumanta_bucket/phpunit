<?php
return [
    /*
      |--------------------------------------------------------------------------
      | Authentication Defaults
      |--------------------------------------------------------------------------
      |
      | This option controls the default authentication "guard" and password
      | reset options for your application. You may change these defaults
      | as required, but they're a perfect start for most applications.
      |
     */

    'defaults' => [
        'guard' => env('AUTH_GUARD', 'api'),
    ],
    /*
      |--------------------------------------------------------------------------
      | Authentication Guards
      |--------------------------------------------------------------------------
      |
      | Next, you may define every authentication guard for your application.
      | Of course, a great default configuration has been defined for you
      | here which uses session storage and the Eloquent user provider.
      |
      | All authentication drivers have a user provider. This defines how the
      | users are actually retrieved out of your database or other storage
      | mechanisms used by this application to persist your user's data.
      |
      | Supported: "token"
      |
     */
    'guards' => [
        'api' => ['driver' => 'api'
            , 'provider' => 'users'],
    ],
    /*
      |--------------------------------------------------------------------------
      | User Providers
      |--------------------------------------------------------------------------
      |
      | All authentication drivers have a user provider. This defines how the
      | users are actually retrieved out of your database or other storage
      | mechanisms used by this application to persist your user's data.
      |
      | If you have multiple user tables or models you may configure multiple
      | sources which represent each model / table. These sources may then
      | be assigned to any extra authentication guards you have defined.
      |
      | Supported: "database", "eloquent"
      |
     */
    'providers' => [
        'users' => [
            'driver' => 'eloquent',
            'model' => App\Models\User::class,
        ],
    ],
    /*
      |--------------------------------------------------------------------------
      | Resetting Passwords
      |--------------------------------------------------------------------------
      |
      | Here you may set the options for resetting passwords including the view
      | that is your password reset e-mail. You may also set the name of the
      | table that maintains all of the reset tokens for your application.
      |
      | You may specify multiple password reset configurations if you have more
      | than one user table or model in the application and you want to have
      | separate password reset settings based on the specific user types.
      |
      | The expire time is the number of minutes that the reset token should be
      | considered valid. This security feature keeps tokens short-lived so
      | they have less time to be guessed. You may change this as needed.
      |
     */
    'passwords' => [
        'users' => [
            'provider' => 'users',
            'email' => 'auth.emails.password',
            'table' => 'password_resets',
            'expire' => 60,
        ],
    ],
    /*
     * Roles and permissions related array
     */
    'user_roles' => ['admin', 'planner', 'employee', 'customer', 'consultant', 'contact-person'],
    'user_permissions' => [
        'view_projects',
        'add_project',
        'edit_project',
        'view_documentation_pdf',
        'create_floor_map',
        'add_delete_pdf_in_floor_map',
        'add_marker',
        'edit_marker',
        'delete_marker',
        'view_customers',
        'add_customer',
        'archive_customer',
        'view_contact_persons',
        'add_contact_person',
        'archive_contact_person',
        'view_employees',
        'add_employee',
        'edit_employee',
        'archive_employee',
        'manage_settings',
        'edit_customer',
        'edit_contact_person',
        'delete_project',
        'view_markers',
        'view_project_information',
        'add_project_information_pdf',
        'delete_project_information_pdf'
    ],
    'planner_default_permissions' => [
        'view_projects',
        'add_project',
        'edit_project',
        'delete_project',
        'view_documentation_pdf',
        'create_floor_map',
        'add_delete_pdf_in_floor_map',
        'add_marker',
        'edit_marker',
        'delete_marker',
        'view_customers',
        'add_customer',
        'edit_customer',
        'view_contact_persons',
        'add_contact_person',
        'edit_contact_person',
        'view_markers'
    ],
    'employee_default_permissions' => [
        'view_projects',
        'add_project',
        'edit_project',
        'delete_project',
        'view_documentation_pdf',
        'create_floor_map',
        'add_delete_pdf_in_floor_map',
        'add_marker',
        'edit_marker',
        'delete_marker',
        'view_customers',
        'add_customer',
        'edit_customer',
        'view_contact_persons',
        'add_contact_person',
        'edit_contact_person',
        'view_markers',
        'view_project_information',
        'add_project_information_pdf',
        'delete_project_information_pdf'
    ],
    'customer_default_permission' => [
        'view_projects',
        'view_documentation_pdf',
        'add_marker',
        'edit_marker',
        'view_markers',
        'view_project_information'
    ],
    'contact_person_default_permission' => [
        'view_projects',
        'view_documentation_pdf',
        'view_markers',
        'view_project_information'
    ],
    'consultant_default_permission' => [
        'view_projects'
    ]
];
