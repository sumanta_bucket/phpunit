<?php
//variables used in controller are defined
return [
    'default_lang' => 2, //default language message without login
    'default_user_language' => 2, //default language message without login
    'table' => [
        'insulation_structural' => 'insulation_profiles',
        'material_settings' => 'insulation_materials',
        'fireboards' => 'fireboard_settings',
        'firesealing_types' => 'firesealing_types',
        'resistance' => 'fire_classes'
    ],
    'model_name' => [
        'insulation_structural' => '\InsulationProfile',
        'material_settings' => '\InsulationMaterial',
        'fireboards' => '\FireboardSetting',
        'firesealing_types' => '\FiresealingType',
        'resistance' => '\FireClass'
    ],
    'fire_class' => [
        'insulation' => 1,
        'firesealing' => 2
    ],
    'hole_type_insulation' => [
        1 => 'beam',
        2 => 'column',
        3 => 'pillar'
    ],
    'hole_type_firesealing' => [
        1 => 'floor',
        2 => 'ceiling',
        3 => 'wall'
    ],
    'email_prefix' => 'BCBRAND_PREFIX__',
    'status_type' => [
        1 => 'blue',
        2 => 'Green',
        3 => 'Yellow',
        4 => 'Green+ Blue',
        5 => 'Green + Yellow',
        6 => 'Red'
    ],
    'options' => [
        'customer_category', 'contact_person_title', 'employee_title', 'overtime_rate_100',
        'overtime_rate_50'
    ],
    'default_user_password' => '123456',
    'index_request_validation' => [
        'order.0.dir' => 'required|string',
        'order.0.column' => 'required|integer',
        'search.value' => 'string',
        'start' => 'required|integer|digits_between:0,10',
        'length' => 'required|integer|digits_between:0,10'
    ],
    'hole_type_insulation_marker' => [1 => 'beam', 2 => 'column', 3 => 'ceiling'],
    'hole_type_firesealing_markers' => [1 => 'floor', 2 => 'wall', 3 => 'ceiling'],
    'marker_status' => [1 => 'completed', 2 => 'damaged', 3 => 'pending'],
    'marker_types' => [1, 2],
    'user_status' => [0, 1, 2],
    'user_status_array' => [
        1 => 'active',
        2 => 'archieved',
        3 => 'suspended',
    ],
    'history_type' => [
        1 => 'new project',
        2 => 'new user',
        3 => 'new marker',
        4 => 'edit marker'
    ]
];
