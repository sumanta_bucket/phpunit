<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('title_id')->nullable();
            $table->string('first_name',191);
            $table->string('middle_name',191)->nullable();
            $table->string('last_name',191)->nullable();
            $table->string('email',191)->unique();
            $table->string('password',100);
            $table->string('mobile',20)->nullable();
            $table->tinyInteger('notify')->default(0)->comment('0=> no, 1 => yes');
            $table->text('address')->nullable();
            $table->string('city',255)->nullable();
            $table->string('zip',10)->nullable();
            $table->integer('status')->default(1)->comment('1=> active, 2 => archieved, 3=> suspended');
            $table->text('notes')->nullable();
            $table->string('confirmation_code')->nullable();
            $table->boolean('confirmed')->default(true);
            $table->string('avatar')->nullable();
            $table->tinyInteger('language')->default(2)->comment('1=> en, 2 => se');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
