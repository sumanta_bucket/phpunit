<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned(); 
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('address2',255)->nullable();
            $table->string('organization',191);
            $table->string('country',10)->nullable();
            $table->integer('customer_category_id');
            $table->string('invoice_email',100);
            $table->string('other_invoice_address',255)->nullable();
            $table->string('other_invoice_city',255)->nullable();
            $table->string('other_shipping_address',255)->nullable();
            $table->string('other_shipping_city',255)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
