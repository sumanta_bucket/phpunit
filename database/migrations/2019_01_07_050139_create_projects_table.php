<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unsigned();
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
            $table->integer('workplace_id')->nullable()->unsigned();
            $table->foreign('workplace_id')->references('id')->on('workplaces')->onDelete('cascade');
            $table->integer('consultant_id')->nullable()->unsigned();
            $table->foreign('consultant_id')->references('id')->on('consultants')->onDelete('cascade');
            $table->string('name',191)->nullable();
            $table->integer('project_number')->nullable();
            $table->integer('status')->default(1)->comment('1=> active, 2 => archieved');
            $table->tinyInteger('old_penetrations')->nullable()->comment('0=>no, 1=>yes');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
