<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarkersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('markers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('section_id')->unsigned();
            $table->foreign('section_id')->references('id')->on('sections')->onDelete('cascade');
            $table->integer('marker_type')->comment('1=>insulation,2=>firesealing');
            $table->integer('hole_type')->comment('1=>beam,2=>pillar,3=>column');
            $table->integer('quantity');
            $table->integer('fire_class_id')->unsigned();
            $table->foreign('fire_class_id')->references('id')->on('fire_classes')->onDelete('cascade');
            $table->integer('insulation_profile_id')->nullable()->unsigned();
            $table->foreign('insulation_profile_id')->references('id')->on('insulation_profiles')->onDelete('cascade');
            $table->integer('fireboard_setting_id')->nullable()->unsigned();
            $table->foreign('fireboard_setting_id')->references('id')->on('fireboard_settings')->onDelete('cascade');
            $table->integer('insuation_material_id')->nullable()->unsigned();
            $table->foreign('insuation_material_id')->references('id')->on('insulation_materials')->onDelete('cascade');
            $table->string('photo',191);
            $table->integer('firesealing_type_id')->nullable()->unsigned();
            $table->foreign('firesealing_type_id')->references('id')->on('firesealing_types')->onDelete('cascade');
            $table->integer('thickness');
            $table->integer('prescribed');
            $table->integer('measured');
            $table->tinyInteger('status')->default(1)->comment('1=>completed, 2=>damaged, 3=>pending');
            $table->tinyInteger('predefined')->comment('0=>no, 1=>yes');
            $table->text('notes');
            $table->integer('updated_by');
            $table->integer('added_by')->comment('refered to users table id field')->unsigned();
            $table->foreign('added_by')->references('id')->on('users')->onDelete('cascade');
            $table->double('x_axis')->nullable();
            $table->double('y_axis')->nullable();
            $table->tinyInteger('status_type')->nullable()->comment('1=>blue,2=> Green,3=>Yellow,4=> Green+ Blue,5=> Green + Yellow,6=> Red');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('markers');
    }
}
