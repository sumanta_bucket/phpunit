<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTablesForOtherSettings extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        // Drop document columns from insulation_materials and fireboard_settings
        Schema::table('insulation_materials', function (Blueprint $table) {
            $table->dropColumn('document');
        });
        
        Schema::table('fireboard_settings', function (Blueprint $table) {
            $table->dropColumn('document');
        });

        // Table for fireboards documents 
        Schema::create('fireboard_setting_documents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fireboard_setting_id')->unsigned();
            $table->foreign('fireboard_setting_id')->references('id')->on('fireboard_settings')->onDelete('cascade');
            $table->string('document', 255)->nullable();
            $table->timestamps();
        });

        // Table for materials settings documents 
        Schema::create('insulation_material_documents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('insulation_material_id')->unsigned();
            $table->foreign('insulation_material_id')->references('id')->on('insulation_materials')->onDelete('cascade');
            $table->string('document', 255)->nullable();
            $table->timestamps();
        });

        // Table for firesealing types documents 
        Schema::create('firesealing_type_documents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('firesealing_type_id')->unsigned();
            $table->foreign('firesealing_type_id')->references('id')->on('firesealing_types')->onDelete('cascade');
            $table->string('document', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('fireboard_setting_documents');
        Schema::dropIfExists('insulation_material_documents');
        Schema::dropIfExists('firesealing_type_documents');
    }

}
