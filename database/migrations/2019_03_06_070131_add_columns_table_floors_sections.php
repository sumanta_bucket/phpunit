<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsTableFloorsSections extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('floors',
            function (Blueprint $table) {
            $table->tinyInteger('is_fireseal_active')->nullable()->default(0)->after('total_sections')->comment('1=>active,0=>inactive');
            $table->tinyInteger('is_firesteel_active')->nullable()->default(0)->after('is_fireseal_active')->comment('1=>active,0=>inactive');
        });
        Schema::table('sections',
            function (Blueprint $table) {
            $table->tinyInteger('is_fireseal_active')->nullable()->default(0)->after('image_steel')->comment('1=>active,0=>inactive');
            $table->tinyInteger('is_firesteel_active')->nullable()->default(0)->after('is_fireseal_active')->comment('1=>active,0=>inactive');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}