<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class ChangeOptionNameEnumValues extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('options',
            function(Blueprint $table) {
            $table->dropColumn('option_name');
        });

        Schema::table('options',
            function(Blueprint $table) {
            $table->enum('option_name',
                ['overtime_rate_50', 'overtime_rate_100', 'customer_category', 'contact_person_title',
                'employee_title']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}