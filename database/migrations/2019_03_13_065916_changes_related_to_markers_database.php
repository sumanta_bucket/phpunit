<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangesRelatedToMarkersDatabase extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('marker_photos');
        Schema::dropIfExists('markers');

        Schema::create('penetration_types',
            function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('material_types',
            function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::table('insulation_materials',
            function (Blueprint $table) {
            $table->integer('material_type_id')->after('name')->nullable()->unsigned();
            $table->foreign('material_type_id')->references('id')->on('material_types')->onDelete('cascade');
        });
        Schema::create('insulation_markers',
            function (Blueprint $table) {
            $table->increments('id');
            $table->integer('section_id')->unsigned();
            $table->foreign('section_id')->references('id')->on('sections')->onDelete('cascade');
            $table->integer('hole_type')->comment('1=>beam,2=>column,3=>ceiling');
            $table->integer('insulation_profile_id')->nullable()->unsigned();
            $table->foreign('insulation_profile_id')->references('id')->on('insulation_profiles')->onDelete('cascade');
            $table->integer('insulation_material_id')->nullable()->unsigned();
            $table->foreign('insulation_material_id')->references('id')->on('insulation_materials')->onDelete('cascade');
            $table->integer('fire_class_id')->unsigned();
            $table->foreign('fire_class_id')->references('id')->on('fire_classes')->onDelete('cascade');
            $table->string('dimensions');
            $table->integer('prescribed')->nullable();
            $table->integer('measured')->nullable();
            $table->integer('thickness')->nullable();
            $table->tinyInteger('status')->default(1)->comment('1=>completed, 2=>damaged, 3=>pending');
            $table->tinyInteger('predefined')->comment('0=>no, 1=>yes');
            $table->text('notes');
            $table->integer('updated_by')->comment('refered to users table id field')->unsigned();
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('cascade');
            $table->integer('added_by')->comment('refered to users table id field')->unsigned();
            $table->foreign('added_by')->references('id')->on('users')->onDelete('cascade');
            $table->string('added_by_name');
            $table->double('x_axis')->nullable();
            $table->double('y_axis')->nullable();
            $table->tinyInteger('status_type')->nullable()->comment('1=>blue,2=> Green,3=>Yellow,4=> Green+ Blue,5=> Green + Yellow,6=> Red');
            $table->date('marker_date');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('firesealing_markers',
            function (Blueprint $table) {
            $table->increments('id');
            $table->integer('section_id')->unsigned();
            $table->foreign('section_id')->references('id')->on('sections')->onDelete('cascade');
            $table->integer('penetration_type_id')->unsigned();
            $table->foreign('penetration_type_id')->references('id')->on('penetration_types')->onDelete('cascade');
            $table->integer('hole_type')->comment('1=>floor,2=>wall,3=>ceiling');
            $table->integer('firesealing_type_id')->nullable()->unsigned();
            $table->foreign('firesealing_type_id')->references('id')->on('firesealing_types')->onDelete('cascade');
            $table->integer('fire_class_id')->unsigned();
            $table->foreign('fire_class_id')->references('id')->on('fire_classes')->onDelete('cascade');
            $table->string('dimension1');
            $table->string('dimension2')->nullable();
            $table->integer('quantity');
            $table->tinyInteger('status')->default(1)->comment('1=>completed, 2=>damaged, 3=>pending');
            $table->tinyInteger('predefined')->comment('0=>no, 1=>yes');
            $table->text('notes');
            $table->integer('updated_by')->comment('refered to users table id field')->unsigned();
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('cascade');
            $table->integer('added_by')->comment('refered to users table id field')->unsigned();
            $table->foreign('added_by')->references('id')->on('users')->onDelete('cascade');
            $table->string('added_by_name');
            $table->double('x_axis')->nullable();
            $table->double('y_axis')->nullable();
            $table->tinyInteger('status_type')->nullable()->comment('1=>blue,2=> Green,3=>Yellow,4=> Green+ Blue,5=> Green + Yellow,6=> Red');
            $table->date('marker_date');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('insulation_materials',
            function (Blueprint $table) {
            $table->dropForeign(['material_type_id']);
            $table->dropColumn('material_type_id');
        });
        Schema::dropIfExists('insulation_markers');
        Schema::dropIfExists('firesealing_markers');
        Schema::dropIfExists('material_types');
        Schema::dropIfExists('penetration_types');
    }
}