<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotosTableForMarkers extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insulation_marker_photos',
            function (Blueprint $table) {
            $table->increments('id');
            $table->integer('insulation_marker_id')->nullable()->unsigned();
            $table->foreign('insulation_marker_id')->references('id')->on('insulation_markers')->onDelete('cascade');
            $table->string('photo');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('firesealing_marker_photos',
            function (Blueprint $table) {
            $table->increments('id');
            $table->integer('firesealing_marker_id')->nullable()->unsigned();
            $table->foreign('firesealing_marker_id')->references('id')->on('firesealing_markers')->onDelete('cascade');
            $table->string('photo');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insulation_marker_photos');
        Schema::dropIfExists('firesealing_marker_photos');
    }
}