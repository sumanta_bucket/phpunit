<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangesInEmployeeReationshipsAndRelatives extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('relatives',
            function (Blueprint $table) {
            $table->dropForeign(['employee_id']);
            $table->dropColumn('employee_id');
            $table->dropForeign(['employee_relationship_id']);
            $table->dropColumn('employee_relationship_id');
        });

        Schema::rename('employee_relationships', 'relationships');

        Schema::table('relatives',
            function (Blueprint $table) {
            $table->integer('relationship_id')->unsigned()->after('id');
            $table->foreign('relationship_id')->references('id')->on('relationships')->onDelete('cascade');
        });
        Schema::create('employee_relative',
            function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id')->unsigned();
            $table->foreign('employee_id')->references('id')->on('employees')->onDelete('cascade');
            $table->integer('relative_id')->unsigned();
            $table->foreign('relative_id')->references('id')->on('relatives')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('relationships', 'employee_relationships');
        Schema::table('relatives',
            function (Blueprint $table) {
            $table->integer('employee_id')->unsigned();
            $table->foreign('employee_id')->references('id')->on('employees')->onDelete('cascade');
            $table->integer('employee_relationship_id')->unsigned();
            $table->foreign('employee_relationship_id')->references('id')->on('employee_relationships')->onDelete('cascade');
        });
        Schema::dropIfExists('employee_relative');
    }
}