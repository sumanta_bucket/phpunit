<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangesInEmployeeRelativeRelationship extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('relatives',
            function (Blueprint $table) {
            $table->dropForeign(['relationship_id']);
            $table->dropColumn('relationship_id');
            $table->string('mobile',20)->nullable()->change();
        });
        Schema::table('employee_relative',
            function (Blueprint $table) {
            $table->integer('relationship_id')->unsigned()->after('id');
            $table->foreign('relationship_id')->references('id')->on('relationships')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}