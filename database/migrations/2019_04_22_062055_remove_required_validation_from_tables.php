<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveRequiredValidationFromTables extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('consultants',
            function (Blueprint $table) {
            $table->integer('consultant_title_id')->unsigned()->nullable()->change();
        });
        Schema::table('firesealing_markers',
            function (Blueprint $table) {
            $table->text('notes')->nullable()->change();
        });
        Schema::table('insulation_markers',
            function (Blueprint $table) {
            $table->text('notes')->nullable()->change();
        });
        Schema::table('relatives',
            function (Blueprint $table) {
            $table->string('email')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}