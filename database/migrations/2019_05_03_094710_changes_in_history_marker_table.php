<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangesInHistoryMarkerTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('history_marker',
            function (Blueprint $table) {
            if (Schema::hasColumn('history_marker', 'status')) {
                $table->dropColumn('status');
            }
            $table->integer('type')->nullable()->comment('1=>insulation,2=>firesealing')->after('user_id');
            $table->integer('project_id')->nullable()->unsigned()->after('type');
            $table->integer('marker_id')->nullable()->after('project_id');
            $table->integer('history_type')->nullable()->comment('1=>new project,2=>new user,3=>new marker,4=>edit marker')->after('marker_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('history_marker',
            function (Blueprint $table) {
            if (Schema::hasColumn('history_marker', 'project_id')) {
                $table->dropColumn('project_id');
            }
            if (Schema::hasColumn('history_marker', 'type')) {
                $table->dropColumn('type');
            }
            if (Schema::hasColumn('history_marker', 'marker_id')) {
                $table->dropColumn('marker_id');
            }
            if (Schema::hasColumn('history_marker', 'history_type')) {
                $table->dropColumn('history_type');
            }
        });
    }
}