<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('categories')->truncate();
        $categories = [
            [
                'name' => 'building'
            ],
            [
                'name' => 'electrical'
            ],
            [
                'name' => 'pipes'
            ],
            [
                'name' => 'spinkler'
            ],
            [
                'name' => 'control'
            ],
            [
                'name' => 'ventilation'
            ]
        ];
        DB::table('categories')->insert($categories);
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}