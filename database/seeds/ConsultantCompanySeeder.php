<?php

use Illuminate\Database\Seeder;

class ConsultantCompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       for ($i = 1 ;$i<10 ;$i++){
           DB::table('consultant_companies')->insert([
               'id'=>$i,
               'name' => str_random(10),
           ]);
       }
    }
}
