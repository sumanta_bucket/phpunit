<?php

use App\Models\ConsultantTitle;
use Illuminate\Database\Seeder;

class ConsultantTitleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i < 10; $i++) {
            ConsultantTitle::insert([
                'id' => $i,
                'name' => str_random(10),
            ]);
        }
    }
}
