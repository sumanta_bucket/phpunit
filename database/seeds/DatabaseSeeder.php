<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        // Register the seeder
        $this->call(PermissionsRoleTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(EmailTemplateTypesTableSeeder::class);
        $this->call(TranslationsTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(MaterialTypesTableSeeder::class);
        $this->call(PenetrationTypesTableSeeder::class);
        $this->call(TestingAccountsSeeder::class);
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        Model::reguard();
    }
}
