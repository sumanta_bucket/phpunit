<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\EmailTemplateType;
use App\Models\EmailTemplate;

class EmailTemplateTypesTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('email_template_types')->truncate();
        DB::table('email_templates')->truncate();
        //create email template types
        EmailTemplateType::create([
            'type' => 1,
            'name' => 'account_forgot_password',
            'placeholder' => '["{{RESET_PASSWORD_LINK}}","{{IP_ADDRESS}}"]',
        ]);
        EmailTemplateType::create([
            'type' => 1,
            'name' => 'user_registration',
            'placeholder' => '["{{EMAIL}}","{{PASSWORD}}"]',
        ]);
        EmailTemplateType::create([
            'type' => 1,
            'name' => 'marker_added',
            'placeholder' => '["{{ADDED_BY}}","{{SECTION_NAME}}","{{X_AXIS}}","{{Y_AXIS}}","{{PROJECT_NAME}}"]',
        ]);
        EmailTemplateType::create([
            'type' => 2,
            'name' => 'account_created',
            'placeholder' => '["{{EMAIL}}","{{PASSWORD}}"]',
        ]);


        //create email templates
        EmailTemplate::create([
            'email_template_type_id' => 1,
            'name' => 'Forgot passsowrd template',
            'subject'=>'Forgot Password',
            'template' => 'Hi, please click this link to reset your password {{RESET_PASSWORD_LINK}} , IP Address {{IP_ADDRESS}}',
            'is_default' => '1',
            'deletes' => '0',
        ]);
        EmailTemplate::create([
            'email_template_type_id' => 2,
            'name' => 'New account created template',
            'subject'=>'account created',
            'template' => 'Hi, your account is created details :Email :{{EMAIL}},Password {{PASSWORD}}',
            'is_default' => '1',
            'deletes' => '0',
        ]);
        EmailTemplate::create([
            'email_template_type_id' => 3,
            'name' => 'Marker added template',
            'subject'=>'marker added',
            'template' => 'Hi, the new marker added details :-project :{{PROJECT_NAME}}, section {{SECTION_NAME}}, location :{{X_AXIS}} ,{{Y_AXIS}}, Added by :{{ADDED_BY}}',
            'is_default' => '1',
            'deletes' => '0',
        ]);
        EmailTemplate::create([
            'email_template_type_id' => 4,
            'name' => 'SMS account created',
            'subject'=>'account created',
            'template' => 'Hi,your new account created with email:{{EMAIL}},password:{{PASSWORD}}',
            'is_default' => '1',
            'deletes' => '0',
        ]);
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}