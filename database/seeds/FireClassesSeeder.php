<?php

use App\Models\FireClass;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class FireClassesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i < 10; $i++) {
            FireClass::create([
                'id' => $i,
                'name' => str_random(12),
                'class_type' => $i % 2 == 0 ? 1 : 2,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
