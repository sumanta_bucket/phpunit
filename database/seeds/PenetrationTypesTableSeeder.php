<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class PenetrationTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('penetration_types')->truncate();
        $penetrations =[
            [
                'name'=>'linear',
                'slug'=>'linear',
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            [
                'name'=>'circular',
                'slug'=>'circular',
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            [
                'name'=>'square',
                'slug'=>'square',
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ],
            [
                'name'=>'rectangular',
                'slug'=>'rectangular',
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now()
            ]
        ];
        DB::table('penetration_types')->insert($penetrations);
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }
}
