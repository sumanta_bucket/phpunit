<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionsRoleTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('roles')->truncate();
        DB::table('permissions')->truncate();
        DB::table('role_has_permissions')->truncate();

        $admin         = Role::create(['name' => 'admin']);
        $planner       = Role::create(['name' => 'planner']);
        $employee      = Role::create(['name' => 'employee']);
        $customer      = Role::create(['name' => 'customer']);
        $consultant    = Role::create(['name' => 'consultant']);
        $contactPerson = Role::create(['name' => 'contact-person']);

        // Create Permissions
        Permission::create(['name' => 'view_projects']);
        Permission::create(['name' => 'add_project']);
        Permission::create(['name' => 'edit_project']);
        Permission::create(['name' => 'view_documentation_pdf']);
        Permission::create(['name' => 'create_floor_map']);
        Permission::create(['name' => 'add_delete_pdf_in_floor_map']);
        Permission::create(['name' => 'add_marker']);
        Permission::create(['name' => 'edit_marker']);
        Permission::create(['name' => 'delete_marker']);
        Permission::create(['name' => 'delete_project']);
        Permission::create(['name' => 'view_markers']);
        Permission::create(['name' => 'view_project_information']);
        Permission::create(['name' => 'add_project_information_pdf']);
        Permission::create(['name' => 'delete_project_information_pdf']);
        
        Permission::create(['name' => 'view_customers']);
        Permission::create(['name' => 'add_customer']);
        Permission::create(['name' => 'archive_customer']);
        Permission::create(['name' => 'view_contact_persons']);
        Permission::create(['name' => 'add_contact_person']);
        Permission::create(['name' => 'archive_contact_person']);
        Permission::create(['name' => 'view_employees']);
        Permission::create(['name' => 'add_employee']);
        Permission::create(['name' => 'edit_employee']);
        Permission::create(['name' => 'archive_employee']);
        Permission::create(['name' => 'manage_settings']);
        Permission::create(['name' => 'edit_customer']);
        Permission::create(['name' => 'edit_contact_person']);

        // give permission to role admin
        $admin_permissions = config('auth.user_permissions');
        $admin->givePermissionTo($admin_permissions);

        //give permissions to  role planner
        $planner_permissions = config('auth.planner_default_permissions');
        $planner->givePermissionTo($planner_permissions);

        //give permissions to the role employee
        $employee_permissions = config('auth.employee_default_permissions');
        $employee->givePermissionTo($employee_permissions);

        //give permissions to the role customer
        $customer_permissions = config('auth.customer_default_permission');
        $customer->givePermissionTo($customer_permissions);

        //give permissions to the role contact-person
        $contact_person_permissions = config('auth.contact_person_default_permission');
        $contactPerson->givePermissionTo($contact_person_permissions);

        //give permissions to role consultant
        $consultant_permissions = config('auth.consultant_default_permission');
        $consultant->givePermissionTo($consultant_permissions);

        //enable foreign key check
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}