<?php

use App\Models\Category;
use App\Models\ConsultantTitle;
use App\Models\ContactPerson;
use App\Models\Customer;
use App\Models\Floor;
use App\Models\Project;
use App\Models\User;
use App\Models\Workplace;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class ProjectDocumentationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i < 10; $i++) {
            $user = User::create([
                'first_name' => str_random(10),
                'email' => str_random(12) . '@mail.com',
                'password' => Hash::make('demo123@'),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);

            $customer = Customer::create([
                'id' => $i,
                'user_id' => $user->id,
                'invoice_email' => str_random(12) . '@mail.com',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
            $workplace = Workplace::create([
                'id' => $i,
                'name' => str_random(12),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
            $projectData = Project::create([
                'id' => $i,
                'user_id' => $user->id,
                'customer_id' => $customer->id,
                'workplace_id' => $workplace->id,
                'name' => str_random(12),
                'project_number' => 12313,
                'status' => 1,
                'old_penetrations' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);



        }
    }
}
