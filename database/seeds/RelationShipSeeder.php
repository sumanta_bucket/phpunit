<?php

use App\Models\Relationship;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class RelationShipSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i < 10; $i++) {
            Relationship::create([
//                'id' => $i,
                'name' => str_random(12),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);

        }
    }
}
