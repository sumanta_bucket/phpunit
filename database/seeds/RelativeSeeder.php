<?php

use App\Models\Employee;
use App\Models\Relationship;
use App\Models\Relative;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class RelativeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($i = 1; $i < 10; $i++) {
            Relationship::create([
//                'id' => $i,
                'name' => str_random(12),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
            Relative::insert([
//                'id' => $i,
                'first_name' => str_random(12),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
            $user = User::create([
                'first_name' => str_random(10),
                'email' => str_random(12) . '@mail.com',
                'password' => Hash::make('demo123@'),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);

            Employee::insert([
                'id' => $i,
                'user_id' => $user->id,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);


        }
    }
}
