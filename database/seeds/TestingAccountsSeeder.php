<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\EmailTemplate;
use App\Models\Employee;
use App\Models\Customer;
use App\Models\ConsultantCompany;
use App\Models\Consultant;
use App\Models\Translation;

class TestingAccountsSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin          = User::create([
                'first_name' => 'LZ',
                'middle_name' => '',
                'last_name' => 'Admin',
                'email' => 'lz_admin@yopmail.com',
                'password' => Hash::make('lzdemo123@'),
                'status' => 1,
                'confirmation_code' => md5(uniqid(mt_rand(), true))
        ]);
        $employee       = User::create([
                'first_name' => 'LZ',
                'middle_name' => '',
                'last_name' => 'Employee',
                'email' => 'lz_employee@yopmail.com',
                'password' => Hash::make('lzdemo123@'),
                'status' => 1,
                'confirmation_code' => md5(uniqid(mt_rand(), true))
        ]);
        $customer       = User::create([
                'first_name' => 'LZ',
                'middle_name' => '',
                'last_name' => 'Customer',
                'email' => 'lz_customer@yopmail.com',
                'password' => Hash::make('lzdemo123@'),
                'status' => 1,
                'confirmation_code' => md5(uniqid(mt_rand(), true))
        ]);
        $contact_person = User::create([
                'first_name' => 'LZ',
                'middle_name' => '',
                'last_name' => 'Contact Person',
                'email' => 'lz_contactperson@yopmail.com',
                'password' => Hash::make('lzdemo123@'),
                'status' => 1,
                'confirmation_code' => md5(uniqid(mt_rand(), true))
        ]);
        $consultant     = User::create([
                'first_name' => 'LZ',
                'middle_name' => '',
                'last_name' => 'Consultant',
                'email' => 'lz_consultant@yopmail.com',
                'password' => Hash::make('lzdemo123@'),
                'status' => 1,
                'confirmation_code' => md5(uniqid(mt_rand(), true))
        ]);

        //admin assign role and permissions
        $admin->assignRole('admin');
        $admin_permissions          = config('auth.user_permissions');
        $admin->givePermissionTo($admin_permissions);
        //employee assign role and permissions
        $employee->assignRole('employee');
        $employee_permissions       = config('auth.employee_default_permissions');
        $employee->givePermissionTo($employee_permissions);
        $employee->employee()->create();
        //customer assign role and permissions
        $customer->assignRole('customer');
        $customer_permissions       = config('auth.customer_default_permission');
        $customer->givePermissionTo($customer_permissions);
        Customer::create(['user_id' => $customer->id, 'invoice_email' => 0]);
        //contact person assign role and permissions
        $contact_person->assignRole('contact-person');
        $contact_person_permissions = config('auth.contact_person_default_permission');
        $contact_person->givePermissionTo($contact_person_permissions);
        $contact_person->contact_person()->create();
        //consultant assign role and permissions
        $consultant->assignRole('consultant');
        $consultant_permissions     = config('auth.consultant_default_permission');
        $consultant->givePermissionTo($consultant_permissions);
        $company                    = ConsultantCompany::create(['name' => 'test company']);
        Consultant::create(['user_id' => $consultant->id, 'consultant_company_id' => $company->id]);
        $company->delete();
        $template                   = EmailTemplate::where('id', 1)->first();
        $template->template         = 'Hi, please click this link to reset your password {{RESET_PASSWORD_LINK}} , IP Address {{IP_ADDRESS}}.This link will be expired in 30 minutes.';
        $template->save();

        Translation::where('short_name', 'beam_pilar')->update(['english' => "Floor/Ceiling/Wall",
            'svenska' => "Golv/Tak/Vägg"]);
        Translation::create(['short_name' => "electrical", 'english' => "Electrical",
            'svenska' => "Elektrisk"]);
        Translation::create(['short_name' => "spinkler", 'english' => "Spinkler",
            'svenska' => "Spinkler"]);
        Translation::create(['short_name' => "control", 'english' => "Control",
            'svenska' => "Kontrollera"]);
    }
}