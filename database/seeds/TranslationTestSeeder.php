<?php

use App\Models\Translation;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class TranslationTestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i < 10; $i++) {
            Translation::insert([
                'id' => $i,
                'short_name' => str_random(12),
                'english' => str_random(12),
                'svenska' => str_random(12),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);

        }
    }
}
