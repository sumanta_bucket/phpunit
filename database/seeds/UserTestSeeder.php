<?php

use App\Models\Permission;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserTestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        // Register the seeder
        $this->call(PermissionsRoleTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(EmailTemplateTypesTableSeeder::class);
        $this->call(TranslationsTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(MaterialTypesTableSeeder::class);
        $this->call(PenetrationTypesTableSeeder::class);
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        Model::reguard();

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('users')->truncate();
        DB::table('role_user')->truncate();
        DB::table('permission_user')->truncate();
        $user = User::create([
            'first_name' => 'Daniel',
            'middle_name' => 'Holm Jalvander',
            'last_name' => '',
            'email' => 'admin@bcbrand.com',
            'password' => Hash::make('demo123@'),
            'status' => 1,
            'language'=>1,
            'confirmation_code' => md5(uniqid(mt_rand(), true))
        ]);
        $user1 = User::create([
            'first_name' => 'Daniel',
            'middle_name' => 'Holm Jalvander',
            'last_name' => '',
            'email' => 'custom@yopmail.com',
            'password' => Hash::make('123456'),
            'status' => 1,
            'language'=>1,
            'confirmation_code' => md5(uniqid(mt_rand(), true))
        ]);
        $user2 = User::create([
            'first_name' => 'Daniel',
            'middle_name' => 'Holm Jalvander',
            'last_name' => '',
            'email' => 'emp1@yopmail.com',
            'password' => Hash::make('123456'),
            'status' => 1,
            'language'=>1,
            'confirmation_code' => md5(uniqid(mt_rand(), true))
        ]);
        $permission = Permission::select('id')->get();
        $user->assignRole('admin');
        $user->permissions()->attach($permission);
        $user1->assignRole('customer');
        $customer_permissions = config('auth.customer_default_permission');
        $user1->givePermissionTo($customer_permissions);
        $user2->assignRole('employee');
        $emp_permissions = config('auth.customer_default_permission'); //assign permissions to user
        $user2->givePermissionTo($emp_permissions);
//        $user2->permissions()->attach(config('auth.employee_default_permissions'));
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
