<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Permission;

class UsersTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('users')->truncate();
        DB::table('role_user')->truncate();
        DB::table('permission_user')->truncate();
        $user       = User::create([
                'first_name' => 'Daniel',
                'middle_name' => 'Holm Jalvander',
                'last_name' => '',
                'email' => 'admin@bcbrand.com',
                'password' => Hash::make('demo123@'),
                'status' => 1,
                'confirmation_code' => md5(uniqid(mt_rand(), true))
        ]);
        $user1       = User::create([
                'first_name' => 'Daniel',
                'middle_name' => 'Holm Jalvander',
                'last_name' => '',
                'email' => 'admin1@bcbrand.com',
                'password' => Hash::make('demo123@'),
                'status' => 1,
                'confirmation_code' => md5(uniqid(mt_rand(), true))
        ]);
        $user2       = User::create([
                'first_name' => 'Daniel',
                'middle_name' => 'Holm Jalvander',
                'last_name' => '',
                'email' => 'admin2@bcbrand.com',
                'password' => Hash::make('demo123@'),
                'status' => 1,
                'confirmation_code' => md5(uniqid(mt_rand(), true))
        ]);
        $user3       = User::create([
                'first_name' => 'Daniel',
                'middle_name' => 'Holm Jalvander',
                'last_name' => '',
                'email' => 'admin3@bcbrand.com',
                'password' => Hash::make('demo123@'),
                'status' => 1,
                'confirmation_code' => md5(uniqid(mt_rand(), true))
        ]);
        $user4       = User::create([
                'first_name' => 'Daniel',
                'middle_name' => 'Holm Jalvander',
                'last_name' => '',
                'email' => 'admin4@bcbrand.com',
                'password' => Hash::make('demo123@'),
                'status' => 1,
                'confirmation_code' => md5(uniqid(mt_rand(), true))
        ]);
        $user5      = User::create([
                'first_name' => 'Daniel',
                'middle_name' => 'Holm Jalvander',
                'last_name' => '',
                'email' => 'admin5@bcbrand.com',
                'password' => Hash::make('demo123@'),
                'status' => 1,
                'confirmation_code' => md5(uniqid(mt_rand(), true))
        ]);
        $permission = Permission::select('id')->get();
        $user->assignRole('admin');
        $user->permissions()->attach($permission);
        $user1->assignRole('admin');
        $user1->permissions()->attach($permission);
        $user2->assignRole('admin');
        $user2->permissions()->attach($permission);
        $user3->assignRole('admin');
        $user3->permissions()->attach($permission);
        $user4->assignRole('admin');
        $user4->permissions()->attach($permission);
        $user5->assignRole('admin');
        $user5->permissions()->attach($permission);
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}