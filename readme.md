1. git clone 
2. composer install 
3. copy .env.example file to .env
4. update database credentials and E-mail credentials and other required details in .env file 
5. Run php artisan migrate --seed
6. install php Imagick if not already installed.use following steps:
    * sudo apt-get install php-imagick
    * php -m | grep imagick
    * sudo service apache2 restart
7. After this enable the pdf read write permissions in imagick policy.xml file .Follow the following steps:
    * sudo nano /etc/ImageMagick-6/policy.xml
    * policy domain="coder" rights="none" pattern="PDF” update rights to ”read|write”.
    * Restart apache server.
8. Redis configuration for queue and cache 
    * For this please make sure the redis is properly setup on your server
    * set CACHE_DRIVER=redis
    * QUEUE_DRIVER=redis
    * REDIS_HOST=127.0.0.1
    * REDIS_PASSWORD=null
    * REDIS_PORT=6379
9. Assign 777 permissions to public, bootstarp and storage folder.
10. Install Supervisor
11. Setup supervisor worker for email queues by using following code and start this worker:
    * [program:laravel-worker]
    * process_name=%(program_name)s_%(process_num)02d
    * command=php /APPLICATION_PATH/artisan queue:work redis --sleep=3 --tries=3
    * autostart=true
    * autorestart=true
    * ;user=forge
    * ;numprocs=8
    * redirect_stderr=true
    * stdout_logfile=/var/log/supervisor/error.log
