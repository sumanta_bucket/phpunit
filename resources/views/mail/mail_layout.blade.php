<div style="background-color: #e1e8ef; padding-top: 25px;">
<header>
    <p style="padding: 25px 0 15px;text-align:center;font-weight:bold;color:#bbbfc3;font-size:19px;margin-top:0;">
        <img src="{{ URL::to('/') }}/img/login-logo-new.png" width="64px" alt="logo">

    </p>
</header>
<div style="padding: 20px 20px 20px; border:1px solid #fff; width: 65%; margin: 25px auto 0; background-color: #fff;">
<div style="line-height: 1.5;">
    {!! $content !!}
</div>
</div>
<footer>
<p style="text-align:center;color:#424242;padding:25px 0;font-size:12px;">Copyright &copy; {{date('Y')}}</p>
</footer>
</div>