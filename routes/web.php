<?php
/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It is a breeze. Simply tell Lumen the URIs it should respond to
  | and give it the Closure to call when that URI is requested.
  |
 */
$router->get('/',
    function () use ($router) {
    return $router->app->version();
});

/* get api key route */
$router->get('/key', function() {
    return str_random(32);
});

$router->group(
    ['prefix' => 'v1'],
    function() use ($router) {
    $router->post('register',
        ['uses' => 'RegisterController@store', 'as' => 'users.create']);
    $router->get('getTranslations',
        ['uses' => 'Settings\TranslationController@getTranslation', 'as' => 'translations.fetch']);
    /* Auth routes */
    $router->group(['prefix' => 'auth'],
        function () use ($router) {
        $router->post('login',
            ['uses' => 'Auth\AuthController@authenticate', 'as' => 'users.login']);
        $router->get('confirmAccount/{token}',
            ['uses' => 'Auth\ConfirmAccountController@confirm', 'as' => 'users.confirm_account']);

        /* Reset password routes */
        $router->group(['prefix' => 'password'],
            function () use ($router) {
            $router->post('email',
                ['uses' => 'Auth\PasswordController@sendPasswordResetLink', 'as' => 'passwords.send']);
            $router->post('reset',
                ['uses' => 'Auth\PasswordController@resetPassword', 'as' => 'passwords.reset']);
        });
    });
    /* Routes that require token authentication */
    $router->group(
        ['middleware' => 'jwt.auth'],
        function() use ($router) {
        //get user transation
//        $router->get('getLanguageTranslation',
//            ['uses' => 'Settings\TranslationController@getLanguageTranslation']);
        $router->get('user/update/language/{lang}',
            ['uses' => 'Settings\UserController@changeUserLanguage', 'as' => 'user.change_language']);
        $router->get('defaultData[/{type}]',
            ['uses' => 'Settings\OtherSettingController@getDefaultData', 'as' => 'otherSettings.defaultData']);
        /* get translated string test route */
        $router->group(['middleware' => 'permission_check:manage_settings'],
            function() use ($router) {
            $router->post('translations',
                ['uses' => 'Settings\TranslationController@index', 'as' => 'translations.index']);
            $router->post('translations/add',
                ['uses' => 'Settings\TranslationController@store', 'as' => 'translations.store']);
            $router->get('translations/get/{id}',
                ['uses' => 'Settings\TranslationController@edit', 'as' => 'translations.edit']);
            $router->put('translations/update/{id}',
                ['uses' => 'Settings\TranslationController@update', 'as' => 'translations.update']);
            $router->delete('translations/delete/{id}',
                ['uses' => 'Settings\TranslationController@destroy', 'as' => 'translations.delete']);

            /* routes related to email template */
            $router->post('emailTemplates',
                ['uses' => 'Settings\EmailTemplateController@index', 'as' => 'templates.index']);
            $router->post('emailTemplates/add',
                ['uses' => 'Settings\EmailTemplateController@store', 'as' => 'templates.store']);
            $router->put('emailTemplates/update/{id}',
                ['uses' => 'Settings\EmailTemplateController@update', 'as' => 'templates.update']);
            $router->get('emailTemplates/get/{id}',
                ['uses' => 'Settings\EmailTemplateController@edit', 'as' => 'templates.edit']);
            $router->delete('emailTemplates/delete/{id}',
                ['uses' => 'Settings\EmailTemplateController@destroy', 'as' => 'templates.delete']);
            $router->get('emailTemplates/getEmailType/{type}',
                ['uses' => 'Settings\EmailTemplateController@getEmailTemplateType',
                'as' => 'templates.getEmailType']);
            $router->get('emailTemplates/getPlaceholders/{id}',
                ['uses' => 'Settings\EmailTemplateController@getEmailTypePlaceholders',
                'as' => 'templates.getPlaceholders']);
            $router->post('emailTemplates/testSmsTemplate',
                ['uses' => 'Settings\EmailTemplateController@testSmsTemplate', 'as' => 'templates.testSms']);
            $router->post('emailTemplates/testEmailTemplate',
                ['uses' => 'Settings\EmailTemplateController@testEmailTemplate',
                'as' => 'templates.testEmailTemplate']);
            $router->put('emailTemplates/change_default/{id}',
                ['uses' => 'Settings\EmailTemplateController@changeDefaultTemplate',
                'as' => 'templates.changeDefaultTemplate']);

            /* routes related to consultants */
            $router->post('consultants',
                ['uses' => 'Settings\ConsultantController@index', 'as' => 'consultant.index']);
            $router->post('consultants/add',
                ['uses' => 'Settings\ConsultantController@store', 'as' => 'consultant.store']);
            $router->get('consultants/get/{id}',
                ['uses' => 'Settings\ConsultantController@edit', 'as' => 'consultant.get']);
            $router->put('consultants/update/{id}',
                ['uses' => 'Settings\ConsultantController@update', 'as' => 'consultant.update']);
            $router->delete('consultants/delete/{id}',
                ['uses' => 'Settings\ConsultantController@destroy', 'as' => 'consultant.delete']);

            /* Other Settings common routes */
            $router->post('other_settings/get/{section}[/{type}]',
                ['uses' => 'Settings\OtherSettingController@index', 'as' => 'otherSettings.index']);
            $router->post('other_settings/add/{section}[/{type}]',
                ['uses' => 'Settings\OtherSettingController@store', 'as' => 'otherSettings.store']);
            $router->get('other_settings/edit/{id}/{section}',
                ['uses' => 'Settings\OtherSettingController@edit', 'as' => 'otherSettings.get']);
            $router->put('other_settings/update/{id}/{section}[/{type}]',
                ['uses' => 'Settings\OtherSettingController@update', 'as' => 'otherSettings.update']);
            $router->post('other_settings/update/{id}/{section}[/{type}]',
                ['uses' => 'Settings\OtherSettingController@update', 'as' => 'otherSettings.update']);
            $router->delete('other_settings/delete/{id}/{section}',
                ['uses' => 'Settings\OtherSettingController@destroy', 'as' => 'otherSettings.delete']);

            /* routes related to options crud */
            $router->post('options/get/{option_name}',
                ['uses' => 'Settings\OptionController@index', 'as' => 'option.get']);
            $router->post('options/add',
                ['uses' => 'Settings\OptionController@store', 'as' => 'option.create']);
            $router->put('options/update/{id}',
                ['uses' => 'Settings\OptionController@update', 'as' => 'option.update']);
            $router->delete('options/delete/{id}',
                ['uses' => 'Settings\OptionController@destroy', 'as' => 'option.delete']);

            /* routes related to consultant title and consltant company */
            $router->post('consultant_company',
                ['uses' => 'Settings\ConsultantCompanyController@index', 'as' => 'consultant_company.get']);
            $router->post('consultant_company/add',
                ['uses' => 'Settings\ConsultantCompanyController@store', 'as' => 'consultant_company.store']);
            $router->put('consultant_company/update/{id}',
                ['uses' => 'Settings\ConsultantCompanyController@update', 'as' => 'consultant_company.update']);
            $router->delete('consultant_company/delete/{id}',
                ['uses' => 'Settings\ConsultantCompanyController@delete', 'as' => 'consultant_company.delete']);

            /* routes related to consultant title */
            $router->post('consultant_title',
                ['uses' => 'Settings\ConsultantTitleController@index', 'as' => 'consultant_title.get']);
            $router->post('consultant_title/add',
                ['uses' => 'Settings\ConsultantTitleController@store', 'as' => 'consultant_title.store']);
            $router->put('consultant_title/update/{id}',
                ['uses' => 'Settings\ConsultantTitleController@update', 'as' => 'consultant_title.update']);
            $router->delete('consultant_title/delete/{id}',
                ['uses' => 'Settings\ConsultantTitleController@delete', 'as' => 'consultant_title.delete']);

            /* routes related to workplace */
            $router->post('workplaces',
                ['uses' => 'Settings\WorkplaceController@index', 'as' => 'workplaces.get']);
            $router->post('workplaces/add',
                ['uses' => 'Settings\WorkplaceController@store', 'as' => 'workplaces.store']);
            $router->put('workplaces/update/{id}',
                ['uses' => 'Settings\WorkplaceController@update', 'as' => 'workplaces.update']);
            $router->delete('workplaces/delete/{id}',
                ['uses' => 'Settings\WorkplaceController@destroy', 'as' => 'workplaces.delete']);

            /* routes related to employee relationship */
            $router->post('relationships',
                ['uses' => 'Settings\RelationshipController@index', 'as' => 'relationship.index']);
            $router->post('relationships/add',
                ['uses' => 'Settings\RelationshipController@store', 'as' => 'relationship.store']);
            $router->get('relationships/edit/{id}',
                ['uses' => 'Settings\RelationshipController@edit', 'as' => 'relationship.edit']);
            $router->put('relationships/update/{id}',
                ['uses' => 'Settings\RelationshipController@update', 'as' => 'relationship.update']);
            $router->delete('relationships/delete/{id}',
                ['uses' => 'Settings\RelationshipController@destroy', 'as' => 'relationship.destroy']);

            /* routes related relatives */
            $router->post('relatives',
                ['uses' => 'Settings\RelativeController@index', 'as' => 'relative.index']);
            $router->post('relatives/add',
                ['uses' => 'Settings\RelativeController@store', 'as' => 'relative.store']);
            $router->post('relatives/update/{id}',
                ['uses' => 'Settings\RelativeController@update', 'as' => 'relative.update']);
            $router->delete('relatives/delete/{id}',
                ['uses' => 'Settings\RelativeController@destroy', 'as' => 'relative.destroy']);
            $router->get('relationships_for_realtives',
                ['uses' => 'Settings\RelativeController@getRelationships', 'as' => 'relative.relationships']);
        });
        /* routes related to projects */
        $router->post('projects/index/{status}',
            ['middleware' => ['permission_check:view_projects'], 'uses' => 'Settings\ProjectController@index',
            'as' => 'project.index']);
        $router->post('projects/add',
            ['middleware' => ['permission_check:add_project'], 'uses' => 'Settings\ProjectController@store',
            'as' => 'project.store']);
        $router->get('projects/edit/{id}',
            ['middleware' => ['permission_check:edit_project'], 'uses' => 'Settings\ProjectController@edit',
            'as' => 'project.edit']);
        $router->put('projects/update/{id}',
            ['middleware' => ['permission_check:edit_project'], 'uses' => 'Settings\ProjectController@update',
            'as' => 'project.update']);
        $router->delete('projects/delete/{id}',
            ['middleware' => ['permission_check:edit_project'], 'uses' => 'Settings\ProjectController@destroy',
            'as' => 'project.delete']);
        $router->get('projects/resolver[/{project_id}]',
            ['uses' => 'Settings\ProjectController@getRequiredDetails',
            'as' => 'project.getDetails']);
        $router->get('projects/information/{project_id}',
            ['uses' => 'Settings\ProjectController@getProjectInformation',
            'as' => 'project.getDocumentInformation']);
        $router->get('projects/restore/{id}',
            ['middleware' => ['permission_check:edit_project'], 'uses' => 'Settings\ProjectController@restore',
            'as' => 'project.restore']);

        /* Project documentation related routes */
        $router->post('project/{project_id}/floor/add',
            ['middleware' => ['permission_check:create_floor_map'], 'uses' => 'Settings\ProjectDocumentationController@store',
            'as' => 'floors.store']);
        $router->post('project/{project_id}/floor/{floor_id}/section/add',
            ['middleware' => ['permission_check:create_floor_map'], 'uses' => 'Settings\ProjectDocumentationController@addSection',
            'as' => 'section.store']);
        $router->delete('project/{project_id}/floor/delete/{id}',
            ['middleware' => ['permission_check:create_floor_map'], 'uses' => 'Settings\ProjectDocumentationController@deleteFloor',
            'as' => 'floor.delete']);
        $router->delete('project/{project_id}/delete_all_floor',
            ['middleware' => ['permission_check:create_floor_map'], 'uses' => 'Settings\ProjectDocumentationController@deleteAllFloors',
            'as' => 'floor.deleteAll']);
        $router->delete('project/{project_id}/section/delete/{section_id}',
            ['middleware' => ['permission_check:create_floor_map'], 'uses' => 'Settings\ProjectDocumentationController@deleteSection',
            'as' => 'section.delete']);
        $router->post('project/{project_id}/section/{section_id}/pdf/add',
            ['middleware' => ['permission_check:add_delete_pdf_in_floor_map'], 'uses' => 'Settings\ProjectDocumentationController@uploadSectionDocument',
            'as' => 'section.addPdf']);
        $router->delete('project/{project_id}/section/pdf/delete/{section_id}/{type}',
            ['middleware' => ['permission_check:add_delete_pdf_in_floor_map'], 'uses' => 'Settings\ProjectDocumentationController@deleteSectionDocument',
            'as' => 'section.deletePdf']);
        $router->post('project/{project_id}/section/upload_all_pdf',
            ['middleware' => ['permission_check:add_delete_pdf_in_floor_map'], 'uses' => 'Settings\ProjectDocumentationController@uploadSectionAllPdf',
            'as' => 'section.uploadAllPdfs']);
        $router->delete('project/{project_id}/section/delete_all_pdf/{type}',
            ['middleware' => ['permission_check:add_delete_pdf_in_floor_map'], 'uses' => 'Settings\ProjectDocumentationController@deleteSectionAllPdf',
            'as' => 'section.deleteAllPdfs']);
        $router->get('project/section/markers/{section_id}/{type}',
            ['middleware' => ['permission_check:view_documentation_pdf'], 'uses' => 'Settings\ProjectDocumentationController@getSectionMarkers',
            'as' => 'section.markers']);
        $router->post('project/{project_id}/change_active_status',
            ['middleware' => ['permission_check:view_documentation_pdf'], 'uses' => 'Settings\ProjectDocumentationController@changeFloorSectionActiveState',
            'as' => 'updateStatus']);
        $router->post('project/last_section',
            ['middleware' => ['permission_check:view_documentation_pdf'], 'uses' => 'Settings\ProjectDocumentationController@updateLastSection',
            'as' => 'last_section']);
        $router->get('project/{project_id}/last_section/{type}',
            ['middleware' => ['permission_check:view_documentation_pdf'], 'uses' => 'Settings\ProjectDocumentationController@getLastSection',
            'as' => 'lastsection.get']);

        /* routes related to marker */
        $router->get('markers/get/{type}',
            ['middleware' => ['permission_check:view_documentation_pdf'], 'uses' => 'Settings\MarkerController@getRequiredDataForNewMarker',
            'as' => 'marger.getInfo']);
        $router->post('sections/{section_id}/markers/add/{type}',
            ['middleware' => ['permission_check:add_marker'], 'uses' => 'Settings\MarkerController@store',
            'as' => 'marker.store']);
        $router->post('sections/{section_id}/markers/update/{id}/{type}',
            ['middleware' => ['permission_check:edit_marker'], 'uses' => 'Settings\MarkerController@update',
            'as' => 'marker.update']);
        $router->delete('markers/delete/{id}/{type}',
            ['middleware' => ['permission_check:delete_marker'], 'uses' => 'Settings\MarkerController@destroy',
            'as' => 'marker.delete']);
        $router->delete('marker_photo/delete/{id}/{type}',
            ['middleware' => ['permission_check:add_marker'], 'uses' => 'Settings\MarkerController@deleteMarkerPhoto',
            'as' => 'marker.photo.delete']);

        /* routes related to customers */
        $router->post('customers/index/{status}',
            ['middleware' => ['permission_check:view_customers'], 'uses' => 'Settings\CustomerController@index',
            'as' => 'customers.index']);
        $router->post('customers/add',
            ['middleware' => ['permission_check:add_customer'], 'uses' => 'Settings\CustomerController@store',
            'as' => 'customers.store']);
        $router->get('customers/get/{id}',
            ['middleware' => ['permission_check:edit_customer'], 'uses' => 'Settings\CustomerController@edit',
            'as' => 'customers.get']);
        $router->put('customers/update/{id}',
            ['middleware' => ['permission_check:edit_customer'], 'uses' => 'Settings\CustomerController@update',
            'as' => 'customers.update']);
        $router->delete('customers/delete/{id}',
            ['middleware' => ['permission_check:archive_customer'], 'uses' => 'Settings\CustomerController@destroy',
            'as' => 'customers.delete']);
        $router->get('customers/{customer_id}/detach_contact_person/{contact_person_id}',
            ['middleware' => ['permission_check:archive_customer'], 'uses' => 'Settings\CustomerController@detachContacts',
            'as' => 'customers.detachContacts']);
        $router->get('customers/restore/{id}',
            ['middleware' => ['permission_check:archive_customer'], 'uses' => 'Settings\CustomerController@restoreCustomers',
            'as' => 'customer.restore']);

        /* routes related to the employees */
        $router->post('employees/index/{status}',
            ['middleware' => ['permission_check:view_employees'], 'uses' => 'Settings\EmployeeController@index',
            'as' => 'employee.index']);
        $router->post('employees/add',
            ['middleware' => ['permission_check:add_employee'], 'uses' => 'Settings\EmployeeController@store',
            'as' => 'employee.store']);
        $router->get('employees/edit/{id}',
            ['middleware' => ['permission_check:edit_employee'], 'uses' => 'Settings\EmployeeController@edit',
            'as' => 'employee.get_records']);
        $router->put('employees/update/{id}',
            ['middleware' => ['permission_check:edit_employee'], 'uses' => 'Settings\EmployeeController@update',
            'as' => 'employee.update']);
        $router->delete('employees/delete/{id}',
            ['middleware' => ['permission_check:archive_employee'], 'uses' => 'Settings\EmployeeController@destroy',
            'as' => 'employee.delete']);
        $router->get('employees/restore/{id}',
            ['middleware' => ['permission_check:archive_employee'], 'uses' => 'Settings\EmployeeController@restore',
            'as' => 'employee.restore']);

        /* routes related to contact persons */
        $router->post('contact_persons/index/{status}',
            ['middleware' => ['permission_check:view_contact_persons'], 'uses' => 'Settings\ContactPersonController@index',
            'as' => 'contact_person.index']);
        $router->get('contact_persons/get_titles',
            ['middleware' => ['permission_check:add_contact_person'], 'uses' => 'Settings\ContactPersonController@getContactPersonTitles',
            'as' => 'contact_person.titles']);
        $router->post('contact_persons/add',
            ['middleware' => ['permission_check:add_contact_person'], 'uses' => 'Settings\ContactPersonController@store',
            'as' => 'contact_person.store']);
        $router->get('contact_persons/edit/{id}',
            ['middleware' => ['permission_check:edit_contact_person'], 'uses' => 'Settings\ContactPersonController@edit',
            'as' => 'contact_person.edit']);
        $router->post('contact_persons/update/{id}',
            ['middleware' => ['permission_check:edit_contact_person'], 'uses' => 'Settings\ContactPersonController@update',
            'as' => 'contact_person.update']);
        $router->delete('contact_persons/delete/{id}',
            ['middleware' => ['permission_check:archive_contact_person'], 'uses' => 'Settings\ContactPersonController@destroy',
            'as' => 'contact_person.delete']);
        $router->get('contact_persons/change_status/{user_id}/{status}',
            ['middleware' => ['permission_check:edit_contact_person'], 'uses' => 'Settings\ContactPersonController@changeUserStatus',
            'as' => 'contact_person.change_status']);
        $router->get('contact_persons/restore/{id}',
            ['middleware' => ['permission_check:archive_contact_person'], 'uses' => 'Settings\ContactPersonController@restore',
            'as' => 'contact_person.restore']);

        /* user settings related end points */
        $router->post('users',
            ['middleware' => ['permission_check:manage_settings'], 'uses' => 'Settings\UserController@index',
            'as' => 'user.index']);
        $router->post('user/upload_photo',
            ['uses' => 'Settings\UserController@uploadPicture', 'as' => 'user.upload_photo']);
        $router->post('user/profile/update',
            ['uses' => 'Settings\UserController@updateProfile', 'as' => 'user.profile.update']);
        $router->post('user/change_password',
            ['uses' => 'Settings\UserController@changePassword', 'as' => 'user.change_password']);
        $router->post('user/edit_details',
            ['middleware' => ['permission_check:manage_settings'], 'uses' => 'Settings\UserController@editUser',
            'as' => 'user.update.user']);
        $router->get('users/change_status/{user_id}/{status}',
            ['middleware' => ['permission_check:manage_settings'], 'uses' => 'Settings\UserController@changeUserStatus',
            'as' => 'contact_person.change_status']);
        $router->get('users/roles_permissions/{user_id}',
            ['middleware' => ['permission_check:manage_settings'], 'uses' => 'Settings\UserController@userRolesPermissions',
            'as' => 'contact_person.roles_permissions']);
        $router->get('user/data',
            ['uses' => 'Settings\UserController@getUserDetails', 'as' => 'user.info']);
        $router->get('users/notify/{user_id}',
            ['uses' => 'Settings\UserController@notifyUser', 'as' => 'user.notify']);
        /* admin dashboard routes for user activity */
        $router->get('dashboard[/{activities}]',
            ['uses' => 'DashboardController@index', 'as' => 'user.dashboard']);

        /* project information documents route */
        $router->post('project/documents/{project_id}',
            ['uses' => 'Settings\DocumentController@uploadDocument', 'as' => 'document.project']);
        $router->delete('project/document/delete/{id}',
            ['uses' => 'Settings\DocumentController@deleteDocument', 'as' => 'project.document.delete']);

        $router->get('logout',
            ['uses' => 'Auth\AuthController@logout', 'as' => 'user.logout']);
    });
    $router->get('document/{path}',
        ['uses' => 'Settings\DocumentController@getDocument', 'as' => 'document.getFile']);
});

