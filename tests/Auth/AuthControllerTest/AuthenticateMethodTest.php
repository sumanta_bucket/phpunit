<?php

/**
 * AuthenticateMethodTest class
 * Include all possible test cases for Authenticate Methods
 */
class AuthenticateMethodTest extends TestCase
{

    /**
     * Case 1
     * This function test response without params
     *
     */
    public function testCaseOne()
    {
        app(DatabaseSeeder::class)->call(UserTestSeeder::class);
        $response = $this->call('POST', '/v1/auth/login', [], [], [], []);

        //check response status code
        $this->assertResponseStatus(422);
//
//        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'email'], 1)],
            $json['email']);
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'password'], 1)],
            $json['password']);
    }

    /**
     * Case 2
     * This function test response without email
     *
     */
    public function testCaseTwo()
    {

        $param = [
            'password' => 'demmo@123'
        ];

        $response = $this->call('POST', '/v1/auth/login', $param, [], [], []);
        //check response status code
        $this->assertResponseStatus(422);
//        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'email'], 1)],
            $json['email']);


    }

    /**
     * Case 3
     * This function test response without password
     *
     */
    public function testCaseThree()
    {
        $param = [
            'email' => 'demmo@gmail.com'
        ];

        $response = $this->call('POST', '/v1/auth/login', $param, [], [], []);
        //check response status code
        $this->assertResponseStatus(422);
//        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'password'], 1)],
            $json['password']);


    }

    /**
     * Case 4
     * This function test response without invalid email
     *
     */
    public function testCaseFour()
    {
        $param = [
            'email' => 'demmogmail.com',
        ];

        $response = $this->call('POST', '/v1/auth/login', $param, [], [], []);
        //check response status code
        $this->assertResponseStatus(422);
//        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value

    }

    /**
     * Case 5
     * This function test response with non exist email
     *
     */
    public function testCaseFive()
    {
        $param = [
            'email' => 'demmo@gmail.com',
        ];

        $response = $this->call('POST', '/v1/auth/login', $param, [], [], []);
        //check response status code
        $this->assertResponseStatus(422);
//        //convert response data in json
        $json = json_decode($response->getContent(), true);
    }

    /**
     * Case 6
     * This function test response with invalid password
     *
     */
    public function testCaseSix()
    {
        $param = [
            'email' => 'admin@bcbrand.com',
            'password' => 'demo123'
        ];

        $response = $this->call('POST', '/v1/auth/login', $param, [], [], []);
        //check response status code
        $this->assertResponseStatus(401);
//        //convert response data in json
        $json = json_decode($response->getContent(), true);
    }

    /**
     * Case 7
     * This function test response with valid params
     *
     */
    public function testCaseSeven()
    {
        $param = [
            'email' => 'admin@bcbrand.com',
            'password' => 'demo123@'
        ];

        $response = $this->call('POST', '/v1/auth/login', $param, [], [], []);
        //check response status code
        $this->assertResponseStatus(200);
//        //convert response data in json
        $json = json_decode($response->getContent(), true);
    }

    public function __getUserData($params)
    {
        // Find the user by email
        $user = App\Models\User::where('email', $params['email'])->with('roles')->first();
        //get the user role from roles object associated with user
        $user->role = $user->roles->pluck('name');
        //send user data as a response parameter
        $userData = $user->only([
            'id',
            'first_name',
            'last_name',
            'email',
            'language',
            'avatar',
            'role'
        ]);
        return $userData;
    }


}
