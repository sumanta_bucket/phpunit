<?php

use App\Services\TranslationService;

/**
 * ConfirmMethodTest class
 * Include all possible test cases for Confirm Methods
 */
class ConfirmMethodTest extends TestCase
{
    /**
     * Test Case 1
     * This function test response without any token
     * 
     */
    public function testCaseOne()
    {

        $this->get('/v1/auth/confirmAccount', [])
        ->seeJsonEquals([
            "error" => $this->__getTranslateString('route_not_found', [], 2)
         ]);
    } 

    /**
     * Test Case 2
     * This function test response with invalid token
     * 
     */
    public function testCaseTwo()
    {
        $this->get('/v1/auth/confirmAccount/invalidtoken', [])
        ->seeJsonEquals([
            "error" => $this->__getTranslateString('user_already_confirmed', [], 2)
         ]);
    }

    /**
     * Test Case 3
     * This function test response with valid token but account is already confirmed
     * 
     */
    public function testCaseThree()
    {

        
        $this->get('/v1/auth/confirmAccount/f4b0183afd4086f11e9392edd133df7f', [])
        ->seeJsonEquals([
            "error" => $this->__getTranslateString('user_already_confirmed', [], 2)
            ]);
    }

    /**
     * Test Case 4
     * This function test response with valid token but account is already confirmed
     * 
     */
    public function testCaseFour()
    {
        $this->get('/v1/auth/confirmAccount/85cc93f0c73ae96b927b2a6ca6323522', [])
        ->seeJsonEquals([
            "error" => $this->__getTranslateString('user_already_confirmed', [], 2)
         ]);
    }
}
