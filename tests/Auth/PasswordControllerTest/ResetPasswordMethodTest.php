<?php

/**
 * ResetPasswordMethodTest class
 * Include all possible test cases for ResetPassword Methods
 */
class ResetPasswordMethodTest extends TestCase
{
    /**
     * Test Case 1
     * This function test response without any param
     *
     */
    public function testCaseOne()
    {

        $response = $this->call('POST', '/v1/auth/password/reset', [], [], [], []);
        //check response status code
        $this->assertResponseStatus(422);

//        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute'=>'token'], 2)], $json['token']);
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute'=>'password'], 2)], $json['password']);
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute'=>'password_confirmation'], 2)], $json['password_confirmation']);
    }


    /**
     * Test Case 2
     * This function test response without token
     *
     */
    public function testCaseTwo()
    {
        $param = [
            "email" => 'demouser@host.com',
            "password" => 'QWerty!@34',
            "password_confirmation" => 'QWerty!@34',
        ];

        $response = $this->call('POST', '/v1/auth/password/reset', $param, [], [], []);
        //check response status code
        $this->assertResponseStatus(422);

//        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute'=>'token'], 2)], $json['token']);

    }

    /**
     * Test Case 3
     * This function test response without email
     *
     */
    public function testCaseThree()
    {
        $param = [
            "token" => 'saaddsafasdfdsfsadafsdfasdfjkjkjfdsgkjfd',
            "password" => 'QWerty!@34',
            "password_confirmation" => 'QWerty!@34',
        ];


        $response = $this->call('POST', '/v1/auth/password/reset', $param, [], [], []);
        //check response status code
        $this->assertResponseStatus(401);

//        //convert response data in json
        $json = json_decode($response->getContent(), true);
    }


    /**
     * Test Case 4
     * This function test response without password
     *
     */
    public function testCaseFour()
    {
        $param = [
            "token" => 'saaddsafasdfdsfsadafsdfasdfjkjkjfdsgkjfd',
            "email" => 'fdjkhg@sdf.fd',
            "password_confirmation" => 'QWerty!@34',
        ];

        $response = $this->call('POST', '/v1/auth/password/reset', $param, [], [], []);
        //check response status code
        $this->assertResponseStatus(422);

//        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute'=>'password'], 2)], $json['password']);


    }

    /**
     * Test Case 5
     * This function test response without password_confirmation
     *
     */
    public function testCaseFive()
    {
        $param = [
            "token" => 'saaddsafasdfdsfsadafsdfasdfjkjkjfdsgkjfd',
            "email" => 'fdjkhg@sdf.fd',
            "password" => 'QWerty!@34',
        ];

        $response = $this->call('POST', '/v1/auth/password/reset', $param, [], [], []);
        //check response status code
        $this->assertResponseStatus(422);

//        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute'=>'password_confirmation'], 2)], $json['password_confirmation']);



    }

    /**
     * Test Case 6
     * This function test response without password and password_confirmation
     *
     */
    public function testCaseSix()
    {
        $param = [
            "token" => 'saaddsafasdfdsfsadafsdfasdfjkjkjfdsgkjfd',
            "email" => 'fdjkhg@sdf.fd'
        ];

        $response = $this->call('POST', '/v1/auth/password/reset', $param, [], [], []);
        //check response status code
        $this->assertResponseStatus(422);

//        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute'=>'password'], 2)], $json['password']);
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute'=>'password_confirmation'], 2)], $json['password_confirmation']);


    }

    /**
     * Test Case 7
     * This function test response with different password_confirmation
     *
     */
    public function testCaseSeven()
    {
        $param = [
            "token" => 'saaddsafasdfdsfsadafsdfasdfjkjkjfdsgkjfd',
            "email" => 'fdjkhg@sdf.fd',
            "password" => 'QWerty!@34',
            "password_confirmation" => 'QWerty!@3456',
        ];

        $response = $this->call('POST', '/v1/auth/password/reset', $param, [], [], []);
        //check response status code
        $this->assertResponseStatus(422);

//        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value

    }

    /**
     * Test Case 8
     * This function test response with different password length less then 6
     *
     */
    public function testCaseEight()
    {
        $param = [
            "token" => 'saaddsafasdfdsfsadafsdfasdfjkjkjfdsgkjfd',
            "email" => 'fdjkhg@sdf.fd',
            "password" => 'QWer',
            "password_confirmation" => 'QWerty!@3456',
        ];


        $response = $this->call('POST', '/v1/auth/password/reset', $param, [], [], []);
        //check response status code
        $this->assertResponseStatus(422);

//        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([$this->__getTranslateString('min_validation', ['attribute'=>'password','min'=>6], 2)], $json['password']);




    }

}
