<?php

/**
 * SendPasswordResetLinkMethodTest class
 * Include all possible test cases for SendPasswordResetLink Methods
 */
class SendPasswordResetLinkMethodTest extends TestCase
{
    /**
     * Test Case 1
     * This function test response without any param
     *
     */
    public function testCaseOne()
    {
        $this->post('/v1/auth/password/email', [])
            ->seeJsonEquals([
                "email" => [$this->__getTranslateString('required_validation', ['attribute' => 'email'], '2')],
                "appurl" => [$this->__getTranslateString('required_validation', ['attribute' => 'appurl'], '2')]
            ]);
    }

    /**
     * Test Case 2
     * This function test response with invalid email
     *
     */
    public function testCaseTwo()
    {
        $param = [
            "email" => 'fdssdfjhsdajkhbsda'
        ];

        $this->post('/v1/auth/password/email', $param)
            ->seeJsonEquals([
                "email" => [$this->__getTranslateString('email_validation', ['attribute' => 'email'], '2')],
                "appurl" => [$this->__getTranslateString('required_validation', ['attribute' => 'appurl'], '2')]
            ]);
    }

    /**
     * Test Case 3
     * This function test response with invalid email
     *
     */
    public function testCaseThree()
    {
        $param = [
            "email" => 'fdssdfjhsdaj@khbs.da'
        ];

        $this->post('/v1/auth/password/email', $param)
            ->seeJsonEquals([
                "appurl" => [$this->__getTranslateString('required_validation', ['attribute' => 'appurl'], '2')]
            ]);
    }

    /**
     * Test Case 4
     * This function test response with invalid email
     *
     */
    public function testCaseFour()
    {
        $param = [
            "email" => 'demo113@gmail.com'
        ];

        $this->post('/v1/auth/password/email', $param)
            ->seeJsonEquals([
                "appurl" => [$this->__getTranslateString('required_validation', ['attribute' => 'appurl'], '2')]

            ]);
    }
}
