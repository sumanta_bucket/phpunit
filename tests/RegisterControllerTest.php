<?php
//
///**
// * RegisterControllerTest class
// * Include all possible test cases for RegisterController class functions
// */
//class RegisterControllerTest extends TestCase
//{
//    /**
//     * This function test response if we dont pass any params
//     */
//    public function testRegisterWithoutAnyParam()
//    {
//        $this->post('/v1/register', [])
//        ->seeJsonEquals([
//            "first_name" => ["The first name field is required."],
//            "email" => ["The email field is required."],
//            "password" => ["The password field is required."],
//            "password_confirmation" => ["The password confirmation field is required."]
//         ]);
//    }
//
//    /**
//     * This function test response if pass all correct param
//     * @param first_name:String
//     * @param email:Email
//     * @param password:String
//     * @param password_confirmation:String|same:password
//     *
//     */
//    public function testRegister()
//    {
//        $this->post('/v1/register', [
//            "first_name" => 'Demo user',
//            "email" => "demouser1@host.com",
//            "password" => "demo123@",
//            "password_confirmation" => "demo123@"
//        ])->seeJsonEquals([
//            "status" => true,
//            "msg" => "user_created"
//         ]);
//    }
//
//    /**
//     * This function test response if we pass already exist email
//     * @param first_name:String
//     * @param email:Email
//     * @param password:String
//     * @param password_confirmation:String|same:password
//     */
//    public function testRegisterWithExistingEmail()
//    {
//        $this->post('/v1/register', [
//            "first_name" => 'Demo user',
//            "email" => "demouser1@host.com",
//            "password" => "demo123@",
//            "password_confirmation" => "demo123@"
//        ])->seeJsonEquals([
//            "email" =>  ["The email has already been taken."]
//         ]);
//    }
//
//    /**
//     * This function testresponse if we pass random string instead of email
//     * @param first_name:String
//     * @param email:String
//     * @param password:String
//     * @param password_confirmation:String|same:password
//     */
//    public function testRegisterUsingEmailWithRandomString()
//    {
//        $this->post('/v1/register', [
//            "first_name" => 'Demo user',
//            "email" => "demouser1@host.com",
//            "password" => "demo123@",
//            "password_confirmation" => "demo123@"
//        ])->seeJsonEquals([
//            "email" =>  ["The email must be a valid email address."]
//         ]);
//    }
//
//    /**
//     * This function test response if we pass password shorter than 6 digit
//     * @param first_name:String
//     * @param email:Email
//     * @param password:String
//     * @param password_confirmation:String|same:password
//     */
//    public function testRegisterUsingShorterPassword()
//    {
//        $this->post('/v1/register', [
//            "first_name" => 'Demo user',
//            "email" => "demouser1@host.com",
//            "password" => "demo",
//            "password_confirmation" => "demo"
//        ])->seeJsonEquals([
//            "email" =>  ["The email must be a valid email address."]
//         ]);
//    }
//
//    /**
//     * This function test response if we pass different password and password_confirmation
//     * @param first_name:String
//     * @param email:Email
//     * @param password:String
//     * @param password_confirmation:String
//     */
//    public function testRegisterUsingDifferentPasswordandConfermPassword()
//    {
//        $this->post('/v1/register', [
//            "first_name" => 'Demo user',
//            "email" => "demouser1@host.com",
//            "password" => "demo123@",
//            "password_confirmation" => "demo"
//        ])->seeJsonEquals([
//            "password_confirmation" =>  ["The password confirmation and password must match."]
//         ]);
//    }
//}
