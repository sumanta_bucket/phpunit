<?php

/**
 * ConsultantStoreMethodTest class
 * Include all possible test cases for Store Methods
 */
class ConsultantCompanyStoreTest extends TestCase
{
    /*
     * Login param for admin , employee , customer to check role based authentication
     * */

    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'custom@yopmail.com',
        'password' => '123456'
    ];
    public $employee=[
        'email'=>'emp1@yopmail.com',
        'password'=>'123456'
    ];

    /**
     *  Admin and Employee holds same permissions .
     *  Customer dont't have permissions to view this page.
     *  Contact-Persons dont't have permission to view this page.
     */



    /**
     * Case 1
     * @test  This function test response without auth token without params
     * @expectedExceptionCode 401
     * create a consultant company
     * @bodyParam name string required the name of the consultant company
     * @response json array type
     */
    public function testConsultantCompanyAddWithoutToken()
    {
        $this->post('/v1/consultant_company/add', [])
            ->seeJsonEquals([
                "error" => $this->__getTranslateString('missing_token_error', [], 2)
            ]);
    }

    /**
     * Case 2
     * @test This function test response with invalid auth token without params
     * @expectedExceptionCode 400
     * create a consultant company
     * @bodyParam name string required the name of the consultant company
     * @response json array type
     */
    public function testConsultantCompanyAddWithInvalidToken()
    {
        $expired_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expired_token
        ];

        $response = $this->call('POST', '/v1/consultant_company/add', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(400);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('token_expired_error', [], 2), $json['error']);
    }

    /**
     * Case 3
     * @test This function test response with valid token without params
     * @expectedExceptionCode 422
     * create a consultant company
     * @bodyParam name string required the name of the consultant company
     * @response json array type
     */
    public function testConsultantCompanyAddValidTokenWithoutPrams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('POST', '/v1/consultant_company/add', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'name'], $lang)
        ], $json['name']);
    }

    /**
     * Case 3B
     * @test Customer dont't have permissions to view this page.
     * @expectedExceptionCode 403
     * create a consultant company
     * @bodyParam name string required the name of the consultant company
     * @response json array type
     */
    public function testConsultantCompanyAddWithValidTokenWithoutPramsForCustomer()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->customer);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);


        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('POST', '/v1/consultant_company/add', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(403);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame(
            $this->__getTranslateString('insufficient_permission', [], $lang)
        , $json['error']);
    }

    /**
     * Case 3c
     * @test Customer dont't have permissions to view this page.
     * @expectedExceptionCode 403
     * create a consultant company
     * @bodyParam name string required the name of the consultant company
     * @response json array type
     */
    public function testConsultantCompanyAddWithValidTokenWithoutPramsForEmployee()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->employee);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);


        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('POST', '/v1/consultant_company/add', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(403);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
//        match each index value
        $this->assertSame(
            $this->__getTranslateString('insufficient_permission', [], $lang)
        , $json['error']);
    }

    /**
     * Case 4
     * @test This function test response with valid token and null params
     * @expectedExceptionCode 422
     * create a consultant company
     * @bodyParam name string required the name of the consultant company
     * @response json array type
     */
    public function testConsultantCompanyAddWithValidTokenNullParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
           'name'=>null
        ];

        $response = $this->call('POST', '/v1/consultant_company/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);

        //match each index value
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'name'], $lang)
        ], $json['name']);
    }

    /**
     * Case 5
     * @test This function test response with valid token but with name having length max to 255
     * @expectedExceptionCode 422
     * create a consultant company
     * @bodyParam name string required the name of the consultant company
     * @response json array type
     */
    public function testConsultantCompanyWithValidTokenNameNotMoreThan()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'name' => 'Jro2nXolefKIdOLWieVX6r6lkT5wq0VQ3b3L7QYG10Ec5yPIGgpb0lFLD4P4cy0StLd05oXrNxS5yG2pUTFsu7LlpyqoJXxyrPoLUzrAgYk548IUoOiNDyFZ4l7ELsIEcOAN7mVxnSkY3SsA0IEmhT5gsebZBPGWkBjoiDsocg2TeEnTTualcoI4qKG0mYkfpZSlr8WqNBnUFevIXzjcrAtEwkl1iSQoJ6Y9fIh44B76Mu6rFmLb8xyHO95GUHLd',
        ];

        $response = $this->call('POST', '/v1/consultant_company/add', $param, [], [], $headers);

        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('max_validation', ['attribute' => 'name', 'max' => 191], $lang)
        ], $json['name']);
    }

    /**
     * Case 6
     * @test This function test response with valid token but with name having String
     * @expectedExceptionCode 200
     * create a consultant company
     * @bodyParam name string required the name of the consultant company
     * @response json array type
     */
    public function testConsultantCompanyWithValidTokenNameIsString()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'name' => 'sadadas',
        ];

        $response = $this->call('POST', '/v1/consultant_company/add', $param, [], [], $headers);

        //check response status code
        $this->assertResponseStatus(200);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame(
            $this->__getTranslateString('consulant_company_created', [], $lang)
        , $json['success']);
    }

    /**
     * Case 7
     * @test This function test response with valid token but with name having integer
     * @expectedExceptionCode 422
     * create a consultant company
     * @bodyParam name string required the name of the consultant company
     * @response json array type
     */
    public function testConsultantCompanyWithValidTokenNameIsInteger()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'name' => 1321321564,
        ];

        $response = $this->call('POST', '/v1/consultant_company/add', $param, [], [], $headers);

        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('string_validation', ['attribute'=>'name'], $lang)
            ], $json['name']);
    }

}
