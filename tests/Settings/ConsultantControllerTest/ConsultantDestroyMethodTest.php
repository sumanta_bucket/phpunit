<?php

use App\Models\Consultant;
use App\Models\ConsultantCompany;
use App\Models\ConsultantTitle;
use Illuminate\Support\Facades\Config;

/**
 * ConsultantDestroyMethodTest class
 * Include all possible test cases for Destroy Methods
 */
class ConsultantDestroyMethodTest extends TestCase
{
    /*
    * Login param for admin , employee , customer to check role based authentication
    * */

    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'customer2@yopmail.com',
        'password' => '123456'
    ];
    public $employee = [
        'email' => 'emp1@yopmail.com',
        'password' => '123456'
    ];

    /**
     *  Admin and Employee holds same permissions .
     *  Customer dont't have permissions to view this page.
     *  Contact-Persons dont't have permission to view this page.
     */


    /**
     * Case 1
     * @test  This function test response without auth token without params
     * @expectedExceptionCode 401
     * @bodyParam request Request required the default request parameter
     * @bodyParam id integer required the id of the record being deleted
     * @response type
     */
    public function testConsultantDestroyWithoutToken()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('consultant_companies')->truncate();
        DB::table('consultant_titles')->truncate();
        DB::table('consultants')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        app(DatabaseSeeder::class)->call(ConsultantSeeder::class);


        $response = $this->call('DELETE', '/v1/consultants/delete/1', [], [], [], []);
        //check response status code
        $this->assertResponseStatus(401);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('missing_token_error', [], Config::get('constant.default_lang')),
            $json['error']);
    }

    /**
     * Case 2
     * @test This function test response without any param and with invalid token
     * @expectedExceptionCode 400
     * @bodyParam request Request required the default request parameter
     * @bodyParam id integer required the id of the record being deleted
     * @response type
     */
    public function testConsultantDestroyWithInvalidTokenWithoutParam()
    {
        $expired_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expired_token
        ];

        $response = $this->call('DELETE', '/v1/consultants/delete/1', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(400);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('token_expired_error', [], Config::get('constant.default_lang')),
            $json['error']);
    }

    /**
     * Case 3
     * @test This function test response without any param and with invalid token
     * @expectedExceptionCode 404
     * @bodyParam request Request required the default request parameter
     * @bodyParam id integer required the id of the record being deleted
     * @response type
     */
    public function testConsultantDestroyWithValidTokenWithoutParam()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);

        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('DELETE', '/v1/consultants/delete/', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(404);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('route_not_found', [], Config::get('constant.default_lang')),
            $json['error']);
    }

    /**
     * Case 4
     * @test This function test response with valid token but with not existing id
     * @expectedExceptionCode 422
     * @bodyParam request Request required the default request parameter
     * @bodyParam id integer required the id of the record being deleted
     * @response type
     */
    public function testConsultantDestroyWithValidTokenButInvalidId()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('DELETE', '/v1/consultants/delete/0', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
    }

    /**
     * Case 5
     * @test This function test response with valid token and valid id
     * @expectedExceptionCode 200
     * @bodyParam request Request required the default request parameter
     * @bodyParam id integer required the id of the record being deleted
     * @response type
     */
    public function testConsultantDestroyWithValidTokenValidId()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('DELETE', '/v1/consultants/delete/1', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(200);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('consultant_deleted', [], $lang), $json['success']);


    }

    /**
     * Case 6
     * @test This function test response with valid token but with soft deleted id
     * @expectedExceptionCode 422
     * @bodyParam request Request required the default request parameter
     * @bodyParam id integer required the id of the record being deleted
     * @response type
     */
    public function testConsultantDestroyWithSofDeletedId()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('DELETE', '/v1/consultants/delete/1', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([$this->__getTranslateString('exists_validation', ['attribute' => 'id'], $lang)],
            $json['id']);
        Consultant::query()->forceDelete();
        ConsultantCompany::query()->forceDelete();
        ConsultantTitle::query()->forceDelete();
    }


}
