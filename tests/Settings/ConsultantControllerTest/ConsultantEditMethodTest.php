<?php

use App\Models\Consultant;

/**
 * ConsultantEditMethodTest class
 * Include all possible test cases for Edit Methods
 */
class ConsultantEditMethodTest extends TestCase
{
    /*
    * Login param for admin , employee , customer to check role based authentication
    * */

    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'custom@yopmail.com',
        'password' => '123456'
    ];
    public $employee = [
        'email' => 'emp1@yopmail.com',
        'password' => '123456'
    ];

    /**
     *  Admin and Employee holds same permissions .
     *  Customer dont't have permissions to view this page.
     *  Contact-Persons dont't have permission to view this page.
     */


    /**
     * Case 1
     * @test  This function test response without auth token without params
     * @expectedExceptionCode 401
     * Retrieve a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam id integer required the id of the record to be fetched
     * @response json array type
     */
    public function testConsultantEditWithoutToken()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('consultant_companies')->truncate();
        DB::table('consultant_titles')->truncate();
        DB::table('consultants')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        app(DatabaseSeeder::class)->call(ConsultantSeeder::class);
        $response = $this->call('GET', '/v1/consultants/get/1', [], [], [], []);
        //check response status code
        $this->assertResponseStatus(401);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('missing_token_error', [], 2), $json['error']);
    }

    /**
     * Case 2
     * @test This function test response with invalid auth token without params
     * @expectedExceptionCode 400
     * Retrieve a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam id integer required the id of the record to be fetched
     * @response json array type
     */
    public function testConsultantEditWithInvalidTokenWithoutParam()
    {
        $expired_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expired_token
        ];

        $response = $this->call('GET', '/v1/consultants/get/1', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(400);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('token_expired_error', [], 2), $json['error']);
    }

    /**
     * Case 3
     * @test This function test response with valid token without params
     * @expectedExceptionCode 404
     * Retrieve a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam id integer required the id of the record to be fetched
     * @response json array type
     */
    public function testConsultantEditWithValidTokenWithoutParam()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        $lang = $loginJson['language'];

        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('GET', '/v1/consultants/get/', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(404);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('route_not_found', [], 2), $json['error']);
    }


    /**
     * Case 3B
     * @test This function test response with valid token without params (customer->Permission Check)
     * @expectedExceptionCode 404
     * Retrieve a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam id integer required the id of the record to be fetched
     * @response json array type
     */
    public function testConsultantEditWithValidTokenWithoutPramsForCustomer()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->customer);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        $lang = $loginJson['language'];

        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('GET', '/v1/consultants/get', [], [], [], $headers);
        $json = json_decode($response->getContent(), true);
        $this->assertSame($this->__getTranslateString('route_not_found', [], 2), $json['error']);
    }


    /**
     * Case 4
     * @test This function test response with valid token without params
     * @expectedExceptionCode 404
     * Retrieve a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam id integer required the id of the record to be fetched
     * @response json array type
     */
    public function testConsultantEditWithValidTokenWithoutParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);

        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('GET', '/v1/consultants/get/', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(404);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('route_not_found', [], 2), $json['error']);
    }

    /**
     * Case 5
     * @test This function test response with valid token but with soft deleted id
     * @expectedExceptionCode 422
     * Retrieve a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam id integer required the id of the record to be fetched
     * @response json array type
     */
    public function testConsultantEditWithValidTokenAndSoftDeletedId()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $consultant = Consultant::find(2);
        $consultant->delete();
        $response = $this->call('GET', '/v1/consultants/get/'.$consultant->id . $consultant->id, [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([$this->__getTranslateString('exists_validation', ['attribute' => 'id'], $lang)],
            $json['id']);
    }


    /**
     * Case 6
     * @test This function test response with valid token and valid id
     * @expectedExceptionCode 200
     * Retrieve a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam id integer required the id of the record to be fetched
     * @response json array type
     */
    public function testConsultantEditWithValidTokenAndValidID()
    {
        $consultantsData = $this->__getConsultantsData(3);
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('GET', '/v1/consultants/get/3', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(200);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('success', [], $lang), $json['success']);
        $this->assertSame($consultantsData['id'], $json['data']['id']);

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('consultant_companies')->truncate();
        DB::table('consultant_titles')->truncate();
        DB::table('consultants')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

    public function __getConsultantsData($id)
    {
        $consultant = App\Models\Consultant::with('user')->where('id', $id)->first();
        return $consultant;
    }
}
