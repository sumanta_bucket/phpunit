<?php

use Illuminate\Support\Facades\DB;

/**
 * ConsultantStoreMethodTest class
 * Include all possible test cases for Store Methods
 */
class ConsultantStoreMethodTest extends TestCase
{
    /*
     * Login param for admin , employee , customer to check role based authentication
     * */

    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'custom@yopmail.com',
        'password' => '123456'
    ];
    public $employee = [
        'email' => 'emp1@yopmail.com',
        'password' => '123456'
    ];

    /**
     *  Admin and Employee holds same permissions .
     *  Customer dont't have permissions to view this page.
     *  Contact-Persons dont't have permission to view this page.
     */


    /**
     * Case 1
     * @test  This function test response without auth token without params
     * @expectedExceptionCode 401
     * Create a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam user.first_name string required the first name of the consultant user
     * @bodyParam user.email string required the email of the consultant user
     * @bodyParam user.mobile string required the mobile of the consultant user
     * @bodyParam consultant_company.id integer required the id of consultant company
     * @bodyParam consultant_title.id integer optional the id of the consultant title
     * @response json array type return the success error message with proper response code
     */
    public function testConsultantStoreWithoutAuthTokenAndWithoutParams()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('consultant_companies')->truncate();
        DB::table('consultant_titles')->truncate();
        DB::table('consultants')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        app(DatabaseSeeder::class)->call(ConsultantSeeder::class);
        $this->post('/v1/consultants/add', [])
            ->seeJsonEquals([
                "error" => $this->__getTranslateString('missing_token_error', [], 2)
            ]);
    }

    /**
     * Case 2
     * @test This function test response with invalid auth token without params
     * @expectedExceptionCode 400
     * Create a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam user.first_name string required the first name of the consultant user
     * @bodyParam user.email string required the email of the consultant user
     * @bodyParam user.mobile string required the mobile of the consultant user
     * @bodyParam consultant_company.id integer required the id of consultant company
     * @bodyParam consultant_title.id integer optional the id of the consultant title
     * @response json array type return the success error message with proper response code
     */
    public function testConsultantStoreWithInvalidAuthTokenAndWithoutParams()
    {
        $expired_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expired_token
        ];

        $response = $this->call('POST', '/v1/consultants/add', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(400);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('token_expired_error', [], 2), $json['error']);
    }

    /**
     * Case 3
     * @test This function test response with valid token without params
     * @expectedExceptionCode 422
     * Create a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam user.first_name string required the first name of the consultant user
     * @bodyParam user.email string required the email of the consultant user
     * @bodyParam user.mobile string required the mobile of the consultant user
     * @bodyParam consultant_company.id integer required the id of consultant company
     * @bodyParam consultant_title.id integer optional the id of the consultant title
     * @response json array type return the success error message with proper response code
     */
    public function testConsultantStoreWithValidTokenWithoutParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('POST', '/v1/consultants/add', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'user.first_name'], $lang)
        ], $json['user.first_name']);
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'user.email'], $lang)],
            $json['user.email']);
    }

    /**
     * Case 4
     * @test This function test response with valid token and null params
     * @expectedExceptionCode 422
     * Create a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam user.first_name string required the first name of the consultant user
     * @bodyParam user.email string required the email of the consultant user
     * @bodyParam user.mobile string required the mobile of the consultant user
     * @bodyParam consultant_company.id integer required the id of consultant company
     * @bodyParam consultant_title.id integer optional the id of the consultant title
     * @response json array type return the success error message with proper response code
     */
    public function testConsultantStoreWithValidTokenAndNullParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'user' => [
                'first_name' => null,
                'email' => null,
                'mobile' => null,
                'notify' => 'null'
            ],
            'consultant_company.id' => null,
            'consultant_title.id' => null
        ];

        $response = $this->call('POST', '/v1/consultants/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'user.first_name'], $lang)
        ], $json['user.first_name']);
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'user.email'], $lang)],
            $json['user.email']);
    }

    /**
     * Case 5
     * @test This function test response with valid token but with user.first_name having length max to 191
     * @expectedExceptionCode 422
     * Create a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam user.first_name string required the first name of the consultant user
     * @bodyParam user.email string required the email of the consultant user
     * @bodyParam user.mobile string required the mobile of the consultant user
     * @bodyParam consultant_company.id integer required the id of consultant company
     * @bodyParam consultant_title.id integer optional the id of the consultant title
     * @response json array type return the success error message with proper response code
     */
    public function testConsultantStoreWithValidTokenWithUserFirstNameHavingLengthMax()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'user' => [
                'first_name' => 'adsfdfjhbjsbdfjaaaaaaaaaaaaaaaaaaaa ajbjjhdf jajbfdj absjdfb jasdbfj basjdfb jasbdfjajsdfsdfffffffffffffffffffffffffffffffsffffffffffffffffffffffffffffffffffffffffffdahjdafajhbdsfhjbhjbjhdsfdsfdfgfdgd',
                'email' => 'dummy2@mail.in',
                'mobile' => '987456231',
            ],
            'consultant_company_id' => 1,
            'consultant_title_id' => 1
        ];

        $response = $this->call('POST', '/v1/consultants/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('max_validation', ['attribute' => 'user.first_name', 'max' => 191], $lang)
        ], $json['user.first_name']);
    }

    /**
     * Case 6
     * @test This function test response with valid token but with user.email having normal string instead of valid email
     * @expectedExceptionCode 422
     * Create a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam user.first_name string required the first name of the consultant user
     * @bodyParam user.email string required the email of the consultant user
     * @bodyParam user.mobile string required the mobile of the consultant user
     * @bodyParam consultant_company.id integer required the id of consultant company
     * @bodyParam consultant_title.id integer optional the id of the consultant title
     * @response json array type return the success error message with proper response code
     */
    public function testConsultantStoreWithValidTokenAndUserEmailHavingNormalStringInsteadOfEmail()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'user' => [
                'first_name' => 'dummy user name',
                'email' => 'dummy2',
                'mobile' => '987456231',
            ],
            'consultant_company_id' => 1,
            'consultant_title_id' => 1
        ];

        $response = $this->call('POST', '/v1/consultants/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([$this->__getTranslateString('email_validation', ['attribute' => 'user.email'], $lang)],
            $json['user.email']);
    }


    /**
     * Case 7
     * @test This function test response with valid token but with user.email existing email
     * @expectedExceptionCode 422
     * Create a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam user.first_name string required the first name of the consultant user
     * @bodyParam user.email string required the email of the consultant user
     * @bodyParam user.mobile string required the mobile of the consultant user
     * @bodyParam consultant_company.id integer required the id of consultant company
     * @bodyParam consultant_title.id integer optional the id of the consultant title
     * @response json array type return the success error message with proper response code
     */
    public function testConsultantStoreWithValidTokenButWithUserEmailExistingEmail()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'user' => [
                'first_name' => 'dummy user name',
                'email' => 'admin@bcbrand.com',
                'mobile' => '987456231'
            ],
            'consultant_company' => ['id' => 1],
            'consultant_title' => ['id' => 1]
        ];

        $response = $this->call('POST', '/v1/consultants/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);

        //match each index value
        $this->assertSame([$this->__getTranslateString('unique_validation', ['attribute' => 'user.email'], $lang)],
            $json['user.email']);
    }

    /**
     * Case 8
     * @test This function test response with valid token but with user.email having length max to 191
     * @expectedExceptionCode 422
     * Create a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam user.first_name string required the first name of the consultant user
     * @bodyParam user.email string required the email of the consultant user
     * @bodyParam user.mobile string required the mobile of the consultant user
     * @bodyParam consultant_company.id integer required the id of consultant company
     * @bodyParam consultant_title.id integer optional the id of the consultant title
     * @response json array type return the success error message with proper response code
     */
    public function testConsultantStoreWithValidTokenButUserEmailHavingLengthMax()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'user' => [
                'first_name' => 'dummy user name',
                'email' => 'QKEwR72lWEoMVTvOwnrHwixxY7UuQ2JwjbsMPtBQ1sK33Jw8Lgyx1lsOBWQU8TTdP37Qltsfxy0uQMWqFQ2Y1nuO0SJUFQWOFGYdf35EGv0oFFgPRvW2CY8xUPHIX9uql4RoIGNz3E3diWcgltFhaH8SvhqL1LEOqBm3p3hvjyaVtee9kT7BYjs9jYCNF8q@host.com',
                'mobile' => '987456231',
            ],
            'consultant_company' => ['id' => 1],
            'consultant_title' => ['id' => 1],
        ];

        $response = $this->call('POST', '/v1/consultants/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('max_validation', ['attribute' => 'user.email', 'max' => 191], $lang),
            $this->__getTranslateString('email_validation', ['attribute' => 'user.email'], $lang)
        ], $json['user.email']);
    }

    /**
     * Case 9
     * @test This function test response with valid token but with user.mobile having length max to 20
     * @expectedExceptionCode 422
     * Create a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam user.first_name string required the first name of the consultant user
     * @bodyParam user.email string required the email of the consultant user
     * @bodyParam user.mobile string required the mobile of the consultant user
     * @bodyParam consultant_company.id integer required the id of consultant company
     * @bodyParam consultant_title.id integer optional the id of the consultant title
     * @response json array type return the success error message with proper response code
     */
    public function testConsultantStoreWithValidTokenWithUserMobileHavingLengthMax()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'user' => [
                'first_name' => 'dummy user name',
                'email' => 'testusderdum1my@host.com',
                'mobile' => '987456231987456231145',
            ],
            'consultant_company' => ['id' => 1],
            'consultant_title' => ['id' => 1],
        ];

        $response = $this->call('POST', '/v1/consultants/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('max_validation', ['attribute' => 'user.mobile', 'max' => 20], $lang)
        ], $json['user.mobile']);
    }

    /**
     * Case 10
     * @test This function test response with valid token but with user.mobile with string instead of number
     * @expectedExceptionCode 200
     * Create a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam user.first_name string required the first name of the consultant user
     * @bodyParam user.email string required the email of the consultant user
     * @bodyParam user.mobile string required the mobile of the consultant user
     * @bodyParam consultant_company.id integer required the id of consultant company
     * @bodyParam consultant_title.id integer optional the id of the consultant title
     * @response json array type return the success error message with proper response code
     */
    public function testConsultantStoreWithValidTokenWithMobileAsString()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'user' => [
                'first_name' => "testMan",
                'email' => str_random(12).'@mail.com',
                "mobile" => "1234554321",
            ],
            'consultant_company' => ['id' => 1],
            'consultant_title' => ['id' => 1],
        ];

        $response = $this->call('POST', '/v1/consultants/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(200);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('consultant_created', [], $lang), $json['success']);
    }

    /**
     * Case 11
     * @test This function test response with valid token but with consultant_company_id string instead of number
     * @expectedExceptionCode 422
     * Create a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam user.first_name string required the first name of the consultant user
     * @bodyParam user.email string required the email of the consultant user
     * @bodyParam user.mobile string required the mobile of the consultant user
     * @bodyParam consultant_company.id integer required the id of consultant company
     * @bodyParam consultant_title.id integer optional the id of the consultant title
     * @response json array type return the success error message with proper response code
     */
    public function testConsultantStoreWithValidTokenAndConsultantCompanyIdAsAsString()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'user' => [
                'first_name' => "testMan",
                'email' => "testasss0987@gmail.com"
            ],
            'consultant_company' => ['id' => "sadasdasads"],
            'consultant_title' => ['id' => 1],
        ];

        $response = $this->call('POST', '/v1/consultants/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('numeric_validation', ['attribute' => 'consultant_company.id'], $lang)
        ], $json['consultant_company.id']);
    }

    /**
     * Case 12
     * @test This function test response with valid token but with consultant_title_id string instead of number
     * @expectedExceptionCode 422
     * Create a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam user.first_name string required the first name of the consultant user
     * @bodyParam user.email string required the email of the consultant user
     * @bodyParam user.mobile string required the mobile of the consultant user
     * @bodyParam consultant_company.id integer required the id of consultant company
     * @bodyParam consultant_title.id integer optional the id of the consultant title
     * @response json array type return the success error message with proper response code
     */
    public function testConsultantStoreWithValidTokenAndConsultantTitleIdAsString()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'user' => [
                'first_name' => 'test user first name',
                'email' => 'testasusderdum1my@host.com',
                'mobile' => '9874563210',
            ],
            'consultant_company' => ['id' => 2],
            'consultant_title' => ['id' => 'sadas']
        ];

        $response = $this->call('POST', '/v1/consultants/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('numeric_validation', ['attribute' => 'consultant_title.id'], $lang)
        ], $json['consultant_title.id']);
    }

    /**
     * Case 13
     * @test This function test response with valid token but with consultant_title_id having length max to 10
     * @expectedExceptionCode 422
     * Create a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam user.first_name string required the first name of the consultant user
     * @bodyParam user.email string required the email of the consultant user
     * @bodyParam user.mobile string required the mobile of the consultant user
     * @bodyParam consultant_company.id integer required the id of consultant company
     * @bodyParam consultant_title.id integer optional the id of the consultant title
     * @response json array type return the success error message with proper response code
     */
    public function testConsultantStoreWithValidTokenWithConsultantTitleHavingLengthMax()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'user' => [
                'first_name' => 'test user first name',
                'email' => 'testusderdum1my@host.com',
                'mobile' => '9874563210',
            ],
            'consultant_company' => [
                'id' => 1
            ],
            'consultant_title' => [
                'id' => 999999999
            ]
        ];

        $response = $this->call('POST', '/v1/consultants/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
    }

    /** Case 15
     * @test This function test response with valid auth token with valid params
     * @expectedExceptionCode 200
     * Create a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam user.first_name string required the first name of the consultant user
     * @bodyParam user.email string required the email of the consultant user
     * @bodyParam user.mobile string required the mobile of the consultant user
     * @bodyParam consultant_company.id integer required the id of consultant company
     * @bodyParam consultant_title.id integer optional the id of the consultant title
     * @response json array type return the success error message with proper response code
     */
    public function testConsultantStoreWithValidTokenAndValidToken()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'user' => [
                'first_name' => 'test user first name',
                'email' => str_random(12).'@mail.com',
                'mobile' => '9874563210',
            ],
            'consultant_company' => ['id' => 1],
            'consultant_title' => ['id' =>1]
        ];

        $response = $this->call('POST', '/v1/consultants/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('consultant_created', [], $lang), $json['success']);
    }

}
