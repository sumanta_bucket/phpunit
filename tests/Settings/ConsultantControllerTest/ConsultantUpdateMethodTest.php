<?php

use App\Models\Consultant;
use App\Models\ConsultantCompany;
use App\Models\ConsultantTitle;

/**
 * ConsultantUpdateMethodTest class
 * Include all possible test cases for Update Methods
 */
class ConsultantUpdateMethodTest extends TestCase
{
    /*
      * Login param for admin , employee , customer to check role based authentication
      * */

    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'custom@yopmail.com',
        'password' => '123456'
    ];
    public $employee = [
        'email' => 'emp1@yopmail.com',
        'password' => '123456'
    ];

    /**
     *  Admin and Employee holds same permissions .
     *  Customer dont't have permissions to view this page.
     *  Contact-Persons dont't have permission to view this page.
     * Create a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam user.first_name string required the first name of the consultant user
     * @bodyParam user.email string required the email of the consultant user
     * @bodyParam user.mobile string required the mobile of the consultant user
     * @bodyParam consultant_company.id integer required the id of consultant company
     * @bodyParam consultant_title.id integer optional the id of the consultant title
     * @response json array type return the success error message with proper response code
     */


    /**
     * Case 1
     * @test  This function test response without auth token without params
     * @expectedExceptionCode 401
     */
    public function testConsultantUpdateWithoutAuthTokenAndWithoutParams()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('consultant_companies')->truncate();
        DB::table('consultant_titles')->truncate();
        DB::table('consultants')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        app(DatabaseSeeder::class)->call(ConsultantSeeder::class);

        $this->put('/v1/consultants/update/1{id}', [])
            ->seeJsonEquals([
                "error" => $this->__getTranslateString('missing_token_error', [], 2)
            ]);
    }

    /**
     * Case 2
     * @test This function test response with invalid auth token without params
     * @expectedExceptionCode 400
     * Create a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam user.first_name string required the first name of the consultant user
     * @bodyParam user.email string required the email of the consultant user
     * @bodyParam user.mobile string required the mobile of the consultant user
     * @bodyParam consultant_company.id integer required the id of consultant company
     * @bodyParam consultant_title.id integer optional the id of the consultant title
     * @response json array type return the success error message with proper response code
     */
    public function testConsultantUpdateWithInvalidAuthTokenAndWithoutParams()
    {
        $expired_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expired_token
        ];

        $response = $this->call('PUT', '/v1/consultants/update/{id}', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(400);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('token_expired_error', [], 2), $json['error']);
    }

    /**
     * Case 3
     * @test This function test response with valid token without params
     * @expectedExceptionCode 422
     * Create a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam user.first_name string required the first name of the consultant user
     * @bodyParam user.email string required the email of the consultant user
     * @bodyParam user.mobile string required the mobile of the consultant user
     * @bodyParam consultant_company.id integer required the id of consultant company
     * @bodyParam consultant_title.id integer optional the id of the consultant title
     * @response json array type return the success error message with proper response code
     */
    public function testConsultantUpdateWithValidTokenAdnWithoutParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('PUT', '/v1/consultants/update/1', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
    }

    /**
     * Case 3B
     * @test This function test response with valid token without params (customer->Permission Check)
     * @expectedExceptionCode 403
     * Create a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam user.first_name string required the first name of the consultant user
     * @bodyParam user.email string required the email of the consultant user
     * @bodyParam user.mobile string required the mobile of the consultant user
     * @bodyParam consultant_company.id integer required the id of consultant company
     * @bodyParam consultant_title.id integer optional the id of the consultant title
     * @response json array type return the success error message with proper response code
     */
    public function testConsultantUpdateWithValidTokenWithoutParamsForCustomer()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->customer);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        $lang = $loginJson['language'];

        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('POST', '/v1/consultants', [], [], [], $headers);
        $json = json_decode($response->getContent(), true);
        $this->assertSame($this->__getTranslateString('insufficient_permission', [], $lang), $json['error']);
    }

    /**
     * Case 4
     * @test This function test response with valid token and null params
     * @expectedExceptionCode 422
     * Create a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam user.first_name string required the first name of the consultant user
     * @bodyParam user.email string required the email of the consultant user
     * @bodyParam user.mobile string required the mobile of the consultant user
     * @bodyParam consultant_company.id integer required the id of consultant company
     * @bodyParam consultant_title.id integer optional the id of the consultant title
     * @response json array type return the success error message with proper response code
     */
    public function testConsultantUpdateWithValidTokenAndNullParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $ConsultantEntry = Consultant::find(1);

        $param = [
            'user' => [
                'id' => null,
                'first_name' => null,
                'email' => null,
                'mobile' => null,

            ],
            'consultant_company' => ['id' => null],
            'consultant_title' => ['id' => null]
        ];

        $response = $this->call('PUT', '/v1/consultants/update/1', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'user.id'], $lang)],
            $json['user.id']);
    }

    /**
     * Case 5
     * @test This function test response with valid token but with user.first_name having length max to 191
     * @expectedExceptionCode 422
     * Create a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam user.first_name string required the first name of the consultant user
     * @bodyParam user.email string required the email of the consultant user
     * @bodyParam user.mobile string required the mobile of the consultant user
     * @bodyParam consultant_company.id integer required the id of consultant company
     * @bodyParam consultant_title.id integer optional the id of the consultant title
     * @response json array type return the success error message with proper response code
     */
    public function testConsultantUpdateWithValidTokenAndUserWithMaxLength()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $ConsultantEntry = Consultant::find(1);
        $userId = $ConsultantEntry->user_id;
        $param = [
            'user' => [
                'id' => $userId,
                'first_name' => 'adsfdfjhbjsbdfjafhdhfddhgdhdhgddhfgdhfddh ajbjjhdf jajbfdj absjdfb jasdbfj bsdfdsfsdfsdasjdfb jasbdfjajsdaghvsfdghsfdghsfdghhjdafajhbdsfhjfghfghfghbhjbjhdsffdggggggggggggggggggggggggggggggggggggggggggggggggggggggggdsfdfgfdgd',
                'email' => 'testusderdum1my@host.com',
                'mobile' => '987456231',
            ],
            'consultant_company' => ['id' => $ConsultantEntry->consultant_company_id],
            'consultant_title' => ['id' => $ConsultantEntry->consultant_title_id]
        ];
        $response = $this->call('PUT', '/v1/consultants/update/1', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('max_validation', ['attribute' => 'user.first_name', 'max' => 191], $lang)
        ], $json['user.first_name']);
    }

    /**
     * Case 6
     * @test This function test response with valid token but with user.email having normal string instead of valid email
     * @expectedExceptionCode 422
     * Create a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam user.first_name string required the first name of the consultant user
     * @bodyParam user.email string required the email of the consultant user
     * @bodyParam user.mobile string required the mobile of the consultant user
     * @bodyParam consultant_company.id integer required the id of consultant company
     * @bodyParam consultant_title.id integer optional the id of the consultant title
     * @response json array type return the success error message with proper response code
     */
    public function testConsultantUpdateWithUserEmailUsingNormalString()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $ConsultantEntry = Consultant::find(1);
        $param = [
            'user' => [
                'id' => $ConsultantEntry->user_id,
                'first_name' => 'test user first name',
                'email' => 'testusderdum1my',
                'mobile' => '987456231',
            ],
            'consultant_company' => ['id' => $ConsultantEntry->consultant_company_id],
            'consultant_title' => ['id' => $ConsultantEntry->consultant_title_id]
        ];

        $response = $this->call('PUT', '/v1/consultants/update/1', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([$this->__getTranslateString('email_validation', ['attribute' => 'user.email'], $lang)],
            $json['user.email']);
    }

    /**
     * Case 7
     * @test This function test response with valid token but with user.email having length max to 191
     * @expectedExceptionCode 422
     * Create a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam user.first_name string required the first name of the consultant user
     * @bodyParam user.email string required the email of the consultant user
     * @bodyParam user.mobile string required the mobile of the consultant user
     * @bodyParam consultant_company.id integer required the id of consultant company
     * @bodyParam consultant_title.id integer optional the id of the consultant title
     * @response json array type return the success error message with proper response code
     */
    public function testConsultantUpdateWithValidTokenAndUserEmailWithMaxLength()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $ConsultantEntry = Consultant::find(1);
        $param = [
            'user' => [
                'id' => $ConsultantEntry->user_id,
                'first_name' => 'test user first name',
                'email' => 'adsfdfjhbjsbdfja ajbjjhdf jajbfdj absjdfb jasdbfj bsdfdsfsdfsdasjdfb jasbdfjajsdaghvsfdghsfdghsfdghhjdafajhbdsfhjfghfghfghbhjbjhdsffdggggggggggggggggggggggggggggggggggggggggggggggggggggggggdsfdfgfdgd@host.com',
                'mobile' => '987456231',
            ],
            'consultant_company' => ['id' => $ConsultantEntry->consultant_company_id],
            'consultant_title' => ['id' => $ConsultantEntry->consultant_title_id]
        ];

        $response = $this->call('PUT', '/v1/consultants/update/1', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('email_validation', ['attribute' => 'user.email'], $lang),
            $this->__getTranslateString('max_validation', ['attribute' => 'user.email', 'max' => 191], $lang)
        ], $json['user.email']);
    }

    /**
     * Case 8
     * @test This function test response with valid token but with user.mobile having length max to 20
     * @expectedExceptionCode 422
     * Create a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam user.first_name string required the first name of the consultant user
     * @bodyParam user.email string required the email of the consultant user
     * @bodyParam user.mobile string required the mobile of the consultant user
     * @bodyParam consultant_company.id integer required the id of consultant company
     * @bodyParam consultant_title.id integer optional the id of the consultant title
     * @response json array type return the success error message with proper response code
     */
    public function testConsultantUpdateWithMobileHavingMaxLength()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $ConsultantEntry = Consultant::find(1);
        $param = [
            'user' => [
                'id' => $ConsultantEntry->user_id,
                'first_name' => 'test user first name',
                'email' => 'testusderdum1my@host.com',
                'mobile' => '987456231987456231145',
            ],
            'consultant_company' => ['id' => $ConsultantEntry->consultant_company_id],
            'consultant_title' => ['id' => $ConsultantEntry->consultant_title_id]
        ];

        $response = $this->call('PUT', '/v1/consultants/update/1', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('max_validation', ['attribute' => 'user.mobile', 'max' => 20], $lang)
        ], $json['user.mobile']);
    }


    /**
     * Case 9
     * @test This function test response with valid token but with consultant_company_id string instead of number
     * @expectedExceptionCode 422
     * Create a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam user.first_name string required the first name of the consultant user
     * @bodyParam user.email string required the email of the consultant user
     * @bodyParam user.mobile string required the mobile of the consultant user
     * @bodyParam consultant_company.id integer required the id of consultant company
     * @bodyParam consultant_title.id integer optional the id of the consultant title
     * @response json array type return the success error message with proper response code
     */
    public
    function testConsultantUpdateWithConsultantCompanyIdBeingStringInsteadOFNumber()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $ConsultantEntry = Consultant::find(1);
        $param = [
            'user' => [
                'id' => $ConsultantEntry->user_id,
                'first_name' => 'test user first name',
                'email' => 'teasdstusderdum1mysasd@host.com',
                'mobile' => '9874563210',
            ],
            'consultant_company' => ['id' => "asdadsasd"],
            'consultant_title' => ['id' => $ConsultantEntry->consultant_title_id]
        ];

        $response = $this->call('PUT', '/v1/consultants/update/1', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('numeric_validation', ['attribute' => 'consultant_company.id'], $lang)
        ], $json['consultant_company.id']);
    }

    /**
     * Case 10
     * @test This function test response with valid token but with consultant_title_id string instead of number
     * @expectedExceptionCode 422
     * Create a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam user.first_name string required the first name of the consultant user
     * @bodyParam user.email string required the email of the consultant user
     * @bodyParam user.mobile string required the mobile of the consultant user
     * @bodyParam consultant_company.id integer required the id of consultant company
     * @bodyParam consultant_title.id integer optional the id of the consultant title
     * @response json array type return the success error message with proper response code
     */
    public
    function testConsultantUpdateButConsultantTitleIdBeingStringInsteadNumber()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $ConsultantEntry = Consultant::find(1);
        $param = [
            'user' => [
                'id' => $ConsultantEntry->user_id,
                'first_name' => 'test user first name',
                'email' => 'testusderdum1my@host.com',
                'mobile' => '9874563210',
            ],
            'consultant_company' => ['id' => 99999999999],
            'consultant_title' => ['id' => $ConsultantEntry->consultant_title_id]
        ];

        $response = $this->call('PUT', '/v1/consultants/update/1', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
    }


    /**
     * Case 11
     * @test This function test response with valid token but with non existing user.id
     * @expectedExceptionCode 422
     * Create a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam user.first_name string required the first name of the consultant user
     * @bodyParam user.email string required the email of the consultant user
     * @bodyParam user.mobile string required the mobile of the consultant user
     * @bodyParam consultant_company.id integer required the id of consultant company
     * @bodyParam consultant_title.id integer optional the id of the consultant title
     * @response json array type return the success error message with proper response code
     */
    public
    function testConsultantUpdateWithNonExistingUserId()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $ConsultantEntry = Consultant::find(1);
        $param = [
            'user' => [
                'id' => '0',
                'first_name' => 'test user first name',
                'email' => 'testusderdum1my@host.com',
                'mobile' => '9874563210',
            ],
            'consultant_company' => ['id' => $ConsultantEntry->consultant_company_id],
            'consultant_title' => ['id' => $ConsultantEntry->consultant_title_id]
        ];

        $response = $this->call('PUT', '/v1/consultants/update/1', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([$this->__getTranslateString('exists_validation', ['attribute' => 'user.id'], $lang)],
            $json['user.id']);
    }

    /**
     * Case 12
     * @test This function test response with valid token but with user.id having length max to 10
     * @expectedExceptionCode 422
     * Create a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam user.first_name string required the first name of the consultant user
     * @bodyParam user.email string required the email of the consultant user
     * @bodyParam user.mobile string required the mobile of the consultant user
     * @bodyParam consultant_company.id integer required the id of consultant company
     * @bodyParam consultant_title.id integer optional the id of the consultant title
     * @response json array type return the success error message with proper response code
     */
    public
    function testConsultantUpdateWithValidTokenWithUserIdHavingMax()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'user' => [
                'id' => '13000000000',
                'first_name' => 'test user first name',
                'email' => 'testusderdum1my@host.com',
                'mobile' => '9874563210',
            ],
            'consultant_company' => ['id' => 1],
            'consultant_title' => ['id' => 1]
        ];

        $response = $this->call('PUT', '/v1/consultants/update/1', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
    }


    /**
     * Case 13
     * @test This function test response with invalid auth token with valid params
     * @expectedExceptionCode 200
     * Create a consultant
     * @bodyParam request Request required the default request parameter
     * @bodyParam user.first_name string required the first name of the consultant user
     * @bodyParam user.email string required the email of the consultant user
     * @bodyParam user.mobile string required the mobile of the consultant user
     * @bodyParam consultant_company.id integer required the id of consultant company
     * @bodyParam consultant_title.id integer optional the id of the consultant title
     * @response json array type return the success error message with proper response code
     */
    public
    function testConsultantUpdateWithInvalidTokenWithValidParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $ConsultantEntry = Consultant::find(1);
        $param = [
            'user' => [
                'id' => $ConsultantEntry->user_id,
                'first_name' => 'test user first name',
                'email' => str_random(12) . '@mail.com',
                'mobile' => '9874563210',
            ],
            'consultant_company' => ['id' => $ConsultantEntry->consultant_company_id],
            'consultant_title' => ['id' => $ConsultantEntry->consultant_title_id]
        ];

        $response = $this->call('PUT', '/v1/consultants/update/1', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(200);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('consultant_updated', [], $lang), $json['success']);
        Consultant::query()->forceDelete();
        ConsultantCompany::query()->forceDelete();
        ConsultantTitle::query()->forceDelete();
    }
}
