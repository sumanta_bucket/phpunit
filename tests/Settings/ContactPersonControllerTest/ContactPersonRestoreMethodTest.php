<?php


use App\Models\ContactPerson;
use App\Models\User;
use Illuminate\Support\Facades\Config;

class ContactPersonRestoreMethodTest extends TestCase
{
    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'custom@yopmail.com',
        'password' => '123456'
    ];
    public $employee = [
        'email' => 'emp1@yopmail.com',
        'password' => '123456'
    ];

    /**
     * Case 1
     * @test This function test response without any param and with out any token
     * @expectedExceptionCode 401
     * Update a contact person
     * @bodyParam id integer required the id of the contact person to update get using route
     * @bodyParam user.id integer required the user_id of the contact person
     * @bodyParam user.first_name string required the first name of the user
     * @bodyParam user.last_name string optional the last name of the user
     * @bodyParam user.email email required the email of the user
     * @bodyParam user.mobile string optional the phone number of the user
     * @bodyParam user.avatar file optional the photo of the user of type:png,jpg,jpeg etc
     * @bodyParam user.notify boolean optional whether to notify the user or not
     * @bodyParam user.notes text optional the extra information regarding the user
     * @bodyParam user.title_id integer optional the title of the user
     * @bodyParam user.telephone string optional the telephone number of the user
     * @response json array type
     */
    public function testContactPersonRestoreWithoutToken()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('contact_persons')->truncate();
        DB::table('consultant_titles')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        app(DatabaseSeeder::class)->call(ContactPersonSeeder::class);
        $response = $this->call('GET', '/v1/contact_persons/restore/2', [], [], [], []);

        //check response status code
        $this->assertResponseStatus(401);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('missing_token_error', [], Config::get('constant.default_lang')),
            $json['error']);
    }

    /**
     * Case 2
     * @test This function test response without any param and with invalid token
     * @expectedExceptionCode 401
     * Update a contact person
     * @bodyParam id integer required the id of the contact person to update get using route
     * @bodyParam user.id integer required the user_id of the contact person
     * @bodyParam user.first_name string required the first name of the user
     * @bodyParam user.last_name string optional the last name of the user
     * @bodyParam user.email email required the email of the user
     * @bodyParam user.mobile string optional the phone number of the user
     * @bodyParam user.avatar file optional the photo of the user of type:png,jpg,jpeg etc
     * @bodyParam user.notify boolean optional whether to notify the user or not
     * @bodyParam user.notes text optional the extra information regarding the user
     * @bodyParam user.title_id integer optional the title of the user
     * @bodyParam user.telephone string optional the telephone number of the user
     * @response json array type
     */
    public function testContactPersonRestoreWithInvalidTokenWithoutParam()
    {
        $expiredToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expiredToken
        ];

        $response = $this->call('GET', '/v1/contact_persons/restore/2', [], [], [], []);
        //check response status code
        $this->assertResponseStatus(401);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('missing_token_error', [], Config::get('constant.default_lang')),
            $json['error']);
    }

    /**
     * Case 3
     * @test This function test response without any param and with invalid token
     * @expectedExceptionCode 401
     * Update a contact person
     * @bodyParam id integer required the id of the contact person to update get using route
     * @bodyParam user.id integer required the user_id of the contact person
     * @bodyParam user.first_name string required the first name of the user
     * @bodyParam user.last_name string optional the last name of the user
     * @bodyParam user.email email required the email of the user
     * @bodyParam user.mobile string optional the phone number of the user
     * @bodyParam user.avatar file optional the photo of the user of type:png,jpg,jpeg etc
     * @bodyParam user.notify boolean optional whether to notify the user or not
     * @bodyParam user.notes text optional the extra information regarding the user
     * @bodyParam user.title_id integer optional the title of the user
     * @bodyParam user.telephone string optional the telephone number of the user
     * @response json array type
     */
    public function testContactPersonRestoreCompanyWithValidTokenWithoutParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);

        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('GET', '/v1/contact_persons/restore/2', [], [], [], []);
        //check response status code
        $this->assertResponseStatus(401);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('missing_token_error', [], Config::get('constant.default_lang')),
            $json['error']);
    }

    /**
     * Case 4
     * @test This function test response with valid token but with not existing id
     * @expectedExceptionCode 401
     * Update a contact person
     * @bodyParam id integer required the id of the contact person to update get using route
     * @bodyParam user.id integer required the user_id of the contact person
     * @bodyParam user.first_name string required the first name of the user
     * @bodyParam user.last_name string optional the last name of the user
     * @bodyParam user.email email required the email of the user
     * @bodyParam user.mobile string optional the phone number of the user
     * @bodyParam user.avatar file optional the photo of the user of type:png,jpg,jpeg etc
     * @bodyParam user.notify boolean optional whether to notify the user or not
     * @bodyParam user.notes text optional the extra information regarding the user
     * @bodyParam user.title_id integer optional the title of the user
     * @bodyParam user.telephone string optional the telephone number of the user
     * @response json array type
     */
    public function testContactPersonRestoreWithValidTokenNonExistingId()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('GET', '/v1/contact_persons/restore/2', [], [], [], []);
        //check response status code
        $this->assertResponseStatus(401);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('missing_token_error', [], Config::get('constant.default_lang')),
            $json['error']);
    }

    /**
     * Case 5
     * @test This function test response with valid token but with soft deleted id
     * @expectedExceptionCode 422
     * Update a contact person
     * @bodyParam id integer required the id of the contact person to update get using route
     * @bodyParam user.id integer required the user_id of the contact person
     * @bodyParam user.first_name string required the first name of the user
     * @bodyParam user.last_name string optional the last name of the user
     * @bodyParam user.email email required the email of the user
     * @bodyParam user.mobile string optional the phone number of the user
     * @bodyParam user.avatar file optional the photo of the user of type:png,jpg,jpeg etc
     * @bodyParam user.notify boolean optional whether to notify the user or not
     * @bodyParam user.notes text optional the extra information regarding the user
     * @bodyParam user.title_id integer optional the title of the user
     * @bodyParam user.telephone string optional the telephone number of the user
     * @response json array type
     */
    public function testContactPersonRestoreWithValidTokenSoftDeletedID()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('GET', '/v1/contact_persons/restore/2', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);

        //match each index value
        $this->assertSame([$this->__getTranslateString('exists_validation', ['attribute' => 'id'], $lang)],
            $json['id']);
    }


    /**
     * Case 6
     * @test This function test response with valid token and valid id
     * @expectedExceptionCode 200
     * Update a contact person
     * @bodyParam id integer required the id of the contact person to update get using route
     * @bodyParam user.id integer required the user_id of the contact person
     * @bodyParam user.first_name string required the first name of the user
     * @bodyParam user.last_name string optional the last name of the user
     * @bodyParam user.email email required the email of the user
     * @bodyParam user.mobile string optional the phone number of the user
     * @bodyParam user.avatar file optional the photo of the user of type:png,jpg,jpeg etc
     * @bodyParam user.notify boolean optional whether to notify the user or not
     * @bodyParam user.notes text optional the extra information regarding the user
     * @bodyParam user.title_id integer optional the title of the user
     * @bodyParam user.telephone string optional the telephone number of the user
     * @response json array type
     */
    public function testContactPersonRestoreWithValidTokenValidID()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $contact_person = ContactPerson::find(2);
        $contact_person->delete();
        User::find($contact_person->user_id)->delete();
        $response = $this->call('GET', '/v1/contact_persons/restore/2', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(200);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('contact_person_restored', [], $lang), $json['success']);
    }

    /**
     * Case 7
     * @test Customer dont't have permissions to view this page.
     * @expectedExceptionCode 403
     * Update a contact person
     * @bodyParam id integer required the id of the contact person to update get using route
     * @bodyParam user.id integer required the user_id of the contact person
     * @bodyParam user.first_name string required the first name of the user
     * @bodyParam user.last_name string optional the last name of the user
     * @bodyParam user.email email required the email of the user
     * @bodyParam user.mobile string optional the phone number of the user
     * @bodyParam user.avatar file optional the photo of the user of type:png,jpg,jpeg etc
     * @bodyParam user.notify boolean optional whether to notify the user or not
     * @bodyParam user.notes text optional the extra information regarding the user
     * @bodyParam user.title_id integer optional the title of the user
     * @bodyParam user.telephone string optional the telephone number of the user
     * @response json array type
     */
    public function testContactPersonRestoreWithValidTokenWithoutPramsForCustomer()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->customer);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);


        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('DELETE', '/v1/consultant_company/delete/2', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(403);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame(
            $this->__getTranslateString('insufficient_permission', [], $lang)
            , $json['error']);
    }

    /**
     * Case 8
     * @test Employee dont't have permissions to view this page.
     * @expectedExceptionCode 403
     * Update a contact person
     * @bodyParam id integer required the id of the contact person to update get using route
     * @bodyParam user.id integer required the user_id of the contact person
     * @bodyParam user.first_name string required the first name of the user
     * @bodyParam user.last_name string optional the last name of the user
     * @bodyParam user.email email required the email of the user
     * @bodyParam user.mobile string optional the phone number of the user
     * @bodyParam user.avatar file optional the photo of the user of type:png,jpg,jpeg etc
     * @bodyParam user.notify boolean optional whether to notify the user or not
     * @bodyParam user.notes text optional the extra information regarding the user
     * @bodyParam user.title_id integer optional the title of the user
     * @bodyParam user.telephone string optional the telephone number of the user
     * @response json array type
     */
    public function testContactPersonRestoreWithValidTokenWithoutPramsForEmployee()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->employee);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);


        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('DELETE', '/v1/consultant_company/delete/2', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(403);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
//        match each index value
        $this->assertSame(
            $this->__getTranslateString('insufficient_permission', [], $lang)
            , $json['error']);
    }
}
