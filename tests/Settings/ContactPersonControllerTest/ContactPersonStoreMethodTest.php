<?php

/**
 * ConsultantStoreMethodTest class
 * Include all possible test cases for Store Methods
 */
class ContactPersonStoreMethodTest extends TestCase
{
    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'custom@yopmail.com',
        'password' => '123456'
    ];
    public $employee = [
        'email' => 'emp1@yopmail.com',
        'password' => '123456'
    ];

    /**
     *  Read.
     *
     *  Admin and Employee holds same permissions .
     *  Customer dont't have permissions to view this page.
     *  Contact-Persons dont't have permission to view this page.
     */

    /**
     * Case 1
     * @test This function test response without auth token without params
     * @expectedExceptionCode 401
     * create a contact person
     * @bodyParam user.first_name string required the first name of the user
     * @bodyParam user.last_name string optional the last name of the user
     * @bodyParam user.email email required the email of the user
     * @bodyParam user.mobile string optional the phone number of the user
     * @bodyParam user.avatar file optional the photo of the user of type:png,jpg,jpeg etc
     * @bodyParam user.notify boolean optional whether to notify the user or not
     * @bodyParam user.notes text optional the extra information regarding the user
     * @bodyParam user.title_id integer optional the title of the user
     * @bodyParam user.telephone string optional the telephone number of the user
     * @response json array type
     */
    public function testContactPersonStoreWithoutToken()
    {
        $this->post('/v1/consultants/add', [])
            ->seeJsonEquals([
                "error" => $this->__getTranslateString('missing_token_error', [], 2)
            ]);
    }

    /**
     * Case 2
     * @test This function test response with invalid auth token without params
     * @expectedExceptionCode 400
     * create a contact person
     * @bodyParam user.first_name string required the first name of the user
     * @bodyParam user.last_name string optional the last name of the user
     * @bodyParam user.email email required the email of the user
     * @bodyParam user.mobile string optional the phone number of the user
     * @bodyParam user.avatar file optional the photo of the user of type:png,jpg,jpeg etc
     * @bodyParam user.notify boolean optional whether to notify the user or not
     * @bodyParam user.notes text optional the extra information regarding the user
     * @bodyParam user.title_id integer optional the title of the user
     * @bodyParam user.telephone string optional the telephone number of the user
     * @response json array type
     */
    public function testContactPersonStoreWithInvalidToken()
    {
        $expired_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expired_token
        ];

        $response = $this->call('POST', '/v1/contact_persons/add', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(400);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('token_expired_error', [], 2), $json['error']);
    }

    /**
     * Case 3
     * @test This function test response with valid token without params
     * @expectedExceptionCode 422
     * create a contact person
     * @bodyParam user.first_name string required the first name of the user
     * @bodyParam user.last_name string optional the last name of the user
     * @bodyParam user.email email required the email of the user
     * @bodyParam user.mobile string optional the phone number of the user
     * @bodyParam user.avatar file optional the photo of the user of type:png,jpg,jpeg etc
     * @bodyParam user.notify boolean optional whether to notify the user or not
     * @bodyParam user.notes text optional the extra information regarding the user
     * @bodyParam user.title_id integer optional the title of the user
     * @bodyParam user.telephone string optional the telephone number of the user
     * @response json array type
     */
    public function testContactPersonStoreWithValidTokenWithoutPrams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('POST', '/v1/contact_persons/add', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'user.first_name'], $lang)
        ], $json['user.first_name']);
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'user.email'], $lang)],
            $json['user.email']);
    }

    /**
     * Case 4
     * @test This function test response with valid token and null params
     * @expectedExceptionCode 422
     * create a contact person
     * @bodyParam user.first_name string required the first name of the user
     * @bodyParam user.last_name string optional the last name of the user
     * @bodyParam user.email email required the email of the user
     * @bodyParam user.mobile string optional the phone number of the user
     * @bodyParam user.avatar file optional the photo of the user of type:png,jpg,jpeg etc
     * @bodyParam user.notify boolean optional whether to notify the user or not
     * @bodyParam user.notes text optional the extra information regarding the user
     * @bodyParam user.title_id integer optional the title of the user
     * @bodyParam user.telephone string optional the telephone number of the user
     * @response json array type
     */
    public function testContactPersonStoreWithValidTokenNullParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'user' => [
                'first_name' => null,
                'email' => null,
                'mobile' => null,
                'notify' => 'null'
            ],
            'consultant_company.id' => null,
            'consultant_title.id' => null
        ];

        $response = $this->call('POST', '/v1/contact_persons/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'user.first_name'], $lang)
        ], $json['user.first_name']);
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'user.email'], $lang)],
            $json['user.email']);
    }

    /**
     * Case 5
     * @test This function test response with valid token but with user.first_name having length max to 191
     * @expectedExceptionCode 422
     * create a contact person
     * @bodyParam user.first_name string required the first name of the user
     * @bodyParam user.last_name string optional the last name of the user
     * @bodyParam user.email email required the email of the user
     * @bodyParam user.mobile string optional the phone number of the user
     * @bodyParam user.avatar file optional the photo of the user of type:png,jpg,jpeg etc
     * @bodyParam user.notify boolean optional whether to notify the user or not
     * @bodyParam user.notes text optional the extra information regarding the user
     * @bodyParam user.title_id integer optional the title of the user
     * @bodyParam user.telephone string optional the telephone number of the user
     * @response json array type
     */
    public function testContactPersonStoreWithValidAuthParamsAndNameHavingLengthMore()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'user' => [
                'first_name' => 'adsfdfjhbjsbdfjaaaaaaaaaaaaaaaaaaaa ajbjjhdf jajbfdj absjdfb jasdbfj basjdfb jasbdfjajsdfsdfffffffffffffffffffffffffffffffsffffffffffffffffffffffffffffffffffffffffffdahjdafajhbdsfhjbhjbjhdsfdsfdfgfdgd',
                'email' => 'dummy2@mail.in',
                'mobile' => '987456231',
                'title_id' => 2,
                'telephone' => 1234554321,
                'notify' => 1,
                'last_name' => 'person',
                'notes' => 'test notes',

            ],
        ];

        $response = $this->call('POST', '/v1/contact_persons/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('max_validation', ['attribute' => 'user.first_name', 'max' => 191], $lang)
        ], $json['user.first_name']);
    }

    /**
     * Case 6
     * @test This function test response with valid token but with user.email having normal string instead of valid email
     * @expectedExceptionCode 422
     * create a contact person
     * @bodyParam user.first_name string required the first name of the user
     * @bodyParam user.last_name string optional the last name of the user
     * @bodyParam user.email email required the email of the user
     * @bodyParam user.mobile string optional the phone number of the user
     * @bodyParam user.avatar file optional the photo of the user of type:png,jpg,jpeg etc
     * @bodyParam user.notify boolean optional whether to notify the user or not
     * @bodyParam user.notes text optional the extra information regarding the user
     * @bodyParam user.title_id integer optional the title of the user
     * @bodyParam user.telephone string optional the telephone number of the user
     * @response json array type
     */
    public function testContactPersonStoreWithValidTokenAndEmailISNormalString()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'user' => [
                'first_name' => 'asdadsdas1',
                'email' => 'dummy2as',
                'mobile' => '987456231',
                'title_id' => 2,
                'telephone' => 1234554321,
                'notify' => 1,
                'last_name' => 'person',
                'notes' => 'test notes',

            ],
        ];
        $response = $this->call('POST', '/v1/contact_persons/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([$this->__getTranslateString('email_validation', ['attribute' => 'user.email'], $lang)],
            $json['user.email']);
    }


    /**
     * Case 7
     * @test This function test response with valid token but with user.email existing email
     * @expectedExceptionCode 422
     * create a contact person
     * @bodyParam user.first_name string required the first name of the user
     * @bodyParam user.last_name string optional the last name of the user
     * @bodyParam user.email email required the email of the user
     * @bodyParam user.mobile string optional the phone number of the user
     * @bodyParam user.avatar file optional the photo of the user of type:png,jpg,jpeg etc
     * @bodyParam user.notify boolean optional whether to notify the user or not
     * @bodyParam user.notes text optional the extra information regarding the user
     * @bodyParam user.title_id integer optional the title of the user
     * @bodyParam user.telephone string optional the telephone number of the user
     * @response json array type
     */
    public function testContactPersonStoreWithValidTokenAndUsingExistingToken()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            'user' => [
                'first_name' => 'TestDummyContactPerson',
                'email' => 'testusderdum1fdgsdfmy@host.com',
                'mobile' => '987456231',
                'title_id' => 2,
                'telephone' => 1234554321,
                'notify' => 1,
                'last_name' => 'person',
                'notes' => 'test notes',

            ],
        ];

        $response = $this->call('POST', '/v1/contact_persons/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
    }

    /**
     * Case 8
     * @test This function test response with valid token but with user.email having length max to 191
     * @expectedExceptionCode 422
     * create a contact person
     * @bodyParam user.first_name string required the first name of the user
     * @bodyParam user.last_name string optional the last name of the user
     * @bodyParam user.email email required the email of the user
     * @bodyParam user.mobile string optional the phone number of the user
     * @bodyParam user.avatar file optional the photo of the user of type:png,jpg,jpeg etc
     * @bodyParam user.notify boolean optional whether to notify the user or not
     * @bodyParam user.notes text optional the extra information regarding the user
     * @bodyParam user.title_id integer optional the title of the user
     * @bodyParam user.telephone string optional the telephone number of the user
     * @response json array type
     */
    public function testContactPersonStoreWithValidTokenAndEmailHavingMaxLength()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            'user' => [
                'first_name' => 'TestDummyContactPerson',
                'email' => 'adsfdfjhbjsbdfja ajbjjhdf jajbfdj absjdfb jasdbfj bsdfdsfsdfsdasjdfb jasbdfjajsdaghvsfdghsfdghsfdghhjdafajhbdsfhjfghfghfghbhjbjhdsffdggggggggggggggggggggggggggggggggggggggggggggggggggggggggdsfdfgfdgd@host.com',
                'mobile' => '987456231',
                'title_id' => 2,
                'telephone' => 1234554321,
                'notify' => 1,
                'last_name' => 'person',
                'notes' => 'test notes',

            ],
        ];

        $response = $this->call('POST', '/v1/contact_persons/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('email_validation', ['attribute' => 'user.email'], $lang),
            $this->__getTranslateString('max_validation', ['attribute' => 'user.email', 'max' => 191], $lang)
        ], $json['user.email']);
    }

    /**
     * Case 9
     * @test This function test response with valid token but with user.mobile having length max to 20
     * @expectedExceptionCode 422
     * create a contact person
     * @bodyParam user.first_name string required the first name of the user
     * @bodyParam user.last_name string optional the last name of the user
     * @bodyParam user.email email required the email of the user
     * @bodyParam user.mobile string optional the phone number of the user
     * @bodyParam user.avatar file optional the photo of the user of type:png,jpg,jpeg etc
     * @bodyParam user.notify boolean optional whether to notify the user or not
     * @bodyParam user.notes text optional the extra information regarding the user
     * @bodyParam user.title_id integer optional the title of the user
     * @bodyParam user.telephone string optional the telephone number of the user
     * @response json array type
     */
    public function testContactPersonStoreWithValidTokenAndMobileHavingMaxLength()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'user' => [
                'first_name' => 'TestDummyContactPerson',
                'email' => 'adsfdfjhbjsbdfja ajbjjhdf jajbfdj absjdfb jasdbfj bsdfdsfsdfsdasjdfb jasbdfjajsdaghvsfdghsfdghsfdghhjdafajhbdsfhjfghfghfghbhjbjhdsffdggggggggggggggggggggggggggggggggggggggggggggggggggggggggdsfdfgfdgd@host.com',
                'mobile' => '987456231987456231145',
                'title_id' => 2,
                'telephone' => 1234554321,
                'notify' => 1,
                'last_name' => 'person',
                'notes' => 'test notes',

            ],
        ];


        $response = $this->call('POST', '/v1/contact_persons/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('max_validation', ['attribute' => 'user.mobile', 'max' => 12], $lang)
        ], $json['user.mobile']);
    }
//

    /**
     * Case 10
     * @test This function test response with valid token but with user.mobile with string instead of number
     * @expectedExceptionCode 422
     * create a contact person
     * @bodyParam user.first_name string required the first name of the user
     * @bodyParam user.last_name string optional the last name of the user
     * @bodyParam user.email email required the email of the user
     * @bodyParam user.mobile string optional the phone number of the user
     * @bodyParam user.avatar file optional the photo of the user of type:png,jpg,jpeg etc
     * @bodyParam user.notify boolean optional whether to notify the user or not
     * @bodyParam user.notes text optional the extra information regarding the user
     * @bodyParam user.title_id integer optional the title of the user
     * @bodyParam user.telephone string optional the telephone number of the user
     * @response json array type
     */
    public function testContactPersonStoreWithValidTokenAndUserMobileStringInsteadOfNumber()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'user' => [
                'first_name' => 'TestDummyContactPerson',
                'email' => 'fgfdgd@host.com',
                'mobile' => 'sadasdsadas',
                'title_id' => 1,
                'telephone' => 1234554321,
                'notify' => 1,
                'last_name' => 'person',
                'notes' => 'test notes',

            ],
        ];

        $response = $this->call('POST', '/v1/contact_persons/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
    }

}
