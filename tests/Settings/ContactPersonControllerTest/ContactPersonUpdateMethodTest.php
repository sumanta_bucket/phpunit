<?php


use App\Models\ContactPerson;

class ContactPersonUpdateMethodTest extends TestCase
{
    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'custom@yopmail.com',
        'password' => '123456'
    ];
    public $employee = [
        'email' => 'emp1@yopmail.com',
        'password' => '123456'
    ];

    /**
     * Case 1
     * @test This function test response without auth token without params
     * @expectedExceptionCode 401
     * Update a contact person
     * @bodyParam id integer required the id of the contact person to update get using route
     * @bodyParam user.id integer required the user_id of the contact person
     * @bodyParam user.first_name string required the first name of the user
     * @bodyParam user.last_name string optional the last name of the user
     * @bodyParam user.email email required the email of the user
     * @bodyParam user.mobile string optional the phone number of the user
     * @bodyParam user.avatar file optional the photo of the user of type:png,jpg,jpeg etc
     * @bodyParam user.notify boolean optional whether to notify the user or not
     * @bodyParam user.notes text optional the extra information regarding the user
     * @bodyParam user.title_id integer optional the title of the user
     * @bodyParam user.telephone string optional the telephone number of the user
     * @response json array type
     */
    public function testContactPersonUpdateWithoutTokenWithoutParams()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('contact_persons')->truncate();
        DB::table('consultant_titles')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        app(DatabaseSeeder::class)->call(ContactPersonSeeder::class);
        $this->post('/v1/contact_persons/update/{id}', [])
            ->seeJsonEquals([
                "error" => $this->__getTranslateString('missing_token_error', [], 2)
            ]);
    }

    /**
     * Case 2
     * @test This function test response with invalid auth token without params
     * @test This function test response without auth token without params
     * @expectedExceptionCode 400
     * Update a contact person
     * @bodyParam id integer required the id of the contact person to update get using route
     * @bodyParam user.id integer required the user_id of the contact person
     * @bodyParam user.first_name string required the first name of the user
     * @bodyParam user.last_name string optional the last name of the user
     * @bodyParam user.email email required the email of the user
     * @bodyParam user.mobile string optional the phone number of the user
     * @bodyParam user.avatar file optional the photo of the user of type:png,jpg,jpeg etc
     * @bodyParam user.notify boolean optional whether to notify the user or not
     * @bodyParam user.notes text optional the extra information regarding the user
     * @bodyParam user.title_id integer optional the title of the user
     * @bodyParam user.telephone string optional the telephone number of the user
     * @response json array type
     */
    public function testContactPersonUpdateWithInvalidAuthTokenWithoutParams()
    {
        $expired_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expired_token
        ];

        $response = $this->call('POST', '/v1/contact_persons/update/{id}', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(400);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('token_expired_error', [], 2), $json['error']);
    }

    /**
     * Case 3
     * @test This function test response with valid token without params
     * @test This function test response without auth token without params
     * @expectedExceptionCode 422
     * Update a contact person
     * @bodyParam id integer required the id of the contact person to update get using route
     * @bodyParam user.id integer required the user_id of the contact person
     * @bodyParam user.first_name string required the first name of the user
     * @bodyParam user.last_name string optional the last name of the user
     * @bodyParam user.email email required the email of the user
     * @bodyParam user.mobile string optional the phone number of the user
     * @bodyParam user.avatar file optional the photo of the user of type:png,jpg,jpeg etc
     * @bodyParam user.notify boolean optional whether to notify the user or not
     * @bodyParam user.notes text optional the extra information regarding the user
     * @bodyParam user.title_id integer optional the title of the user
     * @bodyParam user.telephone string optional the telephone number of the user
     * @response json array type
     */
    public function testContactPersonUpdateWithValidTokenWithoutParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('POST', '/v1/contact_persons/update/1', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'user.id'], $lang)],
            $json['user.id']);
    }

    /**
     * Case 4
     * @test This function test response with valid token and null params
     * @test This function test response without auth token without params
     * @expectedExceptionCode 422
     * Update a contact person
     * @bodyParam id integer required the id of the contact person to update get using route
     * @bodyParam user.id integer required the user_id of the contact person
     * @bodyParam user.first_name string required the first name of the user
     * @bodyParam user.last_name string optional the last name of the user
     * @bodyParam user.email email required the email of the user
     * @bodyParam user.mobile string optional the phone number of the user
     * @bodyParam user.avatar file optional the photo of the user of type:png,jpg,jpeg etc
     * @bodyParam user.notify boolean optional whether to notify the user or not
     * @bodyParam user.notes text optional the extra information regarding the user
     * @bodyParam user.title_id integer optional the title of the user
     * @bodyParam user.telephone string optional the telephone number of the user
     * @response json array type
     */
    public function testContactPersonUpdateWithValidTokenAndNullParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'user' => [
                'id' => 12,
                'first_name' => null,
                'email' => null,
                'mobile' => null,
                'title_id' => null,
                'telephone' => null,
                'notify' => null,
                'last_name' => null,
                'notes' => null,
            ],
        ];

        $response = $this->call('POST', '/v1/contact_persons/update/1', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
//        $this->assertSame([$this->__getTranslateString('numeric_validation', ['attribute' => 'user.title_id'], $lang)],
//            $json['user.title_id']);

    }

    /**
     * Case 5
     * @test This function test response with valid token but with user.first_name having length max to 191
     * @test This function test response without auth token without params
     * @expectedExceptionCode 422
     * Update a contact person
     * @bodyParam id integer required the id of the contact person to update get using route
     * @bodyParam user.id integer required the user_id of the contact person
     * @bodyParam user.first_name string required the first name of the user
     * @bodyParam user.last_name string optional the last name of the user
     * @bodyParam user.email email required the email of the user
     * @bodyParam user.mobile string optional the phone number of the user
     * @bodyParam user.avatar file optional the photo of the user of type:png,jpg,jpeg etc
     * @bodyParam user.notify boolean optional whether to notify the user or not
     * @bodyParam user.notes text optional the extra information regarding the user
     * @bodyParam user.title_id integer optional the title of the user
     * @bodyParam user.telephone string optional the telephone number of the user
     * @response json array type
     */
    public function testCaseFive()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $contactPerson = ContactPerson::first();
        $param = [
            'user' => [
                'id' => $contactPerson->user_id,
                'first_name' => 'adsfdfjhbjsbdfjafhdhfddhgdhdhgddhfgdhfddh ajbjjhdf jajbfdj absjdfb jasdbfj bsdfdsfsdfsdasjdfb jasbdfjajsdaghvsfdghsfdghsfdghhjdafajhbdsfhjfghfghfghbhjbjhdsffdggggggggggggggggggggggggggggggggggggggggggggggggggggggggdsfdfgfdgd',
                'mobile' => 9547996145,
                'title_id' => 'adasdasd',
                'telephone' => 'asdasd',
                'notify' => 1,
                'last_name' => "muasdas",
                'notes' => 'asdasd',
                'email' => 'asdasd@syz.com',
            ],
        ];

        $response = $this->call('POST', '/v1/contact_persons/update/1', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('max_validation', ['attribute' => 'user.first_name', 'max' => 191], $lang)
        ], $json['user.first_name']);
    }

    /**
     * Case 6
     * This function test response with valid token but with user.email having normal string instead of valid email
     *
     */
    public function testContactPersonUpdateWithValidTokenAndUserEmailAsNormalString()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $contactPerson = ContactPerson::first();
        $param = [
            'user' => [
                'id' => $contactPerson->user_id,
                'first_name' => 'test user first name',
                'email' => 'testusderdum1my',
                'mobile' => '987456231',
                'title_id' => 'adasdasd',
                'telephone' => 'asdasd',
                'notify' => 1,
                'last_name' => "muasdas",
                'notes' => 'asdasd',
            ],
        ];

        $response = $this->call('POST', '/v1/contact_persons/update/1', $param, [], [], $headers);

        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([$this->__getTranslateString('email_validation', ['attribute' => 'user.email'], $lang)],
            $json['user.email']);
    }

    /**
     * Case 8
     * This function test response with valid token but with user.email having length max to 191
     *
     */
    public function testContactPersonUpdateWithValidTokenAndUserEmailIsMax()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $contactPerson = ContactPerson::first();
        $param = [
            'user' => [
                'id' => $contactPerson->user_id,
                'first_name' => 'test user first name',
                'email' => 'adsfdfjhbjsbdfja ajbjjhdf jajbfdj absjdfb jasdbfj bsdfdsfsdfsdasjdfb jasbdfjajsdaghvsfdghsfdghsfdghhjdafajhbdsfhjfghfghfghbhjbjhdsffdggggggggggggggggggggggggggggggggggggggggggggggggggggggggdsfdfgfdgd@host.com',
                'mobile' => '987456231',
                'title_id' => 'adasdasd',
                'telephone' => 1321324367541,
                'notify' => 1,
                'last_name' => "muasdas",
                'notes' => 'asdasd',

            ],
        ];

        $response = $this->call('POST', '/v1/contact_persons/update/1', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('email_validation', ['attribute' => 'user.email'], $lang),
            $this->__getTranslateString('max_validation', ['attribute' => 'user.email', 'max' => 191], $lang)
        ], $json['user.email']);
    }

    /**
     * Case 9
     * This function test response with valid token but with user.mobile having length max to 12
     *
     */
    public function testContactPersonUpdateWithValidTokenAndMobileLengthIsMax()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $contactPerson = ContactPerson::first();
        $param = [
            'user' => [
                'id' => $contactPerson->user_id,
                'first_name' => 'test user first name',
                'email' => 'testusderdum1my@host.com',
                'mobile' => '987456231987456231145',
                'title_id' => 'adasdasd',
                'telephone' => 1321324367541,
                'notify' => 1,
                'last_name' => "muasdas",
                'notes' => 'asdasd',
            ],
        ];

        $response = $this->call('POST', '/v1/contact_persons/update/1', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('max_validation', ['attribute' => 'user.mobile', 'max' => 12], $lang)
        ], $json['user.mobile']);

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('contact_persons')->truncate();
        DB::table('consultant_titles')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
