<?php

/**
 * CustomerEditMethodTest class
 * Include all possible test cases for Edit Methods
 */
class CustomerEditMethodTest extends TestCase
{
    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'custom@yopmail.com',
        'password' => '123456'
    ];
    public $employee = [
        'email' => 'emp1@yopmail.com',
        'password' => '123456'
    ];

    /**
     * Case 1
     * @test This function test response without auth token without params
     * @expectedExceptionCode 401
     * Retrieve a customer
     * @bodyParam id integer required the id of the record being fetchd
     * @response json array type
     */
    public function testCustomerControllerEditMethodWithoutAuthTokenAndWithoutParams()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('customers')->truncate();
        DB::table('consultant_titles')->truncate();
        DB::table('categories')->truncate();
        DB::table('contact_persons')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        app(DatabaseSeeder::class)->call(CustomerSeeder::class);


        $response = $this->call('GET', '/v1/customers/get/1', [], [], [], []);
        //check response status code
        $this->assertResponseStatus(401);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('missing_token_error', [], 2), $json['error']);
    }


    /**
     * Case 2
     * @test This function test response with invalid auth token without params
     * @expectedExceptionCode 400
     * Retrieve a customer
     * @bodyParam id integer required the id of the record being fetchd
     * @response json array type
     */
    public function testCustomerControllerEditWithInvalidAuthTokenWithoutParams()
    {
        $expired_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expired_token
        ];

        $response = $this->call('GET', '/v1/customers/get/1', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(400);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('token_expired_error', [], 2), $json['error']);
    }

    /**
     * Case 3
     * @test This function test response with valid token without params
     * @expectedExceptionCode 404
     * Retrieve a customer
     * @bodyParam id integer required the id of the record being fetchd
     * @response json array type
     */
    public function testCustomerControllerEditWithValidTokenWithoutParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);

        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('GET', '/v1/customers/get/', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(404);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('route_not_found', []), $json['error']);
    }

    /**
     * Case 4
     * @test This function test response with valid token With Params and invalid Id
     * @expectedExceptionCode 404
     * Retrieve a customer
     * @bodyParam id integer required the id of the record being fetchd
     * @response json array type
     */
    public function testCustomerControllerEditWithValidAuthAndInvalidId()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('GET', '/v1/customers/get/0', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(404);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('no_record_found', [], $lang), $json['error']);
    }

    /**
     * Case 5
     * This function test response with valid token but with soft deleted id
     * @expectedExceptionCode 422
     * Retrieve a customer
     * @bodyParam id integer required the id of the record being fetchd
     * @response json array type
     */
    public function testCaseFive()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);

        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('GET', '/v1/customers/get/1', [], [], [], $headers);
    }

    /**
     * Case 6
     * @test This function test response with valid token and valid id
     * @expectedExceptionCode 200
     * Retrieve a customer
     * @bodyParam id integer required the id of the record being fetchd
     * @response json array type
     */
    public function testCustomerControllerEditWithValidTokenAndValidID()
    {
        $consultantsData = $this->__getCustomersData(3);
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('GET', '/v1/customers/get/3', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(200);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('success', [], $lang), $json['success']);
        $this->assertSame($consultantsData['id'], $json['data']['id']);
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('customers')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

    public function __getCustomersData($id)
    {
        $consultant = App\Models\Customer::with('user')->where('id', $id)->first();
        return $consultant;
    }
}
