<?php

use Illuminate\Support\Facades\Config;

/**
 * CustomerIndexMethodTest class
 * Include all possible test cases for Index Methods
 */
class CustomerIndexMethodTest extends TestCase
{
    public  $loginParam = [
        'email'=>'admin@bcbrand.com',
        'password'=>'demo123@'
    ];

    /**
     * Case 1
     * @test This function test response without auth token without params
     * @expectedExceptionCode 401
     * List all customers
     * @bodyParam order.0.dir string required the direction of the records in ASC or DESC direction
     * @bodyParam order.0.column string required the order by column name in ASC or DESC direction
     * @bodyParam search.value  string required the value to be searched in the records
     * @bodyParam start integer required the value from start getting records
     * @bodyParam length integer required the number of records want to fetch
     * @response json array type in variable data
     */
    public function testCustomerControllerIndexMethodWithoutAuthTokenAndWithoutParams()
    {
        $this->post('/v1/customers/index/0', [])
        ->seeJsonEquals([
            "error" => $this->__getTranslateString('missing_token_error',[], Config::get('constant.default_lang'))
         ]);
    }

    /**
     * Case 2
     * @test This function test response with invalid auth token without params
     * @expectedExceptionCode 401
     * List all customers
     * @bodyParam order.0.dir string required the direction of the records in ASC or DESC direction
     * @bodyParam order.0.column string required the order by column name in ASC or DESC direction
     * @bodyParam search.value  string required the value to be searched in the records
     * @bodyParam start integer required the value from start getting records
     * @bodyParam length integer required the number of records want to fetch
     * @response json array type in variable data
     */
    public function testCustomerControllerIndexMethodWithInvalidAuthTokenWithoutParams()
    {
        $expired_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expired_token
        ];

        $response = $this->call('POST','/v1/customers/index/', [],[],[],$headers);

        //check response status code
        $this->assertResponseStatus(404);

        //convert response data in json
        $json = json_decode($response->getContent(),true);
        //match each index value
        $this->assertSame($this->__getTranslateString('route_not_found',[],Config::get('constant.default_lang')), $json['error']);
    }

    /**
     * Case 3
     * @test This function test response with valid token without params
     * @expectedExceptionCode 404
     * List all customers
     * @bodyParam order.0.dir string required the direction of the records in ASC or DESC direction
     * @bodyParam order.0.column string required the order by column name in ASC or DESC direction
     * @bodyParam search.value  string required the value to be searched in the records
     * @bodyParam start integer required the value from start getting records
     * @bodyParam length integer required the number of records want to fetch
     * @response json array type in variable data
     */
    public function testCustomerControllerIndexMethodWithValidTokenAndWithoutParams()
    {
     //hit login call fot getting auth token
        $loginJson = $this->getLoginInstance($this->loginParam);

        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('POST','/v1/customers/index/', [],[],[],$headers);
        //check response status code
        $this->assertResponseStatus(404);
    }

    /**
     * Case 4
     * @test This function test response with valid token and null params
     * @expectedExceptionCode 404
     * List all customers
     * @bodyParam order.0.dir string required the direction of the records in ASC or DESC direction
     * @bodyParam order.0.column string required the order by column name in ASC or DESC direction
     * @bodyParam search.value  string required the value to be searched in the records
     * @bodyParam start integer required the value from start getting records
     * @bodyParam length integer required the number of records want to fetch
     * @response json array type in variable data
     */
    public function testCustomerControllerIndexMethodWithValidTokenAndNullParams()
    {
     //hit login call fot getting auth token
        $loginJson = $this->getLoginInstance($this->loginParam);

        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'order' => [
                [
                    'dir' => null,
                    'column' => null
                ]
            ],
            'search' => ['value' => null],
            'start' => null,
            'length' => null
        ];

        $response = $this->call('POST','/v1/customers/index', $param,[],[],$headers);
        //check response status code
        $this->assertResponseStatus(404);
    }

}
