<?php

/**
 * CustomerStoreMethodTest class
 * Include all possible test cases for Store Methods
 */
class CustomerStoreMethodTest extends TestCase
{
    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'custom@yopmail.com',
        'password' => '123456'
    ];
    public $employee = [
        'email' => 'emp1@yopmail.com',
        'password' => '123456'
    ];

    /**
     * Case 1
     * @test This function test response without auth token without params
     * @expectedExceptionCode 401
     * Create a customer
     * @bodyParam user.first_name string required the first_name field of the user
     * @bodyParam user.email string required the email field of the user
     * @bodyParam user.address string optional the address field of the user
     * @bodyParam user.city string optional the city field of the user
     * @bodyParam user.zip string optional the zip code of the customer
     * @bodyParam user.mobile string optional the mobile number of the customer
     * @bodyParam user.notes string optional extra information about the user
     * @bodyParam user.notify integer optional whether to notify the customer about account or not 1=true ,0=false
     * @bodyParam address2 string optional the address2 of the customer
     * @bodyParam country string optional the country code of the customer
     * @bodyParam categories.*. id integer optional the category id of the customer
     * @bodyParam contact_persons.*.id integer optional the contact person id of the customer
     * @response json array type
     */
    public function testCustomerControllerStoreMethodWithoutAuthTokenAndWithoutParams()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('customers')->truncate();
        DB::table('contact_persons')->truncate();
        DB::table('consultant_titles')->truncate();

        DB::table('categories')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        app(DatabaseSeeder::class)->call(CustomerSeeder::class);

        $this->post('/v1/customers/add', [])
            ->seeJsonEquals([
                "error" => $this->__getTranslateString('missing_token_error', [], 2)
            ]);
    }

    /**
     * Case 2
     * @test This function test response with invalid auth token without params
     * @expectedExceptionCode 400
     * Create a customer
     * @bodyParam user.first_name string required the first_name field of the user
     * @bodyParam user.email string required the email field of the user
     * @bodyParam user.address string optional the address field of the user
     * @bodyParam user.city string optional the city field of the user
     * @bodyParam user.zip string optional the zip code of the customer
     * @bodyParam user.mobile string optional the mobile number of the customer
     * @bodyParam user.notes string optional extra information about the user
     * @bodyParam user.notify integer optional whether to notify the customer about account or not 1=true ,0=false
     * @bodyParam address2 string optional the address2 of the customer
     * @bodyParam country string optional the country code of the customer
     * @bodyParam categories.*. id integer optional the category id of the customer
     * @bodyParam contact_persons.*.id integer optional the contact person id of the customer
     * @response json array type
     */
    public function testCustomerControllerStoreMethodWithInvalidAuthTokenAndWithoutParams()
    {
        $expired_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expired_token
        ];

        $response = $this->call('POST', '/v1/customers/add', [], [], [], $headers);
        //check response status code


        $this->assertResponseStatus(400);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('token_expired_error', [], 2), $json['error']);
    }

    /**
     * Case 3
     * @test This function test response with valid token without params
     * @expectedExceptionCode 422
     * Create a customer
     * @bodyParam user.first_name string required the first_name field of the user
     * @bodyParam user.email string required the email field of the user
     * @bodyParam user.address string optional the address field of the user
     * @bodyParam user.city string optional the city field of the user
     * @bodyParam user.zip string optional the zip code of the customer
     * @bodyParam user.mobile string optional the mobile number of the customer
     * @bodyParam user.notes string optional extra information about the user
     * @bodyParam user.notify integer optional whether to notify the customer about account or not 1=true ,0=false
     * @bodyParam address2 string optional the address2 of the customer
     * @bodyParam country string optional the country code of the customer
     * @bodyParam categories.*. id integer optional the category id of the customer
     * @bodyParam contact_persons.*.id integer optional the contact person id of the customer
     * @response json array type
     */
    public function testCustomerControllerStoreMethodWithValidTokenAndWithoutParams()
    {
        //hit login call fot getting auth token
        $loginJson = $this->getLoginInstance($this->loginParam);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('POST', '/v1/customers/add', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'user.first_name'], $lang)
        ], $json['user.first_name']);
    }

    /**
     * Case 4
     * @test This function test response with valid token and null params
     * @expectedExceptionCode 422
     * Create a customer
     * @bodyParam user.first_name string required the first_name field of the user
     * @bodyParam user.email string required the email field of the user
     * @bodyParam user.address string optional the address field of the user
     * @bodyParam user.city string optional the city field of the user
     * @bodyParam user.zip string optional the zip code of the customer
     * @bodyParam user.mobile string optional the mobile number of the customer
     * @bodyParam user.notes string optional extra information about the user
     * @bodyParam user.notify integer optional whether to notify the customer about account or not 1=true ,0=false
     * @bodyParam address2 string optional the address2 of the customer
     * @bodyParam country string optional the country code of the customer
     * @bodyParam categories.*. id integer optional the category id of the customer
     * @bodyParam contact_persons.*.id integer optional the contact person id of the customer
     * @response json array type
     */
    public function testCustomerControllerStoreMethodWithValidTokenAndWithNullParams()
    {
        //hit login call fot getting auth token
        $loginJson = $this->getLoginInstance($this->loginParam);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            'user' => [
                'first_name' => null,
                'city' => null,
                'zip' => null,
                'mobile' => null,
                'email' => null,
                'notes' => null,
                'notify' => null,
                'address' => null,
            ],
            'categories[0]' => ["id" => null],
            'contact_persons[0]' => ["id" => null],
        ];

        $response = $this->call('POST', '/v1/customers/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        //
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'user.first_name'], $lang)
        ], $json['user.first_name']);
    }


    /**
     * Case 5
     * @test This function test response with valid token but with user.first_name having length max to 191
     * @expectedExceptionCode 422
     * Create a customer
     * @bodyParam user.first_name string required the first_name field of the user
     * @bodyParam user.email string required the email field of the user
     * @bodyParam user.address string optional the address field of the user
     * @bodyParam user.city string optional the city field of the user
     * @bodyParam user.zip string optional the zip code of the customer
     * @bodyParam user.mobile string optional the mobile number of the customer
     * @bodyParam user.notes string optional extra information about the user
     * @bodyParam user.notify integer optional whether to notify the customer about account or not 1=true ,0=false
     * @bodyParam address2 string optional the address2 of the customer
     * @bodyParam country string optional the country code of the customer
     * @bodyParam categories.*. id integer optional the category id of the customer
     * @bodyParam contact_persons.*.id integer optional the contact person id of the customer
     * @response json array type
     */
    public function testCustomerControllerStoreMethodWithValidTokenAndUserNameMax()
    {
        //hit login call fot getting auth token
        $loginJson = $this->getLoginInstance($this->loginParam);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'user' => [
                'first_name' => 'sdfshvfhsdfffffffffffffffffffffffffheeeeeeefghhhhhhhhhhhheeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuueeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee',
                'city' => '1',
                'zip' => '5643',
                'mobile' => '9874563210',
                'email' => 'fghfgasdadsaqqhg@dfgdfg.ghj',
                'notes' => 'asdas',
                'notify' => 1,
                'address' => 'address data',
            ],
            'categories[0]' => ["id" => 1],
            'contact_persons[0]' => ["id" => 1],
        ];


        $response = $this->call('POST', '/v1/customers/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        //
        $this->assertSame([
            $this->__getTranslateString('max_validation', ['attribute' => 'user.first_name', 'max' => 191], $lang)
        ], $json['user.first_name']);
    }

    /**
     * Case 6
     * @test This function test response with valid token but with user.city having length max to 191
     * @expectedExceptionCode 422
     * Create a customer
     * @bodyParam user.first_name string required the first_name field of the user
     * @bodyParam user.email string required the email field of the user
     * @bodyParam user.address string optional the address field of the user
     * @bodyParam user.city string optional the city field of the user
     * @bodyParam user.zip string optional the zip code of the customer
     * @bodyParam user.mobile string optional the mobile number of the customer
     * @bodyParam user.notes string optional extra information about the user
     * @bodyParam user.notify integer optional whether to notify the customer about account or not 1=true ,0=false
     * @bodyParam address2 string optional the address2 of the customer
     * @bodyParam country string optional the country code of the customer
     * @bodyParam categories.*. id integer optional the category id of the customer
     * @bodyParam contact_persons.*.id integer optional the contact person id of the customer
     * @response json array type
     */
    public function testCustomerControllerStoreMethodWithValidTokenAndUserCityMax()
    {
        //hit login call fot getting auth token
        $loginJson = $this->getLoginInstance($this->loginParam);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'user' => [
                'first_name' => 'sdfshvfhsd',
                'city' => 'sdfshvfhsdfffffffffffffffffffffffffheeeeeeefghhhhhhhhhhhheeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuueeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeefdlfdklfjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjdklfd',
                'zip' => '5643',
                'mobile' => '9874563210',
                'email' => 'fghfgasdadsaqqhg@dfgdfg.ghj',
                'notes' => 'asdas',
                'notify' => 1,
                'address' => 'address data',
            ],
            'categories[0]' => ["id" => 1],
            'contact_persons[0]' => ["id" => 1],
        ];
        $response = $this->call('POST', '/v1/customers/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
    }

    /**
     * Case 7
     * @test This function test response with valid token but with user.zip having length max to 10
     * @expectedExceptionCode 422
     * Create a customer
     * @bodyParam user.first_name string required the first_name field of the user
     * @bodyParam user.email string required the email field of the user
     * @bodyParam user.address string optional the address field of the user
     * @bodyParam user.city string optional the city field of the user
     * @bodyParam user.zip string optional the zip code of the customer
     * @bodyParam user.mobile string optional the mobile number of the customer
     * @bodyParam user.notes string optional extra information about the user
     * @bodyParam user.notify integer optional whether to notify the customer about account or not 1=true ,0=false
     * @bodyParam address2 string optional the address2 of the customer
     * @bodyParam country string optional the country code of the customer
     * @bodyParam categories.*. id integer optional the category id of the customer
     * @bodyParam contact_persons.*.id integer optional the contact person id of the customer
     * @response json array type
     */
    public function testCustomerControllerStoreMethodWithValidTokenAndUserZipMax()
    {
        //hit login call fot getting auth token
        $loginJson = $this->getLoginInstance($this->loginParam);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            'user' => [
                'first_name' => 'sdfshvfhsd',
                'city' => 'sd',
                'zip' => '56a545456443',
                'mobile' => '9874563210',
                'email' => 'fghfgasdadsaqqhg@dfgdfg.ghj',
                'notes' => 'asdas',
                'notify' => 1,
                'address' => 'address data',
            ],
            'categories[0]' => ["id" => 1],
            'contact_persons[0]' => ["id" => 1],
        ];

        $response = $this->call('POST', '/v1/customers/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);


    }

    /**
     * Case 8
     * @test This function test response with valid token but with user.email having normal string instead of valid email
     * @expectedExceptionCode 422
     * Create a customer
     * @bodyParam user.first_name string required the first_name field of the user
     * @bodyParam user.email string required the email field of the user
     * @bodyParam user.address string optional the address field of the user
     * @bodyParam user.city string optional the city field of the user
     * @bodyParam user.zip string optional the zip code of the customer
     * @bodyParam user.mobile string optional the mobile number of the customer
     * @bodyParam user.notes string optional extra information about the user
     * @bodyParam user.notify integer optional whether to notify the customer about account or not 1=true ,0=false
     * @bodyParam address2 string optional the address2 of the customer
     * @bodyParam country string optional the country code of the customer
     * @bodyParam categories.*. id integer optional the category id of the customer
     * @bodyParam contact_persons.*.id integer optional the contact person id of the customer
     * @response json array type
     */
    public function testCustomerControllerStoreMethodWithValidTokenAndUserEmailAsNormalString()
    {
        //hit login call fot getting auth token
        $loginJson = $this->getLoginInstance($this->loginParam);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            'user' => [
                'first_name' => 'sdfshvfhsd',
                'city' => 'sd',
                'zip' => '56a545456443',
                'mobile' => '9874563210',
                'email' => 'fghfgasdadsaqqh',
                'notes' => 'asdas',
                'notify' => 1,
                'address' => 'address data',
            ],
            'categories[0]' => ["id" => 1],
            'contact_persons[0]' => ["id" => 1],
        ];

        $response = $this->call('POST', '/v1/customers/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

    }


    /**
     * Case 9
     * @test This function test response with valid token but with user.email having length max to 191
     * @expectedExceptionCode 422
     * Create a customer
     * @bodyParam user.first_name string required the first_name field of the user
     * @bodyParam user.email string required the email field of the user
     * @bodyParam user.address string optional the address field of the user
     * @bodyParam user.city string optional the city field of the user
     * @bodyParam user.zip string optional the zip code of the customer
     * @bodyParam user.mobile string optional the mobile number of the customer
     * @bodyParam user.notes string optional extra information about the user
     * @bodyParam user.notify integer optional whether to notify the customer about account or not 1=true ,0=false
     * @bodyParam address2 string optional the address2 of the customer
     * @bodyParam country string optional the country code of the customer
     * @bodyParam categories.*. id integer optional the category id of the customer
     * @bodyParam contact_persons.*.id integer optional the contact person id of the customer
     * @response json array type
     */
    public function testCustomerControllerStoreMethodWithValidTokenAndUsingEmailMax()
    {
        //hit login call fot getting auth token
        $loginJson = $this->getLoginInstance($this->loginParam);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            'user' => [
                'first_name' => null,
                'city' => null,
                'zip' => null,
                'mobile' => null,
                'email' => 'customer1gggggggggggggggggggggggggggggggggggggghhhhhhhhhhhhhhhhhhhhhhhlllllllllllllllllllllllllllllllllllllllllllllhhhhhhhhhhhhhhhhhhhhhhtytttttttttttttttttttttttttttttttttttttttuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu@gmail.com'
            ],
            'address2' => null,
            'organization' => null,
            'country' => null,
            'customer_category_id' => null,
            'notify' => null,
            'invoice_email' => null,
            'other_invoice_address' => null,
            'other_invoice_city' => null,
            'other_shipping_address' => null,
            'other_shipping_city' => null,
            'categories' => [],
            'contact_persons' => null,
            'id' => null,
        ];

        $response = $this->call('POST', '/v1/customers/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('email_validation', ['attribute' => 'user.email'], $lang),
            $this->__getTranslateString('max_validation', ['attribute' => 'user.email', 'max' => 191], $lang)
        ], $json['user.email']);
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'user.first_name'], $lang)
        ], $json['user.first_name']);
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'id'], $lang)],
            $json['id']);
    }

    /**
     * Case 10
     * @test This function test response with valid token but with user.mobile having length max to 20
     * @expectedExceptionCode 422
     * Create a customer
     * @bodyParam user.first_name string required the first_name field of the user
     * @bodyParam user.email string required the email field of the user
     * @bodyParam user.address string optional the address field of the user
     * @bodyParam user.city string optional the city field of the user
     * @bodyParam user.zip string optional the zip code of the customer
     * @bodyParam user.mobile string optional the mobile number of the customer
     * @bodyParam user.notes string optional extra information about the user
     * @bodyParam user.notify integer optional whether to notify the customer about account or not 1=true ,0=false
     * @bodyParam address2 string optional the address2 of the customer
     * @bodyParam country string optional the country code of the customer
     * @bodyParam categories.*. id integer optional the category id of the customer
     * @bodyParam contact_persons.*.id integer optional the contact person id of the customer
     * @response json array type
     */
    public function testCustomerControllerStoreMethodWithValidTokenAndMobileMax()
    {
        //hit login call fot getting auth token
        $loginJson = $this->getLoginInstance($this->loginParam);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            'user' => [
                'first_name' => 'sdfshvfhsd',
                'city' => 'sd',
                'zip' => '56a545456443',
                'mobile' => '98745635432132134322132454210',
                'email' => 'fghfgasdadsaqqh',
                'notes' => 'asdas',
                'notify' => 1,
                'address' => 'address data',
            ],
            'categories[0]' => ["id" => 1],
            'contact_persons[0]' => ["id" => 1],
        ];

        $response = $this->call('POST', '/v1/customers/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
    }

    /**
     * Case 11
     * @test This function test response with valid token but with user.mobile with string instead of number
     * @expectedExceptionCode 422
     * Create a customer
     * @bodyParam user.first_name string required the first_name field of the user
     * @bodyParam user.email string required the email field of the user
     * @bodyParam user.address string optional the address field of the user
     * @bodyParam user.city string optional the city field of the user
     * @bodyParam user.zip string optional the zip code of the customer
     * @bodyParam user.mobile string optional the mobile number of the customer
     * @bodyParam user.notes string optional extra information about the user
     * @bodyParam user.notify integer optional whether to notify the customer about account or not 1=true ,0=false
     * @bodyParam address2 string optional the address2 of the customer
     * @bodyParam country string optional the country code of the customer
     * @bodyParam categories.*. id integer optional the category id of the customer
     * @bodyParam contact_persons.*.id integer optional the contact person id of the customer
     * @response json array type
     */
    public function testCustomerControllerStoreMethodWithValidTokenAndMobilAsNormalString()
    {
        //hit login call fot getting auth token
        $loginJson = $this->getLoginInstance($this->loginParam);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            'user' => [
                'first_name' => 'sdfshvfhsd',
                'city' => 'sd',
                'zip' => '56a545456443',
                'mobile' => 'asdasdasdasd',
                'email' => 'fghfgasdadsaqqh',
                'notes' => 'asdas',
                'notify' => 1,
                'address' => 'address data',
            ],
            'categories[0]' => ["id" => 1],
            'contact_persons[0]' => ["id" => 1],
        ];

        $response = $this->call('POST', '/v1/customers/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
    }

    /**
     * Case 12
     * @test This function test response with valid token but with address2 having length max to 191
     * @expectedExceptionCode 422
     * Create a customer
     * @bodyParam user.first_name string required the first_name field of the user
     * @bodyParam user.email string required the email field of the user
     * @bodyParam user.address string optional the address field of the user
     * @bodyParam user.city string optional the city field of the user
     * @bodyParam user.zip string optional the zip code of the customer
     * @bodyParam user.mobile string optional the mobile number of the customer
     * @bodyParam user.notes string optional extra information about the user
     * @bodyParam user.notify integer optional whether to notify the customer about account or not 1=true ,0=false
     * @bodyParam address2 string optional the address2 of the customer
     * @bodyParam country string optional the country code of the customer
     * @bodyParam categories.*. id integer optional the category id of the customer
     * @bodyParam contact_persons.*.id integer optional the contact person id of the customer
     * @response json array type
     */
    public function testCustomerControllerStoreMethodWithValidTokenAndAddressHavingMax()
    {
        //hit login call fot getting auth token
        $loginJson = $this->getLoginInstance($this->loginParam);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            'user' => [
                'first_name' => 'sdfshvfhsd',
                'city' => 'sd',
                'zip' => '56a545456443',
                'mobile' => '9874563210',
                'email' => 'fghfgasdadsaqqh',
                'notes' => 'asdas',
                'notify' => 1,
                'address' => 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeebbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbyyyyyyyyyyyyyyyyyyyllllllllllllllllllll',
            ],
            'categories[0]' => ["id" => 1],
            'contact_persons[0]' => ["id" => 1],
        ];



        $response = $this->call('POST', '/v1/customers/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
    }


    /**
     * Case 13
     * @test This function test response with valid token but with customer_category_id having length max to 11
     * @expectedExceptionCode 422
     * Create a customer
     * @bodyParam user.first_name string required the first_name field of the user
     * @bodyParam user.email string required the email field of the user
     * @bodyParam user.address string optional the address field of the user
     * @bodyParam user.city string optional the city field of the user
     * @bodyParam user.zip string optional the zip code of the customer
     * @bodyParam user.mobile string optional the mobile number of the customer
     * @bodyParam user.notes string optional extra information about the user
     * @bodyParam user.notify integer optional whether to notify the customer about account or not 1=true ,0=false
     * @bodyParam address2 string optional the address2 of the customer
     * @bodyParam country string optional the country code of the customer
     * @bodyParam categories.*. id integer optional the category id of the customer
     * @bodyParam contact_persons.*.id integer optional the contact person id of the customer
     * @response json array type
     */
    public function testCustomerControllerStoreMethodWithCustomerCategoryHavingLengthMax()
    {
        //hit login call fot getting auth token
        $loginJson = $this->getLoginInstance($this->loginParam);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            'user' => [
                'first_name' => 'sdfshvfhsd',
                'city' => 'sd',
                'zip' => '56a545456443',
                'mobile' => '9874563210',
                'email' => 'fghfgasdadsaqqh',
                'notes' => 'asdas',
                'notify' => 1,
                'address' => 'aaaaaaaaa',
            ],
            'categories[0]' => ["id" => 0],
            'contact_persons[0]' => ["id" => 1],
        ];

        $response = $this->call('POST', '/v1/customers/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
    }

    /**
     * Case 14
     * @test This function test response with valid token but with notify having length max to 1
     * @expectedExceptionCode 422
     * Create a customer
     * @bodyParam user.first_name string required the first_name field of the user
     * @bodyParam user.email string required the email field of the user
     * @bodyParam user.address string optional the address field of the user
     * @bodyParam user.city string optional the city field of the user
     * @bodyParam user.zip string optional the zip code of the customer
     * @bodyParam user.mobile string optional the mobile number of the customer
     * @bodyParam user.notes string optional extra information about the user
     * @bodyParam user.notify integer optional whether to notify the customer about account or not 1=true ,0=false
     * @bodyParam address2 string optional the address2 of the customer
     * @bodyParam country string optional the country code of the customer
     * @bodyParam categories.*. id integer optional the category id of the customer
     * @bodyParam contact_persons.*.id integer optional the contact person id of the customer
     * @response json array type
     */
    public function testCustomerControllerStoreMethodWithNotifyHavingMaxLength()
    {
        //hit login call fot getting auth token
        $loginJson = $this->getLoginInstance($this->loginParam);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            'user' => [
                'first_name' => 'sdfshvfhsd',
                'city' => 'sd',
                'zip' => '56a545456443',
                'mobile' => '9874563210',
                'email' => 'fghfgasdadsaqqh',
                'notes' => 'asdasas',
                'notify' => 'asdadasd',
                'address' => 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeebbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbyyyyyyyyyyyyyyyyyyyllllllllllllllllllll',
            ],
            'categories[0]' => ["id" => 1],
            'contact_persons[0]' => ["id" => 1],
        ];

        $response = $this->call('POST', '/v1/customers/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
    }

    /**
     * Case 15
     * @test This function test response with valid token but with invoice_email having length max to 1
     * @expectedExceptionCode 422
     * Create a customer
     * @bodyParam user.first_name string required the first_name field of the user
     * @bodyParam user.email string required the email field of the user
     * @bodyParam user.address string optional the address field of the user
     * @bodyParam user.city string optional the city field of the user
     * @bodyParam user.zip string optional the zip code of the customer
     * @bodyParam user.mobile string optional the mobile number of the customer
     * @bodyParam user.notes string optional extra information about the user
     * @bodyParam user.notify integer optional whether to notify the customer about account or not 1=true ,0=false
     * @bodyParam address2 string optional the address2 of the customer
     * @bodyParam country string optional the country code of the customer
     * @bodyParam categories.*. id integer optional the category id of the customer
     * @bodyParam contact_persons.*.id integer optional the contact person id of the customer
     * @response json array type
     */
    public function testCustomerControllerStoreMethodWithEmailHavingLengthMax()
    {
        //hit login call fot getting auth token
        $loginJson = $this->getLoginInstance($this->loginParam);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            'user' => [
                'first_name' => 'sdfshvfhsd',
                'city' => 'sd',
                'zip' => '56a545456443',
                'mobile' => '9874563210',
                'email' => 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeebbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbyyyyyyyyyyyyyyyyyyyllllllllllllllllllll@gm.com',
                'notes' => 'asdasas',
                'notify' => 'asdadasd',
                'address' => 'ada',
            ],
            'categories[0]' => ["id" => 1],
            'contact_persons[0]' => ["id" => 1],
        ];


        $response = $this->call('POST', '/v1/customers/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
    }


    /**
     * Case 16
     * @test This function test response with valid token and all param but without user.zip
     * @expectedExceptionCode 422
     * Create a customer
     * @bodyParam user.first_name string required the first_name field of the user
     * @bodyParam user.email string required the email field of the user
     * @bodyParam user.address string optional the address field of the user
     * @bodyParam user.city string optional the city field of the user
     * @bodyParam user.zip string optional the zip code of the customer
     * @bodyParam user.mobile string optional the mobile number of the customer
     * @bodyParam user.notes string optional extra information about the user
     * @bodyParam user.notify integer optional whether to notify the customer about account or not 1=true ,0=false
     * @bodyParam address2 string optional the address2 of the customer
     * @bodyParam country string optional the country code of the customer
     * @bodyParam categories.*. id integer optional the category id of the customer
     * @bodyParam contact_persons.*.id integer optional the contact person id of the customer
     * @response json array type
     */
    public function testCustomerControllerStoreMethodWithValidTokenButWithoutZip()
    {
        //hit login call fot getting auth token
        $loginJson = $this->getLoginInstance($this->loginParam);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            'user' => [
                'first_name' => 'sdfshvfhsd',
                'city' => 'sd',
                'mobile' => '9874563210',
                'email' => 'aaaaa@gm.com',
                'notes' => 'asdasas',
                'notify' => 'asdadasd',
                'address' => 'ada',
            ],
            'categories[0]' => ["id" => 1],
            'contact_persons[0]' => ["id" => 1],
        ];
        $response = $this->call('POST', '/v1/customers/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
    }

    /**
     * Case 17
     * @test This function test response with valid token and all param but without user.email
     * @expectedExceptionCode 422
     * Create a customer
     * @bodyParam user.first_name string required the first_name field of the user
     * @bodyParam user.email string required the email field of the user
     * @bodyParam user.address string optional the address field of the user
     * @bodyParam user.city string optional the city field of the user
     * @bodyParam user.zip string optional the zip code of the customer
     * @bodyParam user.mobile string optional the mobile number of the customer
     * @bodyParam user.notes string optional extra information about the user
     * @bodyParam user.notify integer optional whether to notify the customer about account or not 1=true ,0=false
     * @bodyParam address2 string optional the address2 of the customer
     * @bodyParam country string optional the country code of the customer
     * @bodyParam categories.*. id integer optional the category id of the customer
     * @bodyParam contact_persons.*.id integer optional the contact person id of the customer
     * @response json array type
     */
    public function testCustomerControllerStoreMethodWithValidTokenAndWithoutEmail()
    {
        //hit login call fot getting auth token
        $loginJson = $this->getLoginInstance($this->loginParam);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            'user' => [
                'first_name' => 'sdfshvfhsd',
                'city' => 'sd',
                'zip' => '56a545456443',
                'mobile' => '9874563210',
                'notes' => 'asdasas',
                'notify' => 'asdadasd',
                'address' => 'ada',
            ],
            'categories[0]' => ["id" => 1],
            'contact_persons[0]' => ["id" => 1],
        ];

        $response = $this->call('POST', '/v1/customers/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
    }


    /**
     * Case 18
     * @test This function test response with valid token and all param but without user.mobile
     * @expectedExceptionCode 422
     * Create a customer
     * @bodyParam user.first_name string required the first_name field of the user
     * @bodyParam user.email string required the email field of the user
     * @bodyParam user.address string optional the address field of the user
     * @bodyParam user.city string optional the city field of the user
     * @bodyParam user.zip string optional the zip code of the customer
     * @bodyParam user.mobile string optional the mobile number of the customer
     * @bodyParam user.notes string optional extra information about the user
     * @bodyParam user.notify integer optional whether to notify the customer about account or not 1=true ,0=false
     * @bodyParam address2 string optional the address2 of the customer
     * @bodyParam country string optional the country code of the customer
     * @bodyParam categories.*. id integer optional the category id of the customer
     * @bodyParam contact_persons.*.id integer optional the contact person id of the customer
     * @response json array type
     */
    public function testCustomerControllerStoreMethodWithValidTokenAndWithoutUserMobile()
    {
        //hit login call fot getting auth token
        $loginJson = $this->getLoginInstance($this->loginParam);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            'user' => [
                'first_name' => 'sdfshvfhsd',
                'city' => 'sd',
                'zip' => '56a545456443',
                'email' => 'aaaaaaa@gm.com',
                'notes' => 'asdasas',
                'notify' => 'asdadasd',
                'address' => 'ada',
            ],
            'categories[0]' => ["id" => 1],
            'contact_persons[0]' => ["id" => 1],
        ];

        $response = $this->call('POST', '/v1/customers/add', $param, [], [], $headers);
       //
        //check response status code
        $this->assertResponseStatus(422);
    }

    /**
     * Case 19
     * @test This function test response with valid token and all param but without address2
     * @expectedExceptionCode 200
     * Create a customer
     * @bodyParam user.first_name string required the first_name field of the user
     * @bodyParam user.email string required the email field of the user
     * @bodyParam user.address string optional the address field of the user
     * @bodyParam user.city string optional the city field of the user
     * @bodyParam user.zip string optional the zip code of the customer
     * @bodyParam user.mobile string optional the mobile number of the customer
     * @bodyParam user.notes string optional extra information about the user
     * @bodyParam user.notify integer optional whether to notify the customer about account or not 1=true ,0=false
     * @bodyParam address2 string optional the address2 of the customer
     * @bodyParam country string optional the country code of the customer
     * @bodyParam categories.*. id integer optional the category id of the customer
     * @bodyParam contact_persons.*.id integer optional the contact person id of the customer
     * @response json array type
     */
    public function testCustomerControllerStoreMethodWithValidTokenButWithoutAddress()
    {
        //hit login call fot getting auth token
        $loginJson = $this->getLoginInstance($this->loginParam);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            'user' => [
                'first_name' => 'Test First name',
                'city' => '1',
                'zip' => '5643',
                'mobile' => '9874563210',
                'email' => 'fghfgasdadsaqqhg@dfgdfg.ghj',
                'notes' => 'asdas',
                'notify' => 1,
            ],
            'categories[0]' => ["id" => 1],
            'contact_persons[0]' => ["id" => 1],
        ];

        $response = $this->call('POST', '/v1/customers/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(200);

    }

    /**
     * Case 20
     * @test This function test response with valid token and all param but without notify
     * @expectedExceptionCode 422
     * Create a customer
     * @bodyParam user.first_name string required the first_name field of the user
     * @bodyParam user.email string required the email field of the user
     * @bodyParam user.address string optional the address field of the user
     * @bodyParam user.city string optional the city field of the user
     * @bodyParam user.zip string optional the zip code of the customer
     * @bodyParam user.mobile string optional the mobile number of the customer
     * @bodyParam user.notes string optional extra information about the user
     * @bodyParam user.notify integer optional whether to notify the customer about account or not 1=true ,0=false
     * @bodyParam address2 string optional the address2 of the customer
     * @bodyParam country string optional the country code of the customer
     * @bodyParam categories.*. id integer optional the category id of the customer
     * @bodyParam contact_persons.*.id integer optional the contact person id of the customer
     * @response json array type
     */
    public function testCustomerControllerStoreMethodWithValidParamButWithoutNotify()
    {
        //hit login call fot getting auth token
        $loginJson = $this->getLoginInstance($this->loginParam);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            'user' => [
                'first_name' => 'Test First name',
                'city' => '1',
                'zip' => '5643',
                'mobile' => '9874563210',
                'email' => 'fghfgasdadsaqqhg@dfgdfg.ghj',
                'notes' => 'asdas',
                'address' => 'address data',
            ],
            'categories[0]' => ["id" => 1],
            'contact_persons[0]' => ["id" => 1],
        ];
        $response = $this->call('POST', '/v1/customers/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

    }

    /**
     * Case 21
     * @test This function test response with valid token and all params
     * @expectedExceptionCode 200
     * Create a customer
     * @bodyParam user.first_name string required the first_name field of the user
     * @bodyParam user.email string required the email field of the user
     * @bodyParam user.address string optional the address field of the user
     * @bodyParam user.city string optional the city field of the user
     * @bodyParam user.zip string optional the zip code of the customer
     * @bodyParam user.mobile string optional the mobile number of the customer
     * @bodyParam user.notes string optional extra information about the user
     * @bodyParam user.notify integer optional whether to notify the customer about account or not 1=true ,0=false
     * @bodyParam address2 string optional the address2 of the customer
     * @bodyParam country string optional the country code of the customer
     * @bodyParam categories.*. id integer optional the category id of the customer
     * @bodyParam contact_persons.*.id integer optional the contact person id of the customer
     * @response json array type
     */
    public function testCustomerControllerStoreMethodWithValidTokenAndAllParams()
    {
        //hit login call fot getting auth token
        $loginJson = $this->getLoginInstance($this->loginParam);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            'user' => [
                'first_name' => 'Test First name',
                'city' => '1',
                'zip' => '5643',
                'mobile' => '9874563210',
                'email' =>  str_random(12) . '@mail.com',
                'notes' => 'asdas',
                'notify' => 1,
                'address' => 'address data',
            ],
            'categories[0]' => ["id" => 1],
            'contact_persons[0]' => ["id" => 1],
        ];
        $response = $this->call('POST', '/v1/customers/add', $param, [], [], $headers);

       //
        //check response status code
        $this->assertResponseStatus(200);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value

        $this->assertSame($this->__getTranslateString('customer_created', [], $lang), $json['success']);

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('customers')->truncate();
        DB::table('contact_persons')->truncate();
        DB::table('consultant_titles')->truncate();

        DB::table('categories')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

}
