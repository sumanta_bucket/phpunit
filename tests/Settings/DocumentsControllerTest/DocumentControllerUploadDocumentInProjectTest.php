<?php


use Illuminate\Http\UploadedFile;

class DocumentControllerUploadDocumentInProjectTest extends TestCase
{
    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'custom@yopmail.com',
        'password' => '123456'
    ];
    public $employee = [
        'email' => 'emp1@yopmail.com',
        'password' => '123456'
    ];

    /**
     *  Read.
     *
     *  Admin and Employee holds same permissions .
     *  Customer dont't have permissions to view this page.
     *  Contact-Persons dont't have permission to view this page.
     */

    /**
     * Case 1
     * @test This function test response without auth token without params
     * @expectedExceptionCode 401
     * upload the documents to existing project
     * @bodyParam project_id integer required the id of the project in which we are uploading the documents
     * @bodyParam document array required the document being uploaded and is of type PDF
     * @response json array type
     */

    public function testProjectDocumentationUploadPdfOfSpecificSectionTestWithoutToken()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('customers')->truncate();
        DB::table('floors')->truncate();
        DB::table('users')->truncate();
        DB::table('workplaces')->truncate();
        DB::table('projects')->truncate();
        DB::table('sections')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        app(DatabaseSeeder::class)->call(UserTestSeeder::class);
        app(DatabaseSeeder::class)->call(ProjectDocumentationWithExistingFloorSeeder::class);

        $this->post('/v1/project/13/floor/40/section/add', [])
            ->seeJsonEquals([
                "error" => $this->__getTranslateString('missing_token_error', [], 2)
            ]);
    }

    /**
     * Case 2
     * @test This function test response with invalid auth token without params
     * @expectedExceptionCode 400
     * upload the documents to existing project
     * @bodyParam project_id integer required the id of the project in which we are uploading the documents
     * @bodyParam document array required the document being uploaded and is of type PDF
     * @response json array type
     */
    public function testProjectDocumentationUploadPdfOfSpecificSectionTestWithInvalidToken()
    {
        $expired_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expired_token
        ];

        $response = $this->call('POST', '/v1/project/documents/3', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(400);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('token_expired_error', [], 2), $json['error']);
    }

    /**
     * Case 3
     * @test This function test response with valid token without params
     * @expectedExceptionCode 422
     * upload the documents to existing project
     * @bodyParam project_id integer required the id of the project in which we are uploading the documents
     * @bodyParam document array required the document being uploaded and is of type PDF
     * @response json array type
     */
    public function testProjectDocumentationUploadPdfOfSpecificSectionTestWithValidTokenWithoutPrams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('POST', '/v1/project/documents/3', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'document'], $lang)
        ], $json['document']);

    }

    /**
     * Case 4
     * @test This function test response with valid token and null params
     * @expectedExceptionCode 422
     * upload the documents to existing project
     * @bodyParam project_id integer required the id of the project in which we are uploading the documents
     * @bodyParam document array required the document being uploaded and is of type PDF
     * @response json array type
     */
    public function testProjectDocumentationUploadPdfOfSpecificSectionTestWithValidTokenNullParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            "document[0]" => null,
        ];

        $response = $this->call('POST', '/v1/project/documents/3', $param, [], [], $headers);

        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'document'], $lang)
        ], $json['document']);

    }

    /**
     * Case 5
     * @test This function test response with valid token but with with section not having Integer
     * @expectedExceptionCode 422
     * upload the documents to existing project
     * @bodyParam project_id integer required the id of the project in which we are uploading the documents
     * @bodyParam document array required the document being uploaded and is of type PDF
     * @response json array type
     */
    public function testProjectDocumentationUploadPdfOfSpecificSectionTestWithSectionAsString()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            "document" => [
                '0' => UploadedFile::fake()->create('document.pdf', 2048)
            ]
        ];
        $response = $this->call('POST', '/v1/project/documents/3', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(200);
        //check response status code

    }

    /**
     * Case
     * @test This function test response with valid token but with Valid Token
     * @expectedExceptionCode 200
     * upload the documents to existing project
     * @bodyParam project_id integer required the id of the project in which we are uploading the documents
     * @bodyParam document array required the document being uploaded and is of type PDF
     * @response json array type
     */
    public function testProjectDocumentationUploadPdfOfSpecificSectionTestWithValidTokenAndValidParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            "document" => [
                '0' => UploadedFile::fake()->create('document.pdf', 2048)
            ]
        ];
        $response = $this->call('POST', '/v1/project/documents/3', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(200);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('customers')->truncate();
        DB::table('floors')->truncate();
        DB::table('workplaces')->truncate();
        DB::table('projects')->truncate();
        DB::table('sections')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
