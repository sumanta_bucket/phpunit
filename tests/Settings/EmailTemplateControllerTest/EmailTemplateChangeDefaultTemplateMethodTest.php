<?php


class EmailTemplateChangeDefaultTemplateMethodTest extends TestCase
{
    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'custom@yopmail.com',
        'password' => '123456'
    ];
    public $employee = [
        'email' => 'emp1@yopmail.com',
        'password' => '123456'
    ];

    /**
     * Case 1
     * @test This function test response without auth token without params
     * @expectedExceptionCode 401
     * Change default template
     * @bodyParam id integer required the id of email template whose default value we want to change
     * @response json array type
     */
    public function testEmailTemplateChangeDefaultTemplateMethodWithoutAuthTokenAndWithoutParams()
    {
        app(DatabaseSeeder::class)->call(EmailTemplateTypesTableSeeder::class);
        $this->PUT('/v1/emailTemplates/change_default/{id}', [])
            ->seeJsonEquals([
                "error" => $this->__getTranslateString('missing_token_error', [], 2)
            ]);
    }

    /**
     * Case 2
     * @test This function test response with invalid auth token without params
     * @expectedExceptionCode 400
     * Change default template
     * @bodyParam id integer required the id of email template whose default value we want to change
     * @response json array type
     */
    public function testEmailTemplateChangeDefaultTemplateMethodWithInvalidTokenAndWithoutParams()
    {
        app(DatabaseSeeder::class)->call(EmailTemplateTypesTableSeeder::class);
        $expired_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expired_token
        ];

        $response = $this->call('PUT', '/v1/emailTemplates/change_default/{id}', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(400);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('token_expired_error', [], 2), $json['error']);
    }

    /**
     * Case 3
     * @test This function test response with valid token without params
     * @expectedExceptionCode 422
     * Change default template
     * @bodyParam id integer required the id of email template whose default value we want to change
     * @response json array type
     */
    public function testEmailTemplateChangeDefaultTemplateMethodWithValidTokenAndWithoutParams()
    {
        app(DatabaseSeeder::class)->call(EmailTemplateTypesTableSeeder::class);
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);

        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('PUT', '/v1/emailTemplates/change_default/2', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'is_default'], $lang)
        ],
            $json['is_default']);
    }

    /**
     * Case 4
     * @test This function test response with valid token and null params
     * @expectedExceptionCode 422
     * Change default template
     * @bodyParam id integer required the id of email template whose default value we want to change
     * @response json array type
     */
    public function testEmailTemplateChangeDefaultTemplateMethodWithValidTokenAndNullParams()
    {
        app(DatabaseSeeder::class)->call(EmailTemplateTypesTableSeeder::class);
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            "is_default" => null
        ];

        $response = $this->call('PUT', '/v1/emailTemplates/change_default/2', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
    }

    /**
     * Case 5
     * @test This function test response with valid token but with name having length max to 191
     * @expectedExceptionCode 422
     * Change default template
     * @bodyParam id integer required the id of email template whose default value we want to change
     * @response json array type
     */
    public function testEmailTemplateChangeDefaultTemplateMethodWithValidTokenNameHavingMaxLength()
    {
        app(DatabaseSeeder::class)->call(EmailTemplateTypesTableSeeder::class);
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            "is_default" => "aasdasdas"
        ];

        $response = $this->call('PUT', '/v1/emailTemplates/change_default/3', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
    }

    /**
     * Case 6
     * @test This function test response with valid token but with user.email having normal string instead of valid email
     * @expectedExceptionCode 200
     * Change default template
     * @bodyParam id integer required the id of email template whose default value we want to change
     * @response json array type
     */
    public function testEmailTemplateChangeDefaultTemplateMethodWithSubjectHavingLengthMax()
    {
        app(DatabaseSeeder::class)->call(EmailTemplateTypesTableSeeder::class);
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            "is_default" => 0
        ];

        $response = $this->call('PUT', '/v1/emailTemplates/change_default/4', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
    }

    /**
     * Case 11
     * @test This function test response with valid token and valid data
     * @expectedExceptionCode 200
     * Change default template
     * @bodyParam id integer required the id of email template whose default value we want to change
     * @response json array type
     */
    public function testEmailTemplateChangeDefaultTemplateMethodValidTokenAndValidTokenAndValidData()
    {
        app(DatabaseSeeder::class)->call(EmailTemplateTypesTableSeeder::class);
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            "is_default" => true
        ];

        $response = $this->call('PUT', '/v1/emailTemplates/change_default/2', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(200);
    }
}
