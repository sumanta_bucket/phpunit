<?php


class EmailTemplateStoreMethodTest extends TestCase
{
    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'custom@yopmail.com',
        'password' => '123456'
    ];
    public $employee = [
        'email' => 'emp1@yopmail.com',
        'password' => '123456'
    ];

    /**
     *  Read.
     *
     *  Admin and Employee holds same permissions .
     *  Customer dont't have permissions to view this page.
     *  Contact-Persons dont't have permission to view this page.
     */

    /**
     * Case 1
     * @test This function test response without auth token without params
     * @expectedExceptionCode 401
     * create a email template
     * @bodyParam email_template_type_id number required the email type template id
     * @bodyParam name string required the name of the template
     * @bodyParam template string required the body of the email template
     * @response json array type
     */
    public function testEmailTemplateStoreStoreWithoutToken()
    {
        $this->post('/v1/emailTemplates/add', [])
            ->seeJsonEquals([
                "error" => $this->__getTranslateString('missing_token_error', [], 2)
            ]);
    }

    /**
     * Case 2
     * @test This function test response with invalid auth token without params
     * @expectedExceptionCode 400
     * create a email template
     * @bodyParam email_template_type_id number required the email type template id
     * @bodyParam name string required the name of the template
     * @bodyParam template string required the body of the email template
     * @response json array type
     */
    public function testEmailTemplateStoreStoreWithInvalidToken()
    {
        $expired_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expired_token
        ];

        $response = $this->call('POST', '/v1/emailTemplates/add', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(400);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('token_expired_error', [], 2), $json['error']);
    }

    /**
     * Case 3
     * @test This function test response with valid token without params
     * @expectedExceptionCode 401
     * create a email template
     * @bodyParam email_template_type_id number required the email type template id
     * @bodyParam name string required the name of the template
     * @bodyParam template string required the body of the email template
     * @response json array type
     */
    public function testEmailTemplateStoreStoreWithValidTokenWithoutPrams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('POST', '/v1/emailTemplates/add', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'email_template_type_id'], $lang)
        ], $json['email_template_type_id']);
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'name'], $lang)],
            $json['name']);
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'subject'], $lang)],
            $json['subject']);
    }

    /**
     * Case 4
     * @test This function test response with valid token and null params
     * @expectedExceptionCode 422
     * create a email template
     * @bodyParam email_template_type_id number required the email type template id
     * @bodyParam name string required the name of the template
     * @bodyParam template string required the body of the email template
     * @response json array type
     */
    public function testEmailTemplateStoreStoreWithValidTokenNullParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            "email_template_type_id" => null,
            "name" => null,
            "subject" => null,
            "template" => null
        ];

        $response = $this->call('POST', '/v1/emailTemplates/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'email_template_type_id'], $lang)
        ], $json['email_template_type_id']);
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'name'], $lang)],
            $json['name']);
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'subject'], $lang)],
            $json['subject']);
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'template'], $lang)],
            $json['template']);
    }

    /**
     * Case 5
     * @test This function test response with valid token but with user.first_name having length max to 191
     * @expectedExceptionCode 422
     * create a email template
     * @bodyParam email_template_type_id number required the email type template id
     * @bodyParam name string required the name of the template
     * @bodyParam template string required the body of the email template
     * @response json array type
     */
    public function testEmailTemplateStoreWithValidTokenUserFirstNameHavingLengthMax()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            "email_template_type_id" => 2,
            "name" => "adsfdfjhbjsbdfjaaaaaaaaaaaaaaaaaaaa ajbjjhdf jajbfdj absjdfb jasdbfj basjdfb jasbdfjajsdfsdfffffffffffffffffffffffffffffffsffffffffffffffffffffffffffffffffffffffffffdahjdafajhbdsfhjbhjbjhdsfdsfdfgfdgd",
            "subject" => "test registration template",
            "template" => "Hi, your account is created details :Email :{{EMAIL}},Password {{PASSWORD}}"
        ];

        $response = $this->call('POST', '/v1/emailTemplates/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('max_validation', ['attribute' => 'name', 'max' => 191], $lang)
        ], $json['name']);
    }

    /**
     * Case 6
     * @test This function test response with valid token but with email_template_type_id having normal string instead of valid integer
     * @expectedExceptionCode 422
     * create a email template
     * @bodyParam email_template_type_id number required the email type template id
     * @bodyParam name string required the name of the template
     * @bodyParam template string required the body of the email template
     * @response json array type
     */
    public function testEmailTemplateStoreWithSubjectHavingMaximumLength()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            "email_template_type_id" => 2,
            "name" => "asdasda",
            "subject" => "adsfdfjhbjsbdfjaaaaaaaaaaaaaaaaaaaa ajbjjhdf jajbfdj absjdfb jasdbfj basjdfb jasbdfjajsdfsdfffffffffffffffffffffffffffffffsffffffffffffffffffffffffffffffffffffffffffdahjdafajhbdsfhjbhjbjhdsfdsfdfgfdgd",
            "template" => "Hi, your account is created details :Email :{{EMAIL}},Password {{PASSWORD}}"
        ];

        $response = $this->call('POST', '/v1/emailTemplates/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('max_validation', ['attribute' => 'subject', 'max' => 191], $lang)
        ], $json['subject']);
    }
}
