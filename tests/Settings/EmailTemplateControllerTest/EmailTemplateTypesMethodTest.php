<?php


use Illuminate\Http\Request;

class EmailTemplateTypesMethodTest extends TestCase
{
    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'custom@yopmail.com',
        'password' => '123456'
    ];
    public $employee = [
        'email' => 'emp1@yopmail.com',
        'password' => '123456'
    ];
    /**
     * Case 1
     * @test This function test response without auth token without params'
     * @expectedExceptionCode 401
     * Get Email Template types
     * @boyparam type int required this is as a route parameter like /getType/1, 1= email and 2= sms
     * @response json array
     */
    public function testEmailTemplateTypeWithoutTokenAndWithoutParams()
    {
        $this->get('/v1/emailTemplates/getEmailType/{id}', [])
            ->seeJsonEquals([
                "error" => $this->__getTranslateString('missing_token_error', [], 2)
            ]);
    }
    /**
     * Case 1B
     * @test This function test response without auth token without params
     * @expectedExceptionCode 401
     * Get Email Template types
     * @boyparam type int required this is as a route parameter like /getType/1, 1= email and 2= sms
     * @response json array
     */
    public function testEmailTemplateTypPlaceHolderWithoutTokenAndWithoutParams()
    {
        $this->get('/v1/emailTemplates/getPlaceholders/{id}', [])
            ->seeJsonEquals([
                "error" => $this->__getTranslateString('missing_token_error', [], 2)
            ]);
    }
    /**
     * Case 2
     * @test This function test response without auth token with invalid id
     * @expectedExceptionCode 422
     * Get Email Template types
     * @boyparam type int required this is as a route parameter like /getType/1, 1= email and 2= sms
     * @response json array
     */
    public function testEmailTemplateTypPlaceHolderWithValidTokenAndInvalidId()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];


        $response = $this->call('GET', '/v1/emailTemplates/getPlaceholders/200', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
    }
    /**
     * Case 2
     * @test This function test response without auth token with invalid id
     * @expectedExceptionCode 422
     * Get Email Template types
     * @boyparam type int required this is as a route parameter like /getType/1, 1= email and 2= sms
     * @response json array
     */
    public function testEmailTemplateTypeWithValidTokenAndInvalidId()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];


        $response = $this->call('GET', '/v1/emailTemplates/getEmailType/1200', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
    }
}
