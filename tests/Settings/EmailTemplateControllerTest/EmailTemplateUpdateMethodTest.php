<?php


use App\Models\EmailTemplate;

class EmailTemplateUpdateMethodTest extends TestCase
{
    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'custom@yopmail.com',
        'password' => '123456'
    ];
    public $employee = [
        'email' => 'emp1@yopmail.com',
        'password' => '123456'
    ];

    /**
     * Case 1
     * @test This function test response without auth token without params
     * @expectedExceptionCode 401
     * Update a Email template
     * @bodyParam id integer required this is as a route parameter like update/2
     * @bodyParam email_template_type_id number required the email type template id
     * @bodyParam name string required the name of the template
     * @bodyParam template string required the body of the email template
     * @response json array
     */
    public function testEmailTemplateUpdateWithoutAuthTokenAndWithoutParams()
    {
        app(DatabaseSeeder::class)->call(EmailTemplateTypesTableSeeder::class);
        $this->PUT('/v1/emailTemplates/update/{id}', [])
            ->seeJsonEquals([
                "error" => $this->__getTranslateString('missing_token_error', [], 2)
            ]);
    }

    /**
     * Case 2
     * @test This function test response with invalid auth token without params
     * @expectedExceptionCode 400
     * Update a Email template
     * @bodyParam id integer required this is as a route parameter like update/2
     * @bodyParam email_template_type_id number required the email type template id
     * @bodyParam name string required the name of the template
     * @bodyParam template string required the body of the email template
     * @response json array
     */
    public function testEmailTemplateUpdateWithInvalidTokenAndWithoutParams()
    {
        $expired_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expired_token
        ];

        $response = $this->call('PUT', '/v1/emailTemplates/update/{id}', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(400);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('token_expired_error', [], 2), $json['error']);
    }

    /**
     * Case 3
     * @test This function test response with valid token without params
     * @expectedExceptionCode 422
     * Update a Email template
     * @bodyParam id integer required this is as a route parameter like update/2
     * @bodyParam email_template_type_id number required the email type template id
     * @bodyParam name string required the name of the template
     * @bodyParam template string required the body of the email template
     * @response json array
     */
    public function testEmailTemplateUpdateWithValidTokenAndWithoutParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);

        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('PUT', '/v1/emailTemplates/update/7', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'email_template_type_id'], $lang)
        ],
            $json['email_template_type_id']);
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'name'], $lang)],
            $json['name']);
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'name'], $lang)],
            $json['name']);
    }

    /**
     * Case 4
     * @test This function test response with valid token and null params
     * @expectedExceptionCode 422
     * Update a Email template
     * @bodyParam id integer required this is as a route parameter like update/2
     * @bodyParam email_template_type_id number required the email type template id
     * @bodyParam name string required the name of the template
     * @bodyParam template string required the body of the email template
     * @response json array
     */
    public function testEmailTemplateUpdateWithValidTokenAndNullParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            "email_template_type_id" => null,
            "name" => null,
            "subject" => null,
            "template" => null
        ];

        $response = $this->call('PUT', '/v1/emailTemplates/update/7', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
    }

    /**
     * Case 5
     * @test This function test response with valid token but with name having length max to 191
     * @expectedExceptionCode 422
     * Update a Email template
     * @bodyParam id integer required this is as a route parameter like update/2
     * @bodyParam email_template_type_id number required the email type template id
     * @bodyParam name string required the name of the template
     * @bodyParam template string required the body of the email template
     * @response json array
     */
    public function testEmailTemplateUpdateWithValidTokenNameHavingMaxLength()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            "email_template_type_id" => 1,
            "name" => "adsfdfjhbjsbdfjaaaaaaaaaaaaaaaaaaaaajbjjhdfjajbfdjabsjdfbjasdbfjbasjdfbjasbdfjajsdfsdfffffffffffffffffffffffffffffffsffffffffffffffffffffffffffffffffffffffffffdahjdafajhbdsfhjbhjbjhdsfdsfdfgfdgd",
            "subject" => "asdad",
            "template" => "Hi, your account is created details :Email :{{EMAIL}},Password {{PASSWORD}}"
        ];

        $response = $this->call('PUT', '/v1/emailTemplates/update/1', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('max_validation', ['attribute' => 'name', 'max' => 191], $lang)
        ], $json['name']);
    }

    /**
     * Case 6
     * @test This function test response with valid token but with user.email having normal string instead of valid email
     * @expectedExceptionCode 422
     * Update a Email template
     * @bodyParam id integer required this is as a route parameter like update/2
     * @bodyParam email_template_type_id number required the email type template id
     * @bodyParam name string required the name of the template
     * @bodyParam template string required the body of the email template
     * @response json array
     */
    public function testEmailTemplateUpdateWithSubjectHavingLengthMax()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            "email_template_type_id" => 1,
            "name" => "dsddfsfds",
            "subject" => "adsfdfjhbjsbdfjaaaaaaaaaaaaaaaaaaaaajbjjhdfjajbfdjabsjdfbjasdbfjbasjdfbjasbdfjajsdfsdfffffffffffffffffffffffffffffffsffffffffffffffffffffffffffffffffffffffffffdahjdafajhbdsfhjbhjbjhdsfdsfdfgfdgd",
            "template" => "Hi, your account is created details :Email :{{EMAIL}},Password {{PASSWORD}}"
        ];

        $response = $this->call('PUT', '/v1/emailTemplates/update/7', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('max_validation', ['attribute' => 'subject', 'max' => 191], $lang)
        ],
            $json['subject']);
    }

    /**
     * Case 7
     * @test This function test response with valid token but with valid Data
     * @expectedExceptionCode 200
     * Update a Email template
     * @bodyParam id integer required this is as a route parameter like update/2
     * @bodyParam email_template_type_id number required the email type template id
     * @bodyParam name string required the name of the template
     * @bodyParam template string required the body of the email template
     * @response json array
     */
    public function testEmailTemplateUpdateValidTokenAndValidTokenAndValidData()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $emailTemplate = EmailTemplate::find(1);
        $emailTemplate->deletes = 1;
        $emailTemplate->save();
        $param = [
            "email_template_type_id" => 1,
            "name" => "dsddfsfds",
            "subject" => "sdsdsdd",
            "template" => "Hi, your account is created details :Email :{{EMAIL}},Password {{PASSWORD}}"
        ];

        $response = $this->call('PUT', '/v1/emailTemplates/update/1', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(200);
    }

}
