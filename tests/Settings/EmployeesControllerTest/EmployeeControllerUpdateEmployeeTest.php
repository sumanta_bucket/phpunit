<?php


use App\Models\Employee;

class EmployeeControllerUpdateEmployeeTest extends TestCase
{
    /*
          * Login param for admin , employee , customer to check role based authentication
          * */

    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'custom@yopmail.com',
        'password' => '123456'
    ];
    public $employee = [
        'email' => 'emp1@yopmail.com',
        'password' => '123456'
    ];

    /**
     *  Admin and Employee holds same permissions .
     *  Customer dont't have permissions to view this page.
     *  Contact-Persons dont't have permission to view this page.
     */


    /**
     * Case 1
     * @test  This function test response without auth token without params
     * @expectedExceptionCode 401
     * update a employee
     * @bodyParam id integer required the id of the employee whose records are being updated
     * @bodyParam first_name string required the first name field of the employee
     * @bodyParam last_name string required the last name field of the employee
     * @bodyParam email string required the email field of the employee
     * @bodyParam address string optional the address field of the employee
     * @bodyParam city string optional the city field of the employee
     * @bodyParam zip string optional the zip code of the employee
     * @bodyParam mobile string optional the mobile of the employee
     * @bodyParam title_id integer optional the title of employee
     * @bodyParam notify boolean optional whether to notify or not of account creation
     * @bodyParam relatives array optional the relatives of the employee
     * @response type json array
     */
    public function testEmployeeControllerUpdateWithoutAuthTokenAndWithoutParams()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('employees')->truncate();
        DB::table('options')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        app(DatabaseSeeder::class)->call(EmployeeSeeder::class);

        $this->put('/v1/employees/update/1{id}', [])
            ->seeJsonEquals([
                "error" => $this->__getTranslateString('missing_token_error', [], 2)
            ]);
    }

    /**
     * Case 2
     * @test This function test response with invalid auth token without params
     * @expectedExceptionCode 400
     * update a employee
     * @bodyParam id integer required the id of the employee whose records are being updated
     * @bodyParam first_name string required the first name field of the employee
     * @bodyParam last_name string required the last name field of the employee
     * @bodyParam email string required the email field of the employee
     * @bodyParam address string optional the address field of the employee
     * @bodyParam city string optional the city field of the employee
     * @bodyParam zip string optional the zip code of the employee
     * @bodyParam mobile string optional the mobile of the employee
     * @bodyParam title_id integer optional the title of employee
     * @bodyParam notify boolean optional whether to notify or not of account creation
     * @bodyParam relatives array optional the relatives of the employee
     * @response type json array
     */
    public function testEmployeeControllerUpdateWithInvalidAuthTokenAndWithoutParams()
    {
        $expired_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expired_token
        ];

        $response = $this->call('PUT', '/v1/employees/update/{id}', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(400);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('token_expired_error', [], 2), $json['error']);
    }

    /**
     * Case 3
     * @test This function test response with valid token without params
     * @expectedExceptionCode 422
     * update a employee
     * @bodyParam id integer required the id of the employee whose records are being updated
     * @bodyParam first_name string required the first name field of the employee
     * @bodyParam last_name string required the last name field of the employee
     * @bodyParam email string required the email field of the employee
     * @bodyParam address string optional the address field of the employee
     * @bodyParam city string optional the city field of the employee
     * @bodyParam zip string optional the zip code of the employee
     * @bodyParam mobile string optional the mobile of the employee
     * @bodyParam title_id integer optional the title of employee
     * @bodyParam notify boolean optional whether to notify or not of account creation
     * @bodyParam relatives array optional the relatives of the employee
     * @response type json array
     */
    public function testEmployeeControllerUpdateWithValidTokenAdnWithoutParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('PUT', '/v1/employees/update/1', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
    }

    /**
     * Case 3B
     * @test This function test response with valid token without params (customer->Permission Check)
     * @expectedExceptionCode 403
     * update a employee
     * @bodyParam id integer required the id of the employee whose records are being updated
     * @bodyParam first_name string required the first name field of the employee
     * @bodyParam last_name string required the last name field of the employee
     * @bodyParam email string required the email field of the employee
     * @bodyParam address string optional the address field of the employee
     * @bodyParam city string optional the city field of the employee
     * @bodyParam zip string optional the zip code of the employee
     * @bodyParam mobile string optional the mobile of the employee
     * @bodyParam title_id integer optional the title of employee
     * @bodyParam notify boolean optional whether to notify or not of account creation
     * @bodyParam relatives array optional the relatives of the employee
     * @response type json array
     */
    public function testEmployeeControllerUpdateWithValidTokenWithoutParamsForCustomer()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->customer);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        $lang = $loginJson['language'];

        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('POST', '/v1/consultants', [], [], [], $headers);
        $json = json_decode($response->getContent(), true);
        $this->assertSame($this->__getTranslateString('insufficient_permission', [], $lang), $json['error']);
    }

    /**
     * Case 4
     * @test This function test response with valid token and null params
     * @expectedExceptionCode 422
     * update a employee
     * @bodyParam id integer required the id of the employee whose records are being updated
     * @bodyParam first_name string required the first name field of the employee
     * @bodyParam last_name string required the last name field of the employee
     * @bodyParam email string required the email field of the employee
     * @bodyParam address string optional the address field of the employee
     * @bodyParam city string optional the city field of the employee
     * @bodyParam zip string optional the zip code of the employee
     * @bodyParam mobile string optional the mobile of the employee
     * @bodyParam title_id integer optional the title of employee
     * @bodyParam notify boolean optional whether to notify or not of account creation
     * @bodyParam relatives array optional the relatives of the employee
     * @response type json array
     */
    public function testEmployeeControllerUpdateWithValidTokenAndNullParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $employee = Employee::find(1)->user_id;

        $param = [
            "user_id" => $employee,
            "first_name" => 'Jro2nXolef',
            "email" => str_random(12) . '@mail.com',
            "last_name" => 'test',
            "address" => 'test 129 hdscsd jsd bcdsh chdbcvvvvdhdbv dhbv dbvjd',
            "city" => "JBD",
            "zip" => '25343265',
            "mobile" => '7654321890',
            "title_id" => 1,
            "notify" => null,
            "relatives[1]" =>[
                    "relationship_id" => 1
                ]

        ];

        $response = $this->call('PUT', '/v1/employees/update/1', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(200);

        //convert response data in json
        $json = json_decode($response->getContent(), true);

    }

    /**
     * Case 5
     * @test This function test response with valid token but with user.first_name having length max to 191
     * @expectedExceptionCode 422
     * update a employee
     * @bodyParam id integer required the id of the employee whose records are being updated
     * @bodyParam first_name string required the first name field of the employee
     * @bodyParam last_name string required the last name field of the employee
     * @bodyParam email string required the email field of the employee
     * @bodyParam address string optional the address field of the employee
     * @bodyParam city string optional the city field of the employee
     * @bodyParam zip string optional the zip code of the employee
     * @bodyParam mobile string optional the mobile of the employee
     * @bodyParam title_id integer optional the title of employee
     * @bodyParam notify boolean optional whether to notify or not of account creation
     * @bodyParam relatives array optional the relatives of the employee
     * @response type json array
     */
    public function testEmployeeControllerUpdateWithValidTokenAndUserWithMaxLength()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $employee = Employee::find(1)->user_id;
        $param = [
            "user_id" => $employee,
            "first_name" => 'Jro2nXolefKIdOLWieVX6r6lkT5wq0VQ3b3L7QYG10Ec5yPIGgpb0lFLD4P4cy0StLd05oXrNxS5yG2pUTFsu7LlpyqoJXxyrPoLUzrAgYk548IUoOiNDyFZ4l7ELsIEcOAN7mVxnSkY3SsA0IEmhT5gsebZBPGWkBjoiDsocg2TeEnTTualcoI4qKG0mYkfpZSlr8WqNBnUFevIXzjcrAtEwkl1iSQoJ6Y9fIh44B76Mu6rFmLb8xyHO95GUHLd',
            "email" => str_random(12) . '@mail.com',
            "last_name" => 'test',
            "address" => 'test 129 hdscsd jsd bcdsh chdbcvvvvdhdbv dhbv dbvjd',
            "city" => "JBD",
            "zip" => '25343265',
            "mobile" => '7654321890',
            "title_id" => 1,
            "notify" => null,
            "relatives" => [
                1 => [
                    "relationship_id" => 1
                ]
            ]
        ];
        $response = $this->call('PUT', '/v1/employees/update/1', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('max_validation', ['attribute' => 'first_name', 'max' => 191], $lang)
        ], $json['first_name']);
    }

    /**
     * Case 6
     * @test This function test response with valid token but with user.email having normal string instead of valid email
     * @expectedExceptionCode 422
     * update a employee
     * @bodyParam id integer required the id of the employee whose records are being updated
     * @bodyParam first_name string required the first name field of the employee
     * @bodyParam last_name string required the last name field of the employee
     * @bodyParam email string required the email field of the employee
     * @bodyParam address string optional the address field of the employee
     * @bodyParam city string optional the city field of the employee
     * @bodyParam zip string optional the zip code of the employee
     * @bodyParam mobile string optional the mobile of the employee
     * @bodyParam title_id integer optional the title of employee
     * @bodyParam notify boolean optional whether to notify or not of account creation
     * @bodyParam relatives array optional the relatives of the employee
     * @response type json array
     */
    public function testEmployeeControllerUpdateWithUserEmailUsingNormalString()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $employee = Employee::find(1)->user_id;
        $param = [
            "user_id" => $employee,
            "first_name" => 'Jro2nXolefKIdOLWieVX6r6lkT5wq0VQ3b3L7QYG10Ec5yPIGgpb0lFLD4P4cy0StLd05oXrNxS5yG2pUTFsu7LlpyqoJXxyrPoLUzrAgYk548IUoOiNDyFZ4l7ELsIEcOAN7mVxnSkY3SsA0IEmhT5gsebZBPGWkBjoiDsocg2TeEnTTualcoI4qKG0mYkfpZSlr8WqNBnUFevIXzjcrAtEwkl1iSQoJ6Y9fIh44B76Mu6rFmLb8xyHO95GUHLd',
            "email" => '@mail.com',
            "last_name" => 'test',
            "address" => 'test 129 hdscsd jsd bcdsh chdbcvvvvdhdbv dhbv dbvjd',
            "city" => "JBD",
            "zip" => '25343265',
            "mobile" => '7654321890',
            "title_id" => 1,
            "notify" => null,
            "relatives" => [
                1 => [
                    "relationship_id" => 1
                ]
            ]
        ];

        $response = $this->call('PUT', '/v1/employees/update/1', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([$this->__getTranslateString('email_validation', ['attribute' => 'email'], $lang)],
            $json['email']);
    }

    /**
     * Case 8
     * @test This function test response with valid token but with user.email having length max to 191
     * @expectedExceptionCode 422
     * update a employee
     * @bodyParam id integer required the id of the employee whose records are being updated
     * @bodyParam first_name string required the first name field of the employee
     * @bodyParam last_name string required the last name field of the employee
     * @bodyParam email string required the email field of the employee
     * @bodyParam address string optional the address field of the employee
     * @bodyParam city string optional the city field of the employee
     * @bodyParam zip string optional the zip code of the employee
     * @bodyParam mobile string optional the mobile of the employee
     * @bodyParam title_id integer optional the title of employee
     * @bodyParam notify boolean optional whether to notify or not of account creation
     * @bodyParam relatives array optional the relatives of the employee
     * @response type json array
     */
    public function testEmployeeControllerUpdateWithValidTokenAndUserEmailWithMaxLength()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $employee = Employee::find(1)->user_id;
        $param = [
            "user_id" => $employee,
            "first_name" => 'asxyz',
            "email" => 'Jro2nXolefKIdOLWieVX6r6lkT5wq0VQ3b3L7QYG10Ec5yPIGgpb0lFLD4P4cy0StLd05oXrNxS5yG2pUTFsu7LlpyqoJXxyrPoLUzrAgYk548IUoOiNDyFZ4l7ELsIEcOAN7mVxnSkY3SsA0IEmhT5gsebZBPGWkBjoiDsocg2TeEnTTualcoI4qKG0mYkfpZSlr8WqNBnUFevIXzjcrAtEwkl1iSQoJ6Y9fIh44B76Mu6rFmLb8xyHO95GUHLd@gmail.com',
            "last_name" => 'test',
            "address" => 'test 129 hdscsd jsd bcdsh chdbcvvvvdhdbv dhbv dbvjd',
            "city" => "JBD",
            "zip" => '25343265',
            "mobile" => '7654321890',
            "title_id" => 1,
            "notify" => null,
            "relatives" => [
                1 => [
                    "relationship_id" => 1
                ]
            ]
        ];

        $response = $this->call('PUT', '/v1/employees/update/1', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
    }
    /**
     * Case 9
     * @test This function test response with valid token but with user.mobile having length max to 20
     * @expectedExceptionCode 422
     * update a employee
     * @bodyParam id integer required the id of the employee whose records are being updated
     * @bodyParam first_name string required the first name field of the employee
     * @bodyParam last_name string required the last name field of the employee
     * @bodyParam email string required the email field of the employee
     * @bodyParam address string optional the address field of the employee
     * @bodyParam city string optional the city field of the employee
     * @bodyParam zip string optional the zip code of the employee
     * @bodyParam mobile string optional the mobile of the employee
     * @bodyParam title_id integer optional the title of employee
     * @bodyParam notify boolean optional whether to notify or not of account creation
     * @bodyParam relatives array optional the relatives of the employee
     * @response type json array
     */
    public function testEmployeeControllerUpdateWithMobileHavingMaxLength()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $employee = Employee::find(1)->user_id;
        $param = [
            "user_id" => $employee,
            "first_name" => 'asxyz',
            "email" =>  str_random(12) . '@mail.com',
            "last_name" => 'test',
            "address" => 'test 129 hdscsd jsd bcdsh chdbcvvvvdhdbv dhbv dbvjd',
            "city" => "JBD",
            "zip" => '25343265',
            "mobile" => '987456231987456231145',
            "title_id" => 1,
            "notify" => null,
            "relatives" => [
                1 => [
                    "relationship_id" => 1
                ]
            ]
        ];

        $response = $this->call('PUT', '/v1/employees/update/1', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('max_validation', ['attribute' => 'mobile', 'max' => 10], $lang)
        ], $json['mobile']);
    }



    /**
     * Case 10
     * @test This function test response with invalid auth token with valid params
     * @expectedExceptionCode 200
     * update a employee
     * @bodyParam id integer required the id of the employee whose records are being updated
     * @bodyParam first_name string required the first name field of the employee
     * @bodyParam last_name string required the last name field of the employee
     * @bodyParam email string required the email field of the employee
     * @bodyParam address string optional the address field of the employee
     * @bodyParam city string optional the city field of the employee
     * @bodyParam zip string optional the zip code of the employee
     * @bodyParam mobile string optional the mobile of the employee
     * @bodyParam title_id integer optional the title of employee
     * @bodyParam notify boolean optional whether to notify or not of account creation
     * @bodyParam relatives array optional the relatives of the employee
     * @response type json array
     */
    public function testEmployeeControllerUpdateWithInvalidTokenWithValidParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $employee = Employee::find(1)->user_id;
        $param = [
            "user_id" => $employee,
            "first_name" => 'asxyz',
            "email" =>  str_random(12) . '@mail.com',
            "last_name" => 'test',
            "address" => 'test 129 hdscsd jsd bcdsh chdbcvvvvdhdbv dhbv dbvjd',
            "city" => "JBD",
            "zip" => '25343265',
            "mobile" => '98745623',
            "title_id" => 1,
            "notify" => null,
            "relatives[1]" => [
                    "relationship_id" => 1
            ]
        ];

        $response = $this->call('PUT', '/v1/employees/update/1', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(200);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('employee_updated', [], $lang), $json['success']);
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('employees')->truncate();
        DB::table('options')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
