<?php


class EmployeesControllerCreateEmployeeTest extends TestCase
{
    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'custom@yopmail.com',
        'password' => '123456'
    ];
    public $employee = [
        'email' => 'emp1@yopmail.com',
        'password' => '123456'
    ];

    /**
     *  Read.
     *
     *  Admin and Employee holds same permissions .
     *  Customer dont't have permissions to view this page.
     *  Contact-Persons dont't have permission to view this page.
     */

    /**
     * Case 1
     * @test This function test response without auth token without params
     * @expectedExceptionCode 401
     * Create a employee
     * @bodyParam first_name string required the first name field of the employee
     * @bodyParam last_name string required the last name field of the employee
     * @bodyParam email string required the email field of the employee
     * @bodyParam address string optional the address field of the employee
     * @bodyParam city string optional the city field of the employee
     * @bodyParam zip string optional the zip code of the employee
     * @bodyParam mobile string optional the mobile of the employee
     * @bodyParam title_id integer optional the title of employee
     * @bodyParam notify boolean optional whether to notify or not of account creation
     * @bodyParam relatives array optional the relatives of the employee
     * @response json array type
     */
    public function testEmployeeControllerStoreWithoutToken()
    {
        $this->post('/v1/employees/add', [])
            ->seeJsonEquals([
                "error" => $this->__getTranslateString('missing_token_error', [], 2)
            ]);
    }

    /**
     * Case 2
     * @test This function test response with invalid auth token without params
     * @expectedExceptionCode 401
     * Create a employee
     * @bodyParam first_name string required the first name field of the employee
     * @bodyParam last_name string required the last name field of the employee
     * @bodyParam email string required the email field of the employee
     * @bodyParam address string optional the address field of the employee
     * @bodyParam city string optional the city field of the employee
     * @bodyParam zip string optional the zip code of the employee
     * @bodyParam mobile string optional the mobile of the employee
     * @bodyParam title_id integer optional the title of employee
     * @bodyParam notify boolean optional whether to notify or not of account creation
     * @bodyParam relatives array optional the relatives of the employee
     * @response json array type
     */
    public function testEmployeeControllerStoreWithInvalidToken()
    {
        $expired_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expired_token
        ];

        $response = $this->call('POST', '/v1/employees/add', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(400);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('token_expired_error', [], 2), $json['error']);
    }

    /**
     * Case 3
     * @test This function test response with valid token without params
     * @expectedExceptionCode 422
     * Create a employee
     * @bodyParam first_name string required the first name field of the employee
     * @bodyParam last_name string required the last name field of the employee
     * @bodyParam email string required the email field of the employee
     * @bodyParam address string optional the address field of the employee
     * @bodyParam city string optional the city field of the employee
     * @bodyParam zip string optional the zip code of the employee
     * @bodyParam mobile string optional the mobile of the employee
     * @bodyParam title_id integer optional the title of employee
     * @bodyParam notify boolean optional whether to notify or not of account creation
     * @bodyParam relatives array optional the relatives of the employee
     * @response json array type
     */
    public function testEmployeeControllerStoreValidTokenWithoutPrams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('POST', '/v1/employees/add', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
    }

    /**
     * Case 3B
     * @test Customer dont't have permissions to view this page.
     * @expectedExceptionCode 403
     * Create a employee
     * @bodyParam first_name string required the first name field of the employee
     * @bodyParam last_name string required the last name field of the employee
     * @bodyParam email string required the email field of the employee
     * @bodyParam address string optional the address field of the employee
     * @bodyParam city string optional the city field of the employee
     * @bodyParam zip string optional the zip code of the employee
     * @bodyParam mobile string optional the mobile of the employee
     * @bodyParam title_id integer optional the title of employee
     * @bodyParam notify boolean optional whether to notify or not of account creation
     * @bodyParam relatives array optional the relatives of the employee
     * @response json array type
     */
    public function testEmployeeControllerStoreWithValidTokenWithoutPramsForCustomer()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->customer);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);


        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('POST', '/v1/employees/add', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(403);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame(
            $this->__getTranslateString('insufficient_permission', [], $lang)
            , $json['error']);
    }

    /**
     * Case 3c
     * @test Customer dont't have permissions to view this page.
     * @expectedExceptionCode 403
     * Create a employee
     * @bodyParam first_name string required the first name field of the employee
     * @bodyParam last_name string required the last name field of the employee
     * @bodyParam email string required the email field of the employee
     * @bodyParam address string optional the address field of the employee
     * @bodyParam city string optional the city field of the employee
     * @bodyParam zip string optional the zip code of the employee
     * @bodyParam mobile string optional the mobile of the employee
     * @bodyParam title_id integer optional the title of employee
     * @bodyParam notify boolean optional whether to notify or not of account creation
     * @bodyParam relatives array optional the relatives of the employee
     * @response json array type
     */
    public function testEmployeeControllerStoreWithValidTokenWithoutPramsForEmployee()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->employee);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);


        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('POST', '/v1/employees/add', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(403);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
//        match each index value
        $this->assertSame(
            $this->__getTranslateString('insufficient_permission', [], $lang)
            , $json['error']);
    }

    /**
     * Case 4
     * @test This function test response with valid token and null params
     * @expectedExceptionCode 422
     * Create a employee
     * @bodyParam first_name string required the first name field of the employee
     * @bodyParam last_name string required the last name field of the employee
     * @bodyParam email string required the email field of the employee
     * @bodyParam address string optional the address field of the employee
     * @bodyParam city string optional the city field of the employee
     * @bodyParam zip string optional the zip code of the employee
     * @bodyParam mobile string optional the mobile of the employee
     * @bodyParam title_id integer optional the title of employee
     * @bodyParam notify boolean optional whether to notify or not of account creation
     * @bodyParam relatives array optional the relatives of the employee
     * @response json array type
     */
    public function testEmployeeControllerStoreWithValidTokenNullParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            "first_name" => null,
            "email" => null,
            "last_name" => null,
            "address" => null,
            "city" => null,
            "zip" => null,
            "mobile" => null,
            "title_id" => null,
            "notify" => null,
            "relatives" => [
                1 => [
                    "relationship_id" => null
                ]
            ]
        ];

        $response = $this->call('POST', '/v1/employees/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);

        //match each index value
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'first_name'], $lang)],
            $json['first_name']);
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'email'], $lang)],
            $json['email']);
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'relatives.1.relationship_id'], $lang)
        ], $json['relatives.1.relationship_id']);
    }

    /**
     * Case 5
     * @test This function test response with valid token but with name having length max to 255
     * @expectedExceptionCode 422
     * Create a employee
     * @bodyParam first_name string required the first name field of the employee
     * @bodyParam last_name string required the last name field of the employee
     * @bodyParam email string required the email field of the employee
     * @bodyParam address string optional the address field of the employee
     * @bodyParam city string optional the city field of the employee
     * @bodyParam zip string optional the zip code of the employee
     * @bodyParam mobile string optional the mobile of the employee
     * @bodyParam title_id integer optional the title of employee
     * @bodyParam notify boolean optional whether to notify or not of account creation
     * @bodyParam relatives array optional the relatives of the employee
     * @response json array type
     */
    public function testConsultantCompanyWithValidTokenNameNotMoreThan()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            "first_name" => 'Jro2nXolefKIdOLWieVX6r6lkT5wq0VQ3b3L7QYG10Ec5yPIGgpb0lFLD4P4cy0StLd05oXrNxS5yG2pUTFsu7LlpyqoJXxyrPoLUzrAgYk548IUoOiNDyFZ4l7ELsIEcOAN7mVxnSkY3SsA0IEmhT5gsebZBPGWkBjoiDsocg2TeEnTTualcoI4qKG0mYkfpZSlr8WqNBnUFevIXzjcrAtEwkl1iSQoJ6Y9fIh44B76Mu6rFmLb8xyHO95GUHLd',
            "email" => str_random(12) . '@mail.com',
            "last_name" => 'test',
            "address" => 'test 129 hdscsd jsd bcdsh chdbcvvvvdhdbv dhbv dbvjd',
            "city" => 'JBD',
            "zip" => '25343265',
            "mobile" => '7654321890',
            "title_id" => null,
            "notify" => null,
            "relatives" => [
                1 => [
                    "relationship_id" => 1
                ]
            ]
        ];

        $response = $this->call('POST', '/v1/employees/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
    }

    /**
     * Case 6
     * @test This function test response with valid token but with name having String
     * @expectedExceptionCode 422
     * Create a employee
     * @bodyParam first_name string required the first name field of the employee
     * @bodyParam last_name string required the last name field of the employee
     * @bodyParam email string required the email field of the employee
     * @bodyParam address string optional the address field of the employee
     * @bodyParam city string optional the city field of the employee
     * @bodyParam zip string optional the zip code of the employee
     * @bodyParam mobile string optional the mobile of the employee
     * @bodyParam title_id integer optional the title of employee
     * @bodyParam notify boolean optional whether to notify or not of account creation
     * @bodyParam relatives array optional the relatives of the employee
     * @response json array type
     */
    public function testConsultantCompanyWithValidTokenNameIsString()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            "first_name" => 'Jro2nXolef',
            "email" => null,
            "last_name" => 'test',
            "address" => 'test 129 hdscsd jsd bcdsh chdbcvvvvdhdbv dhbv dbvjd',
            "city" => 'JBD',
            "zip" => '25343265',
            "mobile" => '7654321890',
            "title_id" => null,
            "notify" => null,
            "relatives" => [
                1 => [
                    "relationship_id" => 1
                ]
            ]
        ];

        $response = $this->call('POST', '/v1/employees/add', $param, [], [], $headers);

        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
                $this->__getTranslateString('required_validation', ['attribute' => 'email'], $lang)
            ]
            , $json['email']);
    }

    /**
     * Case 7
     * @test This function test response with valid token but with name having integer
     * @expectedExceptionCode 422
     * Create a employee
     * @bodyParam first_name string required the first name field of the employee
     * @bodyParam last_name string required the last name field of the employee
     * @bodyParam email string required the email field of the employee
     * @bodyParam address string optional the address field of the employee
     * @bodyParam city string optional the city field of the employee
     * @bodyParam zip string optional the zip code of the employee
     * @bodyParam mobile string optional the mobile of the employee
     * @bodyParam title_id integer optional the title of employee
     * @bodyParam notify boolean optional whether to notify or not of account creation
     * @bodyParam relatives array optional the relatives of the employee
     * @response json array type
     */
    public function testConsultantCompanyWithValidTokenNameIsInteger()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            "first_name" => 'Jro2nXolef',
            "email" => str_random(12) . '@mail.com',
            "last_name" => 21313132,
            "address" => 'test 129 hdscsd jsd bcdsh chdbcvvvvdhdbv dhbv dbvjd',
            "city" => 'JBD',
            "zip" => '25343265',
            "mobile" => '7654321890',
            "title_id" => null,
            "notify" => null,
            "relatives" => [
                1 => [
                    "relationship_id" => 1
                ]
            ]
        ];

        $response = $this->call('POST', '/v1/employees/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('string_validation', ['attribute' => 'last_name'], $lang)
        ], $json['last_name']);
    }



    /**
     * Case 8
     * @test This function test response with valid token but with name having integer
     * @expectedExceptionCode 422
     * Create a employee
     * @bodyParam first_name string required the first name field of the employee
     * @bodyParam last_name string required the last name field of the employee
     * @bodyParam email string required the email field of the employee
     * @bodyParam address string optional the address field of the employee
     * @bodyParam city string optional the city field of the employee
     * @bodyParam zip string optional the zip code of the employee
     * @bodyParam mobile string optional the mobile of the employee
     * @bodyParam title_id integer optional the title of employee
     * @bodyParam notify boolean optional whether to notify or not of account creation
     * @bodyParam relatives array optional the relatives of the employee
     * @response json array type
     */
    public function employeeControllerWithValidParamsAndCityBeNumber()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            "first_name" => 'Jro2nXolef',
            "email" => str_random(12) . '@mail.com',
            "last_name" => 'test',
            "address" => 'test 129 hdscsd jsd bcdsh chdbcvvvvdhdbv dhbv dbvjd',
            "city" => 131313,
            "zip" => '25343265',
            "mobile" => '7654321890',
            "title_id" => null,
            "notify" => null,
            "relatives" => [
                1 => [
                    "relationship_id" => 1
                ]
            ]
        ];

        $response = $this->call('POST', '/v1/employees/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('string_validation', ['attribute' => 'city'], $lang)
        ], $json['city']);
    }

    /**
     * Case 9
     * @test This function test response with valid token but with name having integer
     * @expectedExceptionCode 422
     * Create a employee
     * @bodyParam first_name string required the first name field of the employee
     * @bodyParam last_name string required the last name field of the employee
     * @bodyParam email string required the email field of the employee
     * @bodyParam address string optional the address field of the employee
     * @bodyParam city string optional the city field of the employee
     * @bodyParam zip string optional the zip code of the employee
     * @bodyParam mobile string optional the mobile of the employee
     * @bodyParam title_id integer optional the title of employee
     * @bodyParam notify boolean optional whether to notify or not of account creation
     * @bodyParam relatives array optional the relatives of the employee
     * @response json array type
     */
    public function employeeControllerWithValidParamsAndZipBeNumber()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            "first_name" => 'Jro2nXolef',
            "email" => str_random(12) . '@mail.com',
            "last_name" => 'test',
            "address" => 'test 129 hdscsd jsd bcdsh chdbcvvvvdhdbv dhbv dbvjd',
            "city" => "JBD",
            "zip" => 25343265,
            "mobile" => '7654321890',
            "title_id" => null,
            "notify" => null,
            "relatives" => [
                1 => [
                    "relationship_id" => 1
                ]
            ]
        ];

        $response = $this->call('POST', '/v1/employees/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('string_validation', ['attribute' => 'zip'], $lang)
        ], $json['zip']);
    }

    /**
     * Case 10
     * @test This function test response with valid token but with name having integer
     * @expectedExceptionCode 422
     * Create a employee
     * @bodyParam first_name string required the first name field of the employee
     * @bodyParam last_name string required the last name field of the employee
     * @bodyParam email string required the email field of the employee
     * @bodyParam address string optional the address field of the employee
     * @bodyParam city string optional the city field of the employee
     * @bodyParam zip string optional the zip code of the employee
     * @bodyParam mobile string optional the mobile of the employee
     * @bodyParam title_id integer optional the title of employee
     * @bodyParam notify boolean optional whether to notify or not of account creation
     * @bodyParam relatives array optional the relatives of the employee
     * @response json array type
     */
    public function employeeControllerWithValidParamsAndTitleIDBeString()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            "first_name" => 'Jro2nXolef',
            "email" => str_random(12) . '@mail.com',
            "last_name" => 'test',
            "address" => 'test 129 hdscsd jsd bcdsh chdbcvvvvdhdbv dhbv dbvjd',
            "city" => "JBD",
            "zip" => '25343265',
            "mobile" => '7654321890',
            "title_id" => 'asdasd',
            "notify" => null,
            "relatives" => [
                1 => [
                    "relationship_id" => 1
                ]
            ]
        ];

        $response = $this->call('POST', '/v1/employees/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('numeric_validation', ['attribute' => 'title_id'], $lang)
        ], $json['title_id']);
    }


}
