<?php


use App\Models\FireClass;
use App\Models\Section;

class MarkerControllerCreateMarkerOfTypeSecondTest
{
    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];

    /**
     * This function test response without any param and with out any token
     *
     */
    public function testCaseOne()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('customers')->truncate();
        DB::table('floors')->truncate();
        DB::table('users')->truncate();
        DB::table('workplaces')->truncate();
        DB::table('projects')->truncate();
        DB::table('sections')->truncate();
        DB::table('fire_classes')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        app(DatabaseSeeder::class)->call(UserTestSeeder::class);
        app(DatabaseSeeder::class)->call(ProjectDocumentationWithExistingFloorSeeder::class);
        FireClass::create(['name' => "test", 'class_type' => 1]);
//        section id is present in url param as well as functional param.
        $this->post('/v1/sections/', [])
            ->seeJsonEquals([
                "error" => $this->__getTranslateString('route_not_found', [], 2)
            ]);
    }

    /**
     * Case 2
     * This function test response with invalid auth token without params
     *
     */
    public function testCaseTwo()
    {
        $expired_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expired_token
        ];
        $response = $this->call('POST', '/v1/sections/', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(404);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('route_not_found', [], 2), $json['error']);
    }

    /**
     * Case 3
     * This function test response with valid token without params
     *
     */
    public function testCaseThree()
    {
        $loginJson = TestCase::getLoginInstance($this->loginParam);
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('POST', '/v1/sections/', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(404);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('route_not_found', []), $json['error']);
    }

    /**
     * Case 4
     * This function test response with valid token and null params
     *
     */
    public function testCaseFour()
    {
        $loginJson = TestCase::getLoginInstance($this->loginParam);
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $lang = $loginJson['language'];
        $param = [
            'section_id' => null,
            'hole_type' => null,
            'status' => null,
            'photo' => null,
            'notes' => null,
            'y_axis' => null,
            'x_axis' => null,
            'predefined' => null,
            'fire_class_id' => null,
            'added_by_name' => null,
            'marker_date' => null,
            'insulation_material_id' => null,
            'insulation_profile_id' => null,
            'dimensions' => null,
        ];

        $response = $this->call('POST', '/v1/sections/', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(404);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        $this->assertSame($this->__getTranslateString('route_not_found', []), $json['error']);

    }

    /**
     * Case 5
     * This function test response with valid token but with soft deleted section_id
     *
     */
    public function testCaseFive()
    {
        $loginJson = TestCase::getLoginInstance($this->loginParam);
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $lang = $loginJson['language'];
        $sectionDelete=Section::find(2);
        $sectionDelete->delete();
        $param = [
            'hole_type' => null,
            'status' => null,
            'photo' => null,
            'notes' => null,
            'y_axis' => null,
            'x_axis' => null,
            'predefined' => null,
            'fire_class_id' => null,
            'added_by_name' => null,
            'marker_date' => null,
            'insulation_material_id' => null,
            'insulation_profile_id' => null,
            'dimensions' => null,
        ];

        $response = $this->call('POST', '/v1/sections/2/markers/add/1', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        $this->assertSame([$this->__getTranslateString('exists_validation', ['attribute' => 'section_id'], $lang)],
            $json['section_id']);

    }

    /**
     * Case 6
     * This function test response with valid token but with non existing section_id
     *
     */
    public function testCaseSix()
    {
        $loginJson = TestCase::getLoginInstance($this->loginParam);
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $lang = $loginJson['language'];
        $param = [
            'hole_type' => null,
            'status' => null,
            'photo' => null,
            'notes' => null,
            'y_axis' => null,
            'x_axis' => null,
            'predefined' => null,
            'fire_class_id' => null,
            'added_by_name' => null,
            'marker_date' => null,
            'insulation_material_id' => null,
            'insulation_profile_id' => null,
            'dimensions' => null,
        ];

        $response = $this->call('POST', '/v1/sections/2500/markers/add/1', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        $this->assertSame([$this->__getTranslateString('exists_validation', ['attribute' => 'section_id'], $lang)],
            $json['section_id']);

    }

    /**
     * Case 6
     * This function test response with valid token but with non existing section_id
     *
     */
    public function testCaseSeven()
    {
        $loginJson = TestCase::getLoginInstance($this->loginParam);
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $lang = $loginJson['language'];
        $param = [
            'hole_type' => 1,
            'status' => 1,
            'y_axis' => 0.70,
            'x_axis' => 0.75,
            'predefined' => 1,
            'fire_class_id' => 1,
            'added_by_name' => "manpreet",
            'added_by' => 1,
            'updated_by' => 1,
            'marker_date' => "2019-05-16",
            'insulation_material_id' => 3,
            'insulation_profile_id' => 5,
            'dimensions' => "1"
        ];

        $response = $this->call('POST', '/v1/sections/1/markers/add/1', $param, [], [], $headers);


    }
}
