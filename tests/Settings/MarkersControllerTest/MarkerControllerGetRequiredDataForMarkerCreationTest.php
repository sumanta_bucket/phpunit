<?php


class MarkerControllerGetRequiredDataForMarkerCreationTest extends TestCase
{
    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'custom@yopmail.com',
        'password' => '123456'
    ];
    public $employee=[
        'email'=>'emp1@yopmail.com',
        'password'=>'123456'
    ];

    /**
     *  Admin and Employee holds same permissions .
     *  Customer dont't have permissions to view this page.
     *  Contact-Persons dont't have permission to view this page.
     */



    /**
     * Case 1
     * This function test response without auth token without params
     */
    public function testMarkerControllerGetRequiredDataForMarkerCreationWithoutToken()
    {
        $this->get('/v1/markers/get/1', [])
            ->seeJsonEquals([
                "error" => $this->__getTranslateString('missing_token_error', [], 2)
            ]);
    }


    /**
     * Case 2
     * This function test response with invalid auth token without params
     */
    public function testMarkerControllerGetRequiredDataForMarkerCreationWithInvalidToken()
    {
        $expired_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expired_token
        ];

        $response = $this->call('GET', '/v1/markers/get/1', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(400);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('token_expired_error', [], 2), $json['error']);
    }


    /**
     * Case 3
     * This function test response with valid token without params
     *
     */
    public function testMarkerControllerGetRequiredDataForMarkerCreationWithValidTokenWithoutPrams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        $lang = $loginJson['language'];

        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('GET', '/v1/markers/get/1', [], [], [], $headers);
        $this->assertResponseStatus(200);
    }
    /**
     * Case 3B
     * Customer dont't have permissions to view this page.
     *
     */
    public function testMarkerControllerGetRequiredDataForMarkerCreationAddWithValidTokenWithoutPramsForCustomer()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->customer);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);


        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('GET', '/v1/markers/get/1', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(200);
    }

    /**
     * Case 3c
     * Customer dont't have permissions to view this page.
     *
     */
    public function testMarkerControllerGetRequiredDataForMarkerCreationAddWithValidTokenWithoutPramsForEmployee()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->employee);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);


        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('GET', '/v1/markers/get/1', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
    }

    /**
     * Case 4
     * This function test response with valid token and null params
     *
     */
    public function testMarkerControllerGetRequiredDataForMarkerCreationWithValidTokenNullParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];


        $param = [
            'order' => [
                [
                    'dir' => null,
                    'column' => null
                ]
            ],
            'search' => ['value' => null],
            'start' => null,
            'length' => null
        ];

        $response = $this->call('GET', '/v1/markers/get/1', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(200);
    }
//

    /**
     * Case 5
     * This function test response with valid token and all params
     *
     */
    public function testMarkerControllerGetRequiredDataForMarkerCreationWithValidTokenAllParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];

        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];


        $param = [
            'order' => [
                [
                    'dir' => 'asc',
                    'column' => 0
                ]
            ],
            'search' => ['value' => 'test'],
            'start' => 0,
            'length' => 2
        ];

        $response = $this->call('GET', '/v1/markers/get/1', $param, [], [], $headers);
        //check response status code
        $json = json_decode($response->getContent(), true);

        $this->assertResponseStatus(200);
    }

    /**
     * Case 6
     * This function test response with valid token and column null
     *
     */
    public function testMarkerControllerGetRequiredDataForMarkerCreationWithValidTokenAllColumnNull()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];


        $param = [
            'order' => [
                [
                    'dir' => 'asc',
                    'column' => null
                ]
            ],
            'search' => ['value' => 'abc'],
            'start' => 0,
            'length' => 1
        ];

        $response = $this->call('GET', '/v1/markers/get/1', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(200);
    }
}
