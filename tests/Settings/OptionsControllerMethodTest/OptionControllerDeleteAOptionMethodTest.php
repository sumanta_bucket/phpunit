<?php


use Illuminate\Support\Facades\Config;

class OptionControllerDeleteAOptionMethodTest extends TestCase
{
    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'custom@yopmail.com',
        'password' => '123456'
    ];
    public $employee = [
        'email' => 'emp1@yopmail.com',
        'password' => '123456'
    ];

    /**
     * Case 1
     * @test This function test response without any param and with out any token
     * @expectedExceptionCode 401
     * Delete a options
     * @bodyParam id integer required the id of the option to be deleted
     * @response json type array
     */
    public function testOptionControllerDestroyWithoutToken()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('options')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        app(DatabaseSeeder::class)->call(OptionsTableSeeder::class);

        $response = $this->call('DELETE', '/v1/options/delete/3', [], [], [], []);
        //check response status code
        $this->assertResponseStatus(401);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('missing_token_error', [], Config::get('constant.default_lang')), $json['error']);
    }

    /**
     * Case 2
     * @test This function test response without any param and with invalid token
     * @expectedExceptionCode 400
     * Delete a options
     * @bodyParam id integer required the id of the option to be deleted
     * @response json type array
     */
    public function testOptionControllerDestroyWithInvalidTokenWithoutParam()
    {
        $expiredToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expiredToken
        ];

        $response = $this->call('DELETE', '/v1/options/delete/3', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(400);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('token_expired_error', [], Config::get('constant.default_lang')), $json['error']);
    }

    /**
     * Case 3
     * @test This function test response without any param and with invalid token
     * @expectedExceptionCode 404
     * Delete a options
     * @bodyParam id integer required the id of the option to be deleted
     * @response json type array
     */
    public function testOptionControllerDestroyCompanyWithValidTokenWithoutParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);

        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('DELETE', '/v1/options/delete/', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(404);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('route_not_found', [], Config::get('constant.default_lang')), $json['error']);
    }

    /**
     * Case 4
     * @test This function test response with valid token but with not existing id
     * @expectedExceptionCode 422
     * Delete a options
     * @bodyParam id integer required the id of the option to be deleted
     * @response json type array
     */
    public function testOptionControllerDestroyWithValidTokenNonExistingId()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('DELETE', '/v1/options/delete/0', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([$this->__getTranslateString('exists_validation', ['attribute' => 'option_id'], $lang)],
            $json['option_id']);
    }
    /**
     * Case 5
     * @test This function test response with valid token and valid id
     * @expectedExceptionCode 200
     * Delete a options
     * @bodyParam id integer required the id of the option to be deleted
     * @response json type array
     */
    public function testOptionControllerDestroyWithValidTokenValidID()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('DELETE', '/v1/options/delete/1', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(200);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('Overtime rate 100 has been deleted successfully.', [], $lang), $json['success']);
    }
    /**
     * Case 6
     * @test This function test response with valid token but with soft deleted id
     * @expectedExceptionCode 422
     * Delete a options
     * @bodyParam id integer required the id of the option to be deleted
     * @response json type array
     */
    public function testOptionControllerDestroyWithValidTokenSoftDeletedID()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('DELETE', '/v1/options/delete/1', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);

        //match each index value
        $this->assertSame([$this->__getTranslateString('exists_validation', ['attribute' => 'option_id'], $lang)],
            $json['option_id']);
    }



    /**
     * Case 7
     * @test Customer dont't have permissions to view this page.
     * @expectedExceptionCode 403
     * Delete a options
     * @bodyParam id integer required the id of the option to be deleted
     * @response json type array
     */
    public function testOptionControllerDestroyWithValidTokenWithoutPramsForCustomer()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->customer);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);


        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('DELETE', '/v1/options/delete/2', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(403);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame(
            $this->__getTranslateString('insufficient_permission', [], $lang)
            , $json['error']);
    }

    /**
     * Case 8
     * @test Customer dont't have permissions to view this page.
     * @expectedExceptionCode 403
     * Delete a options
     * @bodyParam id integer required the id of the option to be deleted
     * @response json type array
     */
    public function testOptionControllerDestroyWithValidTokenWithoutPramsForEmployee()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->employee);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);


        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('DELETE', '/v1/options/delete/2', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(403);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
//        match each index value
        $this->assertSame(
            $this->__getTranslateString('insufficient_permission', [], $lang)
            , $json['error']);

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('options')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
