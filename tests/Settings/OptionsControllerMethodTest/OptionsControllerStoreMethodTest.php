<?php


class OptionsControllerStoreMethodTest extends TestCase
{
    /*
* Login param for admin , employee , customer to check role based authentication
* */
    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'customh@yopmail.com',
        'password' => '123456'
    ];
    public $employee = [
        'email' => 'emp1@yopmail.com',
        'password' => '123456'
    ];

    /**
     *  Read.
     *
     *  Admin and Employee holds same permissions .
     *  Customer dont't have permissions to view this page.
     *  Contact-Persons dont't have permission to view this page.
     */

    /**
     * Case 1
     * @test This function test response without auth token without params
     * @expectedExceptionCode 401
     * Create a option
     * @bodyParam option_name string required the option whose value we want to create like customer_category or customer_title
     * @bodyParam option_value string required the option value
     * @response json type array
     */
    public function optionControllerStoreWithoutToken()
    {
        $this->post('/v1/options/add', [])
            ->seeJsonEquals([
                "error" => $this->__getTranslateString('missing_token_error', [], 2)
            ]);
    }

    /**
     * Case 2
     * @test This function test response with invalid auth token without params
     * @expectedExceptionCode 400
     * Create a option
     * @bodyParam option_name string required the option whose value we want to create like customer_category or customer_title
     * @bodyParam option_value string required the option value
     * @response json type array
     */
    public function optionControllerStoreWithInvalidToken()
    {
        $expired_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expired_token
        ];

        $response = $this->call('POST', '/v1/options/add', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(400);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('token_expired_error', [], 2), $json['error']);
    }

    /**
     * Case 3
     * @test This function test response with valid token without params
     * @expectedExceptionCode 422
     * Create a option
     * @bodyParam option_name string required the option whose value we want to create like customer_category or customer_title
     * @bodyParam option_value string required the option value
     * @response json type array
     */
    public function optionControllerStoreWithValidTokenWithoutPrams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('POST', '/v1/options/add', [], [], [], $headers);

        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'option_name'], $lang)
        ], $json['option_name']);
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'option_value'], $lang)],
            $json['option_value']);
    }

    /**
     * Case 4
     * @test This function test response with valid token and null params
     * @expectedExceptionCode 422
     * Create a option
     * @bodyParam option_name string required the option whose value we want to create like customer_category or customer_title
     * @bodyParam option_value string required the option value
     * @response json type array
     */
    public function optionControllerStoreWithValidTokenNullParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            "option_name" => null,
            "option_value" => null
        ];

        $response = $this->call('POST', '/v1/options/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'option_name'], $lang)
        ], $json['option_name']);
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'option_value'], $lang)],
            $json['option_value']);
    }

    /**
     * Case 6
     * @test This function test response with valid token but with option having maximum length
     * @expectedExceptionCode 422
     * Create a option
     * @bodyParam option_name string required the option whose value we want to create like customer_category or customer_title
     * @bodyParam option_value string required the option value
     * @response json type array
     */
    public function testEmailTemplateStoreWithOptionHavingMaximumLength()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            "option_name" => "customer_category",
            "option_value" => "az2WVF5O3vAN1ZV7mCec8clEcpLvgUfirVg6vOFpwUlcChNTBs7I8EI58Nd09GmXNJpzXVOz2bRH29ATC1ssAsZXqJLZhtvPUkaBMDNfwvbVEFgeCZyA3M6jEaPWPKVsy7jqfO4JLTZTCcDXFBOsZw71wcgmXd8npA2ca0md0MgIW5j1wejxOf4LhPmoVTYXU3ru7bTMhh6XrKlvRxLrdPAme11KYFy6Ts0iVrMCOtFFmJqNCeNlT9Yg5HwW93Go"
        ];

        $response = $this->call('POST', '/v1/options/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('max_validation', ['attribute' => 'option_value', 'max' => 191], $lang)
        ], $json['option_value']);
    }

    /**
     * Case 7
     * @test This function test response with valid token Valid Param
     * @expectedExceptionCode 200
     * Create a option
     * @bodyParam option_name string required the option whose value we want to create like customer_category or customer_title
     * @bodyParam option_value string required the option value
     * @response json type array
     */
    public function testEmailTemplateStoreWithAllParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            "option_name" => "customer_category",
            "option_value" => "T9Yg5HwW93Go"
        ];

        $response = $this->call('POST', '/v1/options/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(200);

        //convert response data in json
        $json = json_decode($response->getContent(), true);

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('options')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
