<?php


class OptionsControllerUpdateOptionTest extends TestCase
{
    /*
* Login param for admin , employee , customer to check role based authentication
* */
    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'custom@yopmail.com',
        'password' => '123456'
    ];
    public $employee = [
        'email' => 'emp1@yopmail.com',
        'password' => '123456'
    ];

    /**
     * Case 1
     * @test This function test response without auth token without params
     * @expectedExceptionCode 401
     * Update a option
     * @bodyParam id integer required the id of the option model whose value we want to update
     * @bodyParam option_name string required the option whose value we want to create like customer_category or customer_title
     * @bodyParam option_value string required the option value
     * @response json type array
     */
    public function optionControllerUpdateWithoutToken()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('options')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        app(DatabaseSeeder::class)->call(OptionsTableSeeder::class);

        $this->put('/v1/options/update/{id}', [])
            ->seeJsonEquals([
                "error" => $this->__getTranslateString('missing_token_error', [], 2)
            ]);
    }

    /**
     * Case 2
     * @test This function test response with invalid auth token without params
     * @expectedExceptionCode 400
     * Update a option
     * @bodyParam id integer required the id of the option model whose value we want to update
     * @bodyParam option_name string required the option whose value we want to create like customer_category or customer_title
     * @bodyParam option_value string required the option value
     * @response json type array
     */
    public function optionControllerUpdateWithInvalidTokenWithoutParam()
    {
        $expiredToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expiredToken
        ];

        $response = $this->call('PUT', '/v1/options/update/{id}', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(400);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('token_expired_error', [], 2), $json['error']);
    }

    /**
     * Case 3
     * @test This function test response with valid token without params
     * @expectedExceptionCode 500
     * Update a option
     * @bodyParam id integer required the id of the option model whose value we want to update
     * @bodyParam option_name string required the option whose value we want to create like customer_category or customer_title
     * @bodyParam option_value string required the option value
     * @response json type array
     */
    public function optionControllerUpdateWithValidTokenWithoutParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('PUT', '/v1/options/update/{id}', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(500);
    }

    /**
     * Case 4
     * @test This function test response with valid token and null params
     * @expectedExceptionCode 422
     * Update a option
     * @bodyParam id integer required the id of the option model whose value we want to update
     * @bodyParam option_name string required the option whose value we want to create like customer_category or customer_title
     * @bodyParam option_value string required the option value
     * @response json type array
     */
    public function optionControllerUpdateWithValidTokenNullParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            "option_name" => null,
            "option_value" => null
        ];

        $response = $this->call('PUT', '/v1/options/update/1', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'option_value'], $lang)],
            $json['option_value']);
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'option_name'], $lang)],
            $json['option_name']);
    }
//

    /**
     * Case 5
     * @test This function test response with valid token but with name having length max to 255
     * @expectedExceptionCode 422
     * Update a option
     * @bodyParam id integer required the id of the option model whose value we want to update
     * @bodyParam option_name string required the option whose value we want to create like customer_category or customer_title
     * @bodyParam option_value string required the option value
     * @response json type array
     */
    public function optionControllerUpdateWithValidTokenWithParamsWithNameNotMoreThan()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'option_name' => 'Jro2nXolefKIdOLWieVX6r6lkT5wq0VQ3b3L7QYG10Ec5yPIGgpb0lFLD4P4cy0StLd05oXrNxS5yG2pUTFsu7LlpyqoJXxyrPoLUzrAgYk548IUoOiNDyFZ4l7ELsIEcOAN7mVxnSkY3SsA0IEmhT5gsebZBPGWkBjoiDsocg2TeEnTTualcoI4qKG0mYkfpZSlr8WqNBnUFevIXzjcrAtEwkl1iSQoJ6Y9fIh44B76Mu6rFmLb8xyHO95GUHLd',
            "option_value"=>"customer_category upated"
        ];

        $response = $this->call('PUT', '/v1/options/update/1', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('in_validation', ['attribute' => 'option_name'], $lang)
        ], $json['option_name']);
    }



    /**
     * Case 7
     * @test This function test response with invalid auth token with valid params
     * @expectedExceptionCode 200
     * Update a option
     * @bodyParam id integer required the id of the option model whose value we want to update
     * @bodyParam option_name string required the option whose value we want to create like customer_category or customer_title
     * @bodyParam option_value string required the option value
     * @response json type array
     */
    public function optionControllerUpdateWithValidParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            "option_name"=>"customer_category",
            "option_value"=>"customer_category upated"

        ];

        $response = $this->call('PUT', '/v1/options/update/4', $param, [], [], $headers);
        //check response status code

        $this->assertResponseStatus(200);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('customer_category_updated', [], $lang), $json['success']);
    }

    /**
     * Case 8
     * @test Customer dont't have permissions to view this page.
     * @expectedExceptionCode 403
     * Update a option
     * @bodyParam id integer required the id of the option model whose value we want to update
     * @bodyParam option_name string required the option whose value we want to create like customer_category or customer_title
     * @bodyParam option_value string required the option value
     * @response json type array
     */
    public function optionControllerUpdateWithValidTokenWithoutPramsForCustomer()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->customer);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);


        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('PUT', '/v1/options/update/2', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(403);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame(
            $this->__getTranslateString('insufficient_permission', [], $lang)
            , $json['error']);
    }

    /**
     * Case 9
     * @test Customer dont't have permissions to view this page.
     * @expectedExceptionCode 403
     * Update a option
     * @bodyParam id integer required the id of the option model whose value we want to update
     * @bodyParam option_name string required the option whose value we want to create like customer_category or customer_title
     * @bodyParam option_value string required the option value
     * @response json type array
     */
    public function optionControllerUpdateWithValidTokenWithoutPramsForEmployee()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->employee);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);


        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('PUT', '/v1/options/update/2', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(403);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
//        match each index value
        $this->assertSame(
            $this->__getTranslateString('insufficient_permission', [], $lang)
            , $json['error']);

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('options')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
