<?php


class OtherSettingsCreateSettingsNamesMaterialSettingsTest extends TestCase
{
    /*
          * Login param for admin , employee , customer to check role based authentication
          * */

    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'custom@yopmail.com',
        'password' => '123456'
    ];
    public $employee = [
        'email' => 'emp1@yopmail.com',
        'password' => '123456'
    ];

    /**
     *  Admin and Employee holds same permissions .
     *  Customer dont't have permissions to view this page.
     *  Contact-Persons dont't have permission to view this page.
     */


    /**
     * Case 1
     * @test  This function test response without auth token without params
     * @expectedExceptionCode 401
     * create a other settings
     * @bodyparam section string required the name of the table in which records are created
     * @bodyParam type string optional the type for fireclasses like 1=>insulation and 2=>firesealing
     * @bodyparam name string required the name of the value of the name field
     * @bodyparam document file optional the document to be uploaded
     * @bodyparam class_type int optional fire resistance type like 1=>insulation, 2=> firesealing
     * @bodyparam material_type_id int optional material type id
     */
    public function testOtherSettingsCreateSettingsNamesMaterialSettingsTestStoreWithoutToken()
    {
        $this->post('/v1/other_settings/add/material_settings', [])
            ->seeJsonEquals([
                "error" => $this->__getTranslateString('missing_token_error', [], 2)
            ]);
    }

    /**
     * Case 2
     * @test This function test response with invalid auth token without params
     * @expectedExceptionCode 400
     * create a other settings
     * @bodyparam section string required the name of the table in which records are created
     * @bodyParam type string optional the type for fireclasses like 1=>insulation and 2=>firesealing
     * @bodyparam name string required the name of the value of the name field
     * @bodyparam document file optional the document to be uploaded
     * @bodyparam class_type int optional fire resistance type like 1=>insulation, 2=> firesealing
     * @bodyparam material_type_id int optional material type id
     */
    public function testOtherSettingsCreateSettingsNamesMaterialSettingsTestStoreWithInvalidToken()
    {
        $expired_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expired_token
        ];

        $response = $this->call('POST', '/v1/other_settings/add/material_settings', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(400);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('token_expired_error', [], 2), $json['error']);
    }

    /**
     * Case 3
     * @test This function test response with valid token without params
     * @expectedExceptionCode 422
     * create a other settings
     * @bodyparam section string required the name of the table in which records are created
     * @bodyParam type string optional the type for fireclasses like 1=>insulation and 2=>firesealing
     * @bodyparam name string required the name of the value of the name field
     * @bodyparam document file optional the document to be uploaded
     * @bodyparam class_type int optional fire resistance type like 1=>insulation, 2=> firesealing
     * @bodyparam material_type_id int optional material type id
     */
    public function testOtherSettingsCreateSettingsNamesMaterialSettingsTestStoreValidTokenWithoutPrams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('POST', '/v1/other_settings/add/material_settings', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'name'], $lang)
        ], $json['name']);
    }

    /**
     * Case 3B
     * @test Customer dont't have permissions to view this page.
     * @expectedExceptionCode 403
     * create a other settings
     * @bodyparam section string required the name of the table in which records are created
     * @bodyParam type string optional the type for fireclasses like 1=>insulation and 2=>firesealing
     * @bodyparam name string required the name of the value of the name field
     * @bodyparam document file optional the document to be uploaded
     * @bodyparam class_type int optional fire resistance type like 1=>insulation, 2=> firesealing
     * @bodyparam material_type_id int optional material type id
     */
    public function testOtherSettingsCreateSettingsNamesMaterialSettingsTestStoreWithValidTokenWithoutPramsForCustomer()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->customer);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);


        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('POST', '/v1/other_settings/add/material_settings', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(403);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame(
            $this->__getTranslateString('insufficient_permission', [], $lang)
            , $json['error']);
    }

    /**
     * Case 3c
     * @test Customer dont't have permissions to view this page.
     * @expectedExceptionCode 403
     * create a other settings
     * @bodyparam section string required the name of the table in which records are created
     * @bodyParam type string optional the type for fireclasses like 1=>insulation and 2=>firesealing
     * @bodyparam name string required the name of the value of the name field
     * @bodyparam document file optional the document to be uploaded
     * @bodyparam class_type int optional fire resistance type like 1=>insulation, 2=> firesealing
     * @bodyparam material_type_id int optional material type id
     */
    public function testOtherSettingsCreateSettingsNamesMaterialSettingsTestStoreWithValidTokenWithoutPramsForEmployee()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->employee);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);


        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('POST', '/v1/other_settings/add/material_settings', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(403);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
//        match each index value
        $this->assertSame(
            $this->__getTranslateString('insufficient_permission', [], $lang)
            , $json['error']);
    }

    /**
     * Case 4
     * @test This function test response with valid token and null params
     * @expectedExceptionCode 422
     * create a other settings
     * @bodyparam section string required the name of the table in which records are created
     * @bodyParam type string optional the type for fireclasses like 1=>insulation and 2=>firesealing
     * @bodyparam name string required the name of the value of the name field
     * @bodyparam document file optional the document to be uploaded
     * @bodyparam class_type int optional fire resistance type like 1=>insulation, 2=> firesealing
     * @bodyparam material_type_id int optional material type id
     */
    public function testOtherSettingsCreateSettingsNamesMaterialSettingsTestStoreWithValidTokenNullParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'name' => null
        ];

        $response = $this->call('POST', '/v1/other_settings/add/material_settings', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);

        //match each index value
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'name'], $lang)
        ], $json['name']);
    }

    /**
     * Case 5
     * @test This function test response with valid token but with name having length max to 191
     * @expectedExceptionCode   422
     * create a other settings
     * @bodyparam section string required the name of the table in which records are created
     * @bodyParam type string optional the type for fireclasses like 1=>insulation and 2=>firesealing
     * @bodyparam name string required the name of the value of the name field
     * @bodyparam document file optional the document to be uploaded
     * @bodyparam class_type int optional fire resistance type like 1=>insulation, 2=> firesealing
     * @bodyparam material_type_id int optional material type id
     */
    public function testOtherSettingsCreateSettingsNamesMaterialSettingsTestStoreWithValidTokenNameNotMoreThan()
    {
//hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'name' => 'ALzbNApM0IrJPjuamKWo5tBjOssQof6kpe9M66hhlW8yC1gTX9Dh6ouDJjpbP6OMUXonbTO5YSP8a22Fma15T5mdiUGd0iyAv1zbmR4oosnq9sMIOxgDgB8qRECjlDqiqq3yh2g52fCjzjfHn9oWpDOSxSWFkVZn6uACcGPqq47bfJz2s1cSP3D46HbMSg91'
        ];

        $response = $this->call('POST', '/v1/other_settings/add/material_settings', $param, [], [], $headers);
//        check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);

        //match each index value
        $this->assertSame([
            $this->__getTranslateString('max_validation', ['attribute' => 'name', 'max' => 191], $lang)
        ], $json['name']);
    }

    /**
     * Case 6
     * @test This function test response with valid token but with name having String
     * @expectedExceptionCode  200
     * create a other settings
     * @bodyparam section string required the name of the table in which records are created
     * @bodyParam type string optional the type for fireclasses like 1=>insulation and 2=>firesealing
     * @bodyparam name string required the name of the value of the name field
     * @bodyparam document file optional the document to be uploaded
     * @bodyparam class_type int optional fire resistance type like 1=>insulation, 2=> firesealing
     * @bodyparam material_type_id int optional material type id
     */
    public function testOtherSettingsCreateSettingsNamesMaterialSettingsTestStoreWithValidTokenNameIsString()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'name' => 'sadadas',
        ];

        $response = $this->call('POST', '/v1/other_settings/add/material_settings', $param, [], [], $headers);

        //check response status code
        $this->assertResponseStatus(200);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame(
            $this->__getTranslateString('insulation_materials_created', [], $lang)
            , $json['success']);
    }

    /**
     * Case 7
     * @test This function test response with valid token but with name having integer
     * @expectedExceptionCode  422
     * create a other settings
     * @bodyparam section string required the name of the table in which records are created
     * @bodyParam type string optional the type for fireclasses like 1=>insulation and 2=>firesealing
     * @bodyparam name string required the name of the value of the name field
     * @bodyparam document file optional the document to be uploaded
     * @bodyparam class_type int optional fire resistance type like 1=>insulation, 2=> firesealing
     * @bodyparam material_type_id int optional material type id
     */
    public function testOtherSettingsCreateSettingsNamesMaterialSettingsTestStoreWithValidTokenNameIsInteger()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'name' => 1321321564,
        ];

        $response = $this->call('POST', '/v1/other_settings/add/material_settings', $param, [], [], $headers);

        //check response status code
        $this->assertResponseStatus(200);
    }
}
