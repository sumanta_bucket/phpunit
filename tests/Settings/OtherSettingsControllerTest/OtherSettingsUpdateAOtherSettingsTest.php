<?php


use App\Models\ConsultantTitle;

class OtherSettingsUpdateAOtherSettingsTest extends TestCase
{
    /*
          * Login param for admin , employee , customer to check role based authentication
          * */

    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'custom@yopmail.com',
        'password' => '123456'
    ];
    public $employee = [
        'email' => 'emp1@yopmail.com',
        'password' => '123456'
    ];

    /**
     *  Admin and Employee holds same permissions .
     *  Customer dont't have permissions to view this page.
     *  Contact-Persons dont't have permission to view this page.
     */


    /**
     * Case 1
     * @test  This function test response without auth token without params
     * @expectedExceptionCode 401
     * Update a other settings
     * @bodyParam section string required the name of the table
     * @bodyParam type string optional the type for fireclasses like 1=>insulation and 2=>firesealing
     * @bodyParam id integer required the id of the record being updated
     * @bodyparam name string required the name of the value of the name field
     * @bodyparam document file optional the document for to be uploaded
     * @bodyparam class_type integer optional fire resistance type like 1=>insulation, 2=> firesealing
     * @bodyparam material_type_id integer optional the material type id
     */
    public function testOtherSettingsUpdateAOtherSettingsTestUpdateWithoutToken()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('fire_classes')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        app(DatabaseSeeder::class)->call(FireClassesSeeder::class);
        $this->put('/v1/other_settings/update/1/resistance', [])
            ->seeJsonEquals([
                "error" => $this->__getTranslateString('missing_token_error', [], 2)
            ]);
    }

    /**
     * Case 2
     * @test This function test response with invalid auth token without params
     * @expectedExceptionCode 400
     * Update a other settings
     * @bodyParam section string required the name of the table
     * @bodyParam type string optional the type for fireclasses like 1=>insulation and 2=>firesealing
     * @bodyParam id integer required the id of the record being updated
     * @bodyparam name string required the name of the value of the name field
     * @bodyparam document file optional the document for to be uploaded
     * @bodyparam class_type integer optional fire resistance type like 1=>insulation, 2=> firesealing
     * @bodyparam material_type_id integer optional the material type id
     */
    public function testOtherSettingsUpdateAOtherSettingsTestUpdateWithInvalidTokenWithoutParam()
    {
        $expiredToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expiredToken
        ];

        $response = $this->call('PUT', '/v1/other_settings/update/1/resistance', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(400);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('token_expired_error', [], 2), $json['error']);
    }

    /**
     * Case 3
     * @test This function test response with valid token without params
     * @expectedExceptionCode 500
     * Update a other settings
     * @bodyParam section string required the name of the table
     * @bodyParam type string optional the type for fireclasses like 1=>insulation and 2=>firesealing
     * @bodyParam id integer required the id of the record being updated
     * @bodyparam name string required the name of the value of the name field
     * @bodyparam document file optional the document for to be uploaded
     * @bodyparam class_type integer optional fire resistance type like 1=>insulation, 2=> firesealing
     * @bodyparam material_type_id integer optional the material type id
     */
    public function testOtherSettingsUpdateAOtherSettingsTestUpdateWithValidTokenWithoutParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('PUT', '/v1/other_settings/update/1/resistance', [], [], [], $headers);
        // check response status code
        $this->assertResponseStatus(422);
    }

    /**
     * Case 4
     * @test This function test response with valid token and null params
     * @expectedExceptionCode 422
     * Update a other settings
     * @bodyParam section string required the name of the table
     * @bodyParam type string optional the type for fireclasses like 1=>insulation and 2=>firesealing
     * @bodyParam id integer required the id of the record being updated
     * @bodyparam name string required the name of the value of the name field
     * @bodyparam document file optional the document for to be uploaded
     * @bodyparam class_type integer optional fire resistance type like 1=>insulation, 2=> firesealing
     * @bodyparam material_type_id integer optional the material type id
     */
    public function testOtherSettingsUpdateAOtherSettingsTestUpdateWithValidTokenNullParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'name' => null,
        ];

        $response = $this->call('PUT', '/v1/other_settings/update/1/resistance', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'name'], $lang)],
            $json['name']);
    }
//

    /**
     * Case 5
     * @test This function test response with valid token but with name having length max to 255
     * @expectedExceptionCode 422
     * Update a other settings
     * @bodyParam section string required the name of the table
     * @bodyParam type string optional the type for fireclasses like 1=>insulation and 2=>firesealing
     * @bodyParam id integer required the id of the record being updated
     * @bodyparam name string required the name of the value of the name field
     * @bodyparam document file optional the document for to be uploaded
     * @bodyparam class_type integer optional fire resistance type like 1=>insulation, 2=> firesealing
     * @bodyparam material_type_id integer optional the material type id
     */
    public function testOtherSettingsUpdateAOtherSettingsTestUpdateWithValidTokenWithParamsWithNameNotMoreThan()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'name' => 'Jro2nXolefKIdOLWieVX6r6lkT5wq0VQ3b3L7QYG10Ec5yPIGgpb0lFLD4P4cy0StLd05oXrNxS5yG2pUTFsu7LlpyqoJXxyrPoLUzrAgYk548IUoOiNDyFZ4l7ELsIEcOAN7mVxnSkY3SsA0IEmhT5gsebZBPGWkBjoiDsocg2TeEnTTualcoI4qKG0mYkfpZSlr8WqNBnUFevIXzjcrAtEwkl1iSQoJ6Y9fIh44B76Mu6rFmLb8xyHO95GUHLd',
        ];

        $response = $this->call('PUT', '/v1/other_settings/update/1/resistance', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('max_validation', ['attribute' => 'name', 'max' => 191], $lang)
        ], $json['name']);
    }


    /**
     * Case 6
     * @test This function test response with invalid auth token with valid params
     * @expectedExceptionCode 200
     * Update a other settings
     * @bodyParam section string required the name of the table
     * @bodyParam type string optional the type for fireclasses like 1=>insulation and 2=>firesealing
     * @bodyParam id integer required the id of the record being updated
     * @bodyparam name string required the name of the value of the name field
     * @bodyparam document file optional the document for to be uploaded
     * @bodyparam class_type integer optional fire resistance type like 1=>insulation, 2=> firesealing
     * @bodyparam material_type_id integer optional the material type id
     */
    public function testOtherSettingsUpdateAOtherSettingsTestUpdateWithValidParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'name' => 'test user first name',

        ];

        $response = $this->call('PUT', '/v1/other_settings/update/1/resistance', $param, [], [], $headers);
        //check response status code

        $this->assertResponseStatus(200);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('fire_classes_updated', [], $lang), $json['success']);
    }

    /**
     * Case 7
     * @test This function test response with valid token but with existing user.id but its soft deleted
     * @expectedExceptionCode 422
     * Update a other settings
     * @bodyParam section string required the name of the table
     * @bodyParam type string optional the type for fireclasses like 1=>insulation and 2=>firesealing
     * @bodyParam id integer required the id of the record being updated
     * @bodyparam name string required the name of the value of the name field
     * @bodyparam document file optional the document for to be uploaded
     * @bodyparam class_type integer optional fire resistance type like 1=>insulation, 2=> firesealing
     * @bodyparam material_type_id integer optional the material type id
     */
    public function testOtherSettingsUpdateAOtherSettingsTestUpdateWithValidTokenWithParamsWithIdSoftDeleted()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $fireClass = \App\Models\FireClass::find(2);
        $fireClass->delete();
        $param = [
            'name' => 'test user first name',
        ];

        $response = $this->call('PUT', '/v1/other_settings/update/2/resistance', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([$this->__getTranslateString('exists_validation', ['attribute' => 'id'], $lang)],
            $json['id']);
    }

    /**
     * Case 8
     * @test Customer dont't have permissions to view this page.
     * @expectedExceptionCode 403
     * Update a other settings
     * @bodyParam section string required the name of the table
     * @bodyParam type string optional the type for fireclasses like 1=>insulation and 2=>firesealing
     * @bodyParam id integer required the id of the record being updated
     * @bodyparam name string required the name of the value of the name field
     * @bodyparam document file optional the document for to be uploaded
     * @bodyparam class_type integer optional fire resistance type like 1=>insulation, 2=> firesealing
     * @bodyparam material_type_id integer optional the material type id
     */
    public function testOtherSettingsUpdateAOtherSettingsTestUpdateWithValidTokenWithoutPramsForCustomer()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->customer);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);


        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('PUT', '/v1/other_settings/update/1/resistance', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(403);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame(
            $this->__getTranslateString('insufficient_permission', [], $lang)
            , $json['error']);
    }

    /**
     * Case 9
     * @test Customer dont't have permissions to view this page.
     * @expectedExceptionCode 403
     * Update a other settings
     * @bodyParam section string required the name of the table
     * @bodyParam type string optional the type for fireclasses like 1=>insulation and 2=>firesealing
     * @bodyParam id integer required the id of the record being updated
     * @bodyparam name string required the name of the value of the name field
     * @bodyparam document file optional the document for to be uploaded
     * @bodyparam class_type integer optional fire resistance type like 1=>insulation, 2=> firesealing
     * @bodyparam material_type_id integer optional the material type id
     */
    public function testOtherSettingsUpdateAOtherSettingsTestUpdateWithValidTokenWithoutPramsForEmployee()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->employee);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);


        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('PUT', '/v1/other_settings/update/1/resistance', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(403);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
//        match each index value
        $this->assertSame(
            $this->__getTranslateString('insufficient_permission', [], $lang)
            , $json['error']);
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('fire_classes')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
