<?php


class GetRequiredDataForProjectWithSpecificProjectIdTest extends TestCase
{
    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'custom@yopmail.com',
        'password' => '123456'
    ];
    public $employee=[
        'email'=>'emp1@yopmail.com',
        'password'=>'123456'
    ];

    /**
     *  Admin and Employee holds same permissions .
     *  Customer dont't have permissions to view this page.
     *  Contact-Persons dont't have permission to view this page.
     */



    /**
     * Case 1
     * @test This function test response without auth token without params
     * @expectedExceptionCode 401
     * Retrieve a project
     * @bodyParam id integer required the id of the project whose records are being fetched
     * @response json type array
     */
    public function testGetRequiredDataForProjectWithSpecificProjectIdWithoutToken()
    {
        $this->get('/v1/projects/resolver', [])
            ->seeJsonEquals([
                "error" => $this->__getTranslateString('missing_token_error', [], 2)
            ]);
    }


    /**
     * Case 2
     * @test This function test response with invalid auth token without params
     * @expectedExceptionCode 400
     * Retrieve a project
     * @bodyParam id integer required the id of the project whose records are being fetched
     * @response json type array
     */
    public function testGetRequiredDataForProjectWithSpecificProjectIdWithInvalidToken()
    {
        $expired_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expired_token
        ];

        $response = $this->call('GET', '/v1/projects/resolver', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(400);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('token_expired_error', [], 2), $json['error']);
    }


    /**
     * Case 3
     * @test This function test response with valid token without params
     * @expectedExceptionCode 422
     * Retrieve a project
     * @bodyParam id integer required the id of the project whose records are being fetched
     * @response json type array
     */
    public function testGetRequiredDataForProjectWithSpecificProjectIdWithValidTokenWithoutPrams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        $lang = $loginJson['language'];

        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('GET', '/v1/projects/resolver', [], [], [], $headers);
        $json = json_decode($response->getContent(), true);
    }
    /**
     * Case 3B
     * @test Customer dont't have permissions to view this page.
     * @expectedExceptionCode 200
     * Retrieve a project
     * @bodyParam id integer required the id of the project whose records are being fetched
     * @response json type array
     */
    public function testGetRequiredDataForProjectWithSpecificProjectIdWithValidTokenWithoutPramsForCustomer()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->customer);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);


        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('GET', '/v1/projects/resolver', [], [], [], $headers);

        //check response status code
        $this->assertResponseStatus(200);

        //convert response data in json
        $json = json_decode($response->getContent(), true);

    }

    /**
     * Case 3c
     * @test Employee dont't have permissions to view this page.
     * @expectedExceptionCode 200
     * Retrieve a project
     * @bodyParam id integer required the id of the project whose records are being fetched
     * @response json type array
     */
    public function testGetRequiredDataForProjectWithSpecificProjectIdWithValidTokenWithoutPramsForEmployee()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->employee);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);


        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('GET', '/v1/projects/resolver', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $json = json_decode($response->getContent(), true);

    }
}
