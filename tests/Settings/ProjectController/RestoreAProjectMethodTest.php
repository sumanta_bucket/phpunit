<?php


use App\Models\ContactPerson;
use App\Models\User;
use Illuminate\Support\Facades\Config;

class RestoreAProjectMethodTest extends TestCase
{
    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'custom@yopmail.com',
        'password' => '123456'
    ];
    public $employee = [
        'email' => 'emp1@yopmail.com',
        'password' => '123456'
    ];

    /**
     * Case 1
     * @test This function test response without any param and with out any token
     * @expectedExceptionCode 401
     * Restore a project
     * @bodyParam id integer required the id of the record to be restored
     * @response json type array
     */
    public function testRestoreAProjectMethodTestWithoutToken()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('customers')->truncate();
        DB::table('contact_persons')->truncate();
        DB::table('consultant_companies')->truncate();
        DB::table('consultant_titles')->truncate();
        DB::table('consultants')->truncate();
        DB::table('workplaces')->truncate();
        DB::table('projects')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        app(DatabaseSeeder::class)->call(ProjectSeeder::class);
        $response = $this->call('GET', '/v1/projects/restore/2', [], [], [], []);

        //check response status code
        $this->assertResponseStatus(401);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('missing_token_error', [], Config::get('constant.default_lang')),
            $json['error']);
    }

    /**
     * Case 2
     * @test This function test response without any param and with invalid token
     * @expectedExceptionCode 401
     * restore a project
     * @bodyParam id integer required the id of the record to be restored
     * @response json type array
     */
    public function testRestoreAProjectMethodTestWithInvalidTokenWithoutParam()
    {
        $expiredToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expiredToken
        ];

        $response = $this->call('GET', '/v1/projects/restore/2', [], [], [], []);
        //check response status code
        $this->assertResponseStatus(401);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('missing_token_error', [], Config::get('constant.default_lang')),
            $json['error']);
    }

    /**
     * Case 3
     * @test This function test response without any param and with invalid token
     * @expectedExceptionCode 401
     * Restore a project
     * @bodyParam id integer required the id of the record to be restored
     * @response json type array
     */
    public function testRestoreAProjectMethodTestCompanyWithValidTokenWithoutParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);

        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('GET', '/v1/projects/restore/2', [], [], [], []);
        //check response status code
        $this->assertResponseStatus(401);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('missing_token_error', [], Config::get('constant.default_lang')),
            $json['error']);
    }

    /**
     * Case 4
     * @test This function test response with valid token but with not existing id
     * @expectedExceptionCode 401
     * restore a project
     * @bodyParam id integer required the id of the record to be restored
     * @response json type array
     */
    public function testRestoreAProjectMethodTestWithValidTokenNonExistingId()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('GET', '/v1/projects/restore/2', [], [], [], []);
        //check response status code
        $this->assertResponseStatus(401);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('missing_token_error', [], Config::get('constant.default_lang')),
            $json['error']);
    }

    /**
     * Case 5
     * @test This function test response with valid token but with soft deleted id
     * @expectedExceptionCode 422
     * restore a project
     * @bodyParam id integer required the id of the record to be restored
     * @response json type array
     */
    public function testRestoreAProjectMethodTestWithValidTokenSoftDeletedID()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('GET', '/v1/projects/restore/2', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);

    }


    /**
     * Case 6
     * @test This function test response with valid token and valid id
     * @expectedExceptionCode 200
     * restore a project
     * @bodyParam id integer required the id of the record to be restored
     * @response json type array
     */
    public function testRestoreAProjectMethodTestWithValidTokenValidID()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $project = \App\Models\Project::find(2);
        $project->delete();
        User::find($project->user_id)->delete();
        $response = $this->call('GET', '/v1/projects/restore/2', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(200);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('project_restored', [], $lang), $json['success']);
    }

    /**
     * Case 7
     * @test Customer dont't have permissions to view this page.
     * @expectedExceptionCode 403
     * restore a project
     * @bodyParam id integer required the id of the record to be restored
     * @response json type array
     */
    public function testRestoreAProjectMethodTestWithValidTokenWithoutPramsForCustomer()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->customer);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);


        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('DELETE', '/v1/consultant_company/delete/2', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(403);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame(
            $this->__getTranslateString('insufficient_permission', [], $lang)
            , $json['error']);
    }

    /**
     * Case 8
     * @test Employee dont't have permissions to view this page.
     * @expectedExceptionCode 403
     * restore a project
     * @bodyParam id integer required the id of the record to be restored
     * @response json type array
     */
    public function testRestoreAProjectMethodTestWithValidTokenWithoutPramsForEmployee()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->employee);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);


        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('DELETE', '/v1/consultant_company/delete/2', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(403);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
//        match each index value
        $this->assertSame(
            $this->__getTranslateString('insufficient_permission', [], $lang)
            , $json['error']);
    }
}
