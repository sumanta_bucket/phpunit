<?php


class UpdateProjectMethodTest extends TestCase
{
    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'custom@yopmail.com',
        'password' => '123456'
    ];
    public $employee = [
        'email' => 'emp1@yopmail.com',
        'password' => '123456'
    ];

    /**
     *  Read.
     *
     *  Admin and Employee holds same permissions .
     *  Customer dont't have permissions to view this page.
     *  Contact-Persons dont't have permission to view this page.
     */

    /**
     * Case 1
     * @test This function test response without auth token without params
     * @expectedExceptionCode 401
     * Update a project
     * @bodyParam id integer required the id of the updated record
     * @bodyParam name string required the name of the project
     * @bodyParam customer_id integer required the customer related to project
     * @bodyParam workplace_id integer required the workplace related to project
     * @bodyParam consultant_id integer required the consultant related to project
     * @bodyParam old_penetrations integer optional old penetration yes or not
     * @bodyParam project_number integer optional project number of newly created project
     * @bodyParam _categories array optional to create project categories
     * @response json type array
     */
    public function testProjectControllerStoreMethodWithoutToken()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('customers')->truncate();
        DB::table('contact_persons')->truncate();
        DB::table('consultant_companies')->truncate();
        DB::table('consultant_titles')->truncate();
        DB::table('consultants')->truncate();
        DB::table('workplaces')->truncate();
        DB::table('projects')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        app(DatabaseSeeder::class)->call(ProjectSeeder::class);

        $this->put('/v1/projects/update/6', [])
            ->seeJsonEquals([
                "error" => $this->__getTranslateString('missing_token_error', [], 2)
            ]);
    }

    /**
     * Case 2
     * @test This function test response with invalid auth token without params
     * @expectedExceptionCode 400
     * Update a project
     * @bodyParam id integer required the id of the updated record
     * @bodyParam name string required the name of the project
     * @bodyParam customer_id integer required the customer related to project
     * @bodyParam workplace_id integer required the workplace related to project
     * @bodyParam consultant_id integer required the consultant related to project
     * @bodyParam old_penetrations integer optional old penetration yes or not
     * @bodyParam project_number integer optional project number of newly created project
     * @bodyParam _categories array optional to create project categories
     * @response json type array
     */
    public function testProjectControllerStoreMethodWithInvalidToken()
    {
        $expired_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expired_token
        ];

        $response = $this->call('PUT', '/v1/projects/update/6', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(400);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('token_expired_error', [], 2), $json['error']);
    }

    /**
     * Case 3
     * @test This function test response with valid token without params
     * @expectedExceptionCode 422
     * Update a project
     * @bodyParam id integer required the id of the updated record
     * @bodyParam name string required the name of the project
     * @bodyParam customer_id integer required the customer related to project
     * @bodyParam workplace_id integer required the workplace related to project
     * @bodyParam consultant_id integer required the consultant related to project
     * @bodyParam old_penetrations integer optional old penetration yes or not
     * @bodyParam project_number integer optional project number of newly created project
     * @bodyParam _categories array optional to create project categories
     * @response json type array
     */
    public function testProjectControllerStoreMethodWithValidTokenWithoutPrams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('PUT', '/v1/projects/update/6', [], [], [], $headers);

        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'name'], $lang)
        ], $json['name']);
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'customer_id'], $lang)],
            $json['customer_id']);
    }

    /**
     * Case 4
     * @test This function test response with valid token and null params
     * @expectedExceptionCode 422
     * Update a project
     * @bodyParam id integer required the id of the updated record
     * @bodyParam name string required the name of the project
     * @bodyParam customer_id integer required the customer related to project
     * @bodyParam workplace_id integer required the workplace related to project
     * @bodyParam consultant_id integer required the consultant related to project
     * @bodyParam old_penetrations integer optional old penetration yes or not
     * @bodyParam project_number integer optional project number of newly created project
     * @bodyParam _categories array optional to create project categories
     * @response json type array
     */
    public function testProjectControllerStoreMethodWithValidTokenNullParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            "name" => null,
            "customer_id" => null,
            "project_number" => null,
            "consultants" => [
                "0" => [
                    "id" => null
                ]
            ],
            "contact_persons" => [

                "0" => [
                    "id" => null

                ]
            ],

            "workplace" => [

                "id" => null
            ]

        ];

        $response = $this->call('PUT', '/v1/projects/update/6', $param, [], [], $headers);

        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'name'], $lang)
        ], $json['name']);
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'customer_id'], $lang)],
            $json['customer_id']);
    }

    /**
     * Case 5
     * @test This function test response with valid token but with user.first_name having length max to 191
     * @expectedExceptionCode 422
     * Update a project
     * @bodyParam id integer required the id of the updated record
     * @bodyParam name string required the name of the project
     * @bodyParam customer_id integer required the customer related to project
     * @bodyParam workplace_id integer required the workplace related to project
     * @bodyParam consultant_id integer required the consultant related to project
     * @bodyParam old_penetrations integer optional old penetration yes or not
     * @bodyParam project_number integer optional project number of newly created project
     * @bodyParam _categories array optional to create project categories
     * @response json type array
     */
    public function testProjectControllerStoreMethodWithFirstNameWithLengthMax()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            "name" => "adsfdfjhbjsbdfjaaaaaaaaaaaaaaaaaaaa ajbjjhdf jajbfdj absjdfb jasdbfj basjdfb jasbdfjajsdfsdfffffffffffffffffffffffffffffffsffffffffffffffffffffffffffffffffffffffffffdahjdafajhbdsfhjbhjbjhdsfdsfdfgfdgd",
            "customer_id" => 1,
            "project_number" => 422,
            "consultants" => [
                "0" => [
                    "id" => 1
                ]
            ],
            "contact_persons" => [

                "0" => [
                    "id" => 1

                ]
            ],

            "workplace" => [

                "id" => 1
            ]

        ];

        $response = $this->call('PUT', '/v1/projects/update/6', $param, [], [], $headers);

        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('max_validation', ['attribute' => 'name', 'max' => 191], $lang)
        ], $json['name']);
    }

    /**
     * Case 6
     * @test This function test response with valid token retrieveProjectMethodToRetrieveTestData
     * @expectedExceptionCode 200
     * Update a project
     * @bodyParam id integer required the id of the updated record
     * @bodyParam name string required the name of the project
     * @bodyParam customer_id integer required the customer related to project
     * @bodyParam workplace_id integer required the workplace related to project
     * @bodyParam consultant_id integer required the consultant related to project
     * @bodyParam old_penetrations integer optional old penetration yes or not
     * @bodyParam project_number integer optional project number of newly created project
     * @bodyParam _categories array optional to create project categories
     * @response json type array
     */
    public function retrieveProjectMethodToRetrieveTestData()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            "name" => "adsfdfj",
            "customer_id" => 1,
            "project_number" => 422,


        ];
        $response = $this->call('GET', '/v1/projects/edit/1', $param, [], [], $headers);

        //check response status code
        $this->assertResponseStatus(200);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
    }
    /**
     * Case
     * This function test response with valid token but with user.first_name having length max to 191
     * @expectedExceptionCode 200
     * Update a project
     * @bodyParam id integer required the id of the updated record
     * @bodyParam name string required the name of the project
     * @bodyParam customer_id integer required the customer related to project
     * @bodyParam workplace_id integer required the workplace related to project
     * @bodyParam consultant_id integer required the consultant related to project
     * @bodyParam old_penetrations integer optional old penetration yes or not
     * @bodyParam project_number integer optional project number of newly created project
     * @bodyParam _categories array optional to create project categories
     * @response json type array
     */
    public function testProjectControllerStoreMethodWithValidTokenAndValidParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            "name" => "adsfdfj",
            "customer_id" => 1,
            "project_number" => 422,
            "consultants" => [
                "0" => [
                    "id" => 1
                ]
            ],
            "contact_persons" => [

                "0" => [
                    "id" => 1

                ]
            ],

            "workplace" => [

                "id" => 1
            ]

        ];

        $response = $this->call('PUT', '/v1/projects/update/6', $param, [], [], $headers);

        //check response status code
        $this->assertResponseStatus(200);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('customers')->truncate();
        DB::table('contact_persons')->truncate();
        DB::table('consultant_companies')->truncate();
        DB::table('consultant_titles')->truncate();
        DB::table('consultants')->truncate();
        DB::table('workplaces')->truncate();
        DB::table('projects')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
