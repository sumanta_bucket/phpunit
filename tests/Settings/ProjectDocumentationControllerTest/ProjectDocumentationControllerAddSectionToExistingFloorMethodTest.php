<?php


class ProjectDocumentationControllerAddSectionToExistingFloorMethodTest extends TestCase
{
    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'custom@yopmail.com',
        'password' => '123456'
    ];
    public $employee = [
        'email' => 'emp1@yopmail.com',
        'password' => '123456'
    ];

    /**
     *  Read.
     *
     *  Admin and Employee holds same permissions .
     *  Customer dont't have permissions to view this page.
     *  Contact-Persons dont't have permission to view this page.
     */

    /**
     * Case 1
     * @test This function test response without auth token without params
     * @expectedExceptionCode 401
     * Add section to existing floor
     * @bodyParam request Request optional the default parameter of the route
     * @bodyParam project_id integer required the id of project in which we want to add project
     * @bodyParam floor_id integer required the floor id of the project in which sections are added
     * @respone json array type
     */

    public function testProjectDocumentationControllerAddSectionToExistingFloorMethodTestWithoutToken()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('customers')->truncate();
        DB::table('workplaces')->truncate();
        DB::table('users')->truncate();
        DB::table('floors')->truncate();
        DB::table('projects')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        app(DatabaseSeeder::class)->call(UserTestSeeder::class);
        app(DatabaseSeeder::class)->call(ProjectDocumentationWithExistingFloorSeeder::class);

        $this->post('/v1/project/1/floor/1/section/add', [])
            ->seeJsonEquals([
                "error" => $this->__getTranslateString('missing_token_error', [], 2)
            ]);
    }

    /**
     * Case 2
     * @test This function test response with invalid auth token without params
     * @expectedExceptionCode 404
     * Add section to existing floor
     * @bodyParam request Request optional the default parameter of the route
     * @bodyParam project_id integer required the id of project in which we want to add project
     * @bodyParam floor_id integer required the floor id of the project in which sections are added
     * @respone json array type
     */
    public function testProjectDocumentationControllerAddSectionToExistingFloorMethodTestWithInvalidToken()
    {
        $expired_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expired_token
        ];

        $response = $this->call('POST', '/v1/project/{id}', [], [], [], $headers);

        //check response status code
        $this->assertResponseStatus(404);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('route_not_found', [], 2), $json['error']);
    }

    /**
     * Case 3
     * @test This function test response with valid token without params
     * @expectedExceptionCode 404
     * Add section to existing floor
     * @bodyParam request Request optional the default parameter of the route
     * @bodyParam project_id integer required the id of project in which we want to add project
     * @bodyParam floor_id integer required the floor id of the project in which sections are added
     * @respone json array type
     */
    public function testProjectDocumentationControllerAddSectionToExistingFloorMethodTestWithValidTokenWithoutPrams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('POST', '/v1/project/', [], [], [], $headers);

        //check response status code
        $this->assertResponseStatus(404);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame(
            $this->__getTranslateString('route_not_found', [], 2)
            , $json['error']);

    }

    /**
     * Case 4
     * @test This function test response with valid token and null params
     * @expectedExceptionCode 404
     * Add section to existing floor
     * @bodyParam request Request optional the default parameter of the route
     * @bodyParam project_id integer required the id of project in which we want to add project
     * @bodyParam floor_id integer required the floor id of the project in which sections are added
     * @respone json array type
     */
    public function testProjectDocumentationControllerAddSectionToExistingFloorMethodTestWithValidTokenNullParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];


        $response = $this->call('POST', '/v1/project/{id}', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(404);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame(
            $this->__getTranslateString('route_not_found', [], 2)
            , $json['error']);
    }

    /**
     * Case 5
     * @test This function test response with valid token but with with floor that does not exist
     * @expectedExceptionCode 422
     * @expectedExceptionCode 401
     * Add section to existing floor
     * @bodyParam request Request optional the default parameter of the route
     * @bodyParam project_id integer required the id of project in which we want to add project
     * @bodyParam floor_id integer required the floor id of the project in which sections are added
     * @respone json array type
     */
    public function testProjectDocumentationControllerAddSectionToExistingFloorMethodTestWithInvalidFloorId()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('POST', '/v1/project/1/floor/40/section/add', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('exists_validation', ['attribute' => 'floor_id'], $lang)
        ], $json['floor_id']);
    }

    /**
     * Case 5
     * @test This function test response with valid token but with with floor that does not exist
     * @expectedExceptionCode   422
     * @expectedExceptionCode 401
     * Add section to existing floor
     * @bodyParam request Request optional the default parameter of the route
     * @bodyParam project_id integer required the id of project in which we want to add project
     * @bodyParam floor_id integer required the floor id of the project in which sections are added
     * @respone json array type
     */
    public function testProjectDocumentationControllerAddSectionToExistingFloorMethodTestWithInvalidProjectId()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('POST', '/v1/project/40/floor/1/section/add', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('exists_validation', ['attribute' => 'project_id'], $lang)
        ], $json['project_id']);
    }


    /**
     *  Case 6
     * @test This function test response with valid token but with Valid Token
     * @expectedExceptionCode 200
     * Add section to existing floor
     * @bodyParam request Request optional the default parameter of the route
     * @bodyParam project_id integer required the id of project in which we want to add project
     * @bodyParam floor_id integer required the floor id of the project in which sections are added
     * @respone json array type
     */
    public function testProjectDocumentationControllerAddSectionToExistingFloorMethodTestWithValidTokenAndValidParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('POST', '/v1/project/2/floor/2/section/add', [], [], [], $headers);

        //check response status code
        $this->assertResponseStatus(200);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('customers')->truncate();
        DB::table('workplaces')->truncate();
        DB::table('projects')->truncate();
        DB::table('floors')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
