<?php


class ProjectDocumentationControllerDeleteAllFloorsOfProjectTest extends TestCase
{
    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'custom@yopmail.com',
        'password' => '123456'
    ];
    public $employee = [
        'email' => 'emp1@yopmail.com',
        'password' => '123456'
    ];

    /**
     *  Read.
     *
     *  Admin and Employee holds same permissions .
     *  Customer dont't have permissions to view this page.
     *  Contact-Persons dont't have permission to view this page.
     */

    /**
     * Case 1
     * @test This function test response without auth token without params
     *
     */
    public function testProjectDocumentationControllerDeleteAllFloorsOfProjectWithoutToken()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('customers')->truncate();
        DB::table('workplaces')->truncate();
        DB::table('floors')->truncate();
        DB::table('sections')->truncate();
        DB::table('projects')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        app(DatabaseSeeder::class)->call(UserTestSeeder::class);
        app(DatabaseSeeder::class)->call(ProjectDocumentationWithExistingFloorSeeder::class);

        $this->delete('/v1/project/13/delete_all_floor', [])
            ->seeJsonEquals([
                "error" => $this->__getTranslateString('missing_token_error', [], 2)
            ]);
    }

    /**
     * Case 2
     * This function test response with invalid auth token without params
     *
     */
    public function testProjectDocumentationControllerDeleteAllFloorsOfProjectWithInvalidToken()
    {
        $expired_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expired_token
        ];

        $response = $this->call('DELETE', '/v1/project/{id}', [], [], [], $headers);

        //check response status code
        $this->assertResponseStatus(404);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('route_not_found', [], 2), $json['error']);
    }

    /**
     * Case 3
     * This function test response with valid token without params
     *
     */
    public function testProjectDocumentationControllerDeleteAllFloorsOfProjectWithValidTokenWithoutPrams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('DELETE', '/v1/project/', [], [], [], $headers);

        //check response status code
        $this->assertResponseStatus(404);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame(
            $this->__getTranslateString('route_not_found', [], 2)
            , $json['error']);

    }

    /**
     * Case 4
     * This function test response with valid token and null params
     *
     */
    public function testProjectDocumentationControllerDeleteAllFloorsOfProjectWithValidTokenNullParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];


        $response = $this->call('DELETE', '/v1/project/{id}', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(404);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame(
            $this->__getTranslateString('route_not_found', [], 2)
            , $json['error']);
    }

    /**
     * Case 5
     * This function test response with valid token but with with project that does not exist
     *
     */
    public function testProjectDocumentationControllerDeleteAllFloorsOfProjectWithInvalidProjectId()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('DELETE', '/v1/project/13/delete_all_floor', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
//
//        //convert response data in json
        $json = json_decode($response->getContent(), true);
//        //match each index value
        $this->assertSame([
            $this->__getTranslateString('exists_validation', ['attribute' => 'project_id'], $lang)
        ], $json['project_id']);
    }



    /**
     * Case
     * This function test response with valid token but with Valid Token
     *
     */
    public function testProjectDocumentationControllerDeleteAllFloorsOfProjectWithValidTokenAndValidParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('DELETE', '/v1/project/1/delete_all_floor', [], [], [], $headers);

        //check response status code
        $this->assertResponseStatus(200);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('customers')->truncate();
        DB::table('workplaces')->truncate();
        DB::table('projects')->truncate();
        DB::table('floors')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
