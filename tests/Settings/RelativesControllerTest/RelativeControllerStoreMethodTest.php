<?php


class RelativeControllerStoreMethodTest extends TestCase
{
    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'custom@yopmail.com',
        'password' => '123456'
    ];
    public $employee = [
        'email' => 'emp1@yopmail.com',
        'password' => '123456'
    ];

    /**
     *  Read.
     *
     *  Admin and Employee holds same permissions .
     *  Customer dont't have permissions to view this page.
     *  Contact-Persons dont't have permission to view this page.
     */

    /**
     * Case 1
     * @test This function test response without auth token without params
     * @expectedExceptionCode 401
     */
    public function testRelativeControllerStoreWithoutAuthTokenAndWithoutParams()
    {
        $this->post('/v1/relatives/add', [])
            ->seeJsonEquals([
                "error" => $this->__getTranslateString('missing_token_error', [], 2)
            ]);
    }

    /**
     * Case 2
     * @test This function test response with invalid auth token without params
     * @expectedExceptionCode 400
     */
    public function testRelativeControllerEditWithInvalidAuthTokenAndWithoutParams()
    {
        $expired_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expired_token
        ];

        $response = $this->call('POST', '/v1/relatives/add', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(400);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('token_expired_error', [], 2), $json['error']);
    }

    /**
     * Case 3
     * @test This function test response with valid token without params
     * @expectedExceptionCode 422
     */
    public function testRelativeControllerEditWithValidTokenWithoutParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('POST', '/v1/relatives/add', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'relative.first_name'], $lang)
        ], $json['relative.first_name']);
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'relative.mobile'], $lang)
        ], $json['relative.mobile']);
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'employee_id'], $lang)
        ], $json['employee_id']);
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'relationship_id'], $lang)
        ], $json['relationship_id']);
    }

    /**
     * Case 4
     * @test  function test response with valid token and null params
     * @expectedExceptionCode 422
     */
    public function testRelativeControllerEditWithValidTokenAndNullParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'relative' => [
                'first_name' => null,
                'mobile' => null,
            ],
            'employee_id' => null,
            'relationship_id' => null
        ];

        $response = $this->call('POST', '/v1/relatives/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'relative.first_name'], $lang)
        ], $json['relative.first_name']);
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'relative.mobile'], $lang)
        ], $json['relative.mobile']);
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'employee_id'], $lang)
        ], $json['employee_id']);
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'relationship_id'], $lang)
        ], $json['relationship_id']);
    }

    /**
     * Case 5
     * @test This function test response with valid token but with user.first_name having length max to 191
     * @expectedExceptionCode 422
     */
    public function testRelativeControllerEditWithValidTokenWithUserFirstNameHavingLengthMax()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'relative' => [
                'first_name' => 'adsfdfjhbjsbdfjaaaaaaaaaaaaaaaaaaaa ajbjjhdf jajbfdj absjdfb jasdbfj basjdfb jasbdfjajsdfsdfffffffffffffffffffffffffffffffsffffffffffffffffffffffffffffffffffffffffffdahjdafajhbdsfhjbhjbjhdsfdsfdfgfdgd',
                'mobile' => '987456231',
            ],
            'employee_id' => 1,
            'relationship_id' => 1
        ];

        $response = $this->call('POST', '/v1/relatives/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('max_validation', ['attribute' => 'relative.first_name', 'max' => 191], $lang)
        ], $json['relative.first_name']);
    }
    /**
     * Case 9
     * @test This function test response with valid token but with user.mobile having length max to 20
     * @expectedExceptionCode 422
     */
    public function testRelativeControllerEditWithValidTokenWithUserMobileHavingLengthMax()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'relative' => [
                'first_name' => 'dummy user name',
                'mobile' => '987456231987456231145',
            ],
            'employee_id' => 1,
            'relationship_id' => 1
        ];

        $response = $this->call('POST', '/v1/relatives/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('max_validation', ['attribute' => 'relative.mobile', 'max' => 20], $lang)
        ], $json['relative.mobile']);
    }


    /**
     * Case 12
     * @test This function test response with valid token but with relationship_id string instead of number
     * @expectedExceptionCode 422
     */
    public function testCaseThirteen()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'relative' => [
                'first_name' => 'test user first name',
                'mobile' => '9874563210',
            ],
            'consultant_company' => 2,
            'relationship_id' => 'sadas'
        ];

        $response = $this->call('POST', '/v1/relatives/add', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('integer_validation', ['attribute' => 'relationship_id'], $lang)
        ], $json['relationship_id']);
    }
    /**
     * Case 12
     * @test This function test response with valid token but with relationship_id string instead of number
     * @expectedExceptionCode 422
     */
    public function testCaseFifthteen()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST','/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(),true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            'relative' => [
                'first_name' => 'test user first name',
                'mobile' => '9874563210',
            ],
            'employee_id' => 1,
            'relationship_id' => 1
        ];

        $response = $this->call('POST','/v1/relatives/add', $param,[],[],$headers);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $json = json_decode($response->getContent(),true);
        //match each index value
        $this->assertSame($this->__getTranslateString('relative_created',[],$lang), $json['success']);
    }
}
