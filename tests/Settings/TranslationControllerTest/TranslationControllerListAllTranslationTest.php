<?php


class TranslationControllerListAllTranslationTest extends TestCase
{
    /*
* Login param for admin , employee , customer to check role based authentication
* */

    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'custom@yopmail.com',
        'password' => '123456'
    ];
    public $employee = [
        'email' => 'emp1@yopmail.com',
        'password' => '123456'
    ];

    /**
     *  Admin and Employee holds same permissions .
     *  Customer dont't have permissions to view this page.
     *  Contact-Persons dont't have permission to view this page.
     */


    /**
     * Case 1
     * @test  This function test response without auth token without params
     * @expectedExceptionCode 401
     */
    public function testTranslationControllerListAllTranslationWithoutToken()
    {
        $this->post('/v1/translations', [])
            ->seeJsonEquals([
                "error" => $this->__getTranslateString('missing_token_error', [], 2)
            ]);
    }


    /**
     * Case 2
     * @test This function test response with invalid auth token without params
     * @expectedExceptionCode 400
     */
    public function testTranslationControllerListAllTranslationWithInvalidToken()
    {
        $expired_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expired_token
        ];

        $response = $this->call('POST', '/v1/translations', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(400);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('token_expired_error', [], 2), $json['error']);
    }


    /**
     * Case 3
     * @test This function test response with valid token without params
     * @expectedExceptionCode 422
     */
    public function testTranslationControllerListAllTranslationWithValidTokenWithoutPrams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        $lang = $loginJson['language'];

        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('POST', '/v1/translations', [], [], [], $headers);
        $json = json_decode($response->getContent(), true);
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'order.0.dir'], $lang)],
            $json['order.0.dir']);
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'order.0.column'], $lang)
        ], $json['order.0.column']);
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'start'], $lang)],
            $json['start']);
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'length'], $lang)],
            $json['length']);
    }

    /**
     * Case 3B
     * @test Customer dont't have permissions to view this page.
     * @expectedExceptionCode 403
     */
    public function testTranslationControllerListAllTranslationWithValidTokenWithoutPramsForCustomer()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->customer);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);


        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('POST', '/v1/translations', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(403);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame(
            $this->__getTranslateString('insufficient_permission', [], $lang)
            , $json['error']);
    }

    /**
     * Case 3c
     * @test  Employee dont't have permissions to view this page.
     * @expectedExceptionCode 403
     */
    public function testTranslationControllerListAllTranslationWithValidTokenWithoutPramsForEmployee()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->employee);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);


        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('POST', '/v1/translations', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(403);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
//        match each index value
        $this->assertSame(
            $this->__getTranslateString('insufficient_permission', [], $lang)
            , $json['error']);
    }

    /**
     * Case 4
     * @test  This function test response with valid token and null params
     * @expectedExceptionCode 422
     */
    public function testTranslationControllerListAllTranslationWithValidTokenNullParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];


        $param = [
            'order' => [
                [
                    'dir' => null,
                    'column' => null
                ]
            ],
            'search' => ['value' => null],
            'start' => null,
            'length' => null
        ];

        $response = $this->call('POST', '/v1/translations', $param, [], [], $headers);
        //check response status code
        $json = json_decode($response->getContent(), true);
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'order.0.dir'], $lang)],
            $json['order.0.dir']);
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'order.0.column'], $lang)
        ], $json['order.0.column']);
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'start'], $lang)],
            $json['start']);
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'length'], $lang)],
            $json['length']);
    }


    /**
     * Case 5
     * @test This function test response with valid token and all params
     * @expectedExceptionCode 200
     */
    public function testTranslationControllerListAllTranslationWithValidTokenAllParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];

        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];


        $param = [
            'order' => [
                [
                    'dir' => 'asc',
                    'column' => 0
                ]
            ],
            'search' => ['value' => 'test'],
            'start' => 0,
            'length' => 2
        ];

        $response = $this->call('POST', '/v1/translations', $param, [], [], $headers);
        //check response status code
        $json = json_decode($response->getContent(), true);

        $this->assertResponseStatus(200);
    }

    /**
     * Case 6
     * @test  This function test response with valid token and column null
     * @expectedExceptionCode 422
     */
    public function testTranslationControllerListAllTranslationWithValidTokenAllColumnNull()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];


        $param = [
            'order' => [
                [
                    'dir' => 'asc',
                    'column' => null
                ]
            ],
            'search' => ['value' => 'abc'],
            'start' => 0,
            'length' => 1
        ];

        $response = $this->call('POST', '/v1/translations', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        $json = json_decode($response->getContent(), true);

        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'order.0.column'], $lang)
        ], $json['order.0.column']);
    }
}
