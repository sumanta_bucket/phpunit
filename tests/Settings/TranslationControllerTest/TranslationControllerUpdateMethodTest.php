<?php


class TranslationControllerUpdateMethodTest extends TestCase
{

    /*
    * Login param for admin , employee , customer to check role based authentication
    * */

    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'custom@yopmail.com',
        'password' => '123456'
    ];
    public $employee = [
        'email' => 'emp1@yopmail.com',
        'password' => '123456'
    ];

    /**
     * Case 1
     * @test This function test response without auth token without params
     * @expectedExceptionCode 401
     */
    public function testTranslationControllerUpdateWithoutAuthTokenAndWithoutParams()
    {
        $this->PUT('/v1/emailTemplates/update/{id}', [])
            ->seeJsonEquals([
                "error" => $this->__getTranslateString('missing_token_error', [], 2)
            ]);
    }

    /**
     * Case 2
     * @test  This function test response with invalid auth token without params
     * @expectedExceptionCode 400
     */
    public function testTranslationControllerUpdateWithInvalidTokenAndWithoutParams()
    {
        $expired_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expired_token
        ];

        $response = $this->call('PUT', '/v1/emailTemplates/update/{id}', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(400);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('token_expired_error', [], 2), $json['error']);
    }

    /**
     * Case 3
     * @test  This function test response with valid token without params
     * @expectedExceptionCode 422
     */
    public function testTranslationControllerUpdateWithValidTokenAndWithoutParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);

        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('PUT', '/v1/translations/update/6593', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'short_name'], $lang)
        ],
            $json['short_name']);
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'english'], $lang)],
            $json['english']);
        $this->assertSame([$this->__getTranslateString('required_validation', ['attribute' => 'svenska'], $lang)],
            $json['svenska']);
    }

    /**
     * Case 4
     * @test  This function test response with valid token and null params
     * @expectedExceptionCode 422
     */
    public function testTranslationControllerUpdateWithValidTokenAndNullParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            "short_name" => null,
            "english" => null,
            "svenska" => null
        ];

        $response = $this->call('PUT', '/v1/translations/update/6593', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
    }

    /**
     * Case 5
     * @test his function test response with valid token but with name having length max to 191
     * @expectedExceptionCode 422
     */
    public function testTranslationControllerUpdateWithValidTokenNameHavingMaxLength()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            "short_name" => "adsfdfjhbjsbdfjaaaaaaaaaaaaaaaaaaaaajbjjhdfjajbfdjabsjdfbjasdbfjbasjdfbjasbdfjajsdfsdfffffffffffffffffffffffffffffffsffffffffffffffffffffffffffffffffffffffffffdahjdafajhbdsfhjbhjbjhdsfdsfdfgfdgd",
            "english" => "asdad",
            "svenska" => "Hi, your account is created details :Email :{{EMAIL}},Password {{PASSWORD}}"
        ];

        $response = $this->call('PUT', '/v1/translations/update/65931', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('max_validation', ['attribute' => 'short_name', 'max' => 100], $lang)
        ], $json['short_name']);
    }

    /**
     * Case 6
     * @test This function test response with valid token but with user.email having normal string instead of valid email
     * @expectedExceptionCode 422
     */
    public function testTranslationControllerUpdateWithSubjectHavingLengthMax()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            "short_name" => "dsddfsfds",
            "english" => "adsfdfjhbjsbdfjaaaaaaaaaaaaaaaaaaaaajbjjhdfjajbfdjabsjdfbjasdbfjbasjdfbjasbdfjajsdfsdfffffffffffffffffffffffffffffffsffffffffffffffffffffffffffffffffffffffffffdahjdafajhbdsfhjbhjbjhdsfdsfdfgfdgd",
            "svenska" => "Hi, your account is created details :Email :{{EMAIL}},Password {{PASSWORD}}"
        ];

        $response = $this->call('PUT', '/v1/translations/update/6593', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json

    }

    /**
     * Case 11
     * @test  This function test response with valid token and valid id
     * @expectedExceptionCode 200
     */
    public function testTranslationControllerUpdateValidTokenAndValidTokenAndValidData()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            "short_name" => "dsddfsfds",
            "english" => "sdsdsdd",
            "svenska" => "Hi, your account is created details :Email :{{EMAIL}},Password {{PASSWORD}}"
        ];

        $response = $this->call('PUT', '/v1/translations/update/6593', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(200);
    }
}
