<?php


use Illuminate\Http\UploadedFile;

class UserControllerUpdateUserProfilePictureTest extends TestCase
{
    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'custom@yopmail.com',
        'password' => '123456'
    ];
    public $employee = [
        'email' => 'emp1@yopmail.com',
        'password' => '123456'
    ];

    /**
     *  Read.
     *
     *  Admin and Employee holds same permissions .
     *  Customer dont't have permissions to view this page.
     *  Contact-Persons dont't have permission to view this page.
     */

    /**
     * Case 1
     * @test This function test response without auth token without params
     * @expectedExceptionCode 401
     * create a floor with section
     * @bodyParam project_id integer required the id of the project
     * @bodyParam no_of_floors integer required the no of sections in each floor
     * @response json array type
     */

    public function testUserControllerUpdateUserProfilePictureTestWithoutToken()
    {

        app(DatabaseSeeder::class)->call(UserTestSeeder::class);

        $this->post('/v1/user/upload_photo', [])
            ->seeJsonEquals([
                "error" => $this->__getTranslateString('missing_token_error', [], 2)
            ]);
    }

    /**
     * Case 2
     * @test This function test response with invalid auth token without params
     * @expectedExceptionCode 400
     * create a floor with section
     * @bodyParam project_id integer required the id of the project
     * @bodyParam no_of_floors integer required the no of sections in each floor
     * @response json array type
     */
    public function testUserControllerUpdateUserProfilePictureTestWithInvalidToken()
    {
        $expired_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expired_token
        ];

        $response = $this->call('POST', '/v1/user/upload_photo', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(400);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('token_expired_error', [], 2), $json['error']);
    }

    /**
     * Case 3
     * @test This function test response with valid token without params
     * @expectedExceptionCode 422
     * create a floor with section
     * @bodyParam project_id integer required the id of the project
     * @bodyParam no_of_floors integer required the no of sections in each floor
     * @response json array type
     */
    public function testUserControllerUpdateUserProfilePictureTestWithValidTokenWithoutPrams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('POST', '/v1/user/upload_photo', [], [], [], $headers);

        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'image'], $lang)
        ], $json['image']);

    }

    /**
     * Case 4
     * @test This function test response with valid token and null params
     * @expectedExceptionCode 422
     * create a floor with section
     * @bodyParam project_id integer required the id of the project
     * @bodyParam no_of_floors integer required the no of sections in each floor
     * @response json array type
     */
    public function testUserControllerUpdateUserProfilePictureTestWithValidTokenNullParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            "image" => null,
        ];

        $response = $this->call('POST', '/v1/user/upload_photo', $param, [], [], $headers);

        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'image'], $lang)
        ], $json['image']);

    }

    /**
     * Case 5
     * @test This function test response with valid token but with with Image as png
     * @expectedExceptionCode 422
     * create a floor with section
     * @bodyParam project_id integer required the id of the project
     * @bodyParam no_of_floors integer required the no of sections in each floor
     * @response json array type
     */
    public function testUserControllerUpdateUserProfilePictureTestWithValidDataFormatAsPng()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            "image" => UploadedFile::fake()->image('avatar.png'),
        ];

        $response = $this->call('POST', '/v1/user/upload_photo', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(200);

    }

    /**
     * Case
     * @test This function test response with valid token but with Valid Token
     * @expectedExceptionCode 200
     * create a floor with section
     * @bodyParam project_id integer required the id of the project
     * @bodyParam no_of_floors integer required the no of sections in each floor
     * @response json array type
     */
    public function testUserControllerUpdateUserProfilePictureTestWithValidTokenAndValidParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            "image" => UploadedFile::fake()->image('avatar.jpg'),
        ];

        $response = $this->call('POST', '/v1/user/upload_photo', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(200);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value

    }
}
