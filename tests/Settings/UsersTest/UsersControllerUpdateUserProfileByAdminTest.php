<?php


use App\Models\User;

class UsersControllerUpdateUserProfileByAdminTest extends TestCase
{
    /*
    * Login param for admin , employee , customer to check role based authentication
    * */

    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'custom@yopmail.com',
        'password' => '123456'
    ];
    public $employee = [
        'email' => 'emp1@yopmail.com',
        'password' => '123456'
    ];

    /**
     * Case 1
     * @test This function test response without auth token without params
     * @expectedExceptionCode 401
     */
    public function testUsersControllerUpdateUserProfileByAdminTestWithoutAuthTokenAndWithoutParams()
    {
        $this->post('/v1/user/edit_details', [])
            ->seeJsonEquals([
                "error" => $this->__getTranslateString('missing_token_error', [], 2)
            ]);
    }

    /**
     * Case 2
     * @test  This function test response with invalid auth token without params
     * @expectedExceptionCode 400
     */
    public function testUsersControllerUpdateUserProfileByAdminTestWithInvalidTokenAndWithoutParams()
    {
        $expired_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expired_token
        ];

        $response = $this->call('POST', '/v1/user/edit_details', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(400);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('token_expired_error', [], 2), $json['error']);
    }

    /**
     * Case 3
     * @test  This function test response with valid token without params
     * @expectedExceptionCode 422
     */
    public function testUsersControllerUpdateUserProfileByAdminTestWithValidTokenAndWithoutParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);

        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('POST', '/v1/user/edit_details', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'id'], $lang)
        ],
            $json['id']);

    }

    /**
     * Case 4
     * @test  This function test response with valid token and null params
     * @expectedExceptionCode 422
     */
    public function testUsersControllerUpdateUserProfileByAdminTestWithValidTokenAndNullParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            "first_name" => null,
            "status" => null,
            "email" => null,
            "id" => null
        ];

        $response = $this->call('POST', '/v1/user/edit_details', $param, [], [], $headers);
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'id'], $lang)
        ],
            $json['id']);

    }

    /**
     * Case 5
     * @test his function test response with valid token but with name having length max to 191
     * @expectedExceptionCode 422
     */
    public function testUsersControllerUpdateUserProfileByAdminTestWithValidTokenNameHavingMaxLength()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            "first_name" => "adsfdfjhbjsbdfjaaaaaaaaaaaaaaaaaaaaajbjjhdfjajbfdjabsjdfbjasdbfjbasjdfbjasbdfjajsdfsdfffffffffffffffffffffffffffffffsffffffffffffffffffffffffffffffffffffffffffdahjdafajhbdsfhjbhjbjhdsfdsfdfgfdgd",
            "status" => 1,
            "email" => str_random(12) . '@mail.com',
            "id" => 1
        ];

        $response = $this->call('POST', '/v1/user/edit_details', $param, [], [], $headers);

        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('max_validation', ['attribute' => 'first_name', 'max' => 191], $lang)
        ], $json['first_name']);
    }
//

    /**
     * Case 6
     * @test This function test response with valid token but with status having normal string instead of status
     * @expectedExceptionCode 422
     */
    public function testUsersControllerUpdateUserProfileByAdminTestWithStatusAsStringInsteadOfInteger()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            "first_name" => "ads",
            "status" => "asdadasdasdas",
            "email" => str_random(12) . '@mail.com',
            "id" => 1
        ];

        $response = $this->call('POST', '/v1/user/edit_details', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('integer_validation', ['attribute' => 'status'], $lang)
        ], $json['status']);

    }

    /**
     * Case
     * @test  This function test response with valid token and valid id but with invalid email
     * @expectedExceptionCode 422
     */
    public function testUsersControllerUpdateUserProfileByAdminTestValidTokenAndValidTokenAndInvalidEmailData()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            "first_name" => "ads",
            "status" => 1,
            "email" => 'asdasdasd',
            "id" => 1
        ];

        $response = $this->call('POST', '/v1/user/edit_details', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('email_validation', ['attribute' => 'email'], $lang)
        ], $json['email']);
    }

    /**
     * Case
     * @test  This function test response with valid token and valid id and email with max length 191
     * @expectedExceptionCode 422
     */
    public function testUsersControllerUpdateUserProfileByAdminTestValidTokenEmailWithMaxLength()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            "first_name" => "ads",
            "status" => 1,
            "email" => 'adsfdfjhbjsbdfjaaaaaaaaaaaaaaaaaaaaajbjjhdfjajbfdjabsjdfbjasdbfjbasjdfbjasbdfjajsdfsdfffffffffffffffffffffffffffffffsffffffffffffffffffffffffffffffffffffffffffdahjdafajhbdsfhjbhjbjhdsfdsfdfgfdgd@gmail.com',
            "id" => 1
        ];

        $response = $this->call('POST', '/v1/user/edit_details', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('email_validation', ['attribute' => 'email'], $lang),
            $this->__getTranslateString('max_validation', ['attribute' => 'email', 'max' => 191], $lang)
        ], $json['email']);
    }

    /**
     * Case
     * @test  This function test response with valid token and valid id
     * @expectedExceptionCode 200
     */
    public function testUsersControllerUpdateUserProfileByAdminTestValidTokenValidParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            "first_name" => "ads",
            "status" => 1,
            "email" => 'adsfdfj@gmail.com',
            "id" => 1
        ];

        $response = $this->call('POST', '/v1/user/edit_details', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(200);
    }
    /**
     * Case
     * @test  This function test response with valid token and soft deleted id
     * @expectedExceptionCode 422
     */
    public function testUsersControllerUpdateUserProfileByAdminTestValidTokenSoftDeletedId()
    {
        app(DatabaseSeeder::class)->call(UserTestSeeder::class);
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $user = User::find(2);
        $user->delete();
        $param = [
            "first_name" => "ads",
            "status" => 1,
            "email" => 'adsfdfj@gmail.com',
            "id" => $user->id
        ];

        $response = $this->call('POST', '/v1/user/edit_details', $param, [], [], $headers);

        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('exists_validation', ['attribute' => 'id'], $lang),
        ], $json['id']);
    }


}
