<?php


use App\Models\Workplace;
use Illuminate\Support\Facades\Config;

class WorkplaceControllerDeleteMethodTest extends TestCase
{
    /*
     * Login param for admin , employee , customer to check role based authentication
     * */

    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'custom@yopmail.com',
        'password' => '123456'
    ];
    public $employee = [
        'email' => 'emp1@yopmail.com',
        'password' => '123456'
    ];

    /**
     * Case 1
     * @test  This function test response without any param and with out any token
     * @expectedExceptionCode 401
     */
    public function testWorkspaceControllerDestroyWithoutToken()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('workplaces')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        app(DatabaseSeeder::class)->call(UserTestSeeder::class);
        app(DatabaseSeeder::class)->call(WorkplaceSeeder::class);

        $response = $this->call('DELETE', '/v1/workplaces/delete/5', [], [], [], []);
        //check response status code
        $this->assertResponseStatus(401);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('missing_token_error', [], Config::get('constant.default_lang')),
            $json['error']);
    }

    /**
     * Case 2
     * @test  This function test response without any param and with invalid token
     * @expectedExceptionCode 400
     */
    public function testWorkspaceControllerDestroyWithInvalidTokenWithoutParam()
    {
        $expiredToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expiredToken
        ];

        $response = $this->call('DELETE', '/v1/workplaces/delete/5', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(400);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('token_expired_error', [], Config::get('constant.default_lang')),
            $json['error']);
    }

    /**
     * Case 3
     * @test  This function test response without any param and with invalid token
     * @expectedExceptionCode 404
     */
    public function testWorkspaceControllerDestroyWithoutValidTokenWithoutParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);

        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('DELETE', '/v1/v1/contact_persons/delete/', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(404);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('route_not_found', [], Config::get('constant.default_lang')),
            $json['error']);
    }

    /**
     * Case 4
     * @test  This function test response with valid token but with not existing id
     * @expectedExceptionCode 422
     */
    public function testWorkspaceControllerDestroyWithValidTokenNonExistingId()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('DELETE', '/v1/contact_persons/delete/0', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([$this->__getTranslateString('exists_validation', ['attribute' => 'id'], $lang)],
            $json['id']);
    }


    /**
     * Case 5
     * @test  This function test response with valid token and valid id
     * @expectedExceptionCode 200
     */
    public function testWorkspaceControllerDestroyWithValidTokenValidID()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('DELETE', '/v1/workplaces/delete/3', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(200);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('workplace_deleted', [], $lang), $json['success']);
    }

    /**
     * Case 6
     * @test  This function test response with valid token but with soft deleted id
     * @expectedExceptionCode 422
     */
    public function testWorkspaceControllerDestroyWithValidTokenSoftDeletedID()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $workPlace = Workplace::find(1);
        $workPlace->delete();
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('DELETE', '/v1/contact_persons/delete/' . $workPlace->id, [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);

        //match each index value
        $this->assertSame([$this->__getTranslateString('exists_validation', ['attribute' => 'id'], $lang)],
            $json['id']);
    }


    /**
     * Case 7
     * @test Customer dont't have permissions to view this page.
     * @expectedExceptionCode 403
     */
    public function testWorkspaceControllerDestroyValidTokenWithoutPramsForCustomer()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->customer);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);


        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('DELETE', '/v1/workplaces/delete/5', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(403);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame(
            $this->__getTranslateString('insufficient_permission', [], $lang)
            , $json['error']);
    }

    /**
     * Case 8
     * @test  Customer dont't have permissions to view this page.
     * @expectedExceptionCode 403
     */
    public function testWorkspaceControllerDestroyWithValidTokenWithoutPramsForEmployee()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->employee);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);


        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('DELETE', '/v1/workplaces/delete/5', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(403);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
//        match each index value
        $this->assertSame(
            $this->__getTranslateString('insufficient_permission', [], $lang)
            , $json['error']);
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('workplaces')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
