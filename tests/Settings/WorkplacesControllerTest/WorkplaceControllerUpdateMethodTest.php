<?php


use App\Models\Workplace;

class WorkplaceControllerUpdateMethodTest extends TestCase
{
    /*
     * Login param for admin , employee , customer to check role based authentication
     * */

    public $loginParam = [
        'email' => 'admin@bcbrand.com',
        'password' => 'demo123@'
    ];
    public $customer = [
        'email' => 'custom@yopmail.com',
        'password' => '123456'
    ];
    public $employee = [
        'email' => 'emp1@yopmail.com',
        'password' => '123456'
    ];

    /**
     * Case 1
     * @test This function test response without auth token without params
     * @expectedExceptionCode 401
     */

    public function testWorkspaceControllerUpdateWithoutAuthTokenAndWithoutParams()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('workplaces')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        app(DatabaseSeeder::class)->call(WorkplaceSeeder::class);
        $this->PUT('/v1/emailTemplates/update/{id}', [])
            ->seeJsonEquals([
                "error" => $this->__getTranslateString('missing_token_error', [], 2)
            ]);
    }

    /**
     * Case 2
     * @test  This function test response with invalid auth token without params
     * @expectedExceptionCode 400
     */
    public function testWorkspaceControllerUpdateWithInvalidTokenAndWithoutParams()
    {
        $expired_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTU0OTYwNDgzNiwiZXhwIjoxNTQ5NjA4NDM2fQ.JQBLo4wsVq5MB4Xswdw-OrRa1VKQu6FoXCG2qxVhfE8';
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $expired_token
        ];

        $response = $this->call('PUT', '/v1/emailTemplates/update/{id}', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(400);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame($this->__getTranslateString('token_expired_error', [], 2), $json['error']);
    }

    /**
     * Case 3
     * @test  This function test response with valid token without params
     * @expectedExceptionCode 422
     */
    public function testWorkspaceControllerUpdateWithValidTokenAndWithoutParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);

        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $response = $this->call('PUT', '/v1/workplaces/update/5', [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('required_validation', ['attribute' => 'name'], $lang)
        ],
            $json['name']);
    }

    /**
     * Case 4
     * @test This function test response with valid token and null params
     * @expectedExceptionCode 422
     */
    public function testWorkspaceControllerUpdateWithValidTokenAndNullParams()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            "name" => null,
        ];

        $response = $this->call('PUT', '/v1/workplaces/update/5', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
    }

    /**
     * Case 6
     * @test  This function test response with valid token but with user.email having normal string instead of valid email
     * @expectedExceptionCode  422
     */
    public function testWorkplaceControllerUpdateWithSubjectHavingLengthMax()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            "name" => "adsfdfjhbjsbdfjaaaaaaaaaaaaaaaaaaaaajbjjhdfjajbfdjabsjdfbjasdbfjbasjdfbjasbdfjajsdfsdfffffffffffffffffffffffffffffffsffffffffffffffffffffffffffffffffffffffffffdahjdafajhbdsfhjbhjbjhdsfdsfdfgfdgd",
        ];

        $response = $this->call('PUT', '/v1/workplaces/update/5', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);
        //convert response data in json
        $json = json_decode($response->getContent(), true);
        //match each index value
        $this->assertSame([
            $this->__getTranslateString('max_validation', ['attribute' => 'name', 'max' => 191], $lang)
        ],
            $json['name']);
    }

    /**
     * Case 6
     * @test  This function test response with valid token but with soft deleted id
     * @expectedExceptionCode 422
     */
    public function testWorkspaceControllerDestroyWithValidTokenSoftDeletedID()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $workPlace = Workplace::find(1);
        $workPlace->delete();
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];
        $param = [
            "name" => "dsddfsfds",
        ];

        $response = $this->call('PUT', '/v1/workplaces/update/' . $workPlace->id, [], [], [], $headers);
        //check response status code
        $this->assertResponseStatus(422);

        //convert response data in json
        $json = json_decode($response->getContent(), true);

        //match each index value
        $this->assertSame([$this->__getTranslateString('exists_validation', ['attribute' => 'id'], $lang)],
            $json['id']);
    }


    /**
     * Case 11
     * @test  This function test response with valid token and valid param
     * @expectedExceptionCode 200
     */
    public function testWorkplaceControllerUpdateValidTokenAndValidTokenAndValidData()
    {
        //hit login call fot getting auth token
        $loginResponse = $this->call('POST', '/v1/auth/login', $this->loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        $loginJson = json_decode($loginResponse->getContent(), true);
        //assign login user language code
        $lang = $loginJson['language'];
        //set Authorization token to headers
        $headers = [
            'HTTP_Authorization' => $loginJson['token']
        ];

        $param = [
            "name" => "dsddfsfds",
        ];

        $response = $this->call('PUT', '/v1/workplaces/update/5', $param, [], [], $headers);
        //check response status code
        $this->assertResponseStatus(200);
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('workplaces')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

}
