<?php

use Illuminate\Support\Str;
use Laravel\Lumen\Application;

/**
 * Class TestCase
 */
abstract class TestCase extends Laravel\Lumen\Testing\TestCase
{
    /**
     * Creates the application.
     *
     * @return Application
     */
    public function createApplication()
    {
        return require __DIR__ . '/../bootstrap/app.php';
    }

    /**
     *
     *
     * @param [string] $shortname
     * @param [array] $dataArray
     * @param [string] $lang
     * @param [string] $vars
     * @return void
     */
    public function __getTranslateString($shortname, $dataArray, $lang = null, $vars = null)
    {
        $search = [];
        $replace = [];
        if (isset($dataArray)) {
            foreach ($dataArray as $key => $data) {
                if ($key == 'attribute' && substr_count($data, '.') < 2) {
                    $data = str_replace('_', ' ', $data);
                }
                array_push($search, ':' . $key);
                array_push($replace, $data);
            }
        }
        $lang = ($lang == null) ? config('constant.default_lang') : $lang;
        $translatedString = str_replace($search, $replace,
            App\Services\TranslationService::lang($shortname, $lang, $vars));
        return $translatedString;
    }

    /**
     * @return mixed
     */
    public function getLoginInstance($loginParam)
    {
        $loginResponse = $this->call('POST', '/v1/auth/login', $loginParam);
        //check response status code
        $this->assertResponseStatus(200);
        //convert response data in json
        return json_decode($loginResponse->getContent(), true);
    }


}
